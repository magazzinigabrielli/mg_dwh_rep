import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Properties;

public class ParallelMain {
	//parametri di connessione al db oracle
	private static  String DB_DRIVER         = null;
	private static String  DB_CONNECTION     = null;
	private static String  DB_USER           = null;
	private static String  DB_PASSWORD       = null;
	private static String  DB2_C_PASSWORD    = null;
	private static String  DB2_A_PASSWORD    = null;
	private static String  DB2_USER          = null;
	private static String  DB2_NAME          = null;
	private static String  DWH_DB_CONNECTION = null;
	private static String  DWH_DB_USER       = null;
	private static String  DWH_DB_PASSWORD   = null;
	static //variabile che contiene il path
	String path = ParallelMain.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	
	//counter cicli terminati
	public static int controlliEseguiti = 0;
		
	/**
	 * Metodo void per loggare la console
	 * @param message : il messaggio da scrivere sul file log
	 */
	public static void log(String userDir,String message) { 
	    PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter(userDir+"\\LogFile.txt", true), true);
			out.write(message+System.lineSeparator());
		    out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	  }
	/**
		 * Metodo per leggere le proprietà dal file config.
		 * @throws IOException
		 */
		public static  void setProperties(String dir) throws IOException {
			//final String dir = System.getProperty("user.dir");
			InputStream input = new FileInputStream(dir+"/config.properties");
		    Properties prop = new Properties();
		    //carico il file di input
		    prop.load(input);
		    //Leggo le proprietà dal file config.prop
		    DB_DRIVER         = prop.getProperty("DB_DRIVER");
		    DB_CONNECTION     = prop.getProperty("DB_CONNECTION");
		    DB_USER           = prop.getProperty("DB_USER");
		    DB_PASSWORD       = prop.getProperty("DB_PASSWORD");
		    DB2_C_PASSWORD    = prop.getProperty("DB2_C_PASSWORD");
		    DB2_A_PASSWORD    = prop.getProperty("DB2_A_PASSWORD");
		    DB2_USER          = prop.getProperty("DB2_USER");
		    DB2_NAME          = prop.getProperty("DB2_NAME");
		    DWH_DB_CONNECTION = prop.getProperty("DWH_DB_CONNECTION");
		    DWH_DB_USER       = prop.getProperty("DWH_DB_USER");
		    DWH_DB_PASSWORD   = prop.getProperty("DWH_DB_PASSWORD");
		    //END set properties
		}
	   /**
	    * MAIN
	    * @throws SQLException 
	    */
	   public static void main(String args[]) throws SQLException {
		try {
		String dir = URLDecoder.decode(path, "UTF-8");
	    //creo il file per il log della console 
	   	//PrintStream consoleLog = new PrintStream(dir+"//Log.txt");
	   	//devio gli output della console sul file 
	   	//System.setOut(consoleLog);
	    System.out.println("Il file è stato aperto dalla seguente dir = " + dir);
	    //log nel file predisposto
	    log(dir,"Il file è stato aperto dalla seguente dir = " + dir);
	   	System.out.println("INIZIO PROCEDURA : mi connetto al db Oracle");
	   	log(dir,"INIZIO PROCEDURA : mi connetto al db Oracle");
	   	//ccounter ciclo while
	   	int cicloCounter = 0;
	   	//richiamo il metodo per configurare le variabili del db
	    setProperties(dir);
	   	//connessione a db oracle
	   	Connection OracleConn = ParallelControlloAperture.getOracleConnection(DB_DRIVER,DB_CONNECTION,DB_USER,DB_PASSWORD);
	   	System.out.println("Mi sono connesso al db Oracle");
	   	log(dir,"Mi sono connesso al db Oracle");
	   	//mi connetto al dwh
	   	System.out.println("Mi connetto al dwh");
	   	log(dir,"Mi connetto al db dwh");
	   	Connection DwhConn = ParallelControlloAperture.getOracleConnection(DB_DRIVER,DWH_DB_CONNECTION,DWH_DB_USER,DWH_DB_PASSWORD);
	   	System.out.println("Mi sono connesso al dwh");
	   	log(dir,"Mi sono connesso al dwh");
	   	//creo statement per le query
	   	Statement stmt        = OracleConn.createStatement();
	   	//errorStmt =DwhConn.prepareStatement("INSERT INTO mgdwh.MDWH_APERTURE_REL_PDV (PDV_PUNTO_VENDITA_COD,APD_DATA,APD_APERTO,APD_ERROR_MSG)VALUES(?,CURRENT_TIMESTAMP,?,?) ");
	   	//insertStmt =DwhConn.prepareStatement("MERGE INTO MDWH_APERTURE_REL_PDV e USING (SELECT ? as PDV_PUNTO_VENDITA_COD,trunc(sysdate) as APD_DATA FROM dual) h ON (e.PDV_PUNTO_VENDITA_COD = h.PDV_PUNTO_VENDITA_COD  and trunc(e.APD_DATA) = h.APD_DATA) WHEN MATCHED THEN  UPDATE SET e.APD_APERTO = ?,e.APD_ERROR_MSG = ? WHEN NOT MATCHED THEN INSERT (PDV_PUNTO_VENDITA_COD,APD_DATA,APD_APERTO,APD_ERROR_MSG) VALUES (?,CURRENT_TIMESTAMP,?,?)");
	   	//stringa query per ottenere punti di vendita da kpromo
	   	String Query ="SELECT E2STAT,E2CPDV,E2RAP,E2IPPV FROM mg.GEAD020F WHERE (E2DCPV = 0 OR E2CPDV > to_number(to_char(sysdate, 'YYYYMMDD'))) AND E2STAT <>'A' and E2TAFX not like 'SOMMINISTRATI'";
	   	//eseguo la query
	   	ResultSet rs = stmt.executeQuery(Query);
	   	//fino a quando ci sono risultati
	   	while(rs.next()) {
	   		if(cicloCounter!=0 && cicloCounter%10==0) {
	   			Thread.sleep(10000);
	   		}
	   		ParallelControlloAperture checkOpen = new ParallelControlloAperture(DwhConn,rs.getString("E2CPDV"),rs.getString("E2RAP"),rs.getString("E2IPPV"));
	   		checkOpen.start();
	   		//aumento contatore dei cicli 
	   		cicloCounter++;
	   	}
	   	//errori dalle connessioni ai db Oracle o al file config
	   	while(controlliEseguiti!=cicloCounter) 
	   	{
	   	   Thread.sleep(2000);
		   System.out.println("Attendi sto finendo le operazioni di richiesta ai db di filiale, sono stati eseguiti "+controlliEseguiti+" processi");
		   log(dir,"Attendi sto finendo le operazioni di richiesta ai db di filiale, sono stati eseguiti "+controlliEseguiti+" processi");
		}
		//chiudo le connessioni
		OracleConn.close();
        DwhConn.close();
		System.out.println("Eseguito anche il batch finale. HO TERMINATO ALLE ORE"+new Timestamp(System.currentTimeMillis()));
		log(dir,"Eseguito anche il batch finale. HO TERMINATO ALLE ORE"+new Timestamp(System.currentTimeMillis()));
	   	} catch (SQLException e) {
	   		System.out.println(e);
	   	}catch (NullPointerException e) {
	   		System.out.println(e);
	   	}  catch (ClassNotFoundException e) {
	   		System.out.println(e);

	   	} catch (IOException e) {
	   		System.out.println(e);
	   	} catch (InterruptedException e) {
			//errore thread già interrotto 
	   		System.out.println(e);
		}
	
	   }
	   //END
}
