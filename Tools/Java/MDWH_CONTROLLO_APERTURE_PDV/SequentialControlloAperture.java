﻿import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Properties;

import com.ibm.db2.jcc.am.SqlInvalidAuthorizationSpecException;

/**
 * Classe utilizzata per verificare se un negozio, in un determinato periodo, è aperto o meno
 * @author raffaele.doti.stage
 *
 */
public class SequentialControlloAperture {
//parametri di connessione ai db
private static  String DB_DRIVER         = null;
private static String  DB_CONNECTION     = null;
private static String  DB_USER           = null;
private static String  DB_PASSWORD       = null;
private static String  DB2_C_PASSWORD    = null;
private static String  DB2_A_PASSWORD    = null;
private static String  DB2_USER          = null;
private static String  DB2_NAME          = null;
private static String  DWH_DB_CONNECTION = null;
private static String  DWH_DB_USER       = null;
private static String  DWH_DB_PASSWORD   = null;
private boolean checkConnection          = false;

/**
 * Metodo void per loggare la console
 * @param message : il messaggio da scrivere sul file log
 * @throws UnsupportedEncodingException 
 */
public static void log(String message) throws UnsupportedEncodingException { 
	String userDir= getExecutionPath();
    PrintWriter out;
	try {
		out = new PrintWriter(new FileWriter(userDir+"\\LogFile.txt", true), true);
		out.write(message+System.lineSeparator());
	    out.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
  }
/**
 * Metodo che serve per ritornare il path di apertura del file .jar
 * @return stringa contenente il path di esecuzione del source code
 * @throws UnsupportedEncodingException
 */
public static String getExecutionPath() throws UnsupportedEncodingException {
	//prendo il path della classe che sto runnando 
	String jarFileFromSys = System.getProperty("java.class.path").split(";")[0];
	//mi ricavo il nome del file .jar che sto runnando
	String jarName =jarFileFromSys.substring(jarFileFromSys.lastIndexOf("\\")+1, jarFileFromSys.length());
	//ritorno il path assoluto - il nome del file jar 
	return jarFileFromSys.replace(jarName, "");
}
/**
 * Metodo per leggere le proprietà dal file config.
 * @throws IOException
 */
public static  void setProperties() throws IOException {
	//stringa che immagazzina il valore della directory dove si trova l'eseguibile java
	final String dir = getExecutionPath();
	//prendo in input il file config.prop
	InputStream input = new FileInputStream(dir+"/config.properties");
    Properties prop = new Properties();
    //carico il file di input
    prop.load(input);
    //Leggo le proprietà dal file config.prop
    DB_DRIVER         = prop.getProperty("DB_DRIVER");
    DB_CONNECTION     = prop.getProperty("DB_CONNECTION");
    DB_USER           = prop.getProperty("DB_USER");
    DB_PASSWORD       = prop.getProperty("DB_PASSWORD");
    DB2_C_PASSWORD    = prop.getProperty("DB2_C_PASSWORD");
    DB2_A_PASSWORD    = prop.getProperty("DB2_A_PASSWORD");
    DB2_USER          = prop.getProperty("DB2_USER");
    DB2_NAME          = prop.getProperty("DB2_NAME");
    DWH_DB_CONNECTION = prop.getProperty("DWH_DB_CONNECTION");
    DWH_DB_USER       = prop.getProperty("DWH_DB_USER");
    DWH_DB_PASSWORD   = prop.getProperty("DWH_DB_PASSWORD");
    //END set properties
}
/**
 * Metodo per effettuare la connection al db Oracle
 * @param driver
 * @return connection
 * @throws ClassNotFoundException 
 * @throws SQLException 
 */
public static Connection getOracleConnection(String driver,String connectionString,String user,String psw) throws ClassNotFoundException, SQLException {
    //INSTANZIO LA CONNECION
	Connection dbConnection = null;
   //DRIVER
	Class.forName(driver);
	//PROVO LA CONNESSIONE CON LE VARIABILI DICHIARATE NELLA CLASSE 
    //EFFETTUO LA CONNECTION
	dbConnection = DriverManager.getConnection(connectionString, user,psw);
	return dbConnection;

}


/**
 * Metodo per ottenere la connessione a db DB2 IBM
 * @param ip    =db Ip
 * @param dbName=dbname
 * @param dbUser=db user
 * @param dbPsv = db password
 * @return connection if parameters are ok
 * @throws SQLException 
 * @throws UnknownHostException 
 * @throws ClassNotFoundException 
 * @throws UnsupportedEncodingException 
 */
public Connection getDb2Connection(String ip,String dbName,String dbUser,String dbPsv) throws SQLException, UnknownHostException, ClassNotFoundException, UnsupportedEncodingException {
	//se uno dei parametri di connessione è null allora throw null exception 
	if(ip==null||dbName==null||dbUser==null||dbPsv==null) {
		throw new NullPointerException();
	}
	if(ip.isEmpty()) {
		//se l'ip contenuto nel db è vuoto 
		throw new IllegalArgumentException();
	}
	//converting string into address
	InetAddress inet = InetAddress.getByName(ip);
	//driver class
	String jdbcClassName="com.ibm.db2.jcc.DB2Driver";
    String url="jdbc:db2:/"+inet+":50000/"+dbName;
    String user=dbUser;
    String password =dbPsv;
    //Load class into memory
    Class.forName(jdbcClassName);
    //setto il timeout della connection
    DriverManager.setLoginTimeout(10);
    //Establish connection
    Connection connection = DriverManager.getConnection(url, user, password);
    System.out.println("Connected to db with ip: "+ip);
    log("Connected to db with ip: "+ip);
    //modifico il valore del campo di check su true
    checkConnection=true;
	return connection;
}
/**
 * Metodo che si occupa di creare la db2Conn e verificare se il negozio è stato aperto 
 * @param pdvDbIp       = ip del server VisualStore di filiale
 * @param dbUser        = nome utente del database
 * @param dbPsv         = psv dell'utente
 * @param dbName        = ServiceName db
 * @param Data          = Data che viene applicata come condizione in cui verificare se il negozio è aperto
 * @return S ='APERTO' , N='CHIUSO'
 * @throws SQLException 
 * @throws ClassNotFoundException 
 * @throws UnknownHostException 
 * @throws UnsupportedEncodingException 
 */
public String checkApertura(String pdvDbIp,String dbUser,String dbPsw,String dbName) throws UnknownHostException, ClassNotFoundException, SQLException, UnsupportedEncodingException {
	//Se uno dei parametri è nullo allora sollevo l'eccezione
	if(pdvDbIp==null||dbUser==null||dbPsw==null||dbName==null) {
		throw new NullPointerException();	
	}
	Connection con = this.getDb2Connection(pdvDbIp, dbName, dbUser, dbPsw);
	//creating statement
	Statement stmt= con.createStatement();
	//query to perform
	String query="SELECT  count(*) AS QT_VENDUTO FROM TL_ITEMS_NOR WHERE TO_DATE(DT_DATE, 'yyyy-MM-DD') = TO_DATE(CURRENT_DATE, 'yyyy-MM-DD')";
	//executing query
	ResultSet rs=stmt.executeQuery(query);
	int count=0;
	while(rs.next()) 
	{
		count=rs.getInt("QT_VENDUTO");
	}
	//se il counter è > 0 return S ,N altrimenti
	if(count>0)
	{
		//se il counter è maggiore di 0 allora il negozio ha emesso scontrini ed è aperto
		return "S";
	}else
		{
			//se il counter è minore di 0 allora il negozio non ha emesso scontrini ed è chiuuso
			return "N";
		}
	//END CHECKAPERTURA
}
/**
 * MAIN
 * @throws SQLException 
 * @throws UnsupportedEncodingException 
 */
public static void main(String args[]) throws SQLException, UnsupportedEncodingException { 
	//statement per l'inserimento dei record
	PreparedStatement insertStmt =null;
	try 
	{
	//stringa che immagazzina il valore della dyr attuale 
    final String dir = getExecutionPath();
    //creo/pulisco il file di log 
  	FileOutputStream out = new FileOutputStream(dir+"//LogFile.txt");
  	//START
    System.out.println("Il programma è stato aperto dalla seguente dir = " + dir);
    log("Il programma è stato aperto dalla seguente dir = " + dir);
	System.out.println("INIZIO PROCEDURA : mi connetto al db Oracle");
	log("Inizio procedura mi connetto al db Oracle");
    //richiamo il metodo per configurare le variabili del db
    setProperties();
	//connessione a db oracle
	Connection OracleConn = getOracleConnection(DB_DRIVER,DB_CONNECTION,DB_USER,DB_PASSWORD);
	System.out.println("Mi sono connesso al db Oracle");
	log("Mi sono connesso al db Oracle");
	//mi connetto al dwh
	System.out.println("Mi connetto al dwh");
	log("Mi connetto al dwh");
	Connection DwhConn = getOracleConnection(DB_DRIVER,DWH_DB_CONNECTION,DWH_DB_USER,DWH_DB_PASSWORD);
	System.out.println("Mi sono connesso al dwh");
	log("Mi sono connesso al dwh");
	//creo statement per le query
	Statement stmt        = OracleConn.createStatement();
    //statement per l'inserimento
	insertStmt =DwhConn.prepareStatement("MERGE INTO MDWH_APERTURE_REL_PDV e USING (SELECT ? as PDV_PUNTO_VENDITA_COD,trunc(sysdate) as APD_DATA FROM dual) h ON (e.PDV_PUNTO_VENDITA_COD = h.PDV_PUNTO_VENDITA_COD  and trunc(e.APD_DATA) = h.APD_DATA) WHEN MATCHED THEN  UPDATE SET e.APD_APERTO = ?,e.APD_ERROR_MSG = ? WHEN NOT MATCHED THEN INSERT (PDV_PUNTO_VENDITA_COD,APD_DATA,APD_APERTO,APD_ERROR_MSG) VALUES (?,CURRENT_TIMESTAMP,?,?)");
	//stringa query per ottenere punti di vendita da kpromo
	String Query ="SELECT E2STAT,E2CPDV,E2RAP,E2IPPV FROM mg.GEAD020F WHERE (E2DCPV = 0 OR E2CPDV > to_number(to_char(sysdate, 'YYYYMMDD'))) AND E2STAT <>'A' and E2TAFX not like 'SOMMINISTRATI'";
	//eseguo la query
	ResultSet rs = stmt.executeQuery(Query);
	//counter delle connessioni riuscite
	int countConn =0;
	//ccounter ciclo
	int cicloCounter = 0;
	//fino a quando ci sono risultati
	while(rs.next()) {
		//creo oggetto controllo aperura
		SequentialControlloAperture myObj = new SequentialControlloAperture();
		//variabile che immagazzina il valore di ritorno della proceduraa di checkApertura
		String checkReturn="" ;
		//controllo se il pdv è diretto o affiliato
		try
		{
		if(rs.getString("E2RAP").replace(" ", "").equals("A")) {
			//caso punto di vendita affiliato
			checkReturn=myObj.checkApertura(rs.getString("E2IPPV"),DB2_USER, DB2_A_PASSWORD, DB2_NAME);
		}
		if(rs.getString("E2RAP").replace(" ", "").equals("C")) {
			//caso punto di vendita controllato
			checkReturn=myObj.checkApertura(rs.getString("E2IPPV"),DB2_USER, DB2_C_PASSWORD, DB2_NAME);
		}
		//controllo se la connessione è avvenuta o meno
		if(myObj.checkConnection) {
			countConn++;
		}
		System.out.println("Il negozio"+rs.getString("E2CPDV")+" è aperto? "+checkReturn);
		log("Il negozio"+rs.getString("E2CPDV")+" è aperto? "+checkReturn);
		//sostituisco i ? dello statement con i valori del record attuale
		insertStmt.setString(1, rs.getString("E2CPDV"));
		insertStmt.setString(2, checkReturn);
		insertStmt.setString(3, "");
		insertStmt.setString(4, rs.getString("E2CPDV"));
		insertStmt.setString(5, checkReturn);
		insertStmt.setString(6, "");
		//eseguo l'update
		insertStmt.executeUpdate();
		System.out.println("Effettuato inserimento per pdv "+rs.getString("E2CPDV"));
		log("Effettuato inserimento per pdv "+rs.getString("E2CPDV"));
		//errori derivanti dalle connessioni ai db2 di filiale
		}catch(SQLException e) {
			e.printStackTrace();
			log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e.toString());
			try {
				insertStmt.setString(1, rs.getString("E2CPDV"));
				insertStmt.setString(2, "E");
				insertStmt.setString(3, "SQL EXCEPTION");
				insertStmt.setString(4, rs.getString("E2CPDV"));
				insertStmt.setString(5, "E");
				insertStmt.setString(6, "SQL EXCEPTION");
				insertStmt.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e1.toString());
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
			log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e.toString());
			try {
				insertStmt.setString(1, rs.getString("E2CPDV"));
				insertStmt.setString(2, "E");
				insertStmt.setString(3, "IP ERRATO");
				insertStmt.setString(4, rs.getString("E2CPDV"));
				insertStmt.setString(5, "E");
				insertStmt.setString(6, "IP ERRATO");
				insertStmt.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e1.toString());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e.toString());
			try {
				insertStmt.setString(1, rs.getString("E2CPDV"));
				insertStmt.setString(2, "E");
				insertStmt.setString(3, "NO DRIVER");
				insertStmt.setString(4, rs.getString("E2CPDV"));
				insertStmt.setString(5, "E");
				insertStmt.setString(6, "NO DRIVER");
				insertStmt.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e1.toString());
			}
		}catch (IllegalArgumentException e) {
			e.printStackTrace();
			log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e.toString());
			try {
				insertStmt.setString(1, rs.getString("E2CPDV"));
				insertStmt.setString(2, "E");
				insertStmt.setString(3, "ILLEGAL ARGUMENT EXCEPTION");
				insertStmt.setString(4, rs.getString("E2CPDV"));
				insertStmt.setString(5, "E");
				insertStmt.setString(6, "ILLEGAL ARGUMENT EXCEPTION");
				insertStmt.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e1.toString());
			}
		}catch (NullPointerException e) {
			e.printStackTrace();
			log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e.toString());
			//Oracle Sql Error
			//inserisco l'errore in un record del dwh
			try {
				insertStmt.setString(1, rs.getString("E2CPDV"));
				insertStmt.setString(2, "E");
				insertStmt.setString(3, "NULL POINTER EXCEPTION");
				insertStmt.setString(4, rs.getString("E2CPDV"));
				insertStmt.setString(5, "E");
				insertStmt.setString(6, "NULL POINTER EXCEPTION");
				insertStmt.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				log("Eccezione in merito al pdv "+rs.getString("E2CPDV")+e1.toString());
			}
		} 
		//aumento contatore dei cicli 
		cicloCounter++;
	}
	System.out.println("Effettuato numero di cicli pari a : "+cicloCounter+" con "+countConn+" connessioni riuscite");
	log("Effettuato numero di cicli pari a : "+cicloCounter+" con "+countConn+" connessioni riuscite");
	//errori dalle connessioni ai db Oracle o al file config
	} catch (SQLException e) {
		e.printStackTrace();
		log(e.toString());
	}catch (NullPointerException e) {
		e.printStackTrace();
		//Oracle Sql Error
		log(e.toString());
	}  catch (ClassNotFoundException e) {
		e.printStackTrace();
		log(e.toString());
	} catch (IOException e) {
		e.printStackTrace();
		log(e.toString());
	}
    //eseguo il batch
	//System.out.println("eseguo batch dei cicli senza errori");
	//insertStmt.executeBatch();
	//chiudo lo statement
	insertStmt.close();
	System.out.println("HO TERMINATO ALLE ORE"+new Timestamp(System.currentTimeMillis()));
	try {
		log("HO TERMINATO ALLE ORE"+new Timestamp(System.currentTimeMillis()));
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	//END
}
}
