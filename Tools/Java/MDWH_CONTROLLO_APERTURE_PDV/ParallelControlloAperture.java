import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Properties;

public class ParallelControlloAperture implements Runnable {
	public ParallelControlloAperture(Connection Dwh,String SID,String rapporto,String ipPdv) {
		this.SID        = SID;
		this.rapporto   = rapporto;
		this.ip         = ipPdv;
		this.DwhConn    = Dwh;
		
	}
	//connection al dwh
	Connection DwhConn;
	private PreparedStatement Stmt;
	//thread della classe
	private Thread t;
	//caratteristiche punto vendita da ricercare
	String SID = null;
	//rapporto indica se pdv è diretto o controllato
	String rapporto = null;
	//ip pdv
	String ip       = null;
	//bool per verificare se il ciclo è terminato
	public String checkOpen = null;
	//parametri di connessione al db oracle
	private static  String DB_DRIVER         = null;
	private static String  DB_CONNECTION     = null;
	private static String  DB_USER           = null;
	private static String  DB_PASSWORD       = null;
	private static String  DB2_C_PASSWORD    = null;
	private static String  DB2_A_PASSWORD    = null;
	private static String  DB2_USER          = null;
	private static String  DB2_NAME          = null;
	private static String  DWH_DB_CONNECTION = null;
	private static String  DWH_DB_USER       = null;
	private static String  DWH_DB_PASSWORD   = null;
	public boolean checkConnection          = false;
	/**
	 * Metodo per leggere le proprietà dal file config.
	 * @throws IOException
	 */
	public static  void setProperties(String dir) throws IOException {
		//final String dir = System.getProperty("user.dir");
		InputStream input = new FileInputStream(dir+"/config.properties");
	    Properties prop = new Properties();
	    //carico il file di input
	    prop.load(input);
	    //Leggo le proprietà dal file config.prop
	    DB_DRIVER         = prop.getProperty("DB_DRIVER");
	    DB_CONNECTION     = prop.getProperty("DB_CONNECTION");
	    DB_USER           = prop.getProperty("DB_USER");
	    DB_PASSWORD       = prop.getProperty("DB_PASSWORD");
	    DB2_C_PASSWORD    = prop.getProperty("DB2_C_PASSWORD");
	    DB2_A_PASSWORD    = prop.getProperty("DB2_A_PASSWORD");
	    DB2_USER          = prop.getProperty("DB2_USER");
	    DB2_NAME          = prop.getProperty("DB2_NAME");
	    DWH_DB_CONNECTION = prop.getProperty("DWH_DB_CONNECTION");
	    DWH_DB_USER       = prop.getProperty("DWH_DB_USER");
	    DWH_DB_PASSWORD   = prop.getProperty("DWH_DB_PASSWORD");
	    //END set properties
	}
	/**
	 * Metodo per effettuare la connection al db Oracle
	 * @param driver
	 * @return connection
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static Connection getOracleConnection(String driver,String connectionString,String user,String psw) throws ClassNotFoundException, SQLException {
	    //se qualche parametro ricevuto è nullo sollevo eccezione 
		if(driver==null||connectionString==null||user==null||psw==null) {
	    	throw new NullPointerException();
	    }
		//INSTANZIO LA CONNECION
		Connection dbConnection = null;
	   //DRIVER
		Class.forName(driver);
		//PROVO LA CONNESSIONE CON LE VARIABILI DICHIARATE NELLA CLASSE 
	    //EFFETTUO LA CONNECTION
		dbConnection = DriverManager.getConnection(connectionString, user,psw);
		return dbConnection;

	}


	/**
	 * Metodo per ottenere la connessione a db DB2 IBM
	 * @param ip    =db Ip
	 * @param dbName=dbname
	 * @param dbUser=db user
	 * @param dbPsv = db password
	 * @return connection if parameters are ok
	 * @throws SQLException 
	 * @throws UnknownHostException 
	 * @throws ClassNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public Connection getDb2Connection(String ip,String dbName,String dbUser,String dbPsv) throws SQLException, UnknownHostException, ClassNotFoundException, UnsupportedEncodingException {
		//se uno dei parametri di connessione è null allora throw null exception 
		if(ip==null||dbName==null||dbUser==null||dbPsv==null) {
			throw new NullPointerException();
		}
		if(ip.isEmpty()) {
			//se l'ip contenuto nel db è vuoto 
			throw new IllegalArgumentException();
		}
		//converting string into address
		InetAddress inet = InetAddress.getByName(ip);
		//driver class
		String jdbcClassName="com.ibm.db2.jcc.DB2Driver";
	    String url="jdbc:db2:/"+inet+":50000/"+dbName;
	    String user=dbUser;
	    String password =dbPsv;
	    //Load class into memory
	    Class.forName(jdbcClassName);
	    //Establish connection
	    Connection connection = DriverManager.getConnection(url, user, password);
	    System.out.println("Connected to db with ip: "+ip);
	    ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),"Connected to db with ip: "+ip);
	    //modifico il valore del campo di check su true
	    checkConnection=true;
		return connection;
	}
	/**
	 * Metodo che si occupa di creare la db2Conn e verificare se il negozio è stato aperto 
	 * @param pdvDbIp       = ip del server VisualStore di filiale
	 * @param dbUser        = nome utente del database
	 * @param dbPsv         = psv dell'utente
	 * @param dbName        = ServiceName db
	 * @param Data          = Data che viene applicata come condizione in cui verificare se il negozio è aperto
	 * @return S ='APERTO' , N='CHIUSO'
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws UnknownHostException 
	 * @throws UnsupportedEncodingException 
	 */
	public String checkApertura(String pdvDbIp,String dbUser,String dbPsw,String dbName) throws UnknownHostException, ClassNotFoundException, SQLException, UnsupportedEncodingException {
		//Se uno dei parametri è nullo allora sollevo l'eccezione
		if(pdvDbIp==null||dbUser==null||dbPsw==null||dbName==null) {
			throw new NullPointerException();	
		}
		Connection con = this.getDb2Connection(pdvDbIp, dbName, dbUser, dbPsw);
		//creating statement
		Statement stmt= con.createStatement();
		//query to perform
		String query="SELECT  count(*) AS QT_VENDUTO FROM TL_ITEMS_NOR WHERE TO_DATE(DT_DATE, 'yyyy-MM-DD') = TO_DATE(CURRENT_DATE, 'yyyy-MM-DD')";
		//executing query
		ResultSet rs=stmt.executeQuery(query);
		int count=0;
		while(rs.next()) 
		{
			count=rs.getInt("QT_VENDUTO");
		}
		//se il counter è > 0 return S ,N altrimenti
		if(count>0)
		{
			//se il counter è maggiore di 0 allora il negozio ha emesso scontrini ed è aperto
			stmt.close();
            con.close();
			return "S";
		}else
			{
				//se il counter è minore di 0 allora il negozio non ha emesso scontrini ed è chiuuso
			stmt.close();	
			con.close();
			return "N";
			}
		//END CHECKAPERTURA
	}	
	/**
     * Usa questo metodo per inserire le operazioni da eseguire in parallello
     */
	@Override
	public void run() {
		//controllo se il pdv è diretto o affiliato
		try	
		{
		//inizializzo lo statement	
		Stmt = DwhConn.prepareStatement("MERGE INTO MDWH_APERTURE_REL_PDV e USING (SELECT ? as PDV_PUNTO_VENDITA_COD,trunc(sysdate) as APD_DATA FROM dual) h ON (e.PDV_PUNTO_VENDITA_COD = h.PDV_PUNTO_VENDITA_COD  and trunc(e.APD_DATA) = h.APD_DATA) WHEN MATCHED THEN  UPDATE SET e.APD_APERTO = ?,e.APD_ERROR_MSG = ? WHEN NOT MATCHED THEN INSERT (PDV_PUNTO_VENDITA_COD,APD_DATA,APD_APERTO,APD_ERROR_MSG) VALUES (?,CURRENT_TIMESTAMP,?,?)");
		//setto i valori della connessione
		setProperties(URLDecoder.decode(ParallelMain.path, "UTF-8"));
		if(rapporto.replace(" ", "").equals("A")) {
			//caso punto di vendita affiliato
			checkOpen=checkApertura(ip,DB2_USER, DB2_A_PASSWORD, DB2_NAME);
		}
		if(rapporto.replace(" ", "").equals("C")) {
			//caso punto di vendita controllato
			checkOpen=checkApertura(ip,DB2_USER, DB2_C_PASSWORD, DB2_NAME);
		}
		//sostituisco i punti interrogativi dello stmt con i valori che mi servono
		Stmt.setString(1, SID);
		Stmt.setString(2, checkOpen);
		Stmt.setString(3, "");
		Stmt.setString(4, SID);
		Stmt.setString(5, checkOpen);
		Stmt.setString(6, "");
		//stampo a video ed effettuo un log su file 
		System.out.println("Eseguo update per pdv "+SID);
		ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),"Eseguo update per pdv "+SID);
		Stmt.executeUpdate();
		System.out.println("Update eseguito per pdv "+SID);
		ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),"Eseguito udpate per pdv "+SID);
		//aumento il contatore dei controlli totali eseguiti
		ParallelMain.controlliEseguiti++;
		//errori derivanti dalle connessioni ai db2 di filiale
		}catch(SQLException e) {
			System.out.println(e);
			ParallelMain.controlliEseguiti++;
			try {
				ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),e.toString());
				Stmt.setString(1, SID);
				Stmt.setString(2, "E");
				Stmt.setString(3, "SQLEXCEPTION");
				Stmt.setString(4, SID);
				Stmt.setString(5, "E");
				Stmt.setString(6,"SQLEXCEPTION");
				Stmt.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				System.out.println(e1);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		//ip derivante dal database non riconosciuto
		} catch (UnknownHostException e) {
			System.out.println(e);
			ParallelMain.controlliEseguiti++;
			try {
				ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),e.toString());
				Stmt.setString(1, SID);
				Stmt.setString(2, "E");
				Stmt.setString(3, "IP ERRATO IN RIFERIMENTO AL PDV INDICATO");
				Stmt.setString(4, SID);
				Stmt.setString(5, "E");
				Stmt.setString(6,"IP ERRATO IN RIFERIMENTO AL PDV INDICATO");
				Stmt.executeUpdate();
			} catch (SQLException e1) {
				System.out.println(e1);
				
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		//caso in cui non ci sono i driver 
		} catch (ClassNotFoundException e) {
			System.out.println(e);
			ParallelMain.controlliEseguiti++;
			try {
				ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),e.toString());
				Stmt.setString(1, SID);
				Stmt.setString(2, "E");
				Stmt.setString(3, "DRIVER MANCANTI");
				Stmt.setString(4, SID);
				Stmt.setString(5, "E");
				Stmt.setString(6,"DRIVER MANCANTI");
				Stmt.executeUpdate();
			} catch (SQLException e1) {
				System.out.println(e1);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		//se ip è vuoto allora lancio la illegal argument exception
		}catch (IllegalArgumentException e) {
			System.out.println(e);
			ParallelMain.controlliEseguiti++;
			try {
				ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),e.toString());
				Stmt.setString(1, SID);
				Stmt.setString(2, "E");
				Stmt.setString(3, "ILLEGAL ARGUMENT EXCEPTION");
				Stmt.setString(4, SID);
				Stmt.setString(5, "E");
				Stmt.setString(6,"ILLEGAL ARGUMENT EXCEPTION");
				Stmt.executeUpdate();
			} catch (SQLException e1) {
				System.out.println(e1);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}catch (NullPointerException e) {
			System.out.println(e);
			ParallelMain.controlliEseguiti++;
			//Oracle Sql Error
			//inserisco l'errore in un record del dwh
			try {
				ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),e.toString());
				Stmt.setString(1, SID);
				Stmt.setString(2, "E");
				Stmt.setString(3, "NULL POINTEREXCEPTION");
				Stmt.setString(4, SID);
				Stmt.setString(5, "E");
				Stmt.setString(6,"NULL POINTER EXCEPTION");
				Stmt.executeUpdate();
			} catch (SQLException e1) {
				System.out.println(e1);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    //file di configurazione non trovato
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				ParallelMain.log(URLDecoder.decode(ParallelMain.path, "UTF-8"),e.toString());
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ParallelMain.controlliEseguiti++;
		} 
	}
	/**
	 * Metodo utilizzato per avviare il thread 
	 * @param SID    nome del thread
	 */
	   public void start ()
        {
		      if (t == null) {
		         t = new Thread (this, this.SID);
		         t.start ();
		      }
		}
	
}
