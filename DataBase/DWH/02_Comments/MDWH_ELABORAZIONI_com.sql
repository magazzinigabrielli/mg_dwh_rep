COMMENT ON TABLE mdwh_elaborazioni IS
    'Tabella delle elaborazioni';

COMMENT ON COLUMN mdwh_elaborazioni.elb_elaborazione_cod IS
    'Identificativo elaborazione';

COMMENT ON COLUMN mdwh_elaborazioni.jog_id IS
    'Identificativo del JOB';

COMMENT ON COLUMN mdwh_elaborazioni.elb_stato IS
    'Stato dell''elaborazione';

COMMENT ON COLUMN mdwh_elaborazioni.elb_log IS
    'log dell''elaborazione';

COMMENT ON COLUMN mdwh_elaborazioni.ent_entita_cod IS
    'Identificativo dell''entit�';

COMMENT ON COLUMN mdwh_elaborazioni.elb_rows IS
    'Righe elaborate.';

COMMENT ON COLUMN mdwh_elaborazioni.log_id_sessione IS
    'Identificativo sessione per BASE_LOG';

COMMENT ON COLUMN mdwh_elaborazioni.ELB_DATA_INI IS
    'Data inizio elaborazione';

COMMENT ON COLUMN mdwh_elaborazioni.ELB_DATA_FIN IS
    'Data fine elaborazione';

COMMENT ON COLUMN mdwh_elaborazioni.elb_data_dec IS
    'Data decorrenza con cui saranno effettuate le estrazioni';

COMMENT ON COLUMN mdwh_elaborazioni.pdv_punto_vendita_cod IS
    'Identificativo del punto vendita';	
	