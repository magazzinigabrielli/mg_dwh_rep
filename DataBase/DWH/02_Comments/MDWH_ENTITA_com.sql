COMMENT ON TABLE mdwh_entita IS
    'Anagrafica delle entit�';

COMMENT ON COLUMN mdwh_entita.ent_entita_cod IS
    'Identificativo dell''entit�';

COMMENT ON COLUMN mdwh_entita.ent_descrizione IS
    'Descrizione entit�';

COMMENT ON COLUMN mdwh_entita.ent_last_import IS
    'Ultimo caricamento con successo';

COMMENT ON COLUMN mdwh_entita.ent_job_id IS
    'Ultimo Job Id eseguito';

COMMENT ON COLUMN mdwh_entita.plc_policy_cod_import IS
    'Policy di import  dati dell''entit�';

COMMENT ON COLUMN mdwh_entita.plc_policy_cod_delete IS
    'Policy di cancellazione dati dell''entit�';

COMMENT ON COLUMN mdwh_entita.ent_stat IS
    'Flag stato record';

COMMENT ON COLUMN mdwh_entita.ent_tipo_import IS
    'Tipologia import dati
-FULL : Tutta la tabella sorgente
-INCR: incrementale';
COMMENT ON COLUMN mdwh_entita.ENT_GRADO_PARALLEL IS 'Grado di parallelismo da applicare 1-6';
COMMENT ON COLUMN mdwh_entita.ENT_FILTRO	     IS 'Filtro sull'' entit� da applicare Formato(Cod1|Cod2|...|CodN|)';
COMMENT ON COLUMN mdwh_entita.ENT_FILTRO_DATA_DA IS 'Filtro Data Da';

Il parallelismo si attiva se non sono presenti filtri.