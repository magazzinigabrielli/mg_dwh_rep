COMMENT ON TABLE mdwh_sorgenti IS
    'anagrafica delle sorgenti dato';

COMMENT ON COLUMN mdwh_sorgenti.src_sorgente_cod IS
    'Identificativo della sorente dati';

COMMENT ON COLUMN mdwh_sorgenti.src_descrizione IS
    'Descrizione sorgente';

COMMENT ON COLUMN mdwh_sorgenti.src_tipologia IS
    'Tipologia sorgente (dblink/ETL/altro...)';

COMMENT ON COLUMN mdwh_sorgenti.src_host IS
    'Indirizzo della macchina sorgente';

COMMENT ON COLUMN mdwh_sorgenti.src_service_name IS
    'Nome servizio';