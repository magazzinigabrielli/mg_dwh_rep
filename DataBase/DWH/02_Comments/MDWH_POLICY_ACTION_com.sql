COMMENT ON TABLE mdwh_policy_action IS
    'Anagrafica policy di aggiornamento entit�';

COMMENT ON COLUMN mdwh_policy_action.plc_policy_action_cod IS
    'Identificativo policy';

COMMENT ON COLUMN mdwh_policy_action.plc_descrizione IS
    'Descrizione policy';

COMMENT ON COLUMN mdwh_policy_action.plc_tipo_frequenza IS
    'Tipologia frequenza
-ORARIA
-GIORNALIERA
-SETTIMANALE
-MENSILE
-ANNUALE';

COMMENT ON COLUMN mdwh_policy_action.plc_frequenza IS
    'Ricorrenza in base al tipo frequenza';

COMMENT ON COLUMN mdwh_policy_action.plc_data_ini IS
    'Data attivazione policy';

COMMENT ON COLUMN mdwh_policy_action.plc_data_fin IS
    'Fine policy di aggionrnamento';

COMMENT ON COLUMN mdwh_policy_action.plc_stat IS
    'Flag Stato record';