PROMPT CREATING FOREIGN KEY ON 'MDWH_ENTITA';
ALTER TABLE mdwh_entita
    ADD CONSTRAINT mdwh_ent_mdwh_plc_fk1 FOREIGN KEY ( plc_policy_cod_import )
        REFERENCES mdwh_policy_action ( plc_policy_action_cod );
PROMPT CREATING FOREIGN KEY ON 'MDWH_ENTITA';
ALTER TABLE mdwh_entita
    ADD CONSTRAINT mdwh_ent_mdwh_plc_fk2 FOREIGN KEY ( plc_policy_cod_delete )
        REFERENCES mdwh_policy_action ( plc_policy_action_cod );
        
PROMPT CREATING FOREIGN KEY ON 'MDWH_ENTITA';
ALTER TABLE mdwh_entita
    ADD CONSTRAINT mdwh_ent_mdwh_src_fk1 FOREIGN KEY ( src_sorgente_cod )
        REFERENCES mdwh_sorgenti ( src_sorgente_cod );

ALTER TABLE mdwh_entita
    ADD CONSTRAINT mdwh_ent_mdwh_srd_fk1 FOREIGN KEY ( srd_sorgente_dett_cod )
        REFERENCES mdwh_sorgenti_dett ( srd_sorgente_dett_cod );