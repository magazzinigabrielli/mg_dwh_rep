PROMPT CREATING FOREIGN KEY ON 'MDWH_ELABORAZIONI';
ALTER TABLE mdwh_elaborazioni
    ADD CONSTRAINT ent_entita_cod FOREIGN KEY ( ent_entita_cod )
        REFERENCES mdwh_entita ( ent_entita_cod )
    NOT DEFERRABLE;

PROMPT CREATING FOREIGN KEY ON 'MDWH_ELABORAZIONI';
ALTER TABLE mdwh_elaborazioni
    ADD CONSTRAINT mdwh_elb_mdwh_ent_fk FOREIGN KEY ( ent_entita_cod )
        REFERENCES mdwh_entita ( ent_entita_cod )
    NOT DEFERRABLE;

ALTER TABLE mdwh_elaborazioni
    ADD CONSTRAINT mdwh_elb_ana_pdv_fk FOREIGN KEY ( pdv_punto_vendita_cod )
        REFERENCES ana_punti_vendita ( pdv_punto_vendita_cod )
    NOT DEFERRABLE;
