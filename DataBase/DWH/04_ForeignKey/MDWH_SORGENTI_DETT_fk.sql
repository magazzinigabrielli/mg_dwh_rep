PROMPT CREATING FOREIGN KEY ON 'MDWH_SORGENTI_DETT';
ALTER TABLE mdwh_sorgenti_dett
    ADD CONSTRAINT mdvw_srd_mdvw_src_fk FOREIGN KEY ( src_sorgente_cod )
        REFERENCES mdwh_sorgenti ( src_sorgente_cod );