PROMPT CREAZIONE CHECK SULLA TABELLA MDWH_APERTURE_REL_PDV
ALTER TABLE MDWH_APERTURE_REL_PDV ADD CONSTRAINT MDWH_APD_NN1 CHECK ( APD_APERTURA_REL_PDV_ID IS NOT NULL );
ALTER TABLE MDWH_APERTURE_REL_PDV ADD CONSTRAINT MDWH_APD_NN2 CHECK ( PDV_PUNTO_VENDITA_COD IS NOT NULL );
ALTER TABLE MDWH_APERTURE_REL_PDV ADD CONSTRAINT MDWH_APD_NN3 CHECK ( APD_DATA IS NOT NULL );

ALTER TABLE MDWH_APERTURE_REL_PDV
    ADD CONSTRAINT MDWH_APD_STAT_CK CHECK ( "APD_STAT" IN (
        'A'
    ) );

ALTER TABLE MDWH_APERTURE_REL_PDV
    ADD CONSTRAINT MDWH_APD_APERTO_CK CHECK ( "APD_APERTO" IN (
        'S','N','E'
    ) );


PROMPT CREAZIONE PRIMARY KEY MDW_APDV_PK SULLA TABELLA MDWH_APERTURE_REL_PDV
ALTER TABLE MDWH_APERTURE_REL_PDV
ADD CONSTRAINT MDWH_APDV_PK
PRIMARY KEY
(
 APD_APERTURA_REL_PDV_ID
)
DISABLE
/

PROMPT CREAZIONE PRIMARY KEY MDW_APDV_PK SULLA TABELLA MDWH_APERTURE_REL_PDV
ALTER TABLE MDWH_APERTURE_REL_PDV
ENABLE VALIDATE
CONSTRAINT MDWH_APDV_PK
/
