PROMPT CREATING CHECK CONSTRAINT ON 'MDWH_ENTITA';
ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn1 CHECK ( "ENT_ENTITA_COD" IS NOT NULL );


PROMPT CREATING CHECK CONSTRAINT ON 'MDWH_ENTITA';

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn2 CHECK ( "ENT_DESCRIZIONE" IS NOT NULL );


PROMPT CREATING CHECK CONSTRAINT ON 'MDWH_ENTITA';

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn3 CHECK ( "ENT_LAST_IMPORT" IS NOT NULL );


PROMPT CREATING CHECK CONSTRAINT ON 'MDWH_ENTITA';

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn4 CHECK ( "PLC_POLICY_COD_IMPORT" IS NOT NULL );


PROMPT CREATING CHECK CONSTRAINT ON 'MDWH_ENTITA';

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn5 CHECK ( "SRC_SORGENTE_COD" IS NOT NULL );

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn6 CHECK ( "SRD_SORGENTE_DETT_COD" IS NOT NULL );

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_ent_ck_nn7 CHECK ( ent_tipo_import IS NOT NULL );

PROMPT CREATING CHECK CONSTRAINT ON 'MDWH_ENTITA';

ALTER TABLE mdwh_entita
    ADD CONSTRAINT mdwh_ent_stat CHECK ( "ENT_STAT" IN (
        'A'
    ) );

ALTER TABLE mdwh_entita
    ADD CONSTRAINT mdwh_ent_tp_import CHECK ( ent_tipo_import IN (
        'FULL', 'INCR'
    ) );
PROMPT CREATING PRIMARY KEY ON 'MDWH_ENTITA';

ALTER TABLE mdwh_entita ADD CONSTRAINT mdwh_entita_pk PRIMARY KEY ( ent_entita_cod );