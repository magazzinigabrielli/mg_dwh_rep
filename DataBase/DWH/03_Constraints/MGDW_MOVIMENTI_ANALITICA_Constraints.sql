
PROMPT CREAZIONE PRIMARY KEY MDW_MOAN_PK SULLA TABELLA MGDW_MOVIMENTI_ANALITICA
ALTER TABLE MGDW_MOVIMENTI_ANALITICA
ADD CONSTRAINT MDW_MOAN_PK
PRIMARY KEY
(
 MOAN_MOVIMENTI_ANALITICA_ID
)
DISABLE
/

PROMPT CREAZIONE PRIMARY KEY MOAN_ID SULLA TABELLA MGDW_MOVIMENTI_ANALITICA
ALTER TABLE MGDW_MOVIMENTI_ANALITICA
ENABLE VALIDATE
CONSTRAINT MDW_MOAN_PK
/
