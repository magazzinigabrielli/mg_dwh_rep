PROMPT CREATING PRIMARY KEY ON 'VND_VENDITE_HEAD';

PROMPT CREATING CHECK CONSTRAINT ON 'ANA_CARTE';
ALTER TABLE VND_VENDITE_HEAD ADD CONSTRAINT MDWH_VNH_NN1 CHECK ( "PDV_PUNTO_VENDITA_COD" IS NOT NULL );
ALTER TABLE VND_VENDITE_HEAD ADD CONSTRAINT MDWH_VNH_NN2 CHECK ( "VNH_DATA_VEND" IS NOT NULL );
ALTER TABLE VND_VENDITE_HEAD ADD CONSTRAINT MDWH_VNH_NN3 CHECK ( "VNH_NUMERO_CASSA" IS NOT NULL );
ALTER TABLE VND_VENDITE_HEAD ADD CONSTRAINT MDWH_VNH_NN4 CHECK ( "VNH_CASSIERE_COD" IS NOT NULL );
ALTER TABLE VND_VENDITE_HEAD ADD CONSTRAINT MDWH_VNH_NN5 CHECK ( "VNH_NUMERO_SCONTRINO" IS NOT NULL );

ALTER TABLE VND_VENDITE_HEAD ADD CONSTRAINT VND_VENDITE_HEAD_PK PRIMARY KEY ( VNH_VENDUTO_HEAD_ID );