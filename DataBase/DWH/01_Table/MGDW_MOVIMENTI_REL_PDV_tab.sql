
PROMPT Creazione tabella MDW_MOVIMENTI_REL_PDV
CREATE TABLE MDW_MOVIMENTI_REL_PDV
(MPV_MOVIMENTO_REL_PDV_ID           NUMBER GENERATED by default on null as IDENTITY
,MPV_COD_PREFISSO                   NUMBER(1)
,MPV_VOCE_SPESA                     VARCHAR(12)
,MPV_CENTRO_COSTO                   VARCHAR(12)
,MPV_COD_COMMESSA                   VARCHAR(12)
,MPV_COD_DARAV                      VARCHAR(1)
,MPV_DESCRIZIONE                    VARCHAR(35)
,MPV_NUMERO_MOV                     NUMBER(9)
,MPV_RIGA_MOVIMENTO                 NUMBER(3)
,MPV_CODICE_FORNITORE               VARCHAR(6)
,MPV_DATA_COMPETENZA                VARCHAR(8)
,MPV_IMPORTO                        NUMBER(17)
,MPV_ULTIMA_MODIFICA                VARCHAR(10)
,MPV_TIPO_VALORI                    VARCHAR(1)
,MPV_DATA_IMP                       TIMESTAMP(6) DEFAULT SYSTIMESTAMP
,MPV_DATA_MOD                       TIMESTAMP(6) DEFAULT SYSTIMESTAMP
,MPV_STAT                           VARCHAR2(2)
)
/
PROMPT Creazione Commenti MDW_MOVIMENTI_REL_PDV
COMMENT ON TABLE  MDW_MOVIMENTI_REL_PDV IS 'Tabella in cui sono registrati gli immessi, ossia i movimenti di trasferimento merci dal deposito al PDV.' ;
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_DETT_ID                                 IS 'chiave fisica della tabella MG';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_PERIODO_RIFERIMENTO                     IS 'PERIODO DI RIFERIMENTO COMPLESSIVO DI CALCOLO PERCENTUALE E TOTALE VOCE SPESA';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_VOCE_SPESA                              IS 'Voce di spesa/ricavo';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_CODICE_FORNITORE                        IS 'Codice Cliente/Fornitore';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_DATA_COMPETENZA                         IS 'Data registrazione di competenza';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_IMPORTO                                 IS 'Importo in euro DEL MESE ';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_TOTALE_VOCE_SPESA_PERIODO               IS 'TOTALE VOCE SPESA RELATIVA AL PERIODO';
COMMENT ON COLUMN MDW_MOVIMENTI_REL_PDV.MPV_PERCENTUALE_VOCE_SPESA_PERIODO          IS 'Percentuale voce spesa mensile rispetto al totale del periodo';
