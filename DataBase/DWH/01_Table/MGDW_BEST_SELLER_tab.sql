
PROMPT Creazione tabella MGDW_BEST_SELLER
CREATE TABLE MGDW_BEST_SELLER
(
 BTSL_SID                            NUMBER(5)
,BTSL_UG                             VARCHAR(4)
,BTSL_INTCODE                        VARCHAR2(14)
,BTSL_ITEMDESCR                      VARCHAR2(100)
,BTSL_QUANTITA                       NUMBER(5)
,BTSL_DT_RUN                         TIMESTAMP)
/
