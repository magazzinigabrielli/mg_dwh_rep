PROMPT CREATING TABLE 'ANA_ARTICOLI';
CREATE TABLE ana_articoli (
    art_articolo_cod              NUMBER(10) NOT NULL,
    art_descrizione_std           VARCHAR2(255),
    art_descrizione_sito          VARCHAR2(255),
    art_stat                      VARCHAR2(2),
    art_fornitore_gen_cod         VARCHAR2(25),
    art_brand                     VARCHAR2(255),
    art_ufficio_acq_cod           VARCHAR2(25),
    art_linea_prodotto_cod        VARCHAR2(25),
    art_reparto_cod               VARCHAR2(25),
    art_unita_organizzativa_cod   VARCHAR2(25),
    art_raggruppamento_cod        VARCHAR2(25),
    art_famiglia_cod              VARCHAR2(25),
    art_ciclo_vita                VARCHAR2(5),
    art_data_ini                  TIMESTAMP,
    art_data_eto                  TIMESTAMP,
    art_unita_misura              VARCHAR2(5),
    art_prezzo_kg_lt              VARCHAR2(5),
    art_num_pezzi                 NUMBER(10),
    art_tipo_conf                 VARCHAR2(5),
    art_unit_mis_conf             VARCHAR2(5),
    art_qta_pz_conf               NUMBER(10),
    art_qta_tot_conf              NUMBER(10),
    art_unita_misura_prz          VARCHAR2(5),
    art_imballo_vend              NUMBER(10),
    art_imballo_acq               NUMBER(10),
    art_peso_medio                NUMBER(10),
    art_marchio_cod               VARCHAR2(25),
    art_marchio_desc              VARCHAR2(255),
    art_private_lbl               VARCHAR2(5),
    art_espositore                VARCHAR2(5),
    art_multipack                 VARCHAR2(5),
    art_qta_multipack             NUMBER(10),
    art_catering                  VARCHAR2(5),
    art_vino_nov                  VARCHAR2(5),
    art_premio                    VARCHAR2(5),
    art_spot                      VARCHAR2(5),
    art_infiamm                   VARCHAR2(5),
    art_non_vendibile             VARCHAR2(5),
    art_raee                      VARCHAR2(5),
    art_arto                      VARCHAR2(5),
    art_prod_tipico               VARCHAR2(5),
    art_sito                      VARCHAR2(5),
    job_id                        NUMBER(10),
    data_import                   TIMESTAMP,
    art_ecr_lvl1_cod              VARCHAR2(10),
    art_ecr_lvl2_cod              VARCHAR2(10),
    art_ecr_lvl3_cod              VARCHAR2(10),
    art_ecr_lvl4_cod              VARCHAR2(10),
    art_ecr_lvl5_cod              VARCHAR2(10),
    ECR_CLASSIFICAZIONE_COD       VARCHAR2(25) 
)
TABLESPACE dati_mgdwh;

CREATE OR REPLACE PUBLIC SYNONYM ana_articoli FOR ana_articoli;

PROMPT CREATING COMMENTS TABLE 'ANA_ARTICOLI';
COMMENT ON TABLE ana_articoli IS 'anagrafica articoli';
COMMENT ON COLUMN ana_articoli.art_articolo_cod IS 'Codice aricolo';
COMMENT ON COLUMN ana_articoli.art_descrizione_std IS 'Descrizione articolo standard  ';
COMMENT ON COLUMN ana_articoli.art_descrizione_sito IS 'Descrizione sito';
COMMENT ON COLUMN ana_articoli.art_stat IS 'Stato estrazione  record, ‘A’=annullato';
COMMENT ON COLUMN ana_articoli.art_fornitore_gen_cod IS 'Codice fornitore generico ';
COMMENT ON COLUMN ana_articoli.art_brand IS 'Brand articolo ';
COMMENT ON COLUMN ana_articoli.art_ufficio_acq_cod IS 'Codice buyer';
COMMENT ON COLUMN ana_articoli.art_linea_prodotto_cod IS 'Codice Linea Prodotto';
COMMENT ON COLUMN ana_articoli.art_reparto_cod IS 'Codice Reparto';
COMMENT ON COLUMN ana_articoli.art_unita_organizzativa_cod IS 'Codice Unità Organizzativa';
COMMENT ON COLUMN ana_articoli.art_raggruppamento_cod IS 'Codice Raggruppamento';
COMMENT ON COLUMN ana_articoli.art_famiglia_cod IS 'Codice Famiglia';
COMMENT ON COLUMN ana_articoli.art_ciclo_vita IS '"Ciclo di vita  dell''articolo:
Blank=articolo normale attivo
ETO=articolo eliminato (non piu’  acquistato ma possibile  presenza scorte in pdv  per smaltimento giacenza)
SOS=Articolo sospeso (non  richiedibile al momento  dal PdV)"';
COMMENT ON COLUMN ana_articoli.art_data_ini IS 'Data inserimento articolo';
COMMENT ON COLUMN ana_articoli.art_data_eto IS 'Data espulsione articolo(ETO)';
COMMENT ON COLUMN ana_articoli.art_unita_misura IS 'Unità di misura di vendita';
COMMENT ON COLUMN ana_articoli.art_prezzo_kg_lt IS 'S=l’articolo è a peso predeterminato per cui va esposto il prezzo al KG/LT';
COMMENT ON COLUMN ana_articoli.art_num_pezzi IS 'Contiene il numero di pezzi della confezione di vendita';
COMMENT ON COLUMN ana_articoli.art_tipo_conf IS 'Tipo della confezione';
COMMENT ON COLUMN ana_articoli.art_unit_mis_conf IS 'Unità di misura del contenuto';
COMMENT ON COLUMN ana_articoli.art_qta_pz_conf IS 'Quantità di ogni singolo pezzo della confezione.';
COMMENT ON COLUMN ana_articoli.art_qta_tot_conf IS 'Questo campo contiene la quantità totale della confezione riportata nelle unità di misura standard (KG,LT etc)';
COMMENT ON COLUMN ana_articoli.art_unita_misura_prz IS 'UM base di riferimento del prezzo';
COMMENT ON COLUMN ana_articoli.art_imballo_vend IS 'Imballo in vendita ';
COMMENT ON COLUMN ana_articoli.art_imballo_acq IS 'Imballo in acquisto ';
COMMENT ON COLUMN ana_articoli.art_peso_medio IS 'Peso medio collo in Grammi';
COMMENT ON COLUMN ana_articoli.art_marchio_cod IS '"Codice marchio articolo.
A-ALTRI MARCHI PL  
C-CONSILIA         
D-ARTICOLI DISCOUNT
E-PREMIUM          
L-LEADER           
P-PRIMO PREZZO     
Q-SELEZ.QUALITA''   "
';
COMMENT ON COLUMN ana_articoli.art_marchio_desc IS 'Descrizione marchio';
COMMENT ON COLUMN ana_articoli.art_private_lbl IS 'Indica se il marchio è una “private label”';
COMMENT ON COLUMN ana_articoli.art_espositore IS 'Trattasi di articolo di tipo espositore (box che contiene articoli di vendita)';
COMMENT ON COLUMN ana_articoli.art_multipack IS 'Trattasi di articolo multipack (es. fardello da 6 bot acqua)';
COMMENT ON COLUMN ana_articoli.art_qta_multipack IS 'Numero articoli di vendita contenuti nel multipack';
COMMENT ON COLUMN ana_articoli.art_catering IS 'Articolo di tipo Catering';
COMMENT ON COLUMN ana_articoli.art_vino_nov IS 'Vino novello';
COMMENT ON COLUMN ana_articoli.art_premio IS 'Articolo premio raccolta punti';
COMMENT ON COLUMN ana_articoli.art_spot IS 'Articolo con gestione SPOT, acquisto una tantum';
COMMENT ON COLUMN ana_articoli.art_infiamm IS 'Articolo infiammabile';
COMMENT ON COLUMN ana_articoli.art_non_vendibile IS 'Articolo non vendibile al pubblico (es. espositore)';
COMMENT ON COLUMN ana_articoli.art_raee IS 'Info per indicazione che l’eco contributo raee è compreso nel prezzo';
COMMENT ON COLUMN ana_articoli.art_arto IS 'Articolo categoria orto';
COMMENT ON COLUMN ana_articoli.art_prod_tipico IS 'Indica la provincia del prodotto tipico';
COMMENT ON COLUMN ana_articoli.art_sito IS 'Flag che indica se l''articolo è visibile sul sito www.gabrielliunika.it';
COMMENT ON COLUMN ana_articoli.job_id IS 'Identificativo job';
COMMENT ON COLUMN ana_articoli.data_import IS 'data import record';
COMMENT ON COLUMN ana_articoli.art_ecr_lvl1_cod IS 'Codice ECR livello 1';
COMMENT ON COLUMN ana_articoli.art_ecr_lvl2_cod IS 'Codice ECR livello 2';
COMMENT ON COLUMN ana_articoli.art_ecr_lvl3_cod IS 'Codice ECR livello 3';
COMMENT ON COLUMN ana_articoli.art_ecr_lvl4_cod IS 'Codice ECR livello 4';
COMMENT ON COLUMN ana_articoli.art_ecr_lvl5_cod IS 'Codice ECR livello 5';
COMMENT ON COLUMN ana_articoli.ECR_CLASSIFICAZIONE_COD IS 'FK Codice ECR livello 5';
PROMPT CREATING CHECK CONSTRAINT ON 'ANA_ARTICOLI';

ALTER TABLE ana_articoli ADD CHECK ( art_stat IN (
    'A'
) );


CREATE UNIQUE INDEX rti_articolo_cod_idx ON
    ana_articoli (
        art_articolo_cod
    ASC );

CREATE INDEX ANA_ART_ECR_COD_IDX ON
    ANA_ARTICOLI (
        ECR_CLASSIFICAZIONE_COD
    ASC );


PROMPT CREATING PRIMARY KEY ON 'ANA_ARTICOLI';

ALTER TABLE ana_articoli ADD CONSTRAINT ana_articoli_pk PRIMARY KEY ( art_articolo_cod );
--


PROMPT CREATING FOREIGN KEY ON 'ANA_CARTE_REL_CLU';

ALTER TABLE ANA_ARTICOLI
    ADD CONSTRAINT ANA_ART_REL_ANA_ECR_FK FOREIGN KEY ( ECR_CLASSIFICAZIONE_COD )
        REFERENCES ANA_CLASSIFICAZIONI_ECR ( ECR_CLASSIFICAZIONE_COD );