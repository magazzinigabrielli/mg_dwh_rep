PROMPT CREATING TABLE 'MDWH_ELABORAZIONI';
CREATE TABLE mdwh_elaborazioni (
    elb_elaborazione_cod   NUMBER(10),
    jog_id                 NUMBER(10),
    elb_stato              VARCHAR2(50),
    elb_log                VARCHAR2(4000),
    ent_entita_cod         VARCHAR2(25),
    elb_rows               NUMBER,
    log_id_sessione        VARCHAR2(20),
    elb_data_inizio        TIMESTAMP(6),
    elb_data_fine          TIMESTAMP(6),
	elb_data_dec           TIMESTAMP(6),
	pdv_punto_vendita_cod  NUMBER(10)
)
TABLESPACE dati_mgdwh;

CREATE UNIQUE INDEX ela_elaborazione_cod_idx ON
    mdwh_elaborazioni (
        elb_elaborazione_cod
    ASC );

CREATE INDEX ela_entita_cod_idx ON
    mdwh_elaborazioni (
        ent_entita_cod
    ASC );

CREATE INDEX ela_entita_pdv_idx ON
    mdwh_elaborazioni (
        pdv_punto_vendita_cod
    ASC );