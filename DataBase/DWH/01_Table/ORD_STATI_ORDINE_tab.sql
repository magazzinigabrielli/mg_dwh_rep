CREATE TABLE ORD_STATI_ORDINE (
    STR_STATO_ORDINE_COD           NUMBER NOT NULL CONSTRAINT ORD_STATI_ORD_STR_STATO_ORD_PK PRIMARY KEY,
    STR_DESCRIZIONE                VARCHAR2(255)
)
;
COMMENT ON TABLE ORD_STATI_ORDINE IS 'Anagrafica stati ordine';
COMMENT ON COLUMN ORD_STATI_ORDINE.STR_DESCRIZIONE IS 'Descrizione stato ordine';
COMMENT ON COLUMN ORD_STATI_ORDINE.STR_STATO_ORDINE_COD IS 'Codice stato ordine';

