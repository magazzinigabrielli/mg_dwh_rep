PROMPT CREATING TABLE 'MDWH_SORGENTI_DETT';
CREATE TABLE mdwh_sorgenti_dett (
    srd_sorgenti_dett_cod   VARCHAR2(25),
    srd_schema              VARCHAR2(50),
    srd_tabella_nome        VARCHAR2(255),
    srd_descrizione         VARCHAR2(255),
    src_sorgente_cod        VARCHAR2(25),
    srd_stat                VARCHAR2(2)
)
TABLESPACE dati_mgdwh;

CREATE UNIQUE INDEX sor_sorgenti_dett_cod_idx ON
    mdwh_sorgenti_dett (
        srd_sorgenti_dett_cod
    ASC );

CREATE INDEX sor_sorgente_cod_idx1 ON
    mdwh_sorgenti_dett (
        src_sorgente_cod
    ASC );