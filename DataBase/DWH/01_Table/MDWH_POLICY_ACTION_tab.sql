PROMPT CREATING TABLE 'MDWH_POLICY_ACTION';
CREATE TABLE mdwh_policy_action (
    plc_policy_action_cod   VARCHAR2(25),
    plc_descrizione         VARCHAR2(255),
    plc_tipo_frequenza      VARCHAR2(10),
    plc_frequenza           NUMBER,
    plc_data_ini            TIMESTAMP DEFAULT to_timestamp('01010001', 'ddmmyyyy'),
    plc_data_fin            TIMESTAMP DEFAULT to_timestamp('31129999', 'DDMMYYYY'),
    plc_stat                VARCHAR2(2)
)
TABLESPACE dati_mgdwh;

CREATE UNIQUE INDEX pol_policy_action_cod_idx ON
    mdwh_policy_action (
        plc_policy_action_cod
    ASC );