PROMPT CREATING TABLE 'MDWH_SORGENTI';
CREATE TABLE mdwh_sorgenti (
    src_sorgente_cod   VARCHAR2(25),
    src_descrizione    VARCHAR2(255),
    src_tipologia      VARCHAR2(255),
    src_host           VARCHAR2(255),
    src_service_name   VARCHAR2(255),
    src_stat           VARCHAR2(2)
)
TABLESPACE dati_mgdwh;


CREATE UNIQUE INDEX sor_sorgente_cod_idx ON
    mdwh_sorgenti (
        src_sorgente_cod
    ASC );