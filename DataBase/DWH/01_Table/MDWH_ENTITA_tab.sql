PROMPT CREATING TABLE 'MDWH_ENTITA';
CREATE TABLE mdwh_entita (
    ent_entita_cod          VARCHAR2(25),
    ent_descrizione         VARCHAR2(255),
    ent_last_import         TIMESTAMP DEFAULT to_timestamp('01010001', 'ddmmyyyy'),
    ent_job_id              NUMBER(10),
    plc_policy_cod_import   VARCHAR2(25),
    plc_policy_cod_delete   VARCHAR2(25),
    ent_stat                VARCHAR2(2),
    src_sorgente_cod        VARCHAR2(25),
    srd_sorgente_dett_cod   VARCHAR2(25),
    ent_tipo_import         VARCHAR2(5),
	ENT_GRADO_PARALLEL	    NUMBER,
	ENT_FILTRO	            VARCHAR2(4000)
	ENT_FILTRO_DATA_DA      TIMESTAMP
)
TABLESPACE dati_mgdwh;
--
CREATE UNIQUE INDEX ent_entita_cod_idx ON
    mdwh_entita (
        ent_entita_cod
    ASC );

CREATE INDEX ent_policy_cod_delete_idx ON
    mdwh_entita (
        plc_policy_cod_delete
    ASC );

CREATE INDEX ent_policy_cod_import_idx ON
    mdwh_entita (
        plc_policy_cod_import
    ASC );

CREATE INDEX ent_sorgente_cod_idx ON
    mdwh_entita (
        src_sorgente_cod
    ASC );

CREATE INDEX ent_srd_cod_idx ON
    mdwh_entita (
        srd_sorgente_dett_cod
    ASC );