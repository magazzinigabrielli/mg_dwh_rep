SET TIMING ON;
SET SERVEROUTPUT ON;
/* Formatted on 27/09/2019 11:56:48 (QP5 v5.215.12089.38647) */
INSERT INTO VND_VENDITE_HEAD (PDV_PUNTO_VENDITA_COD,
                              CRT_CARTA_COD,
                              CLI_CLIENTE_COD,
                              VNH_DATA_VEND,
                              VNH_NUMERO_CASSA,
                              VNH_NUMERO_SCONTRINO,
                              VNH_CASSIERE_COD,
                              VNH_CASSIERE,
                              VNH_SCONTRINO_TOT,
                              VNH_SCAN_ITEMS_TOT,
                              VNH_ITEMS_TOT,
                              JOB_ID,
                              DATA_IMPORT,
                              VNH_SOURCE,
                              CRT_CARTA_COD_XK,
                              VNH_ANNO,
                              VNH_MESE,
                              VNH_RESO,
                              VNH_ANNULLAMENTO,
                              VNH_EAN_ORDER,
                              VNH_DELIVERY_TYPE,
                              VNH_TERM_TYPE)
   SELECT  PDV_PUNTO_VENDITA_COD,
          NULL CRT_CARTA_COD,
          NULL CLI_CLIENTE_COD,
          TO_DATE (VNH_DATA_VEND_DATA || ' ' || VND_DATA_VEND_ORA,
                   'YYYY-MM-DD hh24:mi:ss')
             VNH_DATA_VEND,
          VNH_NUMERO_CASSA,
          VNH_NUMERO_SCONTRINO,
          VNH_CASSIERE_COD,
          VNH_CASSIERE,
          VNH_SCONTRINO_TOT,
          VNH_SCAN_ITEMS_TOT,
          VNH_ITEMS_TOT,
          JOB_ID,
          SYSDATE DATA_IMPORT,
          'WB' VNH_SOURCE,
          CRT_CARTA_COD_XK,
          VNH_ANNO,
          VNH_MESE,
          VNH_RESO,
          VNH_ANNULLAMENTO,
          VNH_EAN_ORDER,
          VNH_DELIVERY_TYPE,
          VNH_TERM_TYPE
     FROM VND_VENDITE_HEAD_WN
    WHERE VNH_ANNO >= 2018;
    
COMMIT;
