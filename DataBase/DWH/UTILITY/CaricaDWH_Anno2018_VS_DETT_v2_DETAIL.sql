/* Formatted on 09/09/2019 16:39:22 (QP5 v5.215.12089.38647) */
SET TIMING ON;
SET SERVEROUTPUT ON;
DECLARE
   C_LIMIT    PLS_INTEGER := 50000;

   CURSOR VND_CUR
   IS
      SELECT DET.VND_VENDUTO_DETAIL_ID,
             DECODE (LENGTH (CRT_CARTA_COD_XK),
                     13, SUBSTR (CRT_CARTA_COD_XK, 1, 12),
                     CRT_CARTA_COD_XK)
        FROM VND_VENDITE_DETAIL DET
       WHERE     CRT_CARTA_COD_XK IS NOT NULL
             AND CRT_CARTA_COD IS NULL
             AND VND_ANNO = 2018
             AND VND_MESE between 7 anD 12;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt       NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT
         UPDATE VND_VENDITE_DETAIL
            SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                   (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                      FROM ANA_CARTE
                     WHERE CRT_CARTA_COD = vTAB_CRT (INDX))
          WHERE     VND_VENDUTO_DETAIL_ID = vTAB_ID (INDX)
                AND VND_ANNO = 2018;
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
END;