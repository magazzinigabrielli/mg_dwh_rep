/* Formatted on 28/08/2019 18:42:01 (QP5 v5.215.12089.38647) */
INSERT INTO VND_VENDITE_HEAD (PDV_PUNTO_VENDITA_COD,
                              VNH_DATA_VEND,
                              VNH_ANNO,
                              VNH_MESE,
                              VNH_NUMERO_CASSA,
                              VNH_NUMERO_SCONTRINO,
                              VNH_CASSIERE_COD,
                              VNH_CASSIERE,
                              VNH_SCONTRINO_TOT,
                              VNH_SCAN_ITEMS_TOT,
                              VNH_ITEMS_TOT,
                              JOB_ID,
                              DATA_IMPORT,
                              CRT_CARTA_COD_XK,
                              VNH_RESO,
                              VNH_ANNULLAMENTO,
                              VNH_TERM_TYPE,
                              VNH_DELIVERY_TYPE)
   SELECT /*+ PARALLEL(HEAD 12) */
         HEAD.SID PDV_PUNTO_VENDITA_COD,
          TO_TIMESTAMP (HEAD.DT_DATE || ' ' || HEAD.DT_TIME,
                        'YYYY-MM-DD hh24:mi:ss')
             VNH_DATA_VEND,
          EXTRACT (YEAR FROM TO_TIMESTAMP (HEAD.DT_DATE, 'YYYY-MM-DD'))
             VNH_ANNO,
          EXTRACT (MONTH FROM TO_TIMESTAMP (HEAD.DT_DATE, 'YYYY-MM-DD'))
             VNH_MESE,
          HEAD.TID VNH_NUMERO_CASSA,
          HEAD.XACTNBR VNH_NUMERO_SCONTRINO,
          HEAD.OPID VNH_CASSIERE_COD,
          HEAD.OPIDNAME VNH_CASSIERE,
          HEAD.XACTTOTAL VNH_SCONTRINO_TOT,
          HEAD.SCANITEMS VNH_SCAN_ITEMS_TOT,
          HEAD.TOTALITEMS VNH_ITEMS_TOT,
          -1 JOB_ID,
          SYSDATE,
          DECODE (LENGTH (LOY.CARD), 13, SUBSTR (LOY.CARD, 1, 12), LOY.CARD),
          CASE
             WHEN     HEAD.PRV_VOID = 0
                  AND HEAD.PRV_VOID2 = 0
                  AND HEAD.XACTTOTAL < 0
             THEN
                'S'
             ELSE
                NULL
          END
             VNH_RESO,
          CASE
             WHEN HEAD.PRV_VOID = 1 AND HEAD.PRV_VOID2 = 1 THEN 'S'
             ELSE NULL
          END
             VNH_ANNULLAMENTO,
          HEAD.TERM_TYPE VNH_TERM_TYPE,
          HEAD.DELIVERY_TYPE VNH_DELIVERY_TYPE
     FROM    TL_HEADER@CRSMG HEAD
          LEFT JOIN
             TL_LOYALTY@CRSMG LOY
          ON     HEAD.SID = LOY.SID
             AND HEAD.DT_DATE = LOY.DT_DATE
             AND HEAD.DT_TIME = LOY.DT_TIME
             AND HEAD.TID = LOY.TID
             AND HEAD.XACTNBR = LOY.XACTNBR
    WHERE     --HEAD.SID = 401
              HEAD.DT_DATE >= '2019-01-01'
          AND HEAD.DT_DATE < '2020-01-01'
          AND HEAD.XVOID = 0                                                --
          AND HEAD.XSUSP = 0                              --Escludo le sospese
          AND HEAD.XACTTOTAL <> 0