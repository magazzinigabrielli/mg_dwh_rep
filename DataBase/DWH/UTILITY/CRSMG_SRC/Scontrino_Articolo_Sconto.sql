SELECT 
    scntr.sid,
    scntr.dt_date,
    scntr.tid,
    scntr.dt_time,
    scntr.xactnbr,
    scntr.opid,
    scntr.itmidx,
    scntr.ean_scan,
    scntr.intcode,
    scntr.itemdescr,
    scntr.itm_wgt,
    scntr.qty,
    scntr.net_amt,
    scntr.itm_void,
    scntr.itm_vded,
    des.qty AS qty1,
    des.amount,
    des.discount,
    des.itmidx AS itmidx1,
    des.disctype
  FROM tl_items_nor scntr,
    tl_items_dsc des
WHERE scntr.sid = 505
  AND scntr.dt_date = '2019-03-02'
  AND scntr.tid = 3
  AND scntr.xactnbr = 228
  AND scntr.itm_void = 0
  AND scntr.itm_vded = 0
  AND scntr.sid = des.sid(+)
  AND scntr.dt_date = des.dt_date(+)
  AND scntr.dt_time = des.dt_time(+)
  AND scntr.tid = des.tid(+)
  AND scntr.xactnbr = des.xactnbr(+)
  AND scntr.itmidx = des.itmidx(+)
ORDER BY scntr.sid,
    scntr.dt_date,
    scntr.tid,
    scntr.dt_time,
    scntr.xactnbr,
    scntr.itmidx;