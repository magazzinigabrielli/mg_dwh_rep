/* Formatted on 27/11/2019 16:12:43 (QP5 v5.215.12089.38647) */
SET SERVEROUTPUT ON;

BEGIN
   FOR rec IN (SELECT PDV_PUNTO_VENDITA_COD,
                      ZON_ZONA_COD,
                      ZPV_ISOCRONA,
                      -1 JOB_ID,
                      SYSDATE DATA_IMPORT
                 FROM ZON_ZONE_REL_PDV#)
   LOOP
      BEGIN
         MERGE INTO MGDWH.ZON_ZONE_REL_PDV A
              USING (SELECT rec.PDV_PUNTO_VENDITA_COD PDV_PUNTO_VENDITA_COD,
                            rec.ZON_ZONA_COD ZON_ZONA_COD,
                            rec.ZPV_ISOCRONA ZPV_ISOCRONA,
                            -1 JOB_ID,
                            SYSDATE DATA_IMPORT
                       FROM DUAL) B
                 ON (    A.PDV_PUNTO_VENDITA_COD = rec.PDV_PUNTO_VENDITA_COD
                     AND A.ZON_ZONA_COD = rec.ZON_ZONA_COD)
         WHEN NOT MATCHED
         THEN
            INSERT     (PDV_PUNTO_VENDITA_COD,
                        ZON_ZONA_COD,
                        ZPV_ISOCRONA,
                        JOB_ID,
                        DATA_IMPORT)
                VALUES (B.PDV_PUNTO_VENDITA_COD,
                        B.ZON_ZONA_COD,
                        B.ZPV_ISOCRONA,
                        B.JOB_ID,
                        B.DATA_IMPORT)
         WHEN MATCHED
         THEN
            UPDATE SET
               A.ZPV_ISOCRONA = B.ZPV_ISOCRONA,
               A.JOB_ID = B.JOB_ID,
               A.DATA_IMPORT = B.DATA_IMPORT;
      EXCEPTION
         WHEN OTHERS
         THEN
            --DBMS_OUTPUT.PUT_LINE('ERR:'||SQLERRM);
            UPDATE ZON_ZONE_REL_PDV#
               SET IMPORT_ERR = 'ERROR'
             WHERE     PDV_PUNTO_VENDITA_COD = rec.PDV_PUNTO_VENDITA_COD
                   AND ZON_ZONA_COD = rec.ZON_ZONA_COD;
      END;
   END LOOP;
END;