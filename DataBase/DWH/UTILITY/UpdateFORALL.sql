/* Formatted on 30/08/2019 12:23:10 (QP5 v5.215.12089.38647) */
DECLARE
   C_LIMIT    PLS_INTEGER := 50000;

   CURSOR VNH_CUR
   IS
      SELECT VNH_VENDUTO_HEAD_ID, CRT_CARTA_COD_XK
        FROM VND_VENDITE_HEAD HEAD
       WHERE     CRT_CARTA_COD_XK IS NOT NULL
             AND CRT_CARTA_COD IS NULL
             AND VNH_ANNO = 2018
             AND VNH_MESE = 12;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt      NUMBER := 0;
BEGIN
   OPEN VNH_CUR;

   LOOP
      FETCH VNH_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT SAVE EXCEPTIONS
         UPDATE VND_VENDITE_HEAD
            SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                   (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                      FROM ANA_CARTE
                     WHERE CRT_CARTA_COD = vTAB_CRT (INDX))
          WHERE VNH_VENDUTO_HEAD_ID = vTAB_ID(INDX) 
            AND VNH_ANNO = 2018
            AND VNH_MESE =12;
            DBMS_OUTPUT.PUT_LINE (
               'Num:'||SQL%ROWCOUNT);
    vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE (
               'Num:'||vInt);
      COMMIT;
   END LOOP;
   DBMS_OUTPUT.PUT_LINE (
               'FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;