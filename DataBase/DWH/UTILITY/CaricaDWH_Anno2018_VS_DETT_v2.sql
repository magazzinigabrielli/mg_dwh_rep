/* Formatted on 09/09/2019 16:39:22 (QP5 v5.215.12089.38647) */
SET TIMING ON;
SET SERVEROUTPUT ON;
spool "Dett_ven201805.txt" 

DECLARE
   i          NUMBER;
   vAnno      NUMBER;
   vMese      NUMBER;
   vDay       NUMBER;
   vDataDec   VARCHAR2 (10);
   vdata      DATE;

   CURSOR cur_vend_det (
      P_DATA_DEC VARCHAR2)
   IS
      SELECT /*+ PARALLEL(DVEND 12) */
             NULL VND_VENDUTO_DETAIL_ID,
             TO_TIMESTAMP (DVEND.DT_DATE || ' ' || DVEND.DT_TIME,
                           'YYYY-MM-DD hh24:mi:ss')
                VND_DATA_VEND,
             DVEND.SID PDV_PUNTO_VENDITA_COD,
             DVEND.TID VND_NUMERO_CASSA,
             DVEND.XACTNBR VND_NUMERO_SCONTRINO,
             DVEND.OPID VNH_CASSIERE_COD,
             DVEND.SEGNBR VND_PROGRESSIVO_RIGA,
             NULL ART_ARTICOLO_COD,
             DVEND.EAN_SCAN VND_EAN_SCAN,
             DECODE (DVEND.ITM_WGT, 0, 'U', 'P') VND_TIPO_ART,
             CASE
                WHEN DVEND.ITM_WGT = 0 THEN DVEND.QTY
                ELSE ROUND (DVEND.QTY / 1000, 3)
             END
                VND_QUANTITA,
             DVEND.UNITP AS VND_PREZZO_UNITARIO,
             DVEND.NET_AMT AS VND_IMPORTO_VENDUTO,
             DVEND.VATRATE / 100 AS VND_PERCENTUALE_IVA,
             DVEND.ITM_VDED AS VND_ARTICOLO_STORNATO,
             DVEND.ITM_VOID AS VND_STORNO_ARTICOLO,
             DVEND.ITM_REF AS VND_ARTICOLO_RESO,
             DVEND.SUPPCODE AS VND_FORNITORE_COD,
             NULL CRT_CARTA_COD,
             NULL CLI_CLIENTE_COD,
             'VS' AS VND_SOURCE,
             LOY.CARD CRT_CARTA_COD_XK,
             NULL PRM_PROMOZIONE_COD,
             NULL VND_SCONTO_PROMO,
             EXTRACT (YEAR FROM TO_TIMESTAMP (DVEND.DT_DATE, 'YYYY-MM-DD'))
                VND_ANNO,
             EXTRACT (MONTH FROM TO_TIMESTAMP (DVEND.DT_DATE, 'YYYY-MM-DD'))
                VND_MESE,
             -1 JOB_ID,
             SYSDATE DATA_IMPORT,
             DVEND.EAN_ORDER,
             NULL PRM_PROMOZIONE_COD_XK,
             TO_NUMBER (DVEND.INTCODE) ART_ARTICOLO_COD_XK
        FROM    TL_ITEMS_NOR@CRSMG DVEND
             LEFT JOIN
                TL_LOYALTY@CRSMG LOY
             ON     DVEND.SID = LOY.SID
                AND DVEND.DT_DATE = LOY.DT_DATE
                AND DVEND.DT_TIME = LOY.DT_TIME
                AND DVEND.TID = LOY.TID
                AND DVEND.XACTNBR = LOY.XACTNBR
       WHERE DVEND.DT_DATE = P_DATA_DEC;

   TYPE fetch_array IS TABLE OF cur_vend_det%ROWTYPE;

   s_array    fetch_array;
BEGIN
   --FOR i IN 2458151..2458484 LOOP
   FOR i IN 2458454 .. 2458484
   LOOP
      --

      vData := TO_DATE (i, 'J');

      SELECT EXTRACT (YEAR FROM vData) INTO vAnno FROM DUAL;

      SELECT EXTRACT (MONTH FROM vData) INTO vMese FROM DUAL;

      SELECT EXTRACT (DAY FROM vData) INTO vDay FROM DUAL;

      vDataDec :=
         vAnno || '-' || LPAD (vMese, 2, '0') || '-' || LPAD (vDay, 2, '0');
      DBMS_OUTPUT.put_line (vDataDec);

      OPEN cur_vend_det (vDataDec);

      LOOP
         FETCH cur_vend_det
         BULK COLLECT INTO s_array
         LIMIT 50000;
         
         FOR elem IN 1 .. s_array.COUNT LOOP
           SELECT VND_VND_ID.NEXTVAL 
             INTO s_array(elem).VND_VENDUTO_DETAIL_ID
             FROM DUAL;
         END LOOP;

         FORALL i IN 1 .. s_array.COUNT
            INSERT INTO VND_VENDITE_DETAIL
                 VALUES s_array (i);

         --DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
         COMMIT;
         EXIT WHEN cur_vend_det%NOTFOUND;
      END LOOP;

      CLOSE cur_vend_det;
   --
   END LOOP;
END;

