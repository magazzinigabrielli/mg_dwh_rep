/* Formatted on 03/09/2019 09:23:25 (QP5 v5.215.12089.38647) */
SET SERVEROUTPUT ON;

DECLARE
   CURSOR cur_vend_det
   IS
      SELECT /*+ PARALLEL(DVEND 12) */
            MGDWH.VND_VND_ID.NEXTVAL VND_VENDUTO_DETAIL_ID,
             TO_TIMESTAMP (DVEND.DT_DATE || ' ' || DVEND.DT_TIME,
                           'YYYY-MM-DD hh24:mi:ss')
                VND_DATA_VEND,
             DVEND.SID PDV_PUNTO_VENDITA_COD,
             DVEND.TID VND_NUMERO_CASSA,
             DVEND.XACTNBR VND_NUMERO_SCONTRINO,
             DVEND.OPID VNH_CASSIERE_COD,
             DVEND.SEGNBR VND_PROGRESSIVO_RIGA,
             NULL ART_ARTICOLO_COD,
             DVEND.EAN_SCAN VND_EAN_SCAN,
             DECODE (DVEND.ITM_WGT, 0, 'U', 'P') VND_TIPO_ART,
             CASE
                WHEN DVEND.ITM_WGT = 0 THEN DVEND.QTY
                ELSE ROUND (DVEND.QTY / 1000, 3)
             END
                VND_QUANTITA,
             DVEND.UNITP AS VND_PREZZO_UNITARIO,
             DVEND.NET_AMT AS VND_IMPORTO_VENDUTO,
             DVEND.VATRATE / 100 AS VND_PERCENTUALE_IVA,
             DVEND.ITM_VDED AS VND_ARTICOLO_STORNATO,
             DVEND.ITM_VOID AS VND_STORNO_ARTICOLO,
             DVEND.ITM_REF AS VND_ARTICOLO_RESO,
             DVEND.SUPPCODE AS VND_FORNITORE_COD,
             NULL CRT_CARTA_COD,
             NULL CLI_CLIENTE_COD,
             'VS' AS VND_SOURCE,
             LOY.CARD CRT_CARTA_COD_XK,
             NULL PRM_PROMOZIONE_COD,
             NULL VND_SCONTO_PROMO,
             EXTRACT (YEAR FROM TO_TIMESTAMP (DVEND.DT_DATE, 'YYYY-MM-DD'))
                VND_ANNO,
             EXTRACT (MONTH FROM TO_TIMESTAMP (DVEND.DT_DATE, 'YYYY-MM-DD'))
                VND_MESE,
             -1 JOB_ID,
             SYSDATE DATA_IMPORT,
             DVEND.EAN_ORDER,
             NULL PRM_PROMOZIONE_COD_XK,
             TO_NUMBER (DVEND.INTCODE) ART_ARTICOLO_COD_XK
        FROM    TL_ITEMS_NOR@CRSMG DVEND
             LEFT JOIN
                TL_LOYALTY@CRSMG LOY
             ON     DVEND.SID = LOY.SID
                AND DVEND.DT_DATE = LOY.DT_DATE
                AND DVEND.DT_TIME = LOY.DT_TIME
                AND DVEND.TID = LOY.TID
                AND DVEND.XACTNBR = LOY.XACTNBR
       WHERE DVEND.DT_DATE >= '2019-01-01' AND DVEND.DT_DATE < '2019-02-01';

   TYPE fetch_array IS TABLE OF cur_vend_det%ROWTYPE;

   s_array   fetch_array;
BEGIN
   OPEN cur_vend_det;

   LOOP
      FETCH cur_vend_det
      BULK COLLECT INTO s_array
      LIMIT 10000;

      FORALL i IN 1 .. s_array.COUNT
         INSERT INTO VND_VENDITE_DETAIL
              VALUES s_array (i);

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      COMMIT;
      EXIT WHEN cur_vend_det%NOTFOUND;
   END LOOP;

   CLOSE cur_vend_det;
END;
/

/* Formatted on 03/09/2019 10:16:24 (QP5 v5.215.12089.38647) */
DECLARE
   C_LIMIT    PLS_INTEGER := 10000;

   CURSOR VND_CUR
   IS
      SELECT DET.VND_VENDUTO_DETAIL_ID,
             DECODE (LENGTH (CRT_CARTA_COD_XK),
                     13, SUBSTR (CRT_CARTA_COD_XK, 1, 12),
                     CRT_CARTA_COD_XK)
        FROM VND_VENDITE_DETAIL DET
       WHERE     CRT_CARTA_COD_XK IS NOT NULL
             AND CRT_CARTA_COD IS NULL
             AND VND_ANNO = 2017
             AND VND_MESE = 12;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt       NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT SAVE EXCEPTIONS
         UPDATE VND_VENDITE_DETAIL
            SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                   (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                      FROM ANA_CARTE
                     WHERE CRT_CARTA_COD = vTAB_CRT (INDX))
          WHERE     VND_VENDUTO_DETAIL_ID = vTAB_ID (INDX)
                AND VND_ANNO = 2017
                AND VNd_MESE = 12;

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE ('Num:' || vInt);
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;
--
/* Formatted on 03/09/2019 10:24:33 (QP5 v5.215.12089.38647) */
SET SERVEROUTPUT ON;
DECLARE
   C_LIMIT    PLS_INTEGER := 10000;

   CURSOR VND_CUR
   IS
      SELECT DET.VND_VENDUTO_DETAIL_ID, ART_ARTICOLO_COD_XK
        FROM VND_VENDITE_DETAIL DET
       WHERE     ART_ARTICOLO_COD_XK IS NOT NULL
             AND ART_ARTICOLO_COD IS NULL
             AND VND_ANNO = 2017
             AND VND_MESE = 12;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt       NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT SAVE EXCEPTIONS
         UPDATE VND_VENDITE_DETAIL
            SET ART_ARTICOLO_COD =
                   (SELECT ART_ARTICOLO_COD
                      FROM ANA_ARTICOLI
                     WHERE ART_ARTICOLO_COD = vTAB_CRT (INDX))
          WHERE     VND_VENDUTO_DETAIL_ID = vTAB_ID (INDX)
                AND VND_ANNO = 2017
                AND VNd_MESE = 12;

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE ('Num:' || vInt);
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;

--Recupero promo
/* Formatted on 03/09/2019 10:44:27 (QP5 v5.215.12089.38647) */
SET SERVEROUTPUT ON;

DECLARE
   C_LIMIT   PLS_INTEGER := 1000;

   CURSOR VND_CUR
   IS
      SELECT DSCNT.SID,
             DSCNT.DT_DATE,
             DSCNT.XACTNBR,
             DSCNT.TID,
             DSCNT.ITMIDX +1 ITMIDX,
             TPROMO.ID_PROMO_OFF,
             DSCNT.DISCOUNT,
             DSCNT.INTCODE
        FROM    TL_ITEMS_DSC@CRSMG DSCNT
             INNER JOIN
                KCRM.OPROMO_HEAD@KPROMO TPROMO
             ON DSCNT.COMBID = TPROMO.PROMO_LINK_ID
       WHERE     DSCNT.DISCTYPE IN (1, 2, 5, 7, 8, 21, 22, 23, 24)
            AND DSCNT.DT_DATE >= '2017-12-01'
       AND DSCNT.DT_DATE < '2018-01-01';

   TYPE fetch_array IS TABLE OF VND_CUR%ROWTYPE;

   s_array   fetch_array;
   vInt      NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO s_array
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN s_array.COUNT () = 0;


      FORALL INDX IN 1 .. s_array.COUNT SAVE EXCEPTIONS
         UPDATE /*+ PARALLEL(DET 4 )*/ VND_VENDITE_DETAIL DET
            SET PRM_PROMOZIONE_COD_XK = s_array (INDX).ID_PROMO_OFF,
                VND_SCONTO_PROMO = s_array (INDX).DISCOUNT
          WHERE PDV_PUNTO_VENDITA_COD = s_array (INDX).SID
                AND VND_NUMERO_CASSA = s_array (INDX).TID
                AND VND_NUMERO_SCONTRINO = s_array (INDX).XACTNBR
                --AND VND_PROGRESSIVO_RIGA = s_array (INDX).ITMIDX
                AND ART_ARTICOLO_COD = TO_NUMBER(s_array (INDX).INTCODE)
                AND TRUNC (VND_DATA_VEND) =
                       TO_DATE (s_array (INDX).DT_DATE, 'YYYY-MM-DD')
                AND VND_ANNO = 2017
                AND VND_MESE = 12;

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE ('Num:' || vInt);
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;
/

SET SERVEROUTPUT ON;
DECLARE
   C_LIMIT    PLS_INTEGER := 10000;

   CURSOR VND_CUR
   IS
      SELECT DET.VND_VENDUTO_DETAIL_ID, PRM_PROMOZIONE_COD_XK
        FROM VND_VENDITE_DETAIL DET
       WHERE     PRM_PROMOZIONE_COD_XK IS NOT NULL
             AND PRM_PROMOZIONE_COD IS NULL
             AND VND_ANNO = 2017
             AND VND_MESE = 11;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt       NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT SAVE EXCEPTIONS
         UPDATE VND_VENDITE_DETAIL
            SET PRM_PROMOZIONE_COD =
                   (SELECT PRM_PROMOZIONE_COD
                      FROM ANA_PROMOZIONI
                     WHERE PRM_PROMOZIONE_COD = vTAB_CRT (INDX))
          WHERE     VND_VENDUTO_DETAIL_ID = vTAB_ID (INDX)
                AND VND_ANNO = 2017
                AND VNd_MESE = 11;

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE ('Num:' || vInt);
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;
--
SET SERVEROUTPUT ON;
DECLARE
   C_LIMIT    PLS_INTEGER := 10000;

   CURSOR VND_CUR
   IS
      SELECT DET.VND_VENDUTO_DETAIL_ID, PRM_PROMOZIONE_COD_XK
        FROM VND_VENDITE_DETAIL DET
       WHERE     PRM_PROMOZIONE_COD_XK IS NOT NULL
             AND PRM_PROMOZIONE_COD IS NULL
             AND VND_ANNO = 2017
             AND VND_MESE = 12;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt       NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT SAVE EXCEPTIONS
         UPDATE VND_VENDITE_DETAIL
            SET PRM_PROMOZIONE_COD =
                   (SELECT PRM_PROMOZIONE_COD
                      FROM ANA_PROMOZIONI
                     WHERE PRM_PROMOZIONE_COD = vTAB_CRT (INDX))
          WHERE     VND_VENDUTO_DETAIL_ID = vTAB_ID (INDX)
                AND VND_ANNO = 2017
                AND VNd_MESE = 12;

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE ('Num:' || vInt);
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;
--
SET SERVEROUTPUT ON;
DECLARE
   C_LIMIT    PLS_INTEGER := 10000;

   CURSOR VND_CUR
   IS
      SELECT DET.VND_VENDUTO_DETAIL_ID, PRM_PROMOZIONE_COD_XK
        FROM VND_VENDITE_DETAIL DET
       WHERE     PRM_PROMOZIONE_COD_XK IS NOT NULL
             AND PRM_PROMOZIONE_COD IS NULL
             AND VND_ANNO = 2017
             AND VND_MESE = 12;

   TYPE VNH_ID_tab_t IS TABLE OF NUMBER (10);

   TYPE CRT_XK_tab_t IS TABLE OF VARCHAR2 (50);

   vTAB_ID    VNH_ID_tab_t;
   vTAB_CRT   CRT_XK_tab_t;
   vInt       NUMBER := 0;
BEGIN
   OPEN VND_CUR;

   LOOP
      FETCH VND_CUR
      BULK COLLECT INTO vTAB_ID, vTAB_CRT
      LIMIT C_LIMIT; -- This will make sure that every iteration has 100 records selected

      EXIT WHEN vTAB_ID.COUNT () = 0;


      FORALL INDX IN 1 .. vTAB_ID.COUNT SAVE EXCEPTIONS
         UPDATE VND_VENDITE_DETAIL
            SET PRM_PROMOZIONE_COD =
                   (SELECT PRM_PROMOZIONE_COD
                      FROM ANA_PROMOZIONI
                     WHERE PRM_PROMOZIONE_COD = vTAB_CRT (INDX))
          WHERE     VND_VENDUTO_DETAIL_ID = vTAB_ID (INDX)
                AND VND_ANNO = 2017
                AND VNd_MESE = 12;

      DBMS_OUTPUT.PUT_LINE ('Num:' || SQL%ROWCOUNT);
      vInt := vInt + C_LIMIT;
      DBMS_OUTPUT.PUT_LINE ('Num:' || vInt);
      COMMIT;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('FINE');
EXCEPTION
   WHEN OTHERS
   THEN
      IF SQLCODE = -24381
      THEN
         FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>


         END LOOP;
      ELSE
         RAISE;
      END IF;
END;