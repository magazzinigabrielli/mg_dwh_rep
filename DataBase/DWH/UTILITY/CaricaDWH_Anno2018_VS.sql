SET TIMING ON;
SET SERVEROUTPUT ON;
spool "test_ven2018.txt" 
DECLARE
   PROCEDURE INSERT_VEND_HEAD
   IS
   BEGIN
      INSERT INTO VND_VENDITE_HEAD (PDV_PUNTO_VENDITA_COD,
                                    VNH_DATA_VEND,
                                    VNH_ANNO,
                                    VNH_MESE,
                                    VNH_NUMERO_CASSA,
                                    VNH_NUMERO_SCONTRINO,
                                    VNH_CASSIERE_COD,
                                    VNH_CASSIERE,
                                    VNH_SCONTRINO_TOT,
                                    VNH_SCAN_ITEMS_TOT,
                                    VNH_ITEMS_TOT,
                                    JOB_ID,
                                    DATA_IMPORT,
                                    CRT_CARTA_COD_XK,
                                    VNH_RESO,
                                    VNH_ANNULLAMENTO,
                                    VNH_TERM_TYPE,
                                    VNH_DELIVERY_TYPE)
         SELECT /*+ PARALLEL(HEAD 12) */
               HEAD.SID PDV_PUNTO_VENDITA_COD,
                TO_TIMESTAMP (HEAD.DT_DATE || ' ' || HEAD.DT_TIME,
                              'YYYY-MM-DD hh24:mi:ss')
                   VNH_DATA_VEND,
                EXTRACT (YEAR FROM TO_TIMESTAMP (HEAD.DT_DATE, 'YYYY-MM-DD'))
                   VNH_ANNO,
                EXTRACT (
                   MONTH FROM TO_TIMESTAMP (HEAD.DT_DATE, 'YYYY-MM-DD'))
                   VNH_MESE,
                HEAD.TID VNH_NUMERO_CASSA,
                HEAD.XACTNBR VNH_NUMERO_SCONTRINO,
                HEAD.OPID VNH_CASSIERE_COD,
                HEAD.OPIDNAME VNH_CASSIERE,
                HEAD.XACTTOTAL VNH_SCONTRINO_TOT,
                HEAD.SCANITEMS VNH_SCAN_ITEMS_TOT,
                HEAD.TOTALITEMS VNH_ITEMS_TOT,
                -1 JOB_ID,
                SYSDATE,
                DECODE (LENGTH (LOY.CARD),
                        13, SUBSTR (LOY.CARD, 1, 12),
                        LOY.CARD),
                CASE
                   WHEN     HEAD.PRV_VOID = 0
                        AND HEAD.PRV_VOID2 = 0
                        AND HEAD.XACTTOTAL < 0
                   THEN
                      'S'
                   ELSE
                      NULL
                END
                   VNH_RESO,
                CASE
                   WHEN HEAD.PRV_VOID = 1 AND HEAD.PRV_VOID2 = 1 THEN 'S'
                   ELSE NULL
                END
                   VNH_ANNULLAMENTO,
                HEAD.TERM_TYPE VNH_TERM_TYPE,
                HEAD.DELIVERY_TYPE VNH_DELIVERY_TYPE
           FROM    TL_HEADER@CRSMG HEAD
                LEFT JOIN
                   TL_LOYALTY@CRSMG LOY
                ON     HEAD.SID = LOY.SID
                   AND HEAD.DT_DATE = LOY.DT_DATE
                   AND HEAD.DT_TIME = LOY.DT_TIME
                   AND HEAD.TID = LOY.TID
                   AND HEAD.XACTNBR = LOY.XACTNBR
          WHERE                                               --HEAD.SID = 401
               HEAD .DT_DATE >= '2018-01-01'
                AND HEAD.DT_DATE < '2019-01-01'
                AND HEAD.XVOID = 0                                          --
                AND HEAD.XSUSP = 0                        --Escludo le sospese
                AND HEAD.XACTTOTAL <> 0;
   END INSERT_VEND_HEAD;

   --
   PROCEDURE UPDT_CARD_RECUPERO
   IS
      vindex   NUMBER := 0;
   BEGIN
      FOR rec
         IN (SELECT ROWID,
                    VNH_VENDUTO_HEAD_ID,
                    PDV_PUNTO_VENDITA_COD,
                    CRT_CARTA_COD,
                    CLI_CLIENTE_COD,
                    CRT_CARTA_COD_XK
               FROM VND_VENDITE_HEAD HEAD
              WHERE     CRT_CARTA_COD_XK IS NOT NULL
                    AND CRT_CARTA_COD IS NULL
                    AND VNH_ANNO = 2018
                    AND VNH_MESE = 5)
      LOOP
         --
         UPDATE VND_VENDITE_HEAD
            SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                   (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                      FROM ANA_CARTE
                     WHERE CRT_CARTA_COD = rec.CRT_CARTA_COD_XK)
          WHERE ROWID = rec.ROWID AND VNH_ANNO = 2018;

         vindex := vindex + 1;

         IF MOD (vindex, 1000) = 0
         THEN
            COMMIT;
         END IF;
      END LOOP;
   END UPDT_CARD_RECUPERO;
BEGIN
   --INSERT_VEND_HEAD;
   UPDT_CARD_RECUPERO;
   COMMIT;
END;

/
