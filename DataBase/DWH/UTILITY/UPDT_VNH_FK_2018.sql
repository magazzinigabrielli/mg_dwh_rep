/* Formatted on 30/09/2019 08:51:16 (QP5 v5.215.12089.38647) */
SET TIMING ON;
SET SERVEROUTPUT ON;

DECLARE
   PO_ERR_SEVERITA   VARCHAR2 (32767);
   PO_ERR_CODICE     VARCHAR2 (32767);
   PO_MESSAGGIO      VARCHAR2 (32767);
   P_LIMIT           NUMBER;
   P_FK              VARCHAR2 (32767);
   P_ANNO            NUMBER;
   P_MESE            NUMBER;
BEGIN
   PO_ERR_SEVERITA := NULL;
   PO_ERR_CODICE := NULL;
   PO_MESSAGGIO := NULL;
   P_LIMIT := 50000;
   P_FK := 'ANA_CRT';
   P_ANNO := 2019;
   P_MESE := 8;
--   FOR i IN 7 .. 7
--   LOOP
      MGDWH.MGDWH_UTILITY_PCK.UPDT_VNH_FK (PO_ERR_SEVERITA,
                                           PO_ERR_CODICE,
                                           PO_MESSAGGIO,
                                           P_LIMIT,
                                           P_FK,
                                           P_ANNO,
                                           P_MESE);


      DBMS_OUTPUT.Put_Line ('PO_ERR_SEVERITA = ' || PO_ERR_SEVERITA);
      DBMS_OUTPUT.Put_Line ('PO_ERR_CODICE = ' || PO_ERR_CODICE);
      DBMS_OUTPUT.Put_Line ('PO_MESSAGGIO = ' || PO_MESSAGGIO);

      DBMS_OUTPUT.Put_Line ('');

      COMMIT;
--   END LOOP;
END;