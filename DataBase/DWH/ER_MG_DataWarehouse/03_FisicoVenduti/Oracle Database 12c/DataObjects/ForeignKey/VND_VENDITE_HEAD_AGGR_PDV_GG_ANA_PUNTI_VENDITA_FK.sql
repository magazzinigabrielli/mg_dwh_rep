--  ERROR: FK name length exceeds maximum allowed length(30) 
PROMPT CREATING FOREIGN KEY ON 'VND_VENDITE_HEAD_AGGR_PDV_GG';
ALTER TABLE vnd_vendite_head_aggr_pdv_gg
    ADD CONSTRAINT vnd_vendite_head_aggr_pdv_gg_ana_punti_vendita_fk FOREIGN KEY ( pdv_punto_vendita_cod )
        REFERENCES ana_punti_vendita ( pdv_punto_vendita_cod );