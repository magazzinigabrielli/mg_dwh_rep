PROMPT CREATING FOREIGN KEY ON 'VND_VENDITE_HEAD_AGGR_PDV_GG';
ALTER TABLE vnd_vendite_head_aggr_pdv_gg
    ADD CONSTRAINT pdv_punto_vendita_cod FOREIGN KEY ( pdv_punto_vendita_cod2 )
        REFERENCES ana_punti_vendita ( pdv_punto_vendita_cod );