PROMPT CREATING TABLE 'VND_VENDITE_HEAD_AGGR_PDV_GG';
CREATE TABLE vnd_vendite_head_aggr_pdv_gg (
    vhg_head_aggr_pdv_gg_id      NUMBER(10) NOT NULL,
    pdv_punto_vendita_cod        NUMBER(10) NOT NULL,
    vhg_anno                     NUMBER,
    vhg_data_ven                 TIMESTAMP,
    vhg_num_scontrini            NUMBER(10),
    vhg_num_scontrini_resi       NUMBER(10),
    vhg_num_scontrini_ann        NUMBER(10),
    vhg_num_articoli_ven         NUMBER(10),
    vhg_num_articoli_resi        NUMBER(10),
    vhg_tot_importo              NUMBER(13, 2),
    vhg_fid_num_scontrini        NUMBER(10),
    vhg_fid_num_scontrini_resi   NUMBER(10),
    vhg_fid_num_scontrini_ann    NUMBER(10),
    vhg_fid_num_articoli_ven     NUMBER(10),
    vhg_fid_num_articoli_resi    NUMBER(10),
    vhg_fid_tot_importo          NUMBER(13, 2),
    vhg_num_fidelity             NUMBER(10),
    vhg_num_casse                NUMBER(10),
    vhg_num_cassieri             NUMBER(10),
    vhg_source                   VARCHAR2(5),
    job_id                       NUMBER(10),
    data_import                  TIMESTAMP
)
TABLESPACE dati_mgdwh;

COMMENT ON TABLE vnd_vendite_head_aggr_pdv_gg IS
    'Aggregato vendite per pdv-giorno-fascia oraria';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_head_aggr_pdv_gg_id IS
    'Identificativo di riga';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.pdv_punto_vendita_cod IS
    'Identificativo del PDV';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_anno IS
    'Anno vendita';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_data_ven IS
    'Data di vendita';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_scontrini IS
    'Numero scontrini vendita';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_scontrini_resi IS
    'Numero Scontrini di Reso
';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_scontrini_ann IS
    'Numero Scontrini annullati';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_articoli_ven IS
    'Numero Articoli Venduti';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_articoli_resi IS
    'Numero Articoli resi';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_tot_importo IS
    'Importo del venduto';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_fid_num_scontrini IS
    'Numero Scontrini fidelity';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_fid_num_scontrini_resi IS
    'Numero Scontrini di Resi fidelity';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_fid_num_scontrini_ann IS
    'Numero Scontrini annullati fidelity';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_fid_num_articoli_ven IS
    'Numero Articoli Venduti fidelity';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_fid_num_articoli_resi IS
    'Numero articoli resi fidelity';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_fid_tot_importo IS
    'Importo del venduto fidelity';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_fidelity IS
    'Numero Card Utilizzate';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_casse IS
    'Numero Casse Utilizzate';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_num_cassieri IS
    'Numero Cassieri Utilizzati';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.vhg_source IS
    'Applicativo di Provenienza del venduto
';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.job_id IS
    'Identificativo job';

COMMENT ON COLUMN vnd_vendite_head_aggr_pdv_gg.data_import IS
    'data import record';

--  ERROR: Index name length exceeds maximum allowed length(30) 

CREATE UNIQUE INDEX vnd_vendite_head_aggr_pdv_gg_vhg_head_aggr_pdv_gg_id_idx ON
    vnd_vendite_head_aggr_pdv_gg (
        vhg_head_aggr_pdv_gg_id
    ASC );

--  ERROR: Index name length exceeds maximum allowed length(30) 

CREATE INDEX vnd_vendite_head_aggr_pdv_gg_pdv_punto_vendita_cod_idx ON
    vnd_vendite_head_aggr_pdv_gg (
        pdv_punto_vendita_cod
    ASC );