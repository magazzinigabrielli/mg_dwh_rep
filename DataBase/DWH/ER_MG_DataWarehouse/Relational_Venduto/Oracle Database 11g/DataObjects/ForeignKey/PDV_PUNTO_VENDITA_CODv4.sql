PROMPT CREATING FOREIGN KEY ON 'VND_VENDITE_HEAD';
ALTER TABLE vnd_vendite_head
    ADD CONSTRAINT pdv_punto_vendita_codv4 FOREIGN KEY ( pdv_punto_vendita_cod )
        REFERENCES ana_punti_vendita ( pdv_punto_vendita_cod );