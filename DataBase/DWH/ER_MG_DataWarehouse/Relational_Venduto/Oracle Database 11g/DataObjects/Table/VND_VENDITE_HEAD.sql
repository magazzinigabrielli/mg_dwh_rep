PROMPT CREATING TABLE 'VND_VENDITE_HEAD';
CREATE TABLE vnd_vendite_head (
    vnh_venduto_head_id     NUMBER(10) NOT NULL,
    pdv_punto_vendita_cod   NUMBER(10) NOT NULL,
    crt_carta_cod           VARCHAR2(50) NOT NULL,
    cli_cliente_cod         VARCHAR2(25) NOT NULL,
    vnh_data_vend           TIMESTAMP,
    vnh_numero_cassa        NUMBER(10),
    vnh_numero_scontrino    NUMBER(10),
    vnh_cassiere_cod        NUMBER(10),
    vnh_cassiere            VARCHAR2(255),
    vnh_scontrino_tot       NUMBER(13, 2),
    vnh_scan_items_tot      NUMBER(10),
    vnh_items_tot           NUMBER(10),
    job_id                  NUMBER(10),
    data_import             TIMESTAMP
)
TABLESPACE dati_mgdwh;

COMMENT ON TABLE vnd_vendite_head IS
    'Venduto giornaliero dettaglio testata scontrini';

COMMENT ON COLUMN vnd_vendite_head.vnh_venduto_head_id IS
    'Identificativo vendita testata';

COMMENT ON COLUMN vnd_vendite_head.pdv_punto_vendita_cod IS
    'Numero del punto vendita';

COMMENT ON COLUMN vnd_vendite_head.crt_carta_cod IS
    'Identificativo della carta loyalty
';

COMMENT ON COLUMN vnd_vendite_head.cli_cliente_cod IS
    'Identificativo del cliente';

COMMENT ON COLUMN vnd_vendite_head.vnh_data_vend IS
    'Data di vendita';

COMMENT ON COLUMN vnd_vendite_head.vnh_numero_cassa IS
    'Identificativo della cassa';

COMMENT ON COLUMN vnd_vendite_head.vnh_numero_scontrino IS
    'Identificativo dello scontrino';

COMMENT ON COLUMN vnd_vendite_head.vnh_cassiere_cod IS
    'Codice del cassiere';

COMMENT ON COLUMN vnd_vendite_head.vnh_cassiere IS
    'Descrizione cassiere (NOME COGNOME)';

COMMENT ON COLUMN vnd_vendite_head.vnh_scontrino_tot IS
    'Totale dello scontrino';

COMMENT ON COLUMN vnd_vendite_head.vnh_scan_items_tot IS
    'Numero articoli scansionati';

COMMENT ON COLUMN vnd_vendite_head.vnh_items_tot IS
    'Numero articoli totali';

COMMENT ON COLUMN vnd_vendite_head.job_id IS
    'Identificativo job';

COMMENT ON COLUMN vnd_vendite_head.data_import IS
    'data import record';

CREATE UNIQUE INDEX end_venduto_head_id_idx ON
    vnd_vendite_head (
        vnh_venduto_head_id
    ASC );

CREATE INDEX end_cliente_cod_idx ON
    vnd_vendite_head (
        cli_cliente_cod
    ASC );

CREATE INDEX end_carta_cod_idx ON
    vnd_vendite_head (
        crt_carta_cod
    ASC );

CREATE INDEX end_punto_vendita_cod_idx ON
    vnd_vendite_head (
        pdv_punto_vendita_cod
    ASC );