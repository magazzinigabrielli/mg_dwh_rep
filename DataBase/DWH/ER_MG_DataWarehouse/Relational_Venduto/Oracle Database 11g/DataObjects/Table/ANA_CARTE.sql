PROMPT CREATING TABLE 'ANA_CARTE';
CREATE TABLE ana_carte (
    crt_carta_cod           VARCHAR2(50) NOT NULL,
    cli_cliente_cod         VARCHAR2(25) NOT NULL,
    pdv_punto_vendita_cod   NUMBER(10) NOT NULL,
    job_id                  NUMBER(10),
    data_import             TIMESTAMP,
    crt_data_inizio         TIMESTAMP,
    crt_data_fine           TIMESTAMP
)
TABLESPACE dati_mgdwh;

COMMENT ON TABLE ana_carte IS
    'Anagrafica delle carte fedeltà';

COMMENT ON COLUMN ana_carte.crt_carta_cod IS
    'Identificativo carta';

COMMENT ON COLUMN ana_carte.cli_cliente_cod IS
    'Identificativo del cliente';

COMMENT ON COLUMN ana_carte.pdv_punto_vendita_cod IS
    'Identificativo del PDV in cui è stata generata la carta';

COMMENT ON COLUMN ana_carte.job_id IS
    'Identificativo job';

COMMENT ON COLUMN ana_carte.data_import IS
    'data import record';

COMMENT ON COLUMN ana_carte.crt_data_inizio IS
    'Data attivazione carta';

COMMENT ON COLUMN ana_carte.crt_data_fine IS
    'Data disattivazione carta';

CREATE UNIQUE INDEX art_carta_cod_idx ON
    ana_carte (
        crt_carta_cod
    ASC );

CREATE INDEX art_cliente_cod_idx ON
    ana_carte (
        cli_cliente_cod
    ASC );

CREATE INDEX art_punto_vendita_cod_idx ON
    ana_carte (
        pdv_punto_vendita_cod
    ASC );