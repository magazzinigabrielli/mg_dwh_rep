PROMPT CREATING TABLE 'ANA_CLIENTI';
CREATE TABLE ana_clienti (
    cli_cliente_cod             VARCHAR2(25) NOT NULL,
    cli_cognome                 VARCHAR2(255),
    cli_nome                    VARCHAR2(255),
    cli_sesso                   VARCHAR2(5),
    cli_data_nascita            TIMESTAMP,
    cli_indirizzo               VARCHAR2(255),
    cli_numero                  VARCHAR2(50),
    cli_provincia               VARCHAR2(25),
    cli_citta                   VARCHAR2(255),
    cli_cap                     VARCHAR2(25),
    cli_telefono                VARCHAR2(255),
    cli_cellulare               VARCHAR2(255),
    cli_email                   VARCHAR2(255),
    cli_privacy_aut_term        VARCHAR2(5),
    cli_privacy_aut_prfl        VARCHAR2(5),
    cli_privacy_aut_gen_adv     VARCHAR2(5),
    cli_privacy_aut_ven_adv     VARCHAR2(5),
    cli_privacy_auth_cmp_prof   VARCHAR2(5),
    job_id                      NUMBER(10),
    data_import                 TIMESTAMP
)
TABLESPACE dati_mgdwh;

COMMENT ON TABLE ana_clienti IS
    'Anagrafica dei clienti';

COMMENT ON COLUMN ana_clienti.cli_cognome IS
    'Cognome';

COMMENT ON COLUMN ana_clienti.cli_nome IS
    'Nome';

COMMENT ON COLUMN ana_clienti.cli_sesso IS
    'Sesso';

COMMENT ON COLUMN ana_clienti.cli_data_nascita IS
    'Data di nascita';

COMMENT ON COLUMN ana_clienti.cli_indirizzo IS
    'Indirizzo via';

COMMENT ON COLUMN ana_clienti.cli_numero IS
    'Numero civico';

COMMENT ON COLUMN ana_clienti.cli_provincia IS
    'Provincia';

COMMENT ON COLUMN ana_clienti.cli_citta IS
    'Città';

COMMENT ON COLUMN ana_clienti.cli_cap IS
    'CAP';

COMMENT ON COLUMN ana_clienti.cli_telefono IS
    'Telefono';

COMMENT ON COLUMN ana_clienti.cli_cellulare IS
    'Cellulare';

COMMENT ON COLUMN ana_clienti.cli_email IS
    'Email';

COMMENT ON COLUMN ana_clienti.cli_privacy_aut_term IS
    'Autorizzazione alla visualizzazione del nome in cassa
';

COMMENT ON COLUMN ana_clienti.cli_privacy_aut_prfl IS
    'Autorizzazione alla profilazione';

COMMENT ON COLUMN ana_clienti.cli_privacy_aut_gen_adv IS
    'Autorizzazione generica al contatto a fini pubblicitari';

COMMENT ON COLUMN ana_clienti.cli_privacy_aut_ven_adv IS
    'Autorizzazione al contato per proposta di vendita';

COMMENT ON COLUMN ana_clienti.cli_privacy_auth_cmp_prof IS
    'Autorizzazione';

COMMENT ON COLUMN ana_clienti.job_id IS
    'Identificativo job
';

COMMENT ON COLUMN ana_clienti.data_import IS
    'data import record
';

CREATE UNIQUE INDEX lie_cliente_cod_idx ON
    ana_clienti (
        cli_cliente_cod
    ASC );