--  anagrafica articoli			
PROMPT CREATING TABLE 'ANA_ARTICOLI';
CREATE TABLE ana_articoli ( 
--  Codice aricolo
    art_articolo_cod              NUMBER(10) NOT NULL, 
--  Descrizione articolo standard  
    art_descrizione_std           VARCHAR2(255), 
--  Descrizione sito
    art_descrizione_sito          VARCHAR2(255), 
--  Stato estrazione  record, ‘A’=annullato
    art_stat                      VARCHAR2(2), 
--  Codice fornitore generico 
    art_fornitore_gen_cod         VARCHAR2(25), 
--  Brand articolo 
    art_brand                     VARCHAR2(255), 
--  Codice buyer
    art_ufficio_acq_cod           VARCHAR2(25), 
--  Codice Linea Prodotto
    art_linea_prodotto_cod        VARCHAR2(25), 
--  Codice Reparto
    art_reparto_cod               VARCHAR2(25), 
--  Codice Unità Organizzativa
    art_unita_organizzativa_cod   VARCHAR2(25), 
--  Codice Raggruppamento
    art_raggruppamento_cod        VARCHAR2(25), 
--  Codice Famiglia
    art_famiglia_cod              VARCHAR2(25), 
--  Ciclo di vita  dell'articolo:
--  Blank=articolo normale attivo
--  ETO=articolo eliminato (non piu’  acquistato ma possibile  presenza scorte
--  in pdv  per smaltimento giacenza)
--  SOS=Articolo sospeso (non  richiedibile al momento  dal PdV)
    art_ciclo_vita                VARCHAR2(5), 
--  Data inserimento articolo
    art_data_ini                  TIMESTAMP, 
--  Data espulsione articolo(ETO)
    art_data_eto                  TIMESTAMP, 
--  Unità di misura di vendita
    art_unita_misura              VARCHAR2(5), 
--  S=l’articolo è a peso predeterminato per cui va esposto il prezzo al KG/LT
    art_prezzo_kg_lt              VARCHAR2(5), 
--  Contiene il numero di pezzi della confezione di vendita
    art_num_pezzi                 NUMBER(10), 
--  Tipo della confezione
    art_tipo_conf                 VARCHAR2(5), 
--  Unità di misura del contenuto
    art_unit_mis_conf             VARCHAR2(5), 
--  Quantità di ogni singolo pezzo della confezione.
    art_qta_pz_conf               NUMBER(10), 
--  Questo campo contiene la quantità totale della confezione riportata nelle
--  unità di misura standard (KG,LT etc)
    art_qta_tot_conf              NUMBER(10), 
--  UM base di riferimento del prezzo
    art_unita_misura_prz          VARCHAR2(5), 
--  Imballo in vendita       
    art_imballo_vend              NUMBER(10), 
--  Imballo in acquisto 
    art_imballo_acq               NUMBER(10), 
--  Peso medio collo in Grammi
    art_peso_medio                NUMBER(10), 
--  Codice marchio articolo.
--  A-ALTRI MARCHI PL  
--  C-CONSILIA         
--  D-ARTICOLI DISCOUNT
--  E-PREMIUM          
--  L-LEADER           
--  P-PRIMO PREZZO     
--  Q-SELEZ.QUALITA' 
    art_marchio_cod               VARCHAR2(25), 
--  Descrizione marchio
    art_marchio_desc              VARCHAR2(255), 
--  Indica se il marchio è una “private label”
    art_private_lbl               VARCHAR2(5), 
--  Trattasi di articolo di tipo espositore (box che contiene articoli di
--  vendita)
    art_espositore                VARCHAR2(5), 
--  Trattasi di articolo multipack (es. fardello da 6 bot acqua)
    art_multipack                 VARCHAR2(5), 
--  Numero articoli di vendita contenuti nel multipack
    art_qta_multipack             NUMBER(10), 
--  Articolo di tipo Catering
    art_catering                  VARCHAR2(5), 
--  Vino novello
    art_vino_nov                  VARCHAR2(5), 
--  Articolo premio raccolta punti
    art_premio                    VARCHAR2(5), 
--  Articolo con gestione SPOT, acquisto una tantum
    art_spot                      VARCHAR2(5), 
--  Articolo infiammabile   
    art_infiamm                   VARCHAR2(5), 
--  Articolo non vendibile al pubblico (es. espositore)
    art_non_vendibile             VARCHAR2(5), 
--  Info per indicazione che l’eco contributo raee è compreso nel prezzo
    art_raee                      VARCHAR2(5), 
--  Articolo categoria orto
    art_arto                      VARCHAR2(5), 
--  Indica la provincia del prodotto tipico
    art_prod_tipico               VARCHAR2(5), 
--  Flag che indica se l'articolo è visibile sul sito www.gabrielliunika.it
    art_sito                      VARCHAR2(5), 
--  Identificativo job
    job_id                        NUMBER(10), 
--  data import record
    data_import                   TIMESTAMP
)
TABLESPACE dati_mgdwh;

PROMPT CREATING CHECK CONSTRAINT ON 'ANA_ARTICOLI';

ALTER TABLE ana_articoli ADD CHECK ( art_stat IN (
    'A'
) );

COMMENT ON TABLE ana_articoli IS
    'anagrafica articoli			
';

COMMENT ON COLUMN ana_articoli.art_articolo_cod IS
    'Codice aricolo';

COMMENT ON COLUMN ana_articoli.art_descrizione_std IS
    'Descrizione articolo standard  
';

COMMENT ON COLUMN ana_articoli.art_descrizione_sito IS
    'Descrizione sito
';

COMMENT ON COLUMN ana_articoli.art_stat IS
    'Stato estrazione  record, ‘A’=annullato
';

COMMENT ON COLUMN ana_articoli.art_fornitore_gen_cod IS
    'Codice fornitore generico 
';

COMMENT ON COLUMN ana_articoli.art_brand IS
    'Brand articolo 
';

COMMENT ON COLUMN ana_articoli.art_ufficio_acq_cod IS
    'Codice buyer
';

COMMENT ON COLUMN ana_articoli.art_linea_prodotto_cod IS
    'Codice Linea Prodotto
';

COMMENT ON COLUMN ana_articoli.art_reparto_cod IS
    'Codice Reparto
';

COMMENT ON COLUMN ana_articoli.art_unita_organizzativa_cod IS
    'Codice Unità Organizzativa
';

COMMENT ON COLUMN ana_articoli.art_raggruppamento_cod IS
    'Codice Raggruppamento
';

COMMENT ON COLUMN ana_articoli.art_famiglia_cod IS
    'Codice Famiglia
';

COMMENT ON COLUMN ana_articoli.art_ciclo_vita IS
    '"Ciclo di vita  dell''articolo:
Blank=articolo normale attivo
ETO=articolo eliminato (non piu’  acquistato ma possibile  presenza scorte in pdv  per smaltimento giacenza)
SOS=Articolo sospeso (non  richiedibile al momento  dal PdV)"
'
    ;

COMMENT ON COLUMN ana_articoli.art_data_ini IS
    'Data inserimento articolo
';

COMMENT ON COLUMN ana_articoli.art_data_eto IS
    'Data espulsione articolo(ETO)
';

COMMENT ON COLUMN ana_articoli.art_unita_misura IS
    'Unità di misura di vendita
';

COMMENT ON COLUMN ana_articoli.art_prezzo_kg_lt IS
    'S=l’articolo è a peso predeterminato per cui va esposto il prezzo al KG/LT
';

COMMENT ON COLUMN ana_articoli.art_num_pezzi IS
    'Contiene il numero di pezzi della confezione di vendita
';

COMMENT ON COLUMN ana_articoli.art_tipo_conf IS
    'Tipo della confezione
';

COMMENT ON COLUMN ana_articoli.art_unit_mis_conf IS
    'Unità di misura del contenuto
';

COMMENT ON COLUMN ana_articoli.art_qta_pz_conf IS
    'Quantità di ogni singolo pezzo della confezione.
';

COMMENT ON COLUMN ana_articoli.art_qta_tot_conf IS
    'Questo campo contiene la quantità totale della confezione riportata nelle unità di misura standard (KG,LT etc)
';

COMMENT ON COLUMN ana_articoli.art_unita_misura_prz IS
    'UM base di riferimento del prezzo
';

COMMENT ON COLUMN ana_articoli.art_imballo_vend IS
    'Imballo in vendita       
';

COMMENT ON COLUMN ana_articoli.art_imballo_acq IS
    'Imballo in acquisto 
';

COMMENT ON COLUMN ana_articoli.art_peso_medio IS
    'Peso medio collo in Grammi
';

COMMENT ON COLUMN ana_articoli.art_marchio_cod IS
    '"Codice marchio articolo.
A-ALTRI MARCHI PL  
C-CONSILIA         
D-ARTICOLI DISCOUNT
E-PREMIUM          
L-LEADER           
P-PRIMO PREZZO     
Q-SELEZ.QUALITA''   "
'
    ;

COMMENT ON COLUMN ana_articoli.art_marchio_desc IS
    'Descrizione marchio
';

COMMENT ON COLUMN ana_articoli.art_private_lbl IS
    'Indica se il marchio è una “private label”
';

COMMENT ON COLUMN ana_articoli.art_espositore IS
    'Trattasi di articolo di tipo espositore (box che contiene articoli di vendita)
';

COMMENT ON COLUMN ana_articoli.art_multipack IS
    'Trattasi di articolo multipack (es. fardello da 6 bot acqua)
';

COMMENT ON COLUMN ana_articoli.art_qta_multipack IS
    'Numero articoli di vendita contenuti nel multipack
';

COMMENT ON COLUMN ana_articoli.art_catering IS
    'Articolo di tipo Catering
';

COMMENT ON COLUMN ana_articoli.art_vino_nov IS
    'Vino novello
';

COMMENT ON COLUMN ana_articoli.art_premio IS
    'Articolo premio raccolta punti
';

COMMENT ON COLUMN ana_articoli.art_spot IS
    'Articolo con gestione SPOT, acquisto una tantum
';

COMMENT ON COLUMN ana_articoli.art_infiamm IS
    'Articolo infiammabile   
';

COMMENT ON COLUMN ana_articoli.art_non_vendibile IS
    'Articolo non vendibile al pubblico (es. espositore)
';

COMMENT ON COLUMN ana_articoli.art_raee IS
    'Info per indicazione che l’eco contributo raee è compreso nel prezzo
';

COMMENT ON COLUMN ana_articoli.art_arto IS
    'Articolo categoria orto
';

COMMENT ON COLUMN ana_articoli.art_prod_tipico IS
    'Indica la provincia del prodotto tipico
';

COMMENT ON COLUMN ana_articoli.art_sito IS
    'Flag che indica se l''articolo è visibile sul sito www.gabrielliunika.it
';

COMMENT ON COLUMN ana_articoli.job_id IS
    'Identificativo job
';

COMMENT ON COLUMN ana_articoli.data_import IS
    'data import record
';

CREATE UNIQUE INDEX rti_articolo_cod_idx ON
    ana_articoli (
        art_articolo_cod
    ASC );

CREATE OR REPLACE PUBLIC SYNONYM ana_articoli FOR ana_articoli;
PROMPT Creating Journal Table for 'ANA_ARTICOLI';
CREATE TABLE ANA_ARTICOLI_JN
 (JN_OPERATION CHAR(3) NOT NULL
 ,JN_ORACLE_USER VARCHAR2(30) NOT NULL
 ,JN_DATETIME DATE NOT NULL
 ,JN_NOTES VARCHAR2(240)
 ,JN_APPLN VARCHAR2(35)
 ,JN_SESSION NUMBER(38)
 ,ART_ARTICOLO_COD NUMBER (10) NOT NULL
 ,ART_DESCRIZIONE_STD VARCHAR2 (255)
 ,ART_DESCRIZIONE_SITO VARCHAR2 (255)
 ,ART_STAT VARCHAR2 (2)
 ,ART_FORNITORE_GEN_COD VARCHAR2 (25)
 ,ART_BRAND VARCHAR2 (255)
 ,ART_UFFICIO_ACQ_COD VARCHAR2 (25)
 ,ART_LINEA_PRODOTTO_COD VARCHAR2 (25)
 ,ART_REPARTO_COD VARCHAR2 (25)
 ,ART_UNITA_ORGANIZZATIVA_COD VARCHAR2 (25)
 ,ART_RAGGRUPPAMENTO_COD VARCHAR2 (25)
 ,ART_FAMIGLIA_COD VARCHAR2 (25)
 ,ART_CICLO_VITA VARCHAR2 (5)
 ,ART_DATA_INI TIMESTAMP
 ,ART_DATA_ETO TIMESTAMP
 ,ART_UNITA_MISURA VARCHAR2 (5)
 ,ART_PREZZO_KG_LT VARCHAR2 (5)
 ,ART_NUM_PEZZI NUMBER (10)
 ,ART_TIPO_CONF VARCHAR2 (5)
 ,ART_UNIT_MIS_CONF VARCHAR2 (5)
 ,ART_QTA_PZ_CONF NUMBER (10)
 ,ART_QTA_TOT_CONF NUMBER (10)
 ,ART_UNITA_MISURA_PRZ VARCHAR2 (5)
 ,ART_IMBALLO_VEND NUMBER (10)
 ,ART_IMBALLO_ACQ NUMBER (10)
 ,ART_PESO_MEDIO NUMBER (10)
 ,ART_MARCHIO_COD VARCHAR2 (25)
 ,ART_MARCHIO_DESC VARCHAR2 (255)
 ,ART_PRIVATE_LBL VARCHAR2 (5)
 ,ART_ESPOSITORE VARCHAR2 (5)
 ,ART_MULTIPACK VARCHAR2 (5)
 ,ART_QTA_MULTIPACK NUMBER (10)
 ,ART_CATERING VARCHAR2 (5)
 ,ART_VINO_NOV VARCHAR2 (5)
 ,ART_PREMIO VARCHAR2 (5)
 ,ART_SPOT VARCHAR2 (5)
 ,ART_INFIAMM VARCHAR2 (5)
 ,ART_NON_VENDIBILE VARCHAR2 (5)
 ,ART_RAEE VARCHAR2 (5)
 ,ART_ARTO VARCHAR2 (5)
 ,ART_PROD_TIPICO VARCHAR2 (5)
 ,ART_SITO VARCHAR2 (5)
 ,JOB_ID NUMBER (10)
 ,DATA_IMPORT TIMESTAMP
 );

PROMPT Creating Journal Trigger for 'ANA_ARTICOLI';
CREATE OR REPLACE TRIGGER ANA_ARTICOLI_JNtrg
  AFTER 
  INSERT OR 
  UPDATE OR 
  DELETE ON ANA_ARTICOLI for each row 
 Declare 
  rec ANA_ARTICOLI_JN%ROWTYPE; 
  blank ANA_ARTICOLI_JN%ROWTYPE; 
  BEGIN 
    rec := blank; 
    IF INSERTING OR UPDATING THEN 
      rec.ART_ARTICOLO_COD := :NEW.ART_ARTICOLO_COD; 
      rec.ART_DESCRIZIONE_STD := :NEW.ART_DESCRIZIONE_STD; 
      rec.ART_DESCRIZIONE_SITO := :NEW.ART_DESCRIZIONE_SITO; 
      rec.ART_STAT := :NEW.ART_STAT; 
      rec.ART_FORNITORE_GEN_COD := :NEW.ART_FORNITORE_GEN_COD; 
      rec.ART_BRAND := :NEW.ART_BRAND; 
      rec.ART_UFFICIO_ACQ_COD := :NEW.ART_UFFICIO_ACQ_COD; 
      rec.ART_LINEA_PRODOTTO_COD := :NEW.ART_LINEA_PRODOTTO_COD; 
      rec.ART_REPARTO_COD := :NEW.ART_REPARTO_COD; 
      rec.ART_UNITA_ORGANIZZATIVA_COD := :NEW.ART_UNITA_ORGANIZZATIVA_COD; 
      rec.ART_RAGGRUPPAMENTO_COD := :NEW.ART_RAGGRUPPAMENTO_COD; 
      rec.ART_FAMIGLIA_COD := :NEW.ART_FAMIGLIA_COD; 
      rec.ART_CICLO_VITA := :NEW.ART_CICLO_VITA; 
      rec.ART_DATA_INI := :NEW.ART_DATA_INI; 
      rec.ART_DATA_ETO := :NEW.ART_DATA_ETO; 
      rec.ART_UNITA_MISURA := :NEW.ART_UNITA_MISURA; 
      rec.ART_PREZZO_KG_LT := :NEW.ART_PREZZO_KG_LT; 
      rec.ART_NUM_PEZZI := :NEW.ART_NUM_PEZZI; 
      rec.ART_TIPO_CONF := :NEW.ART_TIPO_CONF; 
      rec.ART_UNIT_MIS_CONF := :NEW.ART_UNIT_MIS_CONF; 
      rec.ART_QTA_PZ_CONF := :NEW.ART_QTA_PZ_CONF; 
      rec.ART_QTA_TOT_CONF := :NEW.ART_QTA_TOT_CONF; 
      rec.ART_UNITA_MISURA_PRZ := :NEW.ART_UNITA_MISURA_PRZ; 
      rec.ART_IMBALLO_VEND := :NEW.ART_IMBALLO_VEND; 
      rec.ART_IMBALLO_ACQ := :NEW.ART_IMBALLO_ACQ; 
      rec.ART_PESO_MEDIO := :NEW.ART_PESO_MEDIO; 
      rec.ART_MARCHIO_COD := :NEW.ART_MARCHIO_COD; 
      rec.ART_MARCHIO_DESC := :NEW.ART_MARCHIO_DESC; 
      rec.ART_PRIVATE_LBL := :NEW.ART_PRIVATE_LBL; 
      rec.ART_ESPOSITORE := :NEW.ART_ESPOSITORE; 
      rec.ART_MULTIPACK := :NEW.ART_MULTIPACK; 
      rec.ART_QTA_MULTIPACK := :NEW.ART_QTA_MULTIPACK; 
      rec.ART_CATERING := :NEW.ART_CATERING; 
      rec.ART_VINO_NOV := :NEW.ART_VINO_NOV; 
      rec.ART_PREMIO := :NEW.ART_PREMIO; 
      rec.ART_SPOT := :NEW.ART_SPOT; 
      rec.ART_INFIAMM := :NEW.ART_INFIAMM; 
      rec.ART_NON_VENDIBILE := :NEW.ART_NON_VENDIBILE; 
      rec.ART_RAEE := :NEW.ART_RAEE; 
      rec.ART_ARTO := :NEW.ART_ARTO; 
      rec.ART_PROD_TIPICO := :NEW.ART_PROD_TIPICO; 
      rec.ART_SITO := :NEW.ART_SITO; 
      rec.JOB_ID := :NEW.JOB_ID; 
      rec.DATA_IMPORT := :NEW.DATA_IMPORT; 
      rec.JN_DATETIME := SYSDATE; 
      rec.JN_ORACLE_USER := SYS_CONTEXT ('USERENV', 'SESSION_USER'); 
      rec.JN_APPLN := SYS_CONTEXT ('USERENV', 'MODULE'); 
      rec.JN_SESSION := SYS_CONTEXT ('USERENV', 'SESSIONID'); 
      IF INSERTING THEN 
        rec.JN_OPERATION := 'INS'; 
      ELSIF UPDATING THEN 
        rec.JN_OPERATION := 'UPD'; 
      END IF; 
    ELSIF DELETING THEN 
      rec.ART_ARTICOLO_COD := :OLD.ART_ARTICOLO_COD; 
      rec.ART_DESCRIZIONE_STD := :OLD.ART_DESCRIZIONE_STD; 
      rec.ART_DESCRIZIONE_SITO := :OLD.ART_DESCRIZIONE_SITO; 
      rec.ART_STAT := :OLD.ART_STAT; 
      rec.ART_FORNITORE_GEN_COD := :OLD.ART_FORNITORE_GEN_COD; 
      rec.ART_BRAND := :OLD.ART_BRAND; 
      rec.ART_UFFICIO_ACQ_COD := :OLD.ART_UFFICIO_ACQ_COD; 
      rec.ART_LINEA_PRODOTTO_COD := :OLD.ART_LINEA_PRODOTTO_COD; 
      rec.ART_REPARTO_COD := :OLD.ART_REPARTO_COD; 
      rec.ART_UNITA_ORGANIZZATIVA_COD := :OLD.ART_UNITA_ORGANIZZATIVA_COD; 
      rec.ART_RAGGRUPPAMENTO_COD := :OLD.ART_RAGGRUPPAMENTO_COD; 
      rec.ART_FAMIGLIA_COD := :OLD.ART_FAMIGLIA_COD; 
      rec.ART_CICLO_VITA := :OLD.ART_CICLO_VITA; 
      rec.ART_DATA_INI := :OLD.ART_DATA_INI; 
      rec.ART_DATA_ETO := :OLD.ART_DATA_ETO; 
      rec.ART_UNITA_MISURA := :OLD.ART_UNITA_MISURA; 
      rec.ART_PREZZO_KG_LT := :OLD.ART_PREZZO_KG_LT; 
      rec.ART_NUM_PEZZI := :OLD.ART_NUM_PEZZI; 
      rec.ART_TIPO_CONF := :OLD.ART_TIPO_CONF; 
      rec.ART_UNIT_MIS_CONF := :OLD.ART_UNIT_MIS_CONF; 
      rec.ART_QTA_PZ_CONF := :OLD.ART_QTA_PZ_CONF; 
      rec.ART_QTA_TOT_CONF := :OLD.ART_QTA_TOT_CONF; 
      rec.ART_UNITA_MISURA_PRZ := :OLD.ART_UNITA_MISURA_PRZ; 
      rec.ART_IMBALLO_VEND := :OLD.ART_IMBALLO_VEND; 
      rec.ART_IMBALLO_ACQ := :OLD.ART_IMBALLO_ACQ; 
      rec.ART_PESO_MEDIO := :OLD.ART_PESO_MEDIO; 
      rec.ART_MARCHIO_COD := :OLD.ART_MARCHIO_COD; 
      rec.ART_MARCHIO_DESC := :OLD.ART_MARCHIO_DESC; 
      rec.ART_PRIVATE_LBL := :OLD.ART_PRIVATE_LBL; 
      rec.ART_ESPOSITORE := :OLD.ART_ESPOSITORE; 
      rec.ART_MULTIPACK := :OLD.ART_MULTIPACK; 
      rec.ART_QTA_MULTIPACK := :OLD.ART_QTA_MULTIPACK; 
      rec.ART_CATERING := :OLD.ART_CATERING; 
      rec.ART_VINO_NOV := :OLD.ART_VINO_NOV; 
      rec.ART_PREMIO := :OLD.ART_PREMIO; 
      rec.ART_SPOT := :OLD.ART_SPOT; 
      rec.ART_INFIAMM := :OLD.ART_INFIAMM; 
      rec.ART_NON_VENDIBILE := :OLD.ART_NON_VENDIBILE; 
      rec.ART_RAEE := :OLD.ART_RAEE; 
      rec.ART_ARTO := :OLD.ART_ARTO; 
      rec.ART_PROD_TIPICO := :OLD.ART_PROD_TIPICO; 
      rec.ART_SITO := :OLD.ART_SITO; 
      rec.JOB_ID := :OLD.JOB_ID; 
      rec.DATA_IMPORT := :OLD.DATA_IMPORT; 
      rec.JN_DATETIME := SYSDATE; 
      rec.JN_ORACLE_USER := SYS_CONTEXT ('USERENV', 'SESSION_USER'); 
      rec.JN_APPLN := SYS_CONTEXT ('USERENV', 'MODULE'); 
      rec.JN_SESSION := SYS_CONTEXT ('USERENV', 'SESSIONID'); 
      rec.JN_OPERATION := 'DEL'; 
    END IF; 
    INSERT into ANA_ARTICOLI_JN VALUES rec; 
  END; 
  /