PROMPT CREATING TABLE 'ANA_PUNTI_VENDITA';
CREATE TABLE ana_punti_vendita (
    pdv_punto_vendita_cod         NUMBER(10) NOT NULL,
    pdv_sigla                     VARCHAR2(50),
    pdv_ragione_soc               VARCHAR2(255),
    pdv_stat                      VARCHAR2(2),
    pdv_indirizzo                 VARCHAR2(255),
    pdv_cap                       NUMBER(10),
    pdv_localita                  VARCHAR2(255),
    pdv_provincia                 VARCHAR2(5),
    pdv_data_apertura             TIMESTAMP,
    pdv_data_chiusura             TIMESTAMP,
    pdv_numero_telefonico         VARCHAR2(50),
    pdv_superficie_mq             NUMBER(10),
    pdv_rapporto_cod              VARCHAR2(25),
    pdv_rapporto_desc             VARCHAR2(255),
    pdv_tipo_affiliato_cod        VARCHAR2(25),
    pdv_tipo_affiliato_desc       VARCHAR2(255),
    pdv_canale_cod                VARCHAR2(25),
    pdv_canale_desc               VARCHAR2(255),
    pdv_insegna_cod               VARCHAR2(25),
    pdv_insegna_des               VARCHAR2(255),
    pdv_format_cod                VARCHAR2(25),
    pdv_format_des                VARCHAR2(255),
    pdv_area_geogr_cod            VARCHAR2(25),
    pdv_area_geogr_desc           VARCHAR2(255),
    pdv_ispettore_cod             VARCHAR2(25),
    pdv_ispettore_desc            VARCHAR2(255),
    pdv_cda_cont_cod              VARCHAR2(25),
    pdv_cda_cont_desc             VARCHAR2(255),
    pdv_cliente_cont_cod          VARCHAR2(25),
    pdv_cliente_cont_piva         NUMBER(20),
    pdv_cliente_cont_cf           VARCHAR2(25),
    pdv_cliente_cont_raggr_soc    VARCHAR2(255),
    pdv_cliente_cont_raggr_soc2   VARCHAR2(255),
    pdv_cliente_cont_ind          VARCHAR2(255),
    pdv_cliente_cont_cap          NUMBER(10),
    pdv_cliente_cont_loc          VARCHAR2(255),
    pdv_cliente_cont_pro          VARCHAR2(5),
    pdv_cliente_cont_naz          VARCHAR2(255),
    pdv_cliente_cont_mail         VARCHAR2(255),
    pdv_stat_pdv_cod              VARCHAR2(25),
    pdv_fidelity_ablt             VARCHAR2(5),
    pdv_visual_store              VARCHAR2(5),
    pdv_latitudine                VARCHAR2(50),
    pdv_longitudine               VARCHAR2(50),
    pdv_indirizzo_ip              VARCHAR2(255),
    pdv_sito                      VARCHAR2(5),
    job_id                        NUMBER(10),
    data_import                   TIMESTAMP
)
TABLESPACE dati_mgdwh;

PROMPT CREATING CHECK CONSTRAINT ON 'ANA_PUNTI_VENDITA';

ALTER TABLE ana_punti_vendita ADD CHECK ( pdv_stat IN (
    'A'
) );

COMMENT ON COLUMN ana_punti_vendita.pdv_punto_vendita_cod IS
    'Numero del punto vendita
';

COMMENT ON COLUMN ana_punti_vendita.pdv_sigla IS
    'Sigla del pdv
';

COMMENT ON COLUMN ana_punti_vendita.pdv_ragione_soc IS
    'Ragione sociale ditta
';

COMMENT ON COLUMN ana_punti_vendita.pdv_stat IS
    'Stato estrazione  record, ‘A’=annullato';

COMMENT ON COLUMN ana_punti_vendita.pdv_indirizzo IS
    'Indirizzo
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cap IS
    'Codice avviamento postale';

COMMENT ON COLUMN ana_punti_vendita.pdv_localita IS
    'Località
';

COMMENT ON COLUMN ana_punti_vendita.pdv_data_apertura IS
    'Data inizio attività
';

COMMENT ON COLUMN ana_punti_vendita.pdv_data_chiusura IS
    'Data chiusura  dell’ultimo giorno di apertura del punto vendita. Dopo questo giorno il PdV sarà chiuso definitivamente
';

COMMENT ON COLUMN ana_punti_vendita.pdv_numero_telefonico IS
    'Numero di telefono
';

COMMENT ON COLUMN ana_punti_vendita.pdv_superficie_mq IS
    'Superficie del negozio in mq
';

COMMENT ON COLUMN ana_punti_vendita.pdv_rapporto_cod IS
    'Codice rapporto
';

COMMENT ON COLUMN ana_punti_vendita.pdv_rapporto_desc IS
    'Descrizione rapporto
';

COMMENT ON COLUMN ana_punti_vendita.pdv_tipo_affiliato_cod IS
    'Codice tipo affiliazione
';

COMMENT ON COLUMN ana_punti_vendita.pdv_tipo_affiliato_desc IS
    'Descrizione tipo affiliazione
';

COMMENT ON COLUMN ana_punti_vendita.pdv_canale_cod IS
    'Codice canale
';

COMMENT ON COLUMN ana_punti_vendita.pdv_canale_desc IS
    'Descrizione canale
';

COMMENT ON COLUMN ana_punti_vendita.pdv_insegna_cod IS
    'Codice insegna
';

COMMENT ON COLUMN ana_punti_vendita.pdv_insegna_des IS
    'Descrizione insegna
';

COMMENT ON COLUMN ana_punti_vendita.pdv_format_cod IS
    'Codice Format
';

COMMENT ON COLUMN ana_punti_vendita.pdv_format_des IS
    'Descrizione Format
';

COMMENT ON COLUMN ana_punti_vendita.pdv_area_geogr_cod IS
    'Codice area geografica
';

COMMENT ON COLUMN ana_punti_vendita.pdv_area_geogr_desc IS
    'Descrizione area geografica
';

COMMENT ON COLUMN ana_punti_vendita.pdv_ispettore_cod IS
    'Codice ispettore
';

COMMENT ON COLUMN ana_punti_vendita.pdv_ispettore_desc IS
    'Descrizione ispettore
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cda_cont_cod IS
    'Codice CDA contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cda_cont_desc IS
    'Descrizione CDA contabile      
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_cod IS
    'Codice cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_piva IS
    'Partita Iva cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_cf IS
    'Codice fiscale cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_raggr_soc IS
    'Raggruppamento sociale cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_raggr_soc2 IS
    'Raggruppamento sociale 2 cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_ind IS
    'Indirizzo cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_cap IS
    'Cap cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_loc IS
    'Località cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_pro IS
    'Provincia cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_naz IS
    'Nazione cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_cliente_cont_mail IS
    'Email cliente contabile
';

COMMENT ON COLUMN ana_punti_vendita.pdv_stat_pdv_cod IS
    'Codice PDV statistico. Se non presente significa che i dati vanno statisticati sul Pdv stesso.
';

COMMENT ON COLUMN ana_punti_vendita.pdv_fidelity_ablt IS
    'Gestione fidelity
';

COMMENT ON COLUMN ana_punti_vendita.pdv_visual_store IS
    'Pdv con VisualStore
';

COMMENT ON COLUMN ana_punti_vendita.pdv_latitudine IS
    'Latitudine del PDV
';

COMMENT ON COLUMN ana_punti_vendita.pdv_longitudine IS
    'Longitudine del PDV
';

COMMENT ON COLUMN ana_punti_vendita.pdv_indirizzo_ip IS
    'Indirizzo IP del PDV
';

COMMENT ON COLUMN ana_punti_vendita.pdv_sito IS
    'Flag che indica se l''articolo è visibile sul sito www.gabrielliunika.it
';

COMMENT ON COLUMN ana_punti_vendita.job_id IS
    'Identificativo job
';

COMMENT ON COLUMN ana_punti_vendita.data_import IS
    'data import record
';

CREATE UNIQUE INDEX unt_punto_vendita_cod_idx ON
    ana_punti_vendita (
        pdv_punto_vendita_cod
    ASC );
PROMPT Creating Journal Table for 'ANA_PUNTI_VENDITA';
CREATE TABLE ANA_PUNTI_VENDITA_JN
 (JN_OPERATION CHAR(3) NOT NULL
 ,JN_ORACLE_USER VARCHAR2(30) NOT NULL
 ,JN_DATETIME DATE NOT NULL
 ,JN_NOTES VARCHAR2(240)
 ,JN_APPLN VARCHAR2(35)
 ,JN_SESSION NUMBER(38)
 ,PDV_PUNTO_VENDITA_COD NUMBER (10) NOT NULL
 ,PDV_SIGLA VARCHAR2 (50)
 ,PDV_RAGIONE_SOC VARCHAR2 (255)
 ,PDV_STAT VARCHAR2 (2)
 ,PDV_INDIRIZZO VARCHAR2 (255)
 ,PDV_CAP NUMBER (10)
 ,PDV_LOCALITA VARCHAR2 (255)
 ,PDV_PROVINCIA VARCHAR2 (5)
 ,PDV_DATA_APERTURA TIMESTAMP
 ,PDV_DATA_CHIUSURA TIMESTAMP
 ,PDV_NUMERO_TELEFONICO VARCHAR2 (50)
 ,PDV_SUPERFICIE_MQ NUMBER (10)
 ,PDV_RAPPORTO_COD VARCHAR2 (25)
 ,PDV_RAPPORTO_DESC VARCHAR2 (255)
 ,PDV_TIPO_AFFILIATO_COD VARCHAR2 (25)
 ,PDV_TIPO_AFFILIATO_DESC VARCHAR2 (255)
 ,PDV_CANALE_COD VARCHAR2 (25)
 ,PDV_CANALE_DESC VARCHAR2 (255)
 ,PDV_INSEGNA_COD VARCHAR2 (25)
 ,PDV_INSEGNA_DES VARCHAR2 (255)
 ,PDV_FORMAT_COD VARCHAR2 (25)
 ,PDV_FORMAT_DES VARCHAR2 (255)
 ,PDV_AREA_GEOGR_COD VARCHAR2 (25)
 ,PDV_AREA_GEOGR_DESC VARCHAR2 (255)
 ,PDV_ISPETTORE_COD VARCHAR2 (25)
 ,PDV_ISPETTORE_DESC VARCHAR2 (255)
 ,PDV_CDA_CONT_COD VARCHAR2 (25)
 ,PDV_CDA_CONT_DESC VARCHAR2 (255)
 ,PDV_CLIENTE_CONT_COD VARCHAR2 (25)
 ,PDV_CLIENTE_CONT_PIVA NUMBER (20)
 ,PDV_CLIENTE_CONT_CF VARCHAR2 (25)
 ,PDV_CLIENTE_CONT_RAGGR_SOC VARCHAR2 (255)
 ,PDV_CLIENTE_CONT_RAGGR_SOC2 VARCHAR2 (255)
 ,PDV_CLIENTE_CONT_IND VARCHAR2 (255)
 ,PDV_CLIENTE_CONT_CAP NUMBER (10)
 ,PDV_CLIENTE_CONT_LOC VARCHAR2 (255)
 ,PDV_CLIENTE_CONT_PRO VARCHAR2 (5)
 ,PDV_CLIENTE_CONT_NAZ VARCHAR2 (255)
 ,PDV_CLIENTE_CONT_MAIL VARCHAR2 (255)
 ,PDV_STAT_PDV_COD VARCHAR2 (25)
 ,PDV_FIDELITY_ABLT VARCHAR2 (5)
 ,PDV_VISUAL_STORE VARCHAR2 (5)
 ,PDV_LATITUDINE VARCHAR2 (50)
 ,PDV_LONGITUDINE VARCHAR2 (50)
 ,PDV_INDIRIZZO_IP VARCHAR2 (255)
 ,PDV_SITO VARCHAR2 (5)
 ,JOB_ID NUMBER (10)
 ,DATA_IMPORT TIMESTAMP
 );

PROMPT Creating Journal Trigger for 'ANA_PUNTI_VENDITA';
CREATE OR REPLACE TRIGGER ANA_PUNTI_VENDITA_JNtrg
  AFTER 
  INSERT OR 
  UPDATE OR 
  DELETE ON ANA_PUNTI_VENDITA for each row 
 Declare 
  rec ANA_PUNTI_VENDITA_JN%ROWTYPE; 
  blank ANA_PUNTI_VENDITA_JN%ROWTYPE; 
  BEGIN 
    rec := blank; 
    IF INSERTING OR UPDATING THEN 
      rec.PDV_PUNTO_VENDITA_COD := :NEW.PDV_PUNTO_VENDITA_COD; 
      rec.PDV_SIGLA := :NEW.PDV_SIGLA; 
      rec.PDV_RAGIONE_SOC := :NEW.PDV_RAGIONE_SOC; 
      rec.PDV_STAT := :NEW.PDV_STAT; 
      rec.PDV_INDIRIZZO := :NEW.PDV_INDIRIZZO; 
      rec.PDV_CAP := :NEW.PDV_CAP; 
      rec.PDV_LOCALITA := :NEW.PDV_LOCALITA; 
      rec.PDV_PROVINCIA := :NEW.PDV_PROVINCIA; 
      rec.PDV_DATA_APERTURA := :NEW.PDV_DATA_APERTURA; 
      rec.PDV_DATA_CHIUSURA := :NEW.PDV_DATA_CHIUSURA; 
      rec.PDV_NUMERO_TELEFONICO := :NEW.PDV_NUMERO_TELEFONICO; 
      rec.PDV_SUPERFICIE_MQ := :NEW.PDV_SUPERFICIE_MQ; 
      rec.PDV_RAPPORTO_COD := :NEW.PDV_RAPPORTO_COD; 
      rec.PDV_RAPPORTO_DESC := :NEW.PDV_RAPPORTO_DESC; 
      rec.PDV_TIPO_AFFILIATO_COD := :NEW.PDV_TIPO_AFFILIATO_COD; 
      rec.PDV_TIPO_AFFILIATO_DESC := :NEW.PDV_TIPO_AFFILIATO_DESC; 
      rec.PDV_CANALE_COD := :NEW.PDV_CANALE_COD; 
      rec.PDV_CANALE_DESC := :NEW.PDV_CANALE_DESC; 
      rec.PDV_INSEGNA_COD := :NEW.PDV_INSEGNA_COD; 
      rec.PDV_INSEGNA_DES := :NEW.PDV_INSEGNA_DES; 
      rec.PDV_FORMAT_COD := :NEW.PDV_FORMAT_COD; 
      rec.PDV_FORMAT_DES := :NEW.PDV_FORMAT_DES; 
      rec.PDV_AREA_GEOGR_COD := :NEW.PDV_AREA_GEOGR_COD; 
      rec.PDV_AREA_GEOGR_DESC := :NEW.PDV_AREA_GEOGR_DESC; 
      rec.PDV_ISPETTORE_COD := :NEW.PDV_ISPETTORE_COD; 
      rec.PDV_ISPETTORE_DESC := :NEW.PDV_ISPETTORE_DESC; 
      rec.PDV_CDA_CONT_COD := :NEW.PDV_CDA_CONT_COD; 
      rec.PDV_CDA_CONT_DESC := :NEW.PDV_CDA_CONT_DESC; 
      rec.PDV_CLIENTE_CONT_COD := :NEW.PDV_CLIENTE_CONT_COD; 
      rec.PDV_CLIENTE_CONT_PIVA := :NEW.PDV_CLIENTE_CONT_PIVA; 
      rec.PDV_CLIENTE_CONT_CF := :NEW.PDV_CLIENTE_CONT_CF; 
      rec.PDV_CLIENTE_CONT_RAGGR_SOC := :NEW.PDV_CLIENTE_CONT_RAGGR_SOC; 
      rec.PDV_CLIENTE_CONT_RAGGR_SOC2 := :NEW.PDV_CLIENTE_CONT_RAGGR_SOC2; 
      rec.PDV_CLIENTE_CONT_IND := :NEW.PDV_CLIENTE_CONT_IND; 
      rec.PDV_CLIENTE_CONT_CAP := :NEW.PDV_CLIENTE_CONT_CAP; 
      rec.PDV_CLIENTE_CONT_LOC := :NEW.PDV_CLIENTE_CONT_LOC; 
      rec.PDV_CLIENTE_CONT_PRO := :NEW.PDV_CLIENTE_CONT_PRO; 
      rec.PDV_CLIENTE_CONT_NAZ := :NEW.PDV_CLIENTE_CONT_NAZ; 
      rec.PDV_CLIENTE_CONT_MAIL := :NEW.PDV_CLIENTE_CONT_MAIL; 
      rec.PDV_STAT_PDV_COD := :NEW.PDV_STAT_PDV_COD; 
      rec.PDV_FIDELITY_ABLT := :NEW.PDV_FIDELITY_ABLT; 
      rec.PDV_VISUAL_STORE := :NEW.PDV_VISUAL_STORE; 
      rec.PDV_LATITUDINE := :NEW.PDV_LATITUDINE; 
      rec.PDV_LONGITUDINE := :NEW.PDV_LONGITUDINE; 
      rec.PDV_INDIRIZZO_IP := :NEW.PDV_INDIRIZZO_IP; 
      rec.PDV_SITO := :NEW.PDV_SITO; 
      rec.JOB_ID := :NEW.JOB_ID; 
      rec.DATA_IMPORT := :NEW.DATA_IMPORT; 
      rec.JN_DATETIME := SYSDATE; 
      rec.JN_ORACLE_USER := SYS_CONTEXT ('USERENV', 'SESSION_USER'); 
      rec.JN_APPLN := SYS_CONTEXT ('USERENV', 'MODULE'); 
      rec.JN_SESSION := SYS_CONTEXT ('USERENV', 'SESSIONID'); 
      IF INSERTING THEN 
        rec.JN_OPERATION := 'INS'; 
      ELSIF UPDATING THEN 
        rec.JN_OPERATION := 'UPD'; 
      END IF; 
    ELSIF DELETING THEN 
      rec.PDV_PUNTO_VENDITA_COD := :OLD.PDV_PUNTO_VENDITA_COD; 
      rec.PDV_SIGLA := :OLD.PDV_SIGLA; 
      rec.PDV_RAGIONE_SOC := :OLD.PDV_RAGIONE_SOC; 
      rec.PDV_STAT := :OLD.PDV_STAT; 
      rec.PDV_INDIRIZZO := :OLD.PDV_INDIRIZZO; 
      rec.PDV_CAP := :OLD.PDV_CAP; 
      rec.PDV_LOCALITA := :OLD.PDV_LOCALITA; 
      rec.PDV_PROVINCIA := :OLD.PDV_PROVINCIA; 
      rec.PDV_DATA_APERTURA := :OLD.PDV_DATA_APERTURA; 
      rec.PDV_DATA_CHIUSURA := :OLD.PDV_DATA_CHIUSURA; 
      rec.PDV_NUMERO_TELEFONICO := :OLD.PDV_NUMERO_TELEFONICO; 
      rec.PDV_SUPERFICIE_MQ := :OLD.PDV_SUPERFICIE_MQ; 
      rec.PDV_RAPPORTO_COD := :OLD.PDV_RAPPORTO_COD; 
      rec.PDV_RAPPORTO_DESC := :OLD.PDV_RAPPORTO_DESC; 
      rec.PDV_TIPO_AFFILIATO_COD := :OLD.PDV_TIPO_AFFILIATO_COD; 
      rec.PDV_TIPO_AFFILIATO_DESC := :OLD.PDV_TIPO_AFFILIATO_DESC; 
      rec.PDV_CANALE_COD := :OLD.PDV_CANALE_COD; 
      rec.PDV_CANALE_DESC := :OLD.PDV_CANALE_DESC; 
      rec.PDV_INSEGNA_COD := :OLD.PDV_INSEGNA_COD; 
      rec.PDV_INSEGNA_DES := :OLD.PDV_INSEGNA_DES; 
      rec.PDV_FORMAT_COD := :OLD.PDV_FORMAT_COD; 
      rec.PDV_FORMAT_DES := :OLD.PDV_FORMAT_DES; 
      rec.PDV_AREA_GEOGR_COD := :OLD.PDV_AREA_GEOGR_COD; 
      rec.PDV_AREA_GEOGR_DESC := :OLD.PDV_AREA_GEOGR_DESC; 
      rec.PDV_ISPETTORE_COD := :OLD.PDV_ISPETTORE_COD; 
      rec.PDV_ISPETTORE_DESC := :OLD.PDV_ISPETTORE_DESC; 
      rec.PDV_CDA_CONT_COD := :OLD.PDV_CDA_CONT_COD; 
      rec.PDV_CDA_CONT_DESC := :OLD.PDV_CDA_CONT_DESC; 
      rec.PDV_CLIENTE_CONT_COD := :OLD.PDV_CLIENTE_CONT_COD; 
      rec.PDV_CLIENTE_CONT_PIVA := :OLD.PDV_CLIENTE_CONT_PIVA; 
      rec.PDV_CLIENTE_CONT_CF := :OLD.PDV_CLIENTE_CONT_CF; 
      rec.PDV_CLIENTE_CONT_RAGGR_SOC := :OLD.PDV_CLIENTE_CONT_RAGGR_SOC; 
      rec.PDV_CLIENTE_CONT_RAGGR_SOC2 := :OLD.PDV_CLIENTE_CONT_RAGGR_SOC2; 
      rec.PDV_CLIENTE_CONT_IND := :OLD.PDV_CLIENTE_CONT_IND; 
      rec.PDV_CLIENTE_CONT_CAP := :OLD.PDV_CLIENTE_CONT_CAP; 
      rec.PDV_CLIENTE_CONT_LOC := :OLD.PDV_CLIENTE_CONT_LOC; 
      rec.PDV_CLIENTE_CONT_PRO := :OLD.PDV_CLIENTE_CONT_PRO; 
      rec.PDV_CLIENTE_CONT_NAZ := :OLD.PDV_CLIENTE_CONT_NAZ; 
      rec.PDV_CLIENTE_CONT_MAIL := :OLD.PDV_CLIENTE_CONT_MAIL; 
      rec.PDV_STAT_PDV_COD := :OLD.PDV_STAT_PDV_COD; 
      rec.PDV_FIDELITY_ABLT := :OLD.PDV_FIDELITY_ABLT; 
      rec.PDV_VISUAL_STORE := :OLD.PDV_VISUAL_STORE; 
      rec.PDV_LATITUDINE := :OLD.PDV_LATITUDINE; 
      rec.PDV_LONGITUDINE := :OLD.PDV_LONGITUDINE; 
      rec.PDV_INDIRIZZO_IP := :OLD.PDV_INDIRIZZO_IP; 
      rec.PDV_SITO := :OLD.PDV_SITO; 
      rec.JOB_ID := :OLD.JOB_ID; 
      rec.DATA_IMPORT := :OLD.DATA_IMPORT; 
      rec.JN_DATETIME := SYSDATE; 
      rec.JN_ORACLE_USER := SYS_CONTEXT ('USERENV', 'SESSION_USER'); 
      rec.JN_APPLN := SYS_CONTEXT ('USERENV', 'MODULE'); 
      rec.JN_SESSION := SYS_CONTEXT ('USERENV', 'SESSIONID'); 
      rec.JN_OPERATION := 'DEL'; 
    END IF; 
    INSERT into ANA_PUNTI_VENDITA_JN VALUES rec; 
  END; 
  /