CREATE OR REPLACE PACKAGE MGDWH."MGDWH_UTILITY_PCK" AS
  --
  --
  /******************************************************************************
   NAME:       MGDWH_UTILITY_PCK
   PURPOSE:    Package di utility del DWH

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13/09/2019           r.doti   1. Created this package.
   ******************************************************************************/
  --
  TYPE DateWindowTyp IS RECORD (
      anno    NUMBER,
      mese    NUMBER);
  TYPE DateWindowTab IS TABLE OF DateWindowTyp  -- Associative array type
       INDEX BY PLS_INTEGER;
  --
  /**
   * Description: La funzione restituisce la data minima di transazione del cliente.
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Return: data minima di transazione 
   */
  FUNCTION GET_DATA_MIN_TRANS(
      P_CLIENTE_COD VARCHAR2,
      P_ANNO        NUMBER,
      P_MESE        NUMBER
  )
    RETURN TIMESTAMP PARALLEL_ENABLE;
  /**
   * Description: La funzione restituisce la data massima di transazione del cliente.
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Return: data massima di transazione 
   */
  FUNCTION GET_DATA_MAX_TRANS(
      P_CLIENTE_COD VARCHAR2,
      P_ANNO        NUMBER,
      P_MESE        NUMBER
  )
    RETURN TIMESTAMP PARALLEL_ENABLE;
  --
  FUNCTION GET_itmidx( 
    P_SID       NUMBER,
    P_DATA      VARCHAR2,
    P_TID       NUMBER,
    P_XACTNBR   NUMBER,
    P_SEGNUMBER NUMBER
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione restituisce il PDV utilizzato maggiormente dal clinete per il periodo indicato
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_TAB_WINDOW Collection contennete le finestre di analisi
   * Return: codice del PDV con maggiori transazioni
   */
  FUNCTION GET_PDV_VND( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_TAB_WINDOW  DateWindowTab
  ) RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE;
  --
   /**
   * Description: La funzione restituisce il PDV utilizzato maggiormente dal clinete per il periodo indicato
   *
   * Author: R.Doti
   * Created:26/08/2021
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_DAY_NUM Numero dei giorni da verificare
   * Return: codice del PDV con maggiori transazioni
   */
  FUNCTION GET_PDV_VND_BY_SPESA_DAY ( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_DAY_NUM     NUMBER
  ) RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione restituisce true/false si tratta di abuser
   *
   * Author: R.Doti
   * Created: 26/08/2021
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_TRANS_NUM  Numero di transazioni in un giorno
   * Param: P_EVT_NUM Numro minimo di volte in cui si è verificato il numero di transazioni
   * Param: P_DAY_NUM Numero dei giorni da verificare
   * Return: true/false
   */
  FUNCTION GET_ISABUSER ( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_TRANS_NUM   NUMBER,
    P_EVT_NUM     NUMBER,
    P_DAY_NUM     NUMBER
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione determina se si tratta di un nuovo cliente. Verifica se la data minima di transazione 
   *              è maggiore di OGGI - P_DAY_NEW.
   *
   * Author: R.Doti
   * Created: 26/08/2021
   *
   * Param: P_DATA_MIN_TRANS  Data minima transazione
   * Param: P_DAY_NEW Numero di giorni
   * Return: S/N
   */
  FUNCTION GET_ISNEW ( 
    P_DATA_MIN_TRANS     ANA_CLIENTI.CLI_DATA_MIN_TRANS%TYPE,
    P_DAY_NEW            NUMBER
  ) RETURN VARCHAR PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione determina se si tratta di un cliente perso. Verifica se la data massima di transazione 
   *              è minore di OGGI - P_DAY_LOST.
   *
   * Author: R.Doti
   * Created: 26/08/2021
   *
   * Param: P_DATA_MAX_TRANS  Data massima transazione
   * Param: P_DAY_LOST Numero di giorni
   * Return: S/N
   */
 FUNCTION GET_ISLOST ( 
    P_DATA_MAX_TRANS     ANA_CLIENTI.CLI_DATA_MAX_TRANS%TYPE,
    P_DAY_LOST           NUMBER
  ) RETURN VARCHAR PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_ANNO anno di riferimento
   * Param: P_MESE mese di riferimento
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_TOTAL_SCNTR( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_ANNO        NUMBER,
    P_MESE        NUMBER
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino nella finestra indicata
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_YYYYMM_DA anno mese da
   * Param: P_YYYYMM_A anno mese a
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_TOTAL_SCNTR_WINDOW( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_YYYYMM_DA   NUMBER,
    P_YYYYMM_A    NUMBER
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino per il numero di giorni inidicati
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_DAY_NUM numero di giorni
   * Param: P_TIPO  F=Fatturato-T=Transazioni
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_TOTAL_BY_DAY_NUM( 
    P_CLIENTE_COD     ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_DECORRENZA      TIMESTAMP,
    P_DAY_NUM         NUMBER,
    P_TIPO            VARCHAR  --F=Fatturato-T=Transazioni
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
    /**
   * Description: La funzione restituisce la somma dei totali scontrino per PDV
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_ANNO anno di riferimento
   * Param: P_MESE mese di riferimento
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_PDV_TOTAL_SCNTR( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_ANNO        NUMBER,
    P_MESE        NUMBER
  ) RETURN TAB_PDV_TOT PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino nella finestra indicata
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_YYYYMM_DA anno mese da
   * Param: P_YYYYMM_A anno mese a
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_PDV_TOTAL_SCNTR_WINDOW( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_YYYYMM_DA   NUMBER,
    P_YYYYMM_A    NUMBER
  ) RETURN TAB_PDV_TOT PARALLEL_ENABLE;
  --
  /**
   * Description: La funzione determina lo store di registrazione del cliente sul sito
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   */
  FUNCTION GET_STORE_PREFERITO(
     P_CLIENTE_COD VARCHAR2
     )
  RETURN VARCHAR2 PARALLEL_ENABLE;
  --
    /**
   * Description: La funzione restituisce S/N se il cliente è outlier
   *
   * Author: R.Doti
   * Created: 26/08/2021
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_FATTURATO fatturato del cliente del periodo
   * Param: P_FATTURATO_CTRL fatturato di controllo
   * Return: S/N
   */
  FUNCTION GET_OUTLIER( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_FATTURATO       NUMBER,
    P_FATTURATO_CTRL  NUMBER
  ) RETURN VARCHAR PARALLEL_ENABLE;
 /**
  * VND_HEAD_IMPORT(): procedura che importa le testate scontrino da VS
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE VND_HEAD_IMPORT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  );
   --
  /**
  * UPDT_VNH_FK(): procedura che aggiorna le FK
  * -ANA_CRT -->CRT_CARTA_COD_XK + CLI_CLIENTE_COD_XK
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_VNH_FK(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_FK                  IN     VARCHAR2,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  );
  --
  /**
  * UPDT_VND_FK(): procedura che aggiorna le FK
  * -ANA_CRT -->CRT_CARTA_COD_XK
  * -ANA_PRM -->PRM_PROMOZIONE_COD_XK
  * -ANA_ART --> ART_ARTICOLO_COD_XK
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_VND_FK(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_FK                  IN     VARCHAR2,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
      );
  --
 /**
  * PROMO_IMPORT(): procedura che recupera le promozioni sul dettaglio vendite
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE PROMO_IMPORT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  );
  --
  PROCEDURE UPDT_ANA_CRT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_TIPO_PDT            IN     VARCHAR2 DEFAULT 'ALL'
  );
  --
  /**
  * UPDT_ANA_CLI(): procedura che aggiorna i dati di dettaglio dei clienti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_ANA_CLI(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000
  );
  --
 /**
  * PRE_UPDT_ANA_CLI(): procedura da richimare prima dell'aggiornamento dei dati del clinete ANA_CLIENTI.
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE PRE_UPDT_ANA_CLI(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_DECORRENZA          IN     TIMESTAMP,
      PO_DML_NEW            IN OUT NOCOPY ANA_CLIENTI%ROWTYPE
  );
  --
  /**
  * UPDT_ANA_CLI_INFO(): procedura che aggiorna kpi cliente. 
  * 
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_ANA_CLI_INFO(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 1000
  );
  --
  /**
   * Description: La procedura determina la data minima e massima di transazione della carta.
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CARTA_COD Codice cliente
   * Param: P_GET_DATA_MIN_TRANS true/false per indicare se calcolare la data minima
   * Param: P_GET_DATA_MAX_TRANS true/false per indicare se calcolare la data massima
   * Param: PO_DATA_MIN_TRANS data minima calcolata
   * Param: PO_DATA_MAX_TRANS data massima calcolata
   */
  PROCEDURE REFRESH_MIN_MAX_TRANS_CRT(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_CARTA_COD           IN     ANA_CARTE.CRT_CARTA_COD%TYPE,
    P_GET_DATA_MIN_TRANS  IN     BOOLEAN,
    P_GET_DATA_MAX_TRANS  IN     BOOLEAN,
    PO_DATA_MIN_TRANS         OUT TIMESTAMP,
    PO_DATA_MAX_TRANS         OUT TIMESTAMP
  );
 /**
   * Description: La procedura aggiorna i dati loyalty
   *  
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: PO_ERR_SEVERITA diagnostico
   * Param: PO_ERR_CODICE diagnostico
   * Param: PO_MESSAGGIO diagnostico
   * Param: P_LIMIT numero di record committati
   */
  PROCEDURE REFRESH_INFO_LOY(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 10000
  );
  FUNCTION GET_STATO_ORD ( 
    P_STATO VARCHAR2
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
END MGDWH_UTILITY_PCK;

/

CREATE OR REPLACE PUBLIC SYNONYM MGDWH_UTILITY_PCK FOR MGDWH_UTILITY_PCK
/
