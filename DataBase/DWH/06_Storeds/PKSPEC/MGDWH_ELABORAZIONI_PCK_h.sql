CREATE OR REPLACE PACKAGE MGDWH."MGDWH_ELABORAZIONI_PCK" AS
  --
  --
  /******************************************************************************
   NAME:       MGDWH_ELABORAZIONE_PCK
   PURPOSE:    Package per la gestione delle elaborazioni delle entit� sul DWH

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        30/05/2019           r.doti   1. Created this package.
   ******************************************************************************/
  --
  /**
  * CREA_ELAB(): procedura che creare una elaborazione
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE CREA_ELAB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE,
      P_ENTITA_COD          IN     MDWH_ELABORAZIONI.ENT_ENTITA_COD%TYPE,
      P_JOB_ID              IN     MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_ID_SESSIONE         IN     MDWH_ELABORAZIONI.LOG_ID_SESSIONE%TYPE,
      PO_CREATA                OUT BOOLEAN,
      P_PUNTO_VENDITA_COD   IN     MDWH_ELABORAZIONI.PDV_PUNTO_VENDITA_COD%TYPE DEFAULT NULL
  );
  /**
  * LOAD_ALL(): procedura che carica tutte le entit� presenti sulla tabella MDWH_ENTITA
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ALL(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2);
  --
  PROCEDURE LOAD_ANA_PDV(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
   --
  PROCEDURE LOAD_ANA_ART(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_ANA_ECR(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_ANA_CLI(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_ANA_CRT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_ANA_PRM(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_ANA_CLU(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_ANA_CRC(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  --
  PROCEDURE LOAD_VND_VNH(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_VND_VND(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  /**
  * LOAD_VND_VNS(): procedura che carica il dettaglio degli sconti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_VND_VNS(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
/**
  * LOAD_ORD_ORH(): procedura che carica il la testata degli ordini
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ORD_ORH(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  /**
  * LOAD_ORD_ORD(): procedura che carica dettaglio degli ordini
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ORD_ORD(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_VND_VHG(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  /**
  * LOAD_ANA_MOD(): procedura che carica l'anagrafica delle modalità di pagamento
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_MOD(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  /**
  * LOAD_ANA_MOD(): procedura che carica il dettaglio dei pagamenti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_VND_VNP(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      );
  --
  PROCEDURE LOAD_VENDITE_HEAD_JOB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_JOB_TOT                           NUMBER,
      P_JOB_NUM                           NUMBER,
      P_JOB_ID                            MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_SESSION_ID                        BASE_LOG.LOG_ID_SESSIONE%TYPE,
      P_FILTRI_PDV_CNT                    NUMBER DEFAULT 0,
      P_FILTRI_PDV_TAB                    TAB_COD DEFAULT NULL
      );
  --
  PROCEDURE LOAD_VENDITE_DETT_JOB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_JOB_TOT                           NUMBER,
      P_JOB_NUM                           NUMBER,
      P_JOB_ID                            MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_SESSION_ID                        BASE_LOG.LOG_ID_SESSIONE%TYPE,
      P_FILTRI_PDV_CNT                    NUMBER DEFAULT 0,
      P_FILTRI_PDV_TAB                    TAB_COD DEFAULT NULL
      );
  --
  FUNCTION GET_PESO_MEDIO(
        P_ARTICOLO_COD ANA_ARTICOLI.ART_ARTICOLO_COD%TYPE
      ) RETURN NUMBER PARALLEL_ENABLE;
  --
END MGDWH_ELABORAZIONI_PCK;
/