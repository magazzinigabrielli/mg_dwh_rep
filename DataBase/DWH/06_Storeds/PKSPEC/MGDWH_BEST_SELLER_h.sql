create or replace PACKAGE       MGDWH_BEST_SELLER AS 
  --
  /******************************************************************************
   NAME:       MGDWH_BEST_SELLER
   PURPOSE:    Package di logging

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019       r.dotiJR()       1. Created this package.
   ******************************************************************************/
  PROCEDURE MAIN;
--
  PROCEDURE EXPORT_BEST_SELLER
  (
    dt_import IN TIMESTAMP
  );
--
END  MGDWH_BEST_SELLER;