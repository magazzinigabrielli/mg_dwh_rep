CREATE OR REPLACE PACKAGE BODY MGDWH."MGDWH_UTILITY_PCK" AS
  --
  /******************************************************************************
   NAME:       MGDWH_UTILITY_PCK
   PURPOSE:    Package di utility del DWH

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13/09/2019           r.doti   1. Created this package.
   ******************************************************************************/
  --
  kFlagAnn           CONSTANT VARCHAR2(1)  := 'A';
  kFormatMask        CONSTANT VARCHAR2(8)  := 'YYYYMMDD';
  kFormatMask2       CONSTANT VARCHAR2(10) := 'YYYY-MM-DD';
  --
  kStampIni          CONSTANT TIMESTAMP  := TO_TIMESTAMP('01010001','DDMMYYYY');
  kStampIeri         CONSTANT TIMESTAMP  := TRUNC(SYSDATE) -1;
  kStampOggi         CONSTANT TIMESTAMP  := TRUNC(SYSDATE);
  kDateIniDWH        CONSTANT DATE       := TO_DATE('01012017','DDMMYYYY');
  --
  ErroreGestito EXCEPTION;
  --
  CURSOR cur_vnh_crt(P_ANNO NUMBER, P_MESE NUMBER)
    IS
      SELECT VNH_VENDUTO_HEAD_ID, 
             CASE VNH_SOURCE
               WHEN 'WB' THEN 
                  DECODE(LENGTH(CRT_CARTA_COD_XK),12,'0'||SUBSTR(CRT_CARTA_COD_XK,1,11),CRT_CARTA_COD_XK)
               ELSE CRT_CARTA_COD_XK
             END CRT_CARTA_COD_XK
        FROM VND_VENDITE_HEAD HEAD
       WHERE CRT_CARTA_COD_XK IS NOT NULL
         AND (CRT_CARTA_COD IS NULL
              OR CLI_CLIENTE_COD IS NULL
              OR CLI_CLIENTE_COD = '-1')
         AND VNH_ANNO = P_ANNO
         AND VNH_MESE = NVL(P_MESE, VNH_MESE)
         AND CRT_CARTA_COD_XK != '0';
   --
   TYPE fetch_vnh_crt IS TABLE OF cur_vnh_crt%ROWTYPE; 
   
   /**
   CURSOR cur_vnh_crt(P_ANNO NUMBER, P_MESE NUMBER)
    IS
      SELECT VNH_VENDUTO_HEAD_ID, 
             CASE VNH_SOURCE
               WHEN 'WB' THEN 
                  DECODE(LENGTH(CRT_CARTA_COD_XK),12,'0'||SUBSTR(CRT_CARTA_COD_XK,1,11),CRT_CARTA_COD_XK)
               ELSE CRT_CARTA_COD_XK
             END CRT_CARTA_COD_XK
        FROM VND_VENDITE_HEAD HEAD
       WHERE CRT_CARTA_COD_XK IS NOT NULL
         AND VNH_ANNO = P_ANNO
         AND VNH_MESE = P_MESE
         AND CRT_CARTA_COD_XK != '0';
   
    **/
  --
  CURSOR cur_vnd_crt(P_ANNO NUMBER, P_MESE NUMBER)
    IS
      SELECT VND_VENDUTO_DETAIL_ID, CRT_CARTA_COD_XK
        FROM VND_VENDITE_DETAIL DET
       WHERE CRT_CARTA_COD_XK IS NOT NULL
         AND (CRT_CARTA_COD IS NULL
              OR CLI_CLIENTE_COD IS NULL)
         AND VND_ANNO = P_ANNO
         AND VND_MESE = P_MESE;
  --
  TYPE fetch_vnd_crt IS TABLE OF cur_vnd_crt%ROWTYPE;
  --
  CURSOR cur_vnd_art(P_ANNO NUMBER, P_MESE NUMBER)
    IS
      SELECT DET.VND_VENDUTO_DETAIL_ID, ART_ARTICOLO_COD_XK
        FROM VND_VENDITE_DETAIL DET
       WHERE ART_ARTICOLO_COD_XK IS NOT NULL
         AND ART_ARTICOLO_COD IS NULL
         AND VND_ANNO = P_ANNO
         AND VND_MESE = P_MESE;
  TYPE fetch_vnd_art IS TABLE OF cur_vnd_art%ROWTYPE;
  --
  CURSOR cur_vnd_dett(P_DATA_DA VARCHAR2, P_DATA_A VARCHAR2)
    IS
      SELECT DSCNT.SID,
             DSCNT.DT_DATE,
             DSCNT.XACTNBR,
             DSCNT.TID,
             DSCNT.ITMIDX ITMIDX,
             TPROMO.ID_PROMO_OFF,
             DSCNT.DISCOUNT,
             DSCNT.INTCODE
        FROM TL_ITEMS_DSC@CRSMG DSCNT
  INNER JOIN KCRM.OPROMO_HEAD@KPROMO TPROMO
          ON DSCNT.COMBID = TPROMO.PROMO_LINK_ID
       WHERE DSCNT.DISCTYPE IN (1, 2, 5, 7, 8, 21, 22, 23, 24)
         AND TPROMO.GP_SUP_RULE_FK = '001' --Promo AS400
         AND DSCNT.DT_DATE >= P_DATA_DA --'2017-12-01'
         AND DSCNT.DT_DATE < P_DATA_A --'2018-01-01'
       ;
  TYPE fetch_vnd_dett IS TABLE OF cur_vnd_dett%ROWTYPE;
  --
  CURSOR cur_vnd_promo(P_DATA_DA VARCHAR2, P_DATA_A VARCHAR2)
    IS
      SELECT DSCNT.SID,
             DSCNT.DT_DATE,
             DSCNT.XACTNBR,
             DSCNT.TID,
             DSCNT.ITMIDX ITMIDX,
             TPROMO.ID_PROMO_OFF,
             DSCNT.DISCOUNT,
             DSCNT.INTCODE
        FROM TL_ITEMS_DSC@CRSMG DSCNT
  INNER JOIN KCRM.OPROMO_HEAD@KPROMO TPROMO
          ON DSCNT.COMBID = TPROMO.PROMO_LINK_ID
       WHERE DSCNT.DISCTYPE IN (1, 2, 5, 7, 8, 21, 22, 23, 24)
         AND TPROMO.GP_SUP_RULE_FK = '001' --Promo AS400
         AND DSCNT.DT_DATE >= P_DATA_DA --'2017-12-01'
         AND DSCNT.DT_DATE < P_DATA_A --'2018-01-01'
       ;
  TYPE fetch_vnd_promo IS TABLE OF cur_vnd_promo%ROWTYPE;
  --
  CURSOR cur_vnd_head(P_DATA_DA VARCHAR2, P_DATA_A VARCHAR2)
  IS
    SELECT /*+ PARALLEL(HEAD 12) */
           HEAD.SID                               PDV_PUNTO_VENDITA_COD,
           TO_TIMESTAMP (HEAD.DT_DATE || ' ' || HEAD.DT_TIME,
                         'YYYY-MM-DD hh24:mi:ss') VNH_DATA_VEND,
           EXTRACT (YEAR FROM TO_TIMESTAMP (HEAD.DT_DATE, 'YYYY-MM-DD'))
                                                  VNH_ANNO,
           EXTRACT (MONTH FROM TO_TIMESTAMP (HEAD.DT_DATE, 'YYYY-MM-DD'))
                                                  VNH_MESE,
           HEAD.TID                               VNH_NUMERO_CASSA,
           HEAD.XACTNBR                           VNH_NUMERO_SCONTRINO,
           HEAD.OPID                              VNH_CASSIERE_COD,
           HEAD.OPIDNAME                          VNH_CASSIERE,
           HEAD.XACTTOTAL                         VNH_SCONTRINO_TOT,
           HEAD.SCANITEMS                         VNH_SCAN_ITEMS_TOT,
           HEAD.TOTALITEMS                        VNH_ITEMS_TOT,
           -1                                     JOB_ID,
           SYSDATE                                DATA_IMPORT,
           DECODE (LENGTH (LOY.CARD), 13, SUBSTR (LOY.CARD, 1, 12), LOY.CARD) CRT_CARTA_COD_XK,
           CASE
              WHEN     HEAD.PRV_VOID = 0
                   AND HEAD.PRV_VOID2 = 0
                   AND HEAD.XACTTOTAL < 0
              THEN
                 'S'
              ELSE
                 NULL
           END
                                                  VNH_RESO,
           CASE
              WHEN HEAD.PRV_VOID = 1 AND HEAD.PRV_VOID2 = 1 THEN 'S'
              ELSE NULL
           END
                                                  VNH_ANNULLAMENTO,
           HEAD.TERM_TYPE                         VNH_TERM_TYPE,
           HEAD.DELIVERY_TYPE                     VNH_DELIVERY_TYPE
      FROM TL_HEADER@CRSMG HEAD
 LEFT JOIN TL_LOYALTY@CRSMG LOY
        ON HEAD.SID = LOY.SID
       AND HEAD.DT_DATE = LOY.DT_DATE
       AND HEAD.DT_TIME = LOY.DT_TIME
       AND HEAD.TID = LOY.TID
       AND HEAD.XACTNBR = LOY.XACTNBR
     WHERE HEAD.DT_DATE >= P_DATA_DA
       AND HEAD.DT_DATE < P_DATA_A
       AND HEAD.XVOID = 0 --
       AND HEAD.XSUSP = 0 --Escludo le sospese
       AND HEAD.XACTTOTAL <> 0;
  --
  FUNCTION GET_DATA_MIN_TRANS(
      P_CLIENTE_COD VARCHAR2,
      P_ANNO        NUMBER,
      P_MESE        NUMBER
  )
    RETURN TIMESTAMP PARALLEL_ENABLE
  IS
    vDataMinTrans TIMESTAMP := kDateIniDWH;
  BEGIN
    SELECT MIN(VNH_DATA_VEND)
      INTO vDataMinTrans
      FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
     WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
       AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
       AND VNH_ANNO = P_ANNO
       AND VNH_MESE = P_MESE
     GROUP BY VND.CLI_CLIENTE_COD;
    --
    RETURN vDataMinTrans;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_DATA_MIN_TRANS;
  --
  FUNCTION GET_VAL_PARAMETRO_C(
      P_PARAMETRO_COD VARCHAR2
  )
    RETURN VARCHAR
  IS
    vValore VARCHAR2(255);
  BEGIN
    SELECT PAR_VALORE_CHAR
      INTO vValore
      FROM MDWH_PARAMETRI
     WHERE PAR_PARAMETRO_COD = P_PARAMETRO_COD;
    --
    RETURN vValore;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_VAL_PARAMETRO_C;
  --
  FUNCTION GET_VAL_PARAMETRO_D(
      P_PARAMETRO_COD VARCHAR2
  )
    RETURN TIMESTAMP
  IS
    vValore TIMESTAMP;
  BEGIN
    SELECT PAR_VALORE_DATE
      INTO vValore
      FROM MDWH_PARAMETRI
     WHERE PAR_PARAMETRO_COD = P_PARAMETRO_COD;
    --
    RETURN vValore;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_VAL_PARAMETRO_D;
  --
  FUNCTION GET_VAL_PARAMETRO_N(
      P_PARAMETRO_COD VARCHAR2
  )
    RETURN NUMBER
  IS
    vValore NUMBER;
  BEGIN
    SELECT PAR_VALORE_NUM
      INTO vValore
      FROM MDWH_PARAMETRI
     WHERE PAR_PARAMETRO_COD = P_PARAMETRO_COD;
    --
    RETURN vValore;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_VAL_PARAMETRO_N;
  --
  FUNCTION GET_DATA_MAX_TRANS(
      P_CLIENTE_COD VARCHAR2,
      P_ANNO        NUMBER,
      P_MESE        NUMBER
  )
   RETURN TIMESTAMP PARALLEL_ENABLE
  IS
    vDataMaxTrans TIMESTAMP := SYSTIMESTAMP;
  BEGIN
    SELECT MAX(VNH_DATA_VEND)
      INTO vDataMaxTrans
      FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
     WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
       AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
       AND VNH_ANNO = P_ANNO
       AND VNH_MESE = P_MESE
     GROUP BY VND.CLI_CLIENTE_COD;
    --
    RETURN vDataMaxTrans;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_DATA_MAX_TRANS;
  --
  FUNCTION GET_DATA_MIN_TRANS_CRT(
      P_CARTA_COD   VARCHAR2,
      P_ANNO        NUMBER,
      P_MESE        NUMBER
  )
    RETURN TIMESTAMP PARALLEL_ENABLE
  IS
    vDataMinTrans TIMESTAMP := kDateIniDWH;
  BEGIN
    SELECT MIN(VNH_DATA_VEND)
      INTO vDataMinTrans
      FROM VND_VENDITE_HEAD VND
     WHERE VND.CRT_CARTA_COD = P_CARTA_COD
       AND VNH_ANNO = P_ANNO
       AND VNH_MESE = P_MESE
     GROUP BY VND.CRT_CARTA_COD;
    --
    RETURN vDataMinTrans;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_DATA_MIN_TRANS_CRT;
  --
  FUNCTION GET_DATA_MAX_TRANS_CRT(
      P_CARTA_COD   VARCHAR2,
      P_ANNO        NUMBER,
      P_MESE        NUMBER
  )
   RETURN TIMESTAMP PARALLEL_ENABLE
  IS
    vDataMaxTrans TIMESTAMP := SYSTIMESTAMP;
  BEGIN
    SELECT MAX(VNH_DATA_VEND)
      INTO vDataMaxTrans
      FROM VND_VENDITE_HEAD VND
     WHERE VND.CRT_CARTA_COD = P_CARTA_COD
       AND VNH_ANNO = P_ANNO
       AND VNH_MESE = P_MESE
     GROUP BY VND.CRT_CARTA_COD;
    --
    RETURN vDataMaxTrans;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_DATA_MAX_TRANS_CRT;
  --
  FUNCTION GET_MIN_WEEK(P_DATA DATE)
    RETURN NUMBER
  IS
    minweek NUMBER := 0;
  BEGIN
    --Determino il numero di settimana minimo
    minweek := ROUND(TRUNC(P_DATA - kDateIniDWH)/7);
    return minweek;
  END GET_MIN_WEEK;
  --
  FUNCTION GET_WINDOW(
    P_DATA         IN     DATE,
    P_LIMIT_WINDOW IN     NUMBER
  ) RETURN DateWindowTab
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vAnno               NUMBER;
    vMonth              NUMBER;
    win_end_app         NUMBER;
    --
    vDateWindow_rec DateWindowTyp;
    vDateWindow_tab DateWindowTab;
  BEGIN
  --
    vProcName := 'GET_WINDOW';
    --
    vPasso := 'INPUT';
    --BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DATA:'||P_DATA ,vPasso);
    FOR rec IN ( SELECT ADD_MONTHS(TRUNC(P_DATA), -LEVEL + 1) Data
                   FROM DUAL
                CONNECT BY LEVEL <= P_LIMIT_WINDOW
               )
    LOOP
    --
      vDateWindow_rec.Anno := EXTRACT(YEAR FROM rec.Data);
      vDateWindow_rec.Mese := EXTRACT(MONTH FROM rec.Data);
      --
      vDateWindow_tab(vDateWindow_tab.COUNT) := vDateWindow_rec;
    END LOOP;
    --
    RETURN vDateWindow_tab;
  --
  END GET_WINDOW;
  --
  /*La funzione determina il PDV su cui il cliente ha transato maggiormente nella finestra indicata*/
  FUNCTION GET_PDV_VND( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_TAB_WINDOW  DateWindowTab
  ) 
    RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vYear               NUMBER;
    vMonth              NUMBER;
    vDataA              DATE;
    --
    vO_CodPDV           ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    vTabAnni            TAB_COD_NUM := TAB_COD_NUM();
    vTabMesi            TAB_COD_NUM := TAB_COD_NUM();
    vData               DATE;
    vCnt                NUMBER;
  BEGIN
    vProcName := 'GET_PDV_VND';
    vPasso := 'INPUT';
    --
    WITH PDV_CNT
     AS (  SELECT PDV_PUNTO_VENDITA_COD, COUNT (VNH_VENDUTO_HEAD_ID)
             FROM (SELECT VND.PDV_PUNTO_VENDITA_COD, VNH_VENDUTO_HEAD_ID
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(0).Anno
                      AND VNH_MESE = P_TAB_WINDOW(0).Mese
                    UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VNH_VENDUTO_HEAD_ID
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(1).Anno
                      AND VNH_MESE = P_TAB_WINDOW(1).Mese
                    UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VNH_VENDUTO_HEAD_ID
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(2).Anno
                      AND VNH_MESE = P_TAB_WINDOW(2).Mese
                    UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VNH_VENDUTO_HEAD_ID
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(3).Anno
                      AND VNH_MESE = P_TAB_WINDOW(3).Mese
                   UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VNH_VENDUTO_HEAD_ID
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(4).Anno
                      AND VNH_MESE = P_TAB_WINDOW(4).Mese
                   UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VNH_VENDUTO_HEAD_ID
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(5).Anno
                      AND VNH_MESE = P_TAB_WINDOW(5).Mese
                      ) SCR
         GROUP BY PDV_PUNTO_VENDITA_COD
         ORDER BY COUNT (VNH_VENDUTO_HEAD_ID) DESC)
    SELECT PDV_PUNTO_VENDITA_COD
      INTO vO_CodPDV
      FROM PDV_CNT
    WHERE ROWNUM = 1;
    --
    RETURN vO_CodPDV;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_PDV_VND;
  --
  /*La funzione determina il PDV su cui il cliente ha speso maggiormente nella finestra indicata*/
  FUNCTION GET_PDV_VND_BY_SPESA( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_TAB_WINDOW  DateWindowTab
  ) 
    RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vYear               NUMBER;
    vMonth              NUMBER;
    vDataA              DATE;
    --
    vO_CodPDV           ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    vTabAnni            TAB_COD_NUM := TAB_COD_NUM();
    vTabMesi            TAB_COD_NUM := TAB_COD_NUM();
    vData               DATE;
    vCnt                NUMBER;
  BEGIN
    vProcName := 'GET_PDV_VND_BY_SPESA';
    vPasso := 'INPUT';
    --
    WITH PDV_CNT
     AS (  SELECT PDV_PUNTO_VENDITA_COD, SUM (VNH_SCONTRINO_TOT)
             FROM (SELECT VND.PDV_PUNTO_VENDITA_COD, VND.VNH_SCONTRINO_TOT
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(0).Anno
                      AND VNH_MESE = P_TAB_WINDOW(0).Mese
                    UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VND.VNH_SCONTRINO_TOT
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(1).Anno
                      AND VNH_MESE = P_TAB_WINDOW(1).Mese
                    UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VND.VNH_SCONTRINO_TOT
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(2).Anno
                      AND VNH_MESE = P_TAB_WINDOW(2).Mese
                    UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VND.VNH_SCONTRINO_TOT
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(3).Anno
                      AND VNH_MESE = P_TAB_WINDOW(3).Mese
                   UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VND.VNH_SCONTRINO_TOT
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(4).Anno
                      AND VNH_MESE = P_TAB_WINDOW(4).Mese
                   UNION
                   SELECT VND.PDV_PUNTO_VENDITA_COD, VND.VNH_SCONTRINO_TOT
                     FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
                    WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                      AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                      AND VNH_ANNO = P_TAB_WINDOW(5).Anno
                      AND VNH_MESE = P_TAB_WINDOW(5).Mese
                      ) SCR
         GROUP BY PDV_PUNTO_VENDITA_COD
         ORDER BY SUM (VNH_SCONTRINO_TOT) DESC)
    SELECT PDV_PUNTO_VENDITA_COD
      INTO vO_CodPDV
      FROM PDV_CNT
    WHERE ROWNUM = 1;
    --
    RETURN vO_CodPDV;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_PDV_VND_BY_SPESA;
  --
  /*La funzione determina il PDV su cui il cliente ha speso maggiormente nel numero di giorni indicati sul parametro  P_DAY_NUM*/
  FUNCTION GET_PDV_VND_BY_SPESA_DAY ( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_DAY_NUM     NUMBER
  ) 
    RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vYear               NUMBER;
    vMonth              NUMBER;
    vDataA              DATE;
    --
    vO_CodPDV           ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    vTabAnni            TAB_COD_NUM := TAB_COD_NUM();
    vTabMesi            TAB_COD_NUM := TAB_COD_NUM();
    vData               DATE;
    vCnt                NUMBER;
  BEGIN
    vProcName := 'GET_PDV_VND_BY_SPESA';
    vPasso := 'INPUT';
    --
    WITH PDV_CNT
      AS (  SELECT PDV_PUNTO_VENDITA_COD, SUM (VNH_SCONTRINO_TOT)
              FROM ( SELECT DECODE(PDV_STAT_PDV_COD,0, PDV.PDV_PUNTO_VENDITA_COD, PDV_STAT_PDV_COD) PDV_PUNTO_VENDITA_COD, 
                            VNH_SCONTRINO_TOT
                       FROM VND_VENDITE_HEAD VND, 
                            ANA_CARTE CRT,
                            ANA_PUNTI_VENDITA PDV
                      WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
                        AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
                        AND TRUNC(VND.VNH_DATA_VEND) BETWEEN kStampOggi -P_DAY_NUM AND kStampOggi
                        AND VND.PDV_PUNTO_VENDITA_COD = PDV.PDV_PUNTO_VENDITA_COD
                        --AND EXISTS( SELECT 0 FROM ANA_PUNTI_VENDITA PDV
                        --             WHERE VND.PDV_PUNTO_VENDITA_COD = PDV.PDV_PUNTO_VENDITA_COD
                        --                AND NVL(PDV.PDV_DATA_CHIUSURA,kStampOggi )  <= kStampOggi
                        --)
                    ) SCR
             GROUP BY PDV_PUNTO_VENDITA_COD
         ORDER BY SUM (VNH_SCONTRINO_TOT) DESC)
    SELECT PDV_PUNTO_VENDITA_COD
      INTO vO_CodPDV
      FROM PDV_CNT
    WHERE ROWNUM = 1;

    --
    RETURN vO_CodPDV;
  EXCEPTION
    WHEN no_data_found THEN RETURN NULL;
  END GET_PDV_VND_BY_SPESA_DAY;
  --
  /**
   * Description: La funzione determina lo store di registrazione del cliente sul sito
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   */
  FUNCTION GET_STORE_PREFERITO(
     P_CLIENTE_COD VARCHAR2
     )
  RETURN VARCHAR2 PARALLEL_ENABLE
  IS
    vStorePreferito VARCHAR2(25);
    kNomePackage    CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100) :='GET_STORE_PREFERITO';
  BEGIN
  --
    SELECT KFC.STORE
      INTO vStorePreferito
      FROM FRAME.KFCUSTOMER_HOST@KPROMO KFC
     WHERE KFC.ID_CUSTOMER = P_CLIENTE_COD
       AND ROWNUM <= 1;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' vStorePreferito: '||vStorePreferito);
    RETURN vStorePreferito;
   --
   EXCEPTION
    WHEN OTHERS THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN NULL;
  END GET_STORE_PREFERITO;
  --
  /**
   * Description: La funzione restituisce il numero di volte in cui si è verificato l'evento
   * L'evento è dato dalle seguenti condizioni: 
   *       P_TRANS_NUM= numero di transazioni in un giorno su carte attive
   *       P_EVT_NUM= numero di vole in cui si è verificato un numero di transazioni maggiore di P_TRANS_NUM
   *       P_DAY_NUM= arco temporale da verificare in GG
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_TRANS_NUM  Numero di transazioni in un giorno
   * Param: P_EVT_NUM Numro minimo di volte in cui si è verificato il numero di transazioni
   * Param: P_DAY_NUM Numero dei giorni da verificare
   * Return: Numero di giorni in cui si è verificato evento di abuser. 
   */
  FUNCTION GET_ISABUSER ( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_TRANS_NUM   NUMBER,
    P_EVT_NUM     NUMBER,
    P_DAY_NUM     NUMBER
  ) RETURN NUMBER PARALLEL_ENABLE
   IS
    kNomePackage    CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100) :='GET_ISABUSER';
    vNumIsAbuser    NUMBER := 0;
  BEGIN
  --
    SELECT COUNT(0) ABUSER
     INTO vNumIsAbuser
     FROM (SELECT TRUNC(VND.VNH_DATA_VEND), COUNT(0)
             FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
            WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
              AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD
              AND CRT.CRT_DATA_FINE IS NULL
              AND TRUNC(VND.VNH_DATA_VEND) BETWEEN kStampOggi -P_DAY_NUM AND kStampOggi
            GROUP BY TRUNC(VND.VNH_DATA_VEND)
           HAVING COUNT(0) > P_TRANS_NUM
           )
      HAVING COUNT(0) >= P_EVT_NUM;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' vNumIsAbuser: '||vNumIsAbuser);
    RETURN vNumIsAbuser;
   --
   EXCEPTION
    WHEN OTHERS THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN NULL;
  END GET_ISABUSER;

  --
  /**
   * Description: La procedura determina la data minima e massima di transazione del cliente.
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_GET_DATA_MIN_TRANS true/false per indicare se calcolare la data minima
   * Param: P_GET_DATA_MAX_TRANS true/false per indicare se calcolare la data massima
   * Param: PO_DATA_MIN_TRANS data minima calcolata
   * Param: PO_DATA_MAX_TRANS data massima calcolata
   */
  PROCEDURE REFRESH_DATE_MIN_MAX_TRANS(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_CLIENTE_COD         IN     ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_GET_DATA_MIN_TRANS  IN     BOOLEAN,
    P_GET_DATA_MAX_TRANS  IN     BOOLEAN,
    PO_DATA_MIN_TRANS         OUT TIMESTAMP,
    PO_DATA_MAX_TRANS         OUT TIMESTAMP
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vYear               NUMBER;
    vMonth              NUMBER;
    --
    vLimitDownMin        NUMBER; 
    vLimitDownMax        NUMBER;
  BEGIN
    vProcName := 'GET_DATE_MIN_MAX_TRANS';
    vPasso := 'LIM_DOWN';
    vLimitDownMin := ROUND(EXTRACT(DAY FROM (SYSTIMESTAMP - kDateIniDWH))/30);
    vLimitDownMax := ROUND(EXTRACT(DAY FROM (SYSTIMESTAMP - kDateIniDWH))/30);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vLimitDownMin:'||vLimitDownMin,vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vLimitDownMax:'||vLimitDownMax,vPasso);
    --
    PO_DATA_MIN_TRANS := NULL;
    PO_DATA_MAX_TRANS := NULL;
    --
    IF P_GET_DATA_MIN_TRANS THEN
    --
      vPasso := 'LOOP';
      FOR rec IN ( SELECT ADD_MONTHS(ADD_MONTHS(kDateIniDWH,-1), LEVEL) Data
                     FROM DUAL
                  CONNECT BY LEVEL <= vLimitDownMin
                 )
      LOOP
      --
        BEGIN
          vYear := EXTRACT(YEAR FROM rec.Data);
          vMonth := EXTRACT(MONTH FROM rec.Data);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Data : '||vYear ||' '||vMonth ,vPasso);
          --
          vPasso := 'GET_DATA_MIN_TRANS';
          PO_DATA_MIN_TRANS := GET_DATA_MIN_TRANS(P_CLIENTE_COD => P_CLIENTE_COD,
                                                  P_ANNO        => vYear,
                                                  P_MESE        => vMonth);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_DATA_MIN_TRANS : '||PO_DATA_MIN_TRANS ,vPasso);
          EXIT WHEN PO_DATA_MIN_TRANS IS NOT NULL;
        --
        EXCEPTION
          WHEN OTHERS THEN
            --DBMS_OUTPUT.PUT_LINE('ERR:'||SQLERRM ||'P_CLIENTE_COD : '||P_CLIENTE_COD);
            BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'ERR:'||SQLERRM ||'P_CLIENTE_COD : '||P_CLIENTE_COD,BASE_LOG_PCK.Errore);
        END;
      END LOOP;
    END IF;
    --
    IF P_GET_DATA_MAX_TRANS THEN
    --
      FOR rec IN (SELECT ADD_MONTHS(ADD_MONTHS(TRUNC(SYSTIMESTAMP),1), -LEVEL) Data
                     FROM DUAL
                  CONNECT BY LEVEL <= vLimitDownMax
                 )
      LOOP
      --
        BEGIN
          vPasso := 'LOOP';
          vYear := EXTRACT(YEAR FROM rec.Data);
          vMonth := EXTRACT(MONTH FROM rec.Data);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Data : '||vYear ||' '||vMonth ,vPasso);
          --
          vPasso := 'GET_DATA_MAX_TRANS';
          PO_DATA_MAX_TRANS := GET_DATA_MAX_TRANS(P_CLIENTE_COD => P_CLIENTE_COD,
                                                  P_ANNO        => vYear,
                                                  P_MESE        => vMonth);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_DATA_MAX_TRANS : '||PO_DATA_MAX_TRANS ,vPasso);
          EXIT WHEN PO_DATA_MAX_TRANS IS NOT NULL;
        --
        EXCEPTION
          WHEN OTHERS THEN
            --DBMS_OUTPUT.PUT_LINE('ERR:'||SQLERRM ||'P_CLIENTE_COD : '||P_CLIENTE_COD);
            BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'ERR:'||SQLERRM ||'P_CLIENTE_COD : '||P_CLIENTE_COD,BASE_LOG_PCK.Errore);
        END;
      END LOOP;
    END IF;
    --
    vPasso := 'UPDT_CLI';
    UPDATE ANA_CLIENTI
       SET CLI_DATA_MIN_TRANS = NVL(PO_DATA_MIN_TRANS, CLI_DATA_MIN_TRANS),
           CLI_DATA_MAX_TRANS = NVL(PO_DATA_MAX_TRANS, CLI_DATA_MAX_TRANS)
     WHERE CLI_CLIENTE_COD = P_CLIENTE_COD;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_DATE_MIN_MAX_TRANS;
  --
  /**
   * Description: La procedura determina la data minima e massima di transazione della carta.
   *
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_CARTA_COD Codice della carta
   * Param: P_GET_DATA_MIN_TRANS true/false per indicare se calcolare la data minima
   * Param: P_GET_DATA_MAX_TRANS true/false per indicare se calcolare la data massima
   * Param: PO_DATA_MIN_TRANS data minima calcolata
   * Param: PO_DATA_MAX_TRANS data massima calcolata
   */
  PROCEDURE REFRESH_MIN_MAX_TRANS_CRT(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_CARTA_COD           IN     ANA_CARTE.CRT_CARTA_COD%TYPE,
    P_GET_DATA_MIN_TRANS  IN     BOOLEAN,
    P_GET_DATA_MAX_TRANS  IN     BOOLEAN,
    PO_DATA_MIN_TRANS         OUT TIMESTAMP,
    PO_DATA_MAX_TRANS         OUT TIMESTAMP
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vYear               NUMBER;
    vMonth              NUMBER;
    --
    vLimitDownMin        NUMBER; 
    vLimitDownMax        NUMBER;
  BEGIN
    vProcName := 'REFRESH_MIN_MAX_TRANS_CRT';
    vPasso := 'LIM_DOWN';
    vLimitDownMin := ROUND(EXTRACT(DAY FROM (SYSTIMESTAMP - kDateIniDWH))/30);
    vLimitDownMax := ROUND(EXTRACT(DAY FROM (SYSTIMESTAMP - kDateIniDWH))/30);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vLimitDownMin:'||vLimitDownMin,vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vLimitDownMax:'||vLimitDownMax,vPasso);
    --
    PO_DATA_MIN_TRANS := NULL;
    PO_DATA_MAX_TRANS := NULL;
    --
    IF P_GET_DATA_MIN_TRANS THEN
    --
      vPasso := 'LOOP';
      FOR rec IN ( SELECT ADD_MONTHS(ADD_MONTHS(kDateIniDWH,-1), LEVEL) Data
                     FROM DUAL
                  CONNECT BY LEVEL <= vLimitDownMin
                 )
      LOOP
      --
        BEGIN
          vYear := EXTRACT(YEAR FROM rec.Data);
          vMonth := EXTRACT(MONTH FROM rec.Data);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Data : '||vYear ||' '||vMonth ,vPasso);
          --
          vPasso := 'GET_DATA_MIN_TRANS';
          PO_DATA_MIN_TRANS := GET_DATA_MIN_TRANS_CRT(P_CARTA_COD => P_CARTA_COD,
                                                      P_ANNO        => vYear,
                                                      P_MESE        => vMonth);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_DATA_MIN_TRANS : '||PO_DATA_MIN_TRANS ,vPasso);
          EXIT WHEN PO_DATA_MIN_TRANS IS NOT NULL;
        --
        EXCEPTION
          WHEN OTHERS THEN
            --DBMS_OUTPUT.PUT_LINE('ERR:'||SQLERRM ||'P_CARTA_COD : '||P_CARTA_COD);
            BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'ERR:'||SQLERRM ||'P_CARTA_COD : '||P_CARTA_COD,BASE_LOG_PCK.Errore);
        END;
      END LOOP;
    END IF;
    --
    IF P_GET_DATA_MAX_TRANS THEN
    --
      FOR rec IN (SELECT ADD_MONTHS(ADD_MONTHS(TRUNC(SYSTIMESTAMP),1), -LEVEL) Data
                     FROM DUAL
                  CONNECT BY LEVEL <= vLimitDownMax
                 )
      LOOP
      --
        BEGIN
          vPasso := 'LOOP';
          vYear := EXTRACT(YEAR FROM rec.Data);
          vMonth := EXTRACT(MONTH FROM rec.Data);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Data : '||vYear ||' '||vMonth ,vPasso);
          --
          vPasso := 'GET_DATA_MAX_TRANS';
          PO_DATA_MAX_TRANS := GET_DATA_MAX_TRANS_CRT(P_CARTA_COD => P_CARTA_COD,
                                                      P_ANNO        => vYear,
                                                      P_MESE        => vMonth);
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_DATA_MAX_TRANS : '||PO_DATA_MAX_TRANS ,vPasso);
          EXIT WHEN PO_DATA_MAX_TRANS IS NOT NULL;
        --
        EXCEPTION
          WHEN OTHERS THEN
            --DBMS_OUTPUT.PUT_LINE('ERR:'||SQLERRM ||'P_CARTA_COD : '||P_CARTA_COD);
            BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'ERR:'||SQLERRM ||'P_CARTA_COD : '||P_CARTA_COD,BASE_LOG_PCK.Errore);
        END;
      END LOOP;
    END IF;
    --
    vPasso := 'UPDT_CRT';
    UPDATE ANA_CARTE
       SET CRT_DATA_MIN_TRANS = NVL(PO_DATA_MIN_TRANS, CRT_DATA_MIN_TRANS),
           CRT_DATA_MAX_TRANS = NVL(PO_DATA_MAX_TRANS, CRT_DATA_MAX_TRANS)
     WHERE CRT_CARTA_COD = P_CARTA_COD;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_MIN_MAX_TRANS_CRT;
  --
  /**
   * Description: La funzione restituisce il PDV di sottoscrizione della carta
   *
   * Author: R.Doti
   * Created: 26/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Return: codice del PDV di sottoscrizione della carta
   */
  FUNCTION GET_PDV_DEF( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE
  ) 
    RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vPDVCod             ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
  BEGIN
    vProcName := 'GET_PDV_DEF';
    --
    SELECT PDV_PUNTO_VENDITA_COD
      INTO vPDVCod
      FROM (SELECT PDV_PUNTO_VENDITA_COD, 
                   CRT_DATA_INIZIO,
                   CRT_DATA_FINE,
                   RANK() OVER (PARTITION BY PDV_PUNTO_VENDITA_COD 
                                    ORDER BY CRT_DATA_INIZIO DESC , 
                                            CRT_DATA_FINE DESC) RNK
              FROM ANA_CARTE CRT
             WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD)
     WHERE RNK = 1
       AND ROWNUM <= 1;
    --
    RETURN vPDVCod;
  EXCEPTION
    WHEN no_data_found THEN 
      vPasso := 'CodCli:'||P_CLIENTE_COD;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso||'-->'||SQLERRM,BASE_LOG_PCK.Errore); 
      RETURN NULL;
    WHEN OTHERS THEN
      vPasso := 'CodCli:'||P_CLIENTE_COD;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso||'-->'||SQLERRM,BASE_LOG_PCK.Errore); 
      RETURN NULL;
  END GET_PDV_DEF;
  --
  /**
   * Description: La funzione restituisce il PDV su cui ho speso maggiormente o transato maggiormente nell'ultimo anno.
   *
   * Author: R.Doti
   * Created: 26/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Return: codice del PDV di sottoscrizione della carta
   */
  FUNCTION GET_PDV_BY_ACQ( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_TIPO        VARCHAR2 --TRANSATO/SPESA      
  ) 
    RETURN ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vDataInput          DATE;
    vDataInputMob       DATE;
    vDataInputLimit     DATE;
    vStartMonth         DATE;
    --
    vPDVCod             ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    vPDVCodDef          ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    --
    vRowElab            NUMBER := 0;
    vTabWindow          DateWindowTab;
    done                BOOLEAN := TRUE;
    vNumDay             NUMBER;
  BEGIN
    vProcName := 'GET_PDV_BY_ACQ';
    --
    vDataInput := TRUNC(SYSDATE);
    --
    vPasso  := 'DATA_LIM';
    vDataInputLimit := ADD_MONTHS(vDataInput,-12);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vDataInputLimit:'||vDataInputLimit,vPasso);
    vPasso := 'GET_VAL_PARAMETRO';
    vNumDay := GET_VAL_PARAMETRO_N('P_DAY_NUM');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vNumDay:'||vNumDay,vPasso);
    --
    vPasso := 'WHILE';
    WHILE done
      LOOP
      --
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vPDVCod:'||vPDVCod,vPasso); 
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vDataInput:'||vDataInput,vPasso); 
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vDataInputLimit:'||vDataInputLimit,vPasso);
        -- 
        vStartMonth := TO_DATE(EXTRACT(YEAR FROM vDataInput) ||EXTRACT(MONTH FROM vDataInput),'YYYYMM');
        --
        vPasso := 'GET_WINDOW';
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DATA:'||vStartMonth,vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_LIMIT_WINDOW:'||6,vPasso);
        vTabWindow := GET_WINDOW(P_DATA => vStartMonth, P_LIMIT_WINDOW => 6);
        --
        for i in vTabWindow.FIRST..vTabWindow.LAST loop
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vTabWindow:['||i||']-->'||vTabWindow(i).Anno,vPasso);
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vTabWindow:['||i||']-->'||vTabWindow(i).Mese,vPasso);
        end loop;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vTabWindow:'||vTabWindow.COUNT,vPasso);
        --
        IF P_TIPO = 'TRANSATO' THEN
          vPasso := 'GET_PDV_VND';
          vPDVCod := GET_PDV_VND( 
                      P_CLIENTE_COD => P_CLIENTE_COD,
                      P_TAB_WINDOW  => vTabWindow
                      );
        END IF;
        --
        IF P_TIPO = 'SPESA' THEN
          vPasso := 'GET_PDV_VND_BY_SPESA';
          vPDVCod := GET_PDV_VND_BY_SPESA_DAY( 
                      P_CLIENTE_COD => P_CLIENTE_COD,
                      P_DAY_NUM  => vNumDay
                      );
        END IF;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vPDVCod:'||vPDVCod,vPasso);
        --
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vTabWindow(vTabWindow.LAST).Anno:'||vTabWindow(vTabWindow.LAST).Anno,vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vTabWindow(vTabWindow.LAST).Mese:'||vTabWindow(vTabWindow.LAST).Mese,vPasso);
        vDataInput := ADD_MONTHS(TO_DATE(vTabWindow(vTabWindow.LAST).Anno||vTabWindow(vTabWindow.LAST).Mese, 'YYYYMM'), -1);
        --
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vDataInput:'||vDataInput,vPasso);
        --
        vPasso := 'EXIT_PDV_FOUND';
        IF vPDVCod IS NOT NULL THEN
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'exitWHILE',vPasso); 
          done := FALSE; 
        END IF;
        vPasso := 'EXIT_DATA_LIM';
        IF vDataInput <= vDataInputLimit THEN 
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'exitWHILE',vPasso);
          done := FALSE; 
        END IF;
        
      END LOOP;
    --
    RETURN vPDVCod;
  EXCEPTION
    WHEN no_data_found THEN 
      vPasso := 'CodCli:'||P_CLIENTE_COD;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso||'-->'||SQLERRM,BASE_LOG_PCK.Errore); 
      RETURN NULL;
    WHEN OTHERS THEN
      vPasso := 'CodCli:'||P_CLIENTE_COD;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso||'-->'||SQLERRM,BASE_LOG_PCK.Errore); 
      RETURN NULL;
  END GET_PDV_BY_ACQ;
  --
  /**
   * Description: La procedura aggiorna i dati loyalty
   *  
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: PO_ERR_SEVERITA diagnostico
   * Param: PO_ERR_CODICE diagnostico
   * Param: PO_MESSAGGIO diagnostico
   * Param: P_LIMIT numero di record committati
   */
  PROCEDURE REFRESH_INFO_LOY(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 10000
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowElab            INTEGER := 0;
    vODataMin           TIMESTAMP;
    vODataMax           TIMESTAMP;
    --
    CURSOR CUR_CARD_EAN
    IS
      SELECT CRT.CRT_CARTA_COD
        FROM ANA_CARTE CRT
        WHERE CRT_EAN_COD	IS NULL;
  --
    TYPE FETCH_ARRAY_EAN IS TABLE OF CUR_CARD_EAN%ROWTYPE;
    S_ARRAY_EAN   FETCH_ARRAY_EAN;
    --
    CURSOR CUR_CARD_MASTER
    IS
      SELECT CRT.CLI_CLIENTE_COD,
             CRT.CRT_CARTA_COD,
             CRT.CRT_DATA_MIN_TRANS,
             CRT.CRT_DATA_MAX_TRANS
        FROM ANA_CARTE CRT,
             (SELECT CLI_CLIENTE_COD,
                     MIN (CRT_DATA_MIN_TRANS),
                     MAX (CRT_DATA_MAX_TRANS) CRT_DATA_MAX_TRANS
                FROM ANA_CARTE
            GROUP BY CLI_CLIENTE_COD) MAX_CRT
        WHERE CRT.CLI_CLIENTE_COD = MAX_CRT.CLI_CLIENTE_COD
          AND CRT.CRT_DATA_MAX_TRANS = MAX_CRT.CRT_DATA_MAX_TRANS;
  --
    TYPE FETCH_ARRAY_MASTER IS TABLE OF CUR_CARD_MASTER%ROWTYPE;
    S_ARRAY_MASTER   FETCH_ARRAY_MASTER;
    --
    CURSOR CUR_CARD
    IS
      SELECT CRT.CLI_CLIENTE_COD,
             CRT.CRT_CARTA_COD,
             CRT.CRT_DATA_MIN_TRANS,
             CRT.CRT_DATA_MAX_TRANS
        FROM ANA_CARTE CRT,
             (SELECT CLI_CLIENTE_COD,
                     MIN (CRT_DATA_MIN_TRANS),
                     MAX (CRT_DATA_MAX_TRANS) CRT_DATA_MAX_TRANS
                FROM ANA_CARTE
            GROUP BY CLI_CLIENTE_COD) MAX_CRT
        WHERE CRT.CLI_CLIENTE_COD = MAX_CRT.CLI_CLIENTE_COD
          AND CRT.CRT_DATA_MAX_TRANS = MAX_CRT.CRT_DATA_MAX_TRANS;
    --
    CURSOR CUR_CARD_CLI
    IS
      SELECT CLI_CLIENTE_COD,
             MIN (CRT_DATA_MIN_TRANS) CRT_DATA_MIN_TRANS,
             MAX (CRT_DATA_MAX_TRANS) CRT_DATA_MAX_TRANS
        FROM ANA_CARTE CRT
       --WHERE CRT.CLI_CLIENTE_COD = '1077083'
       GROUP BY CLI_CLIENTE_COD;
  --
    TYPE FETCH_ARRAY IS TABLE OF CUR_CARD%ROWTYPE;
    TYPE FETCH_ARRAY_CLI IS TABLE OF CUR_CARD_CLI%ROWTYPE;
    S_ARRAY   FETCH_ARRAY;
    S_ARRAY_CLI   FETCH_ARRAY_CLI;
    tsStartTime TIMESTAMP;
    tsEndTime TIMESTAMP;
  BEGIN
    --
    vProcName := 'REFRESH_INFO_LOY';
    -- Start the timer
    tsStartTime := CURRENT_TIMESTAMP;
    --
    DBMS_APPLICATION_INFO.set_action(action_name => vProcName);
    --
    vPasso := 'UPDT_VNH_FK';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '0.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, '0.'||vPasso,BASE_LOG_PCK.Info);
    --0) Refresh dati testata venduti
    UPDT_VNH_FK(
      PO_ERR_SEVERITA,
      PO_ERR_CODICE,
      PO_MESSAGGIO,
      P_LIMIT=> 50000,
      P_FK   => 'ANA_CRT',
      P_ANNO => EXTRACT (YEAR FROM SYSDATE),
      P_MESE => NULL
     );
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
    --
    --1) Aggiornamento transato min e max delle carte
    vPasso := 'UPT_ANA_CRT';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '1.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, '1.'||vPasso,BASE_LOG_PCK.Info);
    --
    BEGIN
      FOR rec IN (SELECT CRT_CARTA_COD
                    FROM ANA_CARTE
                   --WHERE CRT_DATA_MIN_TRANS IS NULL 
                   --  AND CRT_DATA_MAX_TRANS IS NULL
                 )
      LOOP
      --
        BEGIN
          vPasso  := '@@@@@@@@@@@@@@***CRT_CARTA_COD***@@@@@@@@@@@@@@';
          --
          vODataMin := NULL;
          vODataMax := NULL;
          --
          vRowElab := vRowElab + 1;
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vRowElab:'||vRowElab,vPasso);
          --
          vPasso := 'REFRESH_MIN_MAX_TRANS_CRT';
          REFRESH_MIN_MAX_TRANS_CRT (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                                     P_CARTA_COD => rec.CRT_CARTA_COD,
                                     P_GET_DATA_MIN_TRANS => TRUE,
                                     P_GET_DATA_MAX_TRANS => TRUE,
                                     PO_DATA_MIN_TRANS    => vODataMin,
                                     PO_DATA_MAX_TRANS    => vODataMax
                                     );
          --
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
          IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
          --
          IF MOD(vRowElab, P_LIMIT) = 0 THEN
          --
            vPasso := 'COMMIT';
            BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Commit RowCNT:'||vRowElab,vPasso);
            MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
          END IF;
        --
        EXCEPTION
          WHEN ErroreGestito THEN
            BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
            BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
            PO_ERR_SEVERITA := NULL;
            PO_ERR_CODICE:= NULL; 
            PO_MESSAGGIO:= NULL;
        END;
      END LOOP;
      tsEndTime := CURRENT_TIMESTAMP;
      MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
      DBMS_APPLICATION_INFO.set_client_info(client_info => '1.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime));
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '1.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime),BASE_LOG_PCK.Info);
    END;
    --
    --2) Aggiornamento EAN Code carte
    vPasso := 'UPT_ANA_CRT_EAN';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '2.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '2.'||vPasso,BASE_LOG_PCK.Info);
    tsStartTime := CURRENT_TIMESTAMP;
    BEGIN
      OPEN CUR_CARD_EAN;
      LOOP
        FETCH CUR_CARD_EAN
        BULK COLLECT INTO S_ARRAY_EAN
        LIMIT P_LIMIT;
        --
          FORALL I IN 1 .. S_ARRAY_EAN.COUNT
          UPDATE /*+ PARALLEL (CRT, 6)*/
                 ANA_CARTE CRT
            SET CRT_EAN_COD = BASE_UTIL_PCK.GETCHKDIGITEAN13(S_ARRAY_EAN(I).CRT_CARTA_COD)
          WHERE CRT.CRT_CARTA_COD   = S_ARRAY_EAN(I).CRT_CARTA_COD;
        COMMIT;
        EXIT WHEN CUR_CARD_EAN%NOTFOUND;
      END LOOP;
      --
      CLOSE CUR_CARD_EAN;
      tsEndTime := CURRENT_TIMESTAMP;
      MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
      DBMS_APPLICATION_INFO.set_client_info(client_info => '2.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime));
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '2.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime) ,BASE_LOG_PCK.Info);
    END;
    --
    --3) Aggiornamento Master Card
    vPasso := 'UPT_MASTER_CRT';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '3.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '3.'||vPasso ,BASE_LOG_PCK.Info);
    tsStartTime := CURRENT_TIMESTAMP;
    BEGIN
      --
      UPDATE /*+ PARALLEL (CRT, 6)*/
             ANA_CARTE CRT
         SET CRT_MASTER  = 'N';
      --
      MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
      OPEN CUR_CARD_MASTER;
      LOOP
        FETCH CUR_CARD_MASTER
        BULK COLLECT INTO S_ARRAY_MASTER
        LIMIT P_LIMIT;
        --
          FORALL I IN 1 .. S_ARRAY_MASTER.COUNT
          UPDATE /*+ PARALLEL (CRT, 6)*/
                 ANA_CARTE CRT
            SET CRT_MASTER  = 'S'
          WHERE CRT.CRT_CARTA_COD = S_ARRAY_MASTER(I).CRT_CARTA_COD;
        COMMIT;
        EXIT WHEN CUR_CARD_MASTER%NOTFOUND;
      END LOOP;
      --
      CLOSE CUR_CARD_MASTER;
      --
      MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
      tsEndTime := CURRENT_TIMESTAMP;
      DBMS_APPLICATION_INFO.set_client_info(client_info => '3.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime));
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '3.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime) ,BASE_LOG_PCK.Info);
    END;
    --
    --4) Aggiornamento transato min e max del cliente
    vPasso := 'UPDT_CLI_MIN_MAX_TRANS';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '4.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '4.'||vPasso ,BASE_LOG_PCK.Info);
    tsStartTime := CURRENT_TIMESTAMP;
    BEGIN
      OPEN CUR_CARD_CLI;
      LOOP
        FETCH CUR_CARD_CLI
        BULK COLLECT INTO S_ARRAY_CLI
        LIMIT P_LIMIT;
        --
          FORALL I IN 1 .. S_ARRAY_CLI.COUNT
          UPDATE /*+ PARALLEL (CLI, 6)*/ ANA_CLIENTI CLI
             SET CLI_DATA_MIN_TRANS = S_ARRAY_CLI(I).CRT_DATA_MIN_TRANS,
                 CLI_DATA_MAX_TRANS = S_ARRAY_CLI(I).CRT_DATA_MAX_TRANS
           WHERE CLI.CLI_CLIENTE_COD = S_ARRAY_CLI(I).CLI_CLIENTE_COD;
        COMMIT;
        EXIT WHEN CUR_CARD_CLI%NOTFOUND;
       END LOOP;
      --
      CLOSE CUR_CARD_CLI;
      MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
      tsEndTime := CURRENT_TIMESTAMP;
      DBMS_APPLICATION_INFO.set_client_info(client_info => '4.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime));
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '4.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime) ,BASE_LOG_PCK.Info);
    END;
    --
    --5) Aggiornamento kpi del cliente
    vPasso := 'PO_ERR_SEVERITA';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '5.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '5.'||vPasso ,BASE_LOG_PCK.Info);
    tsStartTime := CURRENT_TIMESTAMP;
    UPDT_ANA_CLI_INFO(
      PO_ERR_SEVERITA       => PO_ERR_SEVERITA,
      PO_ERR_CODICE         => PO_ERR_CODICE,
      PO_MESSAGGIO          => PO_MESSAGGIO,
      P_LIMIT               => P_LIMIT
    );
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    tsEndTime := CURRENT_TIMESTAMP;
    DBMS_APPLICATION_INFO.set_client_info(client_info => '5.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime));
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '5.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime) ,BASE_LOG_PCK.Info);
    MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
    IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
    --
    --6) Aggiornamento Movimentazione Punti e Saldo Carta
    vPasso := 'UPDT_ANA_CRT';
    DBMS_APPLICATION_INFO.set_client_info(client_info => '6.'||vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '6.'||vPasso ,BASE_LOG_PCK.Info);
    tsStartTime := CURRENT_TIMESTAMP;
    UPDT_ANA_CRT(
      PO_ERR_SEVERITA       => PO_ERR_SEVERITA,
      PO_ERR_CODICE         => PO_ERR_CODICE,
      PO_MESSAGGIO          => PO_MESSAGGIO,
      P_LIMIT               => P_LIMIT,
      P_TIPO_PDT            => 'ALL'
    );
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    tsEndTime := CURRENT_TIMESTAMP;
    DBMS_APPLICATION_INFO.set_client_info(client_info => '6.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime));
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName,  '6.'||vPasso||' -Time elapsed:'||to_char(tsEndTime - tsStartTime) ,BASE_LOG_PCK.Info);
    MGDWH.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=>vProcName , P_PASSO=>vPasso );
    IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
  --
  EXCEPTION
    WHEN ErroreGestito THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso||')=>'||PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_INFO_LOY;
  /**
  * UPDT_ANA_CLI_INFO(): procedura che aggiorna il PDV_PUNTO_VENDITA_COD_VND in base al numero di transazioni ed il 
  * PDV_PUNTO_VENDITA_COD_PREF. 
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_ANA_CLI_INFO(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 1000
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vDataInput          DATE;
    vDataInputMob       DATE;
    vDataInputLimit     DATE;
    vStartMonth         DATE;
    --
    vPDVCodSpesa        ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    vPDVCodTrans        ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    vPDVCodDef          ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
    --
    vRowElab            NUMBER := 0;
    vTabWindow          DateWindowTab;
    done                BOOLEAN := TRUE;
    --
    vParTransNum           NUMBER;
    vParEvtNum             NUMBER;
    vParDayNum             NUMBER;
    vParFattOutlier        NUMBER;
    vParDayNumFatt         NUMBER;
    vParDayNew             NUMBER;
    vParDayLost            NUMBER;
    --
    vAbuser                NUMBER:=0;
    vFattPeriodo           NUMBER:=0;
    vNumTransazioni        NUMBER:=0;
    vOutlier               VARCHAR2(5);
    vIsNew                 VARCHAR2(5);
    vIsLost                 VARCHAR2(5);
  BEGIN
    --
    vProcName := 'UPDT_ANA_CLI_PDV_VND';
    --
    vPasso := 'GET_VAL_PARAMETRO_N';
    vParTransNum := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_TRANS_NUM');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_TRANS_NUM:'||vParTransNum,vPasso);
    vParEvtNum := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_EVT_NUM');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_EVT_NUM:'||vParEvtNum,vPasso);
    vParDayNum := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_DAY_NUM_AB');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DAY_NUM_AB:'||vParDayNum,vPasso);
    vParDayNumFatt := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_DAY_FATT');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DAY_FATT:'||vParDayNumFatt,vPasso);
    vParFattOutlier := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_FATT_OTL');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_FATT_OTL:'||vParFattOutlier,vPasso);
    vParDayNew := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_DAY_NEW');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DAY_NEW:'||vParDayNew,vPasso);
    vParDayLost := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_DAY_LOST');
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DAY_LOST:'||vParDayLost,vPasso);
    --
    vPasso := 'CUR_ANA_CLI';
    FOR rec_ana_cli IN (SELECT CLI_CLIENTE_COD,
                               PDV_PUNTO_VENDITA_COD_PREF, 
                               PDV_PUNTO_VENDITA_COD_VND, 
                               PDV_PUNTO_VENDITA_COD_SIT,
                               PDV_PUNTO_VENDITA_COD_FATT,
                               CLI_DATA_MIN_TRANS,
                               CLI_DATA_MAX_TRANS
                          FROM ANA_CLIENTI
                          --WHERE CLI_CLIENTE_COD  IN  ('1882270')
                         )
    LOOP
    --
      vPasso  := '@@@@@@@@@@@@@@***CLIENTE***@@@@@@@@@@@@@@';
      vPDVCodSpesa := NULL;
      vPDVCodDef   := NULL;
      vPDVCodTrans := NULL;
      done         := TRUE;
      vAbuser      := 0;
      vFattPeriodo := NULL;
      vNumTransazioni := NULL;
      vOutlier := NULL;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Cod.Cliente:'||rec_ana_cli.CLI_CLIENTE_COD,vPasso);
      vDataInput := TRUNC(SYSDATE);
      --Cancello il PDV_VND e PDV_PREF
      vPasso  := 'DEL_PDV_VND_PREF';
      UPDATE ANA_CLIENTI
         SET PDV_PUNTO_VENDITA_COD_PREF = NULL,
             PDV_PUNTO_VENDITA_COD_VND = NULL,
             PDV_PUNTO_VENDITA_COD_FATT = NULL,
             CLI_ABUSER = NULL
       WHERE CLI_CLIENTE_COD = rec_ana_cli.CLI_CLIENTE_COD;
       BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'RowCNT:'||SQL%ROWCOUNT,vPasso);
      --
      vPasso  := 'GET_PDV_BY_ACQ-SPESA';
      vPDVCodSpesa := GET_PDV_BY_ACQ( 
        P_CLIENTE_COD => rec_ana_cli.CLI_CLIENTE_COD,
        P_TIPO        => 'SPESA'
      );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vPDVCodSpesa:'||vPDVCodSpesa,vPasso);
      --
      vPasso  := 'GET_PDV_BY_ACQ-TRANSATO';
      vPDVCodTrans := GET_PDV_BY_ACQ( 
        P_CLIENTE_COD => rec_ana_cli.CLI_CLIENTE_COD,
        P_TIPO        => 'TRANSATO'
      );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vPDVCodTrans:'||vPDVCodTrans,vPasso);
      --
      --Il pdv di default si calcola solo se non è presente quello del transato e del sito
      IF vPDVCodSpesa IS NULL AND rec_ana_cli.PDV_PUNTO_VENDITA_COD_SIT IS NULL THEN 
        vPasso := 'GET_PDV_DEF';
        vPDVCodDef := GET_PDV_DEF( 
                      P_CLIENTE_COD => rec_ana_cli.CLI_CLIENTE_COD
                      );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vPDVCodDef:'||vPDVCodDef,vPasso);
      END IF;
      --
      vPasso := 'GET_ISABUSER';
      vAbuser := GET_ISABUSER(P_CLIENTE_COD => rec_ana_cli.CLI_CLIENTE_COD,
                              P_TRANS_NUM   => vParTransNum,
                              P_EVT_NUM     => vParEvtNum,
                              P_DAY_NUM     => VParDayNum
                              );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vAbuser:'||vAbuser,vPasso);
      --
      vPasso := 'GET_TOTAL_BY_DAY_NUM';
      vFattPeriodo := GET_TOTAL_BY_DAY_NUM( 
                         P_CLIENTE_COD => rec_ana_cli.CLI_CLIENTE_COD,
                         P_DECORRENZA  => NVL(rec_ana_cli.CLI_DATA_MAX_TRANS, kStampOggi),
                         P_DAY_NUM     => vParDayNumFatt,
                         P_TIPO        => 'F'
                      ) ;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vFattPeriodo:'||vFattPeriodo,vPasso);
      --
      vPasso := 'GET_TOTAL_BY_DAY_NUM';
      vNumTransazioni := GET_TOTAL_BY_DAY_NUM( 
                         P_CLIENTE_COD => rec_ana_cli.CLI_CLIENTE_COD,
                         P_DECORRENZA  => NVL(rec_ana_cli.CLI_DATA_MAX_TRANS, kStampOggi),
                         P_DAY_NUM     => vParDayNumFatt,
                         P_TIPO        => 'T'
                      ) ;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vNumTransazioni:'||vNumTransazioni,vPasso);
      --
      vPasso := 'GET_OUTLIER';
      vOutlier :=  GET_OUTLIER( 
                            P_CLIENTE_COD    => rec_ana_cli.CLI_CLIENTE_COD,
                            P_FATTURATO      => vFattPeriodo,
                            P_FATTURATO_CTRL => vParFattOutlier
                      );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vOutlier:'||vOutlier,vPasso);
      --
      vPasso := 'GET_ISNEW';
      vIsNew :=  GET_ISNEW( P_DATA_MIN_TRANS    => rec_ana_cli.CLI_DATA_MIN_TRANS,
                            P_DAY_NEW           => vParDayNew
                          );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vIsNew:'||vIsNew,vPasso);
      --
      vPasso := 'GET_ISLOST';
      vIsLost :=  GET_ISLOST(P_DATA_MAX_TRANS     => rec_ana_cli.CLI_DATA_MAX_TRANS,
                             P_DAY_LOST           => vParDayLost
                           );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vIsLost:'||vIsLost,vPasso);
      --
      vPasso := 'UPDT_PDV';
      UPDATE ANA_CLIENTI
         SET PDV_PUNTO_VENDITA_COD_FATT = vPDVCodSpesa,
             PDV_PUNTO_VENDITA_COD_VND  = vPDVCodTrans,
             PDV_PUNTO_VENDITA_COD_PREF = COALESCE(PDV_PUNTO_VENDITA_COD_SIT, vPDVCodSpesa, vPDVCodDef),
             CLI_ABUSER                 = NVL2(vAbuser, 'S','N'),
             CLI_ABUSER_EVT_NUM	        = NVL(vAbuser, NULL),
             CLI_FATTURATO_PERIODO      = vFattPeriodo,
             CLI_NUMERO_TRANSAZIONI     = vNumTransazioni,
             CLI_SCONTRINO_MEDIO        = TRUNC(vFattPeriodo/vNumTransazioni,2),
             CLI_OUTLIER                = vOutlier,
             CLI_NUOVO                  = vIsNew,
             CLI_PERSO                  = vIsLost
       WHERE CLI_CLIENTE_COD = rec_ana_cli.CLI_CLIENTE_COD;
       --
      vRowElab := vRowElab + 1;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vRowElab:'||vRowElab,vPasso);
      --
      IF MOD(vRowElab, P_LIMIT) = 0 THEN
      --
        vPasso := 'COMMIT';
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Commit RowCNT:'||vRowElab,vPasso);
        BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME =>vProcName , P_PASSO=> 'Commit RowCNT:'||vRowElab);
      END IF;
      --
    END LOOP;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END UPDT_ANA_CLI_INFO;
  /**
  * UPDT_ANA_CLI(): procedura che aggiorna i dati di dettaglio dei clienti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_ANA_CLI(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    CURSOR CUR_CLI_KONCENTRO
    IS
      SELECT ID_CUSTOMER,
             FLG_WEBSITE,
             FLG_WEBSITE_USE,
             FLG_WEBSITE_DATE,
             FLG_WEBSITE_USE_DATE
        FROM FRAME.KFCUSTOMER_HOST@KPROMO CLI;
    --
    TYPE FETCH_ARRAY IS TABLE OF CUR_CLI_KONCENTRO%ROWTYPE;
    S_ARRAY   FETCH_ARRAY;
    --
  BEGIN
    --
    vProcName := 'UPDT_ANA_CLI';
    --
    vPasso := 'CUR_CLI_KONCENTRO';
    OPEN CUR_CLI_KONCENTRO;
    LOOP
       FETCH CUR_CLI_KONCENTRO
       BULK COLLECT INTO S_ARRAY
       LIMIT P_LIMIT;
       --
       FORALL I IN 1 .. S_ARRAY.COUNT
          UPDATE /*+ PARALLEL (CRT, 6)*/
                ANA_CLIENTI CLI
             SET CLI_FLG_WEBSITE          = S_ARRAY (I).FLG_WEBSITE,
                 CLI_FLG_WEBSITE_USE      = S_ARRAY (I).FLG_WEBSITE_USE,
                 CLI_FLG_WEBSITE_DATE     = S_ARRAY (I).FLG_WEBSITE_DATE,
                 CLI_FLG_WEBSITE_USE_DATE = S_ARRAY (I).FLG_WEBSITE_USE_DATE
           WHERE CLI.CLI_CLIENTE_COD   = S_ARRAY (I).ID_CUSTOMER;
       COMMIT;
       EXIT WHEN CUR_CLI_KONCENTRO%NOTFOUND;
    END LOOP;
    --
    CLOSE CUR_CLI_KONCENTRO;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END UPDT_ANA_CLI;
  /**
  * UPDT_ANA_CRT(): procedura che aggiorna i dati di dettaglio delle carte
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_ANA_CRT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_TIPO_PDT            IN     VARCHAR2 DEFAULT 'ALL' --'MOV'--'PNT'
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    CURSOR CUR_CARD
    IS
      SELECT BAL.CARD_FK,
             MIN (BAL.TS_INS) MIN_TRANS,
             MAX (BAL.TS_UPD) MAX_TRANS,
             0 ACM_VAL
        FROM KCRM.OBALANCE@KPROMO BAL
    GROUP BY BAL.CARD_FK;
    --
    TYPE FETCH_ARRAY IS TABLE OF CUR_CARD%ROWTYPE;
    S_ARRAY   FETCH_ARRAY;
    --
    CURSOR CUR_CARD_POINT
    IS
      SELECT /*+ LEADING(AC,BAL) 
                 USE_NL(BAL) INDEX(BAL)
                 */
             BAL.CARD_FK,
             sysdate MIN_TRANS,
             sysdate MAX_TRANS,
             BAL.ACM_VAL
        FROM KCRM.OACM@KPROMO AC,
             KCRM.OBALANCE@KPROMO BAL
       WHERE LOCALTIMESTAMP BETWEEN AC.DT_START AND AC.DT_END
         AND AC.ID_ACM = '0002020007' --da modificare
         AND BAL.ACM_FK = AC.ID_ACM 
         AND BAL.CHAN_FK  = AC.CHAN_FK
         AND BAL.CHAN_FK  = '001';
    --
  BEGIN
    --
    vProcName := 'UPDT_ANA_CRT';
    --
    IF P_TIPO_PDT = 'ALL' OR P_TIPO_PDT = 'MOV' THEN
    --
      vPasso := 'OpenCur_ANA_CRT';
      OPEN CUR_CARD;
      LOOP
         FETCH CUR_CARD
         BULK COLLECT INTO S_ARRAY
         LIMIT P_LIMIT;
         --
         FORALL I IN 1 .. S_ARRAY.COUNT
            UPDATE /*+ PARALLEL (CRT, 6)*/
                  ANA_CARTE CRT
               SET CRT_DATA_PRIMA_MOV  = S_ARRAY (I).MIN_TRANS,
                   CRT_DATA_ULTIMA_MOV = S_ARRAY (I).MAX_TRANS
             WHERE CRT.CRT_CARTA_COD   = S_ARRAY (I).CARD_FK;
         COMMIT;
         EXIT WHEN CUR_CARD%NOTFOUND;
      END LOOP;
      --
      CLOSE CUR_CARD;
    END IF;
    --
    IF P_TIPO_PDT = 'ALL' OR P_TIPO_PDT = 'PNT' THEN
    --
      vPasso := 'OpenCur_ANA_CRT';
      OPEN CUR_CARD_POINT;
      LOOP
         FETCH CUR_CARD_POINT
         BULK COLLECT INTO S_ARRAY
         LIMIT P_LIMIT;
         --
         FORALL I IN 1 .. S_ARRAY.COUNT
            UPDATE /*+ PARALLEL (CRT, 6)*/
                  ANA_CARTE CRT
               SET CRT_SALDO_PUNTI = S_ARRAY (I).ACM_VAL
             WHERE CRT.CRT_CARTA_COD = S_ARRAY (I).CARD_FK;
         COMMIT;
         EXIT WHEN CUR_CARD_POINT%NOTFOUND;
      END LOOP;
      --
      CLOSE CUR_CARD_POINT;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END UPDT_ANA_CRT;
  --
  /**
  * UPDT_VNH_FK(): procedura che aggiorna le FK
  * -ANA_CRT -->CRT_CARTA_COD_XK + CLI_CLIENTE_COD_XK
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_VNH_FK(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_FK                  IN     VARCHAR2,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    s_vnh_crt    fetch_vnh_crt;
  BEGIN
    --
    vProcName := 'UPDT_VNH_FK';
    vPasso := 'INIT';
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'FK:'||P_FK||' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FK:'||P_FK||' Anno:'||P_ANNO||' Mese:'||P_MESE ,vPasso);
    --
    IF P_FK = 'ANA_CRT' THEN
    --
      vPasso := 'OpenCur_ANA_CRT';
      OPEN cur_vnh_crt(P_ANNO, P_MESE);
      --
      LOOP
        FETCH cur_vnh_crt
        BULK COLLECT INTO s_vnh_crt
        LIMIT P_LIMIT;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ArrayCNT:'||s_vnh_crt.COUNT,vPasso);
        FORALL INDX IN 1 .. s_vnh_crt.COUNT
        --
          UPDATE VND_VENDITE_HEAD
              SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                     (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                        FROM ANA_CARTE
                       WHERE CRT_CARTA_COD = s_vnh_crt(INDX).CRT_CARTA_COD_XK)
            WHERE VNH_VENDUTO_HEAD_ID = s_vnh_crt(INDX).VNH_VENDUTO_HEAD_ID
              AND VNH_ANNO = P_ANNO
              AND VNH_MESE = NVL(P_MESE,VNH_MESE) ;
        --
        COMMIT;
        EXIT WHEN cur_vnh_crt%NOTFOUND;
      END LOOP;
      --
      CLOSE cur_vnh_crt;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END UPDT_VNH_FK;
  /**
  * UPDT_VND_FK(): procedura che aggiorna le FK
  * -ANA_CRT -->CRT_CARTA_COD_XK + CLI_CLIENTE_COD_XK
  * -ANA_ART --> ART_ARTICOLO_COD_XK
  * -ANA_PRM -->PRM_PROMOZIONE_COD_XK
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE UPDT_VND_FK(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_FK                  IN     VARCHAR2,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    s_vnd_crt    fetch_vnd_crt;
    s_vnd_art    fetch_vnd_art;
  BEGIN
    --
    vProcName := 'UPDT_VND_FK';
    vPasso := 'INIT';
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'FK:'||P_FK||' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FK:'||P_FK||' Anno:'||P_ANNO||' Mese:'||P_MESE ,vPasso);
    --
    IF P_FK = 'ANA_CRT' THEN
    --
      vPasso := 'OpenCur_ANA_CRT';
      OPEN cur_vnd_crt(P_ANNO, P_MESE);
      --
      LOOP
        FETCH cur_vnd_crt
        BULK COLLECT INTO s_vnd_crt
        LIMIT P_LIMIT;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ArrayCNT:'||s_vnd_crt.COUNT,vPasso);
        FORALL INDX IN 1 .. s_vnd_crt.COUNT
        --
          UPDATE VND_VENDITE_DETAIL
              SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                     (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                        FROM ANA_CARTE
                       WHERE CRT_CARTA_COD = s_vnd_crt(INDX).CRT_CARTA_COD_XK)
            WHERE VND_VENDUTO_DETAIL_ID = s_vnd_crt(INDX).VND_VENDUTO_DETAIL_ID
              AND VND_ANNO = P_ANNO
              AND VND_MESE = P_MESE;
        --
        COMMIT;
        EXIT WHEN cur_vnd_crt%NOTFOUND;
      END LOOP;
      --
      CLOSE cur_vnd_crt;
    END IF;
    --
    IF P_FK = 'ANA_ART' THEN
    --
      vPasso := 'OpenCur_ANA_ART';
      OPEN cur_vnd_art(P_ANNO, P_MESE);
      --
      LOOP
        FETCH cur_vnd_art
        BULK COLLECT INTO s_vnd_art
        LIMIT P_LIMIT;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ArrayCNT:'||s_vnd_art.COUNT,vPasso);
        FORALL INDX IN 1 .. s_vnd_art.COUNT
        --
          UPDATE VND_VENDITE_DETAIL DET
             SET DET.ART_ARTICOLO_COD =
                     (SELECT ART_ARTICOLO_COD
                      FROM ANA_ARTICOLI
                     WHERE ART_ARTICOLO_COD = s_vnd_art(INDX).ART_ARTICOLO_COD_XK)
            WHERE VND_VENDUTO_DETAIL_ID = s_vnd_art(INDX).VND_VENDUTO_DETAIL_ID
              AND VND_ANNO = P_ANNO
              AND VND_MESE = P_MESE;
        --
        COMMIT;
        EXIT WHEN cur_vnd_art%NOTFOUND;
      END LOOP;
      --
      CLOSE cur_vnd_art;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END UPDT_VND_FK;
  --
  /**
  * VND_HEAD_IMPORT(): procedura che importa le testate scontrino da VS
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE VND_HEAD_IMPORT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    TYPE fetch_vnd_head IS TABLE OF cur_vnd_head%ROWTYPE;
    s_vnd_head      fetch_vnd_head;
    vDataDa        VARCHAR2(10);
    vDataA         VARCHAR2(10);
  BEGIN
  --
    vProcName := 'VND_HEAD_IMPORT';
    vPasso := 'INIT';
    --
    vDataDa := P_ANNO||'-'||LPAD(P_MESE,2,'0')||'-01';
    IF P_MESE < 12 THEN
      vDataA  := P_ANNO||'-'||LPAD(P_MESE+1,2,'0')||'-01';
    ELSE
      vDataA  := P_ANNO+1||'-01'||'-01';
    END IF;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, ' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDa:'||vDataDa||' vDataA:'||vDataA ,vPasso);
    --
    vPasso := 'OpenCur';
    OPEN cur_vnd_head(vDataDa, vDataA);
    --
    LOOP
      FETCH cur_vnd_head
      BULK COLLECT INTO s_vnd_head
      LIMIT P_LIMIT; -- This will make sure that every iteration has 100 records selected
      --
      EXIT WHEN s_vnd_head.COUNT () = 0;
      --
      FORALL INDX IN 1 .. s_vnd_head.COUNT SAVE EXCEPTIONS
        INSERT INTO VND_VENDITE_HEAD(
                      PDV_PUNTO_VENDITA_COD,
                      VNH_DATA_VEND,
                      VNH_ANNO,
                      VNH_MESE,
                      VNH_NUMERO_CASSA,
                      VNH_NUMERO_SCONTRINO,
                      VNH_CASSIERE_COD,
                      VNH_CASSIERE,
                      VNH_SCONTRINO_TOT,
                      VNH_SCAN_ITEMS_TOT,
                      VNH_ITEMS_TOT,
                      JOB_ID,
                      DATA_IMPORT,
                      CRT_CARTA_COD_XK,
                      VNH_RESO,
                      VNH_ANNULLAMENTO,
                      VNH_TERM_TYPE,
                      VNH_DELIVERY_TYPE)
        VALUES(s_vnd_head(INDX).PDV_PUNTO_VENDITA_COD,
               s_vnd_head(INDX).VNH_DATA_VEND,
               s_vnd_head(INDX).VNH_ANNO,
               s_vnd_head(INDX).VNH_MESE,
               s_vnd_head(INDX).VNH_NUMERO_CASSA,
               s_vnd_head(INDX).VNH_NUMERO_SCONTRINO,
               s_vnd_head(INDX).VNH_CASSIERE_COD,
               s_vnd_head(INDX).VNH_CASSIERE,
               s_vnd_head(INDX).VNH_SCONTRINO_TOT,
               s_vnd_head(INDX).VNH_SCAN_ITEMS_TOT,
               s_vnd_head(INDX).VNH_ITEMS_TOT,
               s_vnd_head(INDX).JOB_ID,
               s_vnd_head(INDX).DATA_IMPORT,
               s_vnd_head(INDX).CRT_CARTA_COD_XK,
               s_vnd_head(INDX).VNH_RESO,
               s_vnd_head(INDX).VNH_ANNULLAMENTO,
               s_vnd_head(INDX).VNH_TERM_TYPE,
               s_vnd_head(INDX).VNH_DELIVERY_TYPE
        );
      --
      COMMIT;
    END LOOP;
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, ' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDa:'||vDataDa||' vDataA:'||vDataA ,vPasso);
    --
  EXCEPTION
    WHEN OTHERS THEN
    --
      IF SQLCODE = -24381 THEN
      --
        FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
        LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>
        END LOOP;
      END IF;
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END VND_HEAD_IMPORT;
  --
  /**
  * PROMO_IMPORT(): procedura che recupera le promozioni sul dettaglio vendite
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE PROMO_IMPORT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    s_vnd_promo    fetch_vnd_promo;
    vDataDa        VARCHAR2(10);
    vDataA         VARCHAR2(10);
  BEGIN
  --
    vProcName := 'PROMO_IMPORT';
    vPasso := 'INIT';
    --
    vDataDa := P_ANNO||'-'||LPAD(P_MESE,2,'0')||'-01';
    IF P_MESE < 12 THEN
      vDataA  := P_ANNO||'-'||LPAD(P_MESE+1,2,'0')||'-01';
    ELSE
      vDataA  := P_ANNO+1||'-01'||'-01';
    END IF;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, ' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDa:'||vDataDa||' vDataA:'||vDataA ,vPasso);
    --
    vPasso := 'OpenCur';
    OPEN cur_vnd_promo(vDataDa, vDataA);
    --
    LOOP
      FETCH cur_vnd_promo
      BULK COLLECT INTO s_vnd_promo
      LIMIT P_LIMIT; -- This will make sure that every iteration has 100 records selected
      --
      EXIT WHEN s_vnd_promo.COUNT () = 0;
      --
      FORALL INDX IN 1 .. s_vnd_promo.COUNT SAVE EXCEPTIONS
         UPDATE /*+ PARALLEL(DET 4 )*/ VND_VENDITE_DETAIL DET
            SET PRM_PROMOZIONE_COD_XK = s_vnd_promo(INDX).ID_PROMO_OFF,
                VND_SCONTO_PROMO = s_vnd_promo(INDX).DISCOUNT
          WHERE PDV_PUNTO_VENDITA_COD = s_vnd_promo(INDX).SID
                AND VND_NUMERO_CASSA = s_vnd_promo(INDX).TID
                AND VND_NUMERO_SCONTRINO = s_vnd_promo(INDX).XACTNBR
                AND VND_ITMIDX = s_vnd_promo(INDX).ITMIDX
                AND ART_ARTICOLO_COD = TO_NUMBER(s_vnd_promo(INDX).INTCODE)
                AND TRUNC (VND_DATA_VEND) = TO_DATE (s_vnd_promo(INDX).DT_DATE, 'YYYY-MM-DD')
                AND VND_ANNO = P_ANNO
                AND VND_MESE = P_MESE;
      --
      COMMIT;
    END LOOP;
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, ' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDa:'||vDataDa||' vDataA:'||vDataA ,vPasso);
    --
  EXCEPTION
    WHEN OTHERS THEN
    --
      IF SQLCODE = -24381 THEN
      --
        FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
        LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>
        END LOOP;
      END IF;
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END PROMO_IMPORT;
  --
  FUNCTION GET_itmidx
  (
    P_SID       NUMBER,
    P_DATA      VARCHAR2,
    P_TID       NUMBER,
    P_XACTNBR   NUMBER,
    P_SEGNUMBER NUMBER
  )
  RETURN NUMBER
  PARALLEL_ENABLE
  IS
    vOut NUMBER;
  BEGIN
    --
     SELECT DVEND.itmidx
       INTO vOut
      FROM TL_ITEMS_NOR@CRSMG DVEND
     WHERE   DVEND.SID = P_SID
       AND DVEND.DT_DATE = P_DATA
       AND DVEND.TID = P_TID
       AND DVEND.XACTNBR =P_XACTNBR
       AND DVEND.SEGNBR = P_SEGNUMBER
       AND ROWNUM <=1;
  RETURN vOut;
  END GET_itmidx;
  --
 /**
  * VND_DETT_IMPORT(): procedura che recupera le promozioni sul dettaglio vendite
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE VND_DETT_IMPORT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_LIMIT               IN     NUMBER DEFAULT 50000,
      P_ANNO                IN     NUMBER,
      P_MESE                IN     NUMBER,
      P_DAY                 IN     NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    s_vnd_promo    fetch_vnd_promo;
    vDataDa        VARCHAR2(10);
    vDataA         VARCHAR2(10);
  BEGIN
  --
    vProcName := 'PROMO_IMPORT';
    vPasso := 'INIT';
    --
    vDataDa := P_ANNO||'-'||LPAD(P_MESE,2,'0')||'-01';
    IF P_MESE < 12 THEN
      vDataA  := P_ANNO||'-'||LPAD(P_MESE+1,2,'0')||'-01';
    ELSE
      vDataA  := P_ANNO+1||'-01'||'-01';
    END IF;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, ' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDa:'||vDataDa||' vDataA:'||vDataA ,vPasso);
    --
    vPasso := 'OpenCur';
    OPEN cur_vnd_promo(vDataDa, vDataA);
    --
    LOOP
      FETCH cur_vnd_promo
      BULK COLLECT INTO s_vnd_promo
      LIMIT P_LIMIT; -- This will make sure that every iteration has 100 records selected
      --
      EXIT WHEN s_vnd_promo.COUNT () = 0;
      --
      FORALL INDX IN 1 .. s_vnd_promo.COUNT SAVE EXCEPTIONS
         UPDATE /*+ PARALLEL(DET 4 )*/ VND_VENDITE_DETAIL DET
            SET PRM_PROMOZIONE_COD_XK = s_vnd_promo(INDX).ID_PROMO_OFF,
                VND_SCONTO_PROMO = s_vnd_promo(INDX).DISCOUNT
          WHERE PDV_PUNTO_VENDITA_COD = s_vnd_promo(INDX).SID
                AND VND_NUMERO_CASSA = s_vnd_promo(INDX).TID
                AND VND_NUMERO_SCONTRINO = s_vnd_promo(INDX).XACTNBR
                AND VND_ITMIDX = s_vnd_promo(INDX).ITMIDX
                AND ART_ARTICOLO_COD = TO_NUMBER(s_vnd_promo(INDX).INTCODE)
                AND TRUNC (VND_DATA_VEND) = TO_DATE (s_vnd_promo(INDX).DT_DATE, 'YYYY-MM-DD')
                AND VND_ANNO = P_ANNO
                AND VND_MESE = P_MESE;
      --
      COMMIT;
    END LOOP;
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, ' Anno:'||P_ANNO||' Mese:'||P_MESE, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDa:'||vDataDa||' vDataA:'||vDataA ,vPasso);
    --
  EXCEPTION
    WHEN OTHERS THEN
    --
      IF SQLCODE = -24381 THEN
      --
        FOR INDX IN 1 .. SQL%BULK_EXCEPTIONS.COUNT
        LOOP
            -- Caputring errors occured during update
            DBMS_OUTPUT.PUT_LINE (
                  SQL%BULK_EXCEPTIONS (INDX).ERROR_INDEX
               || ': '
               || SQL%BULK_EXCEPTIONS (INDX).ERROR_CODE);
         --<You can inset the error records to a table here>
        END LOOP;
      END IF;
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END VND_DETT_IMPORT;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_ANNO anno di riferimento
   * Param: P_MESE mese di riferimento
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_TOTAL_SCNTR( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_ANNO        NUMBER,
    P_MESE        NUMBER
  )
    RETURN NUMBER
    PARALLEL_ENABLE
  IS
    vTotOut NUMBER;
  BEGIN
    --
    SELECT SUM(VNH_SCONTRINO_TOT)
      INTO vTotOut
      FROM VND_VENDITE_HEAD
     WHERE CLI_CLIENTE_COD = P_CLIENTE_COD
       AND VNH_ANNO = P_ANNO
       AND VNH_MESE = P_MESE;
    --
    RETURN vTotOut;
  END GET_TOTAL_SCNTR;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino nella finestra indicata
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_YYYYMM_DA anno mese da
   * Param: P_YYYYMM_A anno mese a
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_TOTAL_SCNTR_WINDOW( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_YYYYMM_DA   NUMBER,
    P_YYYYMM_A    NUMBER
  )
    RETURN NUMBER
    PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(30);
    vPasso              VARCHAR2(100);
    --
    vDateWindow_rec     DateWindowTyp;
    vDateWindow_tab     DateWindowTab;
    vTotOut             NUMBER :=0;
    vTotOutApp          NUMBER :=0;
    vLimit              NUMBER;
    vDataDa             DATE;
    vDataA              DATE;
  BEGIN
    --
    vProcName := 'GET_TOTAL_SCNTR_WINDOW';
    --
    vDataDa := TO_DATE (TO_CHAR(P_YYYYMM_DA), 'YYYYMM');
    vDataA := TO_DATE (TO_CHAR(P_YYYYMM_A), 'YYYYMM');
    --
    vPasso := 'Limit';
    vLimit := MONTHS_BETWEEN(vDataA, vDataDa) + 1;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vLimit:'||vLimit,vPasso);
    --
    vPasso := 'DateWindow';
    FOR rec IN (SELECT ADD_MONTHS (TO_DATE (TO_CHAR(P_YYYYMM_DA), 'YYYYMM'), LEVEL - 1) Data
                  FROM DUAL
               CONNECT BY LEVEL <= vLimit
               )
    LOOP
    --
      vDateWindow_rec.Anno := EXTRACT(YEAR FROM rec.Data);
      vDateWindow_rec.Mese := EXTRACT(MONTH FROM rec.Data);
      --
      vDateWindow_tab(vDateWindow_tab.COUNT) := vDateWindow_rec;
    END LOOP;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDateWindow_tab:'||vDateWindow_tab.COUNT,vPasso);
    --
    FOR i IN vDateWindow_tab.FIRST..vDateWindow_tab.LAST
    LOOP
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'i :'||i ,'RUN');
      vTotOutApp := 0;
      vPasso := 'GET_TOTAL_SCNTR';
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Anno:'||vDateWindow_tab(i).Anno,vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Mese:'||vDateWindow_tab(i).Mese,vPasso);
      vTotOutApp := GET_TOTAL_SCNTR(P_CLIENTE_COD => P_CLIENTE_COD,
                                    P_ANNO => vDateWindow_tab(i).Anno,
                                    P_MESE => vDateWindow_tab(i).Mese
                                    );
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTotOutApp:'||vTotOutApp,vPasso);
      vTotOut := NVL(vTotOut,0) + NVL(vTotOutApp,0);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTotOut:'||vTotOut,vPasso);
      --
    END LOOP;
    --
    RETURN vTotOut;
  END GET_TOTAL_SCNTR_WINDOW;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_ANNO anno di riferimento
   * Param: P_MESE mese di riferimento
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_PDV_TOTAL_SCNTR( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_ANNO        NUMBER,
    P_MESE        NUMBER
  )
    RETURN TAB_PDV_TOT
    PARALLEL_ENABLE
  IS
    vTabOut TAB_PDV_TOT;
  BEGIN
    --
    SELECT REC_PDV_TOT( PDV_PUNTO_VENDITA_COD, SUM(VNH_SCONTRINO_TOT))
      BULK COLLECT INTO vTabOut
      FROM VND_VENDITE_HEAD
     WHERE CLI_CLIENTE_COD = P_CLIENTE_COD
       AND VNH_ANNO = P_ANNO
       AND VNH_MESE = P_MESE
     GROUP BY PDV_PUNTO_VENDITA_COD;
    --
    RETURN vTabOut;
  exception
    when no_data_found then
      RETURN TAB_PDV_TOT(REC_PDV_TOT(NULL,NULL));
  END GET_PDV_TOTAL_SCNTR;
  --
  /**
   * Description: La funzione restituisce la somma dei totali scontrino nella finestra indicata
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_YYYYMM_DA anno mese da
   * Param: P_YYYYMM_A anno mese a
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_PDV_TOTAL_SCNTR_WINDOW( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_YYYYMM_DA   NUMBER,
    P_YYYYMM_A    NUMBER
  )
    RETURN TAB_PDV_TOT
    PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(30);
    vPasso              VARCHAR2(100);
    --
    vDateWindow_rec     DateWindowTyp;
    vDateWindow_tab     DateWindowTab;
    vTotOut             NUMBER :=0;
    vTotOutApp          NUMBER :=0;
    vLimit              NUMBER;
    vDataDa             DATE;
    vDataA              DATE;
    vTabPdvTOT          TAB_PDV_TOT := TAB_PDV_TOT();
    vTabPdvAPP          TAB_PDV_TOT := TAB_PDV_TOT();
    vTabPdvRET          TAB_PDV_TOT := TAB_PDV_TOT();
  BEGIN
    --
    vProcName := 'GET_TOTAL_SCNTR_WINDOW';
    --
    vDataDa := TO_DATE (TO_CHAR(P_YYYYMM_DA), 'YYYYMM');
    vDataA := TO_DATE (TO_CHAR(P_YYYYMM_A), 'YYYYMM');
    --
    vPasso := 'Limit';
    vLimit := MONTHS_BETWEEN(vDataA, vDataDa) + 1;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vLimit:'||vLimit,vPasso);
    --
    vPasso := 'DateWindow';
    FOR rec IN (SELECT ADD_MONTHS (TO_DATE (TO_CHAR(P_YYYYMM_DA), 'YYYYMM'), LEVEL - 1) Data
                  FROM DUAL
               CONNECT BY LEVEL <= vLimit
               )
    LOOP
    --
      vDateWindow_rec.Anno := EXTRACT(YEAR FROM rec.Data);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDateWindow_rec.Anno:'||vDateWindow_rec.Anno,vPasso);
      vDateWindow_rec.Mese := EXTRACT(MONTH FROM rec.Data);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDateWindow_rec.Mese:'||vDateWindow_rec.Mese,vPasso);
      --
      vDateWindow_tab(vDateWindow_tab.COUNT) := vDateWindow_rec;
    END LOOP;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDateWindow_tab:'||vDateWindow_tab.COUNT,vPasso);
    --
    FOR i IN vDateWindow_tab.FIRST..vDateWindow_tab.LAST
    LOOP
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'i :'||i ,'RUN');
      vTotOutApp := 0;
      vPasso := 'GET_TOTAL_SCNTR';
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Anno:'||vDateWindow_tab(i).Anno,vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Mese:'||vDateWindow_tab(i).Mese,vPasso);
      vTabPdvAPP := GET_PDV_TOTAL_SCNTR(P_CLIENTE_COD => P_CLIENTE_COD,
                                        P_ANNO => vDateWindow_tab(i).Anno,
                                        P_MESE => vDateWindow_tab(i).Mese
                                       );
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabPdvTOT:'||vTabPdvTOT.COUNT,vPasso);
      --
      IF vTabPdvAPP.COUNT > 0 THEN
      FOR i IN vTabPdvAPP.FIRST..vTabPdvAPP.LAST
        LOOP
          vTabPdvTOT.EXTEND(1);
          vTabPdvTOT(vTabPdvTOT.COUNT) := vTabPdvAPP(i);
        END LOOP;
      END IF;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabPdvTOT:'||vTabPdvAPP.COUNT,vPasso);
      --
    END LOOP;
    --
    SELECT REC_PDV_TOT( tab.PDV_PUNTO_VENDITA_COD, SUM(tab.TOTAL))
      BULK COLLECT INTO vTabPdvRET
      FROM TABLE (vTabPdvTOT) tab
     GROUP BY tab.PDV_PUNTO_VENDITA_COD;
  --
    RETURN vTabPdvRET;
  --
  END GET_PDV_TOTAL_SCNTR_WINDOW;
  /**
   * Description: La funzione restituisce la somma dei totali scontrino per il numero di giorni inidicati
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_DAY_NUM numero di giorni
   * Param: P_TIPO  F=Fatturato-T=Transazioni
   * Return: somma dei totali scontrino
   */
  FUNCTION GET_TOTAL_BY_DAY_NUM( 
    P_CLIENTE_COD   ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_DECORRENZA    TIMESTAMP,
    P_DAY_NUM       NUMBER,
    P_TIPO          VARCHAR  --F=Fatturato-T=Transazioni
  ) RETURN NUMBER PARALLEL_ENABLE
  IS
    vTotFatt  NUMBER;
  BEGIN
    --
    SELECT CASE 
             WHEN P_TIPO= 'F' THEN SUM(VND.VNH_SCONTRINO_TOT)
             WHEN P_TIPO= 'T' THEN COUNT(0)
           ELSE 0
           END
      INTO vTotFatt
      FROM VND_VENDITE_HEAD VND, ANA_CARTE CRT
     WHERE CRT.CLI_CLIENTE_COD = P_CLIENTE_COD
       AND VND.CRT_CARTA_COD = CRT.CRT_CARTA_COD   
       AND TRUNC(VND.VNH_DATA_VEND) BETWEEN TRUNC(P_DECORRENZA) - P_DAY_NUM AND kStampOggi;
    --
    RETURN vTotFatt;
  exception
    when no_data_found then
      RETURN 0;
  END GET_TOTAL_BY_DAY_NUM;
  --
  /**
   * Description: La funzione restituisce S/N se il cliente è outlier
   *
   * Author: R.Doti
   * Created: 25/11/2019
   *
   * Param: P_CLIENTE_COD Codice cliente
   * Param: P_FATTURATO fatturato del cliente del periodo
   * Param: P_FATTURATO_CTRL fatturato di controllo
   * Return: S/N
   */
  FUNCTION GET_OUTLIER( 
    P_CLIENTE_COD ANA_CLIENTI.CLI_CLIENTE_COD%TYPE,
    P_FATTURATO       NUMBER,
    P_FATTURATO_CTRL  NUMBER
  ) RETURN VARCHAR PARALLEL_ENABLE
  IS
    vOutlier        VARCHAR2(5);
    vFattCtrl       NUMBER;
  BEGIN
    --
    IF P_FATTURATO_CTRL IS NULL OR P_FATTURATO_CTRL = 0 THEN
    --
      vFattCtrl := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_FATT_OTL');
    ELSE 
      vFattCtrl  := P_FATTURATO_CTRL;
    END IF;
    --
    IF P_FATTURATO >= vFattCtrl THEN
      vOutlier := 'S';
    ELSE
      vOutlier := 'N';
    END IF;
    --
    RETURN vOutlier;
  exception
    when no_data_found then
      RETURN 'N';
  END GET_OUTLIER;
  --
  /**
   * Description: La funzione determina se si tratta di un nuovo cliente. Verifica se la data minima di transazione 
   *              è maggiore di OGGI - P_DAY_NEW.
   *
   * Author: R.Doti
   * Created: 26/08/2021
   *
   * Param: P_DATA_MIN_TRANS  Data minima transazione
   * Param: P_DAY_NEW Numero di giorni
   * Return: S/N
   */
 FUNCTION GET_ISNEW ( 
    P_DATA_MIN_TRANS     ANA_CLIENTI.CLI_DATA_MIN_TRANS%TYPE,
    P_DAY_NEW            NUMBER
  ) RETURN VARCHAR PARALLEL_ENABLE
  IS
    vOutIsNew        VARCHAR2(5);
    vParDayNum       NUMBER;
  BEGIN
    --
    IF P_DAY_NEW IS NULL OR P_DAY_NEW = 0 THEN
    --
      vParDayNum := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_DAY_NEW');
    ELSE 
      vParDayNum  := P_DAY_NEW;
    END IF;
    --
    IF NVL(P_DATA_MIN_TRANS, kStampOggi - vParDayNum) >  kStampOggi - vParDayNum THEN
      vOutIsNew := 'S';
    ELSE
      vOutIsNew := 'N';
    END IF;
    --
    RETURN vOutIsNew;
  exception
    when no_data_found then
      RETURN 'S';
  END GET_ISNEW;
  /**
   * Description: La funzione determina se si tratta di un cliente perso. Verifica se la data massima di transazione 
   *              è minore di OGGI - P_DAY_LOST.
   *
   * Author: R.Doti
   * Created: 26/08/2021
   *
   * Param: P_DATA_MAX_TRANS  Data massima transazione
   * Param: P_DAY_LOST Numero di giorni
   * Return: S/N
   */
 FUNCTION GET_ISLOST ( 
    P_DATA_MAX_TRANS     ANA_CLIENTI.CLI_DATA_MAX_TRANS%TYPE,
    P_DAY_LOST           NUMBER
  ) RETURN VARCHAR PARALLEL_ENABLE
  IS
    vOutIsLost       VARCHAR2(5);
    vParDayNum       NUMBER;
  BEGIN
    --
    IF P_DAY_LOST IS NULL OR P_DAY_LOST = 0 THEN
    --
      vParDayNum := GET_VAL_PARAMETRO_N(P_PARAMETRO_COD=> 'P_DAY_LOST');
    ELSE 
      vParDayNum  := P_DAY_LOST;
    END IF;
    --
    IF NVL(P_DATA_MAX_TRANS, kStampOggi) <  kStampOggi - vParDayNum THEN
      vOutIsLost := 'S';
    ELSE
      vOutIsLost := 'N';
    END IF;
    --
    RETURN vOutIsLost;
  exception
    when no_data_found then
      RETURN NULL;
  END GET_ISLOST;
  --
   /**
  * PRE_UPDT_ANA_CLI(): procedura da richimare prima dell'aggiornamento dei dati del clinete ANA_CLIENTI.
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE PRE_UPDT_ANA_CLI(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_DECORRENZA          IN     TIMESTAMP,
      PO_DML_NEW            IN OUT NOCOPY ANA_CLIENTI%ROWTYPE
  )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    v_CurrRecSTRC       MG.ANA_STORICO_CLIENTI%ROWTYPE;
    v_NewRecSTRC        MG.ANA_STORICO_CLIENTI%ROWTYPE;
    vIsNewRec           BOOLEAN := FALSE;
    vIsAddressMod       BOOLEAN := FALSE;
    vIsCelMod           BOOLEAN := FALSE;
    vIsEmailMod         BOOLEAN := FALSE;
    vIsPrvAutMod        BOOLEAN := FALSE;
    vIsPrvMod           BOOLEAN := FALSE;
    vIsPrvMktActMod     BOOLEAN := FALSE;
    vIsPrvMktProfMod    BOOLEAN := FALSE;
  BEGIN
    --
    vProcName := 'PRE_UPDT_ANA_CLI';
    --
    vPasso := 'GetCLI';
    
    BEGIN
    --
      SELECT CLI.*
        INTO v_CurrRecSTRC
        FROM ANA_STORICO_CLIENTI CLI
       WHERE CLI_CLIENTE_COD = PO_DML_NEW.CLI_CLIENTE_COD
         AND STRC_DATA_MODIFICA = (SELECT MAX(STRC_DATA_MODIFICA)
                                     FROM ANA_STORICO_CLIENTI
                                     WHERE CLI_CLIENTE_COD = PO_DML_NEW.CLI_CLIENTE_COD );
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN
      --
        vPasso := 'NuovoCliente';
        vIsNewRec := TRUE;
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '-->',vPasso);
    END;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'v_CurrRecSTRC.CLI_CLIENTE_COD:'||v_CurrRecSTRC.CLI_CLIENTE_COD, vPasso);
    --
    v_NewRecSTRC.STRC_STORICO_CLIENTE_ID      := MGDWH.BASE_UTIL_PCK.MG_GUID();
    v_NewRecSTRC.CLI_CLIENTE_COD              := PO_DML_NEW.CLI_CLIENTE_COD;
    --
    v_NewRecSTRC.CLI_INDIRIZZO  := PO_DML_NEW.CLI_INDIRIZZO;
    v_NewRecSTRC.CLI_NUMERO     := PO_DML_NEW.CLI_NUMERO;
    v_NewRecSTRC.CLI_PROVINCIA  := PO_DML_NEW.CLI_PROVINCIA;
    v_NewRecSTRC.CLI_CITTA      := PO_DML_NEW.CLI_CITTA;
    v_NewRecSTRC.CLI_CAP        := PO_DML_NEW.CLI_CAP;
    --
    vPasso := 'Indirizzo';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'v_CurrRecSTRC.CLI_INDIRIZZO:'||NVL(v_CurrRecSTRC.CLI_INDIRIZZO,'KO'), vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_DML_NEW.CLI_INDIRIZZO:'||NVL(PO_DML_NEW.CLI_INDIRIZZO,'KO') , vPasso);
    IF NVL(v_CurrRecSTRC.CLI_INDIRIZZO,'KO') != NVL(PO_DML_NEW.CLI_INDIRIZZO,'KO') THEN         
      vIsAddressMod := TRUE;
    END IF;
    --
    IF NVL(v_CurrRecSTRC.CLI_NUMERO,'KO') != NVL(PO_DML_NEW.CLI_NUMERO,'KO') THEN        
      vIsAddressMod := TRUE;
    END IF;
    --
    IF NVL(v_CurrRecSTRC.CLI_PROVINCIA,'KO') != NVL(PO_DML_NEW.CLI_PROVINCIA,'KO') THEN
      vIsAddressMod := TRUE;
    END IF;
    --
    IF NVL(v_CurrRecSTRC.CLI_CITTA,'KO') != NVL(PO_DML_NEW.CLI_CITTA,'KO') THEN
      vIsAddressMod := TRUE;
    END IF;
    --
    IF NVL(v_CurrRecSTRC.CLI_CAP,'KO') != NVL(PO_DML_NEW.CLI_CAP,'KO') THEN
      vIsAddressMod := TRUE;
    END IF;
    --
    vPasso := 'vIsAddressMod';
    --
    IF vIsAddressMod THEN 
      v_NewRecSTRC.STRC_INDIRIZZO_DATAMOD     := P_DECORRENZA; 
    END IF;
    --
    --
    vPasso := 'vIsCelMod';
    v_NewRecSTRC.CLI_CELLULARE     := PO_DML_NEW.CLI_CELLULARE;
    --
    IF NVL(v_CurrRecSTRC.CLI_CELLULARE,'KO') != NVL(PO_DML_NEW.CLI_CELLULARE,'KO') THEN
      v_NewRecSTRC.STRC_CELLULARE_DATAMOD    := P_DECORRENZA; 
      vIsCelMod := TRUE;
    END IF;
    --
    vPasso := 'vIsEmailMod';
    --
    v_NewRecSTRC.CLI_EMAIL     := PO_DML_NEW.CLI_EMAIL;
    IF NVL(v_CurrRecSTRC.CLI_EMAIL,'KO') != NVL(PO_DML_NEW.CLI_EMAIL,'KO') THEN
      v_NewRecSTRC.STRC_EMAIL_DATAMOD    := P_DECORRENZA; 
      vIsEmailMod := TRUE;
    END IF;  
    --
    vPasso := 'vIsPrvAutMod';
    v_NewRecSTRC.CLI_PRIVACY_AUTH     := PO_DML_NEW.CLI_PRIVACY_AUTH;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'v_CurrRecSTRC.CLI_PRIVACY_AUTH:'||NVL(v_CurrRecSTRC.CLI_PRIVACY_AUTH,'KO'), vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_DML_NEW.CLI_PRIVACY_AUTH:'||NVL(PO_DML_NEW.CLI_PRIVACY_AUTH,'KO') , vPasso);  
    --
    IF NVL(v_CurrRecSTRC.CLI_PRIVACY_AUTH,'KO') != NVL(PO_DML_NEW.CLI_PRIVACY_AUTH,'KO') THEN
     
      v_NewRecSTRC.STRC_PRIVACY_AUTH_DATAMOD    := P_DECORRENZA; 
      vIsPrvAutMod := TRUE;
    END IF;
    --
    v_NewRecSTRC.CLI_PRIVACY_MKTG_ACTIVITY     := PO_DML_NEW.CLI_PRIVACY_MKTG_ACTIVITY;
    vPasso := 'vIsPrvMktActMod';
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'v_CurrRecSTRC.CLI_PRIVACY_MKTG_ACTIVITY:'||NVL(v_CurrRecSTRC.CLI_PRIVACY_MKTG_ACTIVITY,'KO'), vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_DML_NEW.CLI_PRIVACY_MKTG_ACTIVITY:'||NVL(PO_DML_NEW.CLI_PRIVACY_MKTG_ACTIVITY,'KO') , vPasso);     
    IF NVL(v_CurrRecSTRC.CLI_PRIVACY_MKTG_ACTIVITY,'KO') != NVL(PO_DML_NEW.CLI_PRIVACY_MKTG_ACTIVITY,'KO') THEN
      
      v_NewRecSTRC.STRC_PRVCY_MKTG_ACT_DATAMOD    := P_DECORRENZA; 
      vIsPrvMktActMod := TRUE;
    END IF;
    --
    v_NewRecSTRC.CLI_PRIVACY_MKTG_PROFILING     := PO_DML_NEW.CLI_PRIVACY_MKTG_PROFILING;
    vPasso := 'vIsPrvMktProfMod';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'v_CurrRecSTRC.CLI_PRIVACY_MKTG_PROFILING:'||NVL(v_CurrRecSTRC.CLI_PRIVACY_MKTG_PROFILING,'KO'), vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_DML_NEW.CLI_PRIVACY_MKTG_PROFILING:'||NVL(PO_DML_NEW.CLI_PRIVACY_MKTG_PROFILING,'KO') , vPasso);      
    IF NVL(v_CurrRecSTRC.CLI_PRIVACY_MKTG_PROFILING,'KO') != NVL(PO_DML_NEW.CLI_PRIVACY_MKTG_PROFILING,'KO') THEN
      v_NewRecSTRC.STRC_PRVCY_MKTG_PROF_DATAMOD    := P_DECORRENZA; 
      vIsPrvMktProfMod := TRUE;
    END IF;
    --
    IF vIsAddressMod    OR  
       vIsCelMod        OR
       vIsEmailMod      OR
       vIsPrvAutMod     OR
       vIsPrvMod        OR
       vIsPrvMktActMod  OR
       vIsPrvMktProfMod 
    THEN
    --
      SELECT MAX(DATA_MOD)
        INTO v_NewRecSTRC.STRC_DATA_MODIFICA
        FROM(SELECT v_NewRecSTRC.STRC_INDIRIZZO_DATAMOD DATA_MOD
               FROM DUAL
              UNION
             SELECT v_NewRecSTRC.STRC_CELLULARE_DATAMOD
               FROM DUAL
              UNION
             SELECT v_NewRecSTRC.STRC_EMAIL_DATAMOD
               FROM DUAL
              UNION
             SELECT v_NewRecSTRC.STRC_PRIVACY_AUTH_DATAMOD
               FROM DUAL
              UNION
             SELECT v_NewRecSTRC.STRC_PRVCY_MKTG_ACT_DATAMOD
               FROM DUAL
              UNION
             SELECT v_NewRecSTRC.STRC_PRVCY_MKTG_PROF_DATAMOD
               FROM DUAL);
      --
      vPasso := 'INS_STORICO_CLI';
      INSERT INTO ANA_STORICO_CLIENTI VALUES v_NewRecSTRC;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'v_NewId:'||v_NewRecSTRC.STRC_STORICO_CLIENTE_ID, vPasso);
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END PRE_UPDT_ANA_CLI;
  --
  /*La funzione determina il PDV su cui il cliente ha speso maggiormente nella finestra indicata*/
 FUNCTION GET_STATO_ORD ( 
    P_STATO VARCHAR2
  ) RETURN NUMBER PARALLEL_ENABLE
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_UTILITY_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vStatoORD           ORD_STATI_ARTICOLI.STA_STATO_ARTICOLO_COD%TYPE;
  BEGIN
    vProcName := 'GET_PDV_VND_BY_SPESA';
    vPasso := 'INPUT';
    --
    SELECT STA_STATO_ARTICOLO_COD
      INTO vStatoORD
      FROM ORD_STATI_ARTICOLI
     WHERE STA_STATO_ARTICOLO_COD = TO_NUMBER(P_STATO);
    --
    RETURN vStatoORD;
  EXCEPTION
    WHEN OTHERS THEN 
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_STATO:'||P_STATO, vPasso);
      RETURN NULL;
  END GET_STATO_ORD;
  --
END MGDWH_UTILITY_PCK;
/