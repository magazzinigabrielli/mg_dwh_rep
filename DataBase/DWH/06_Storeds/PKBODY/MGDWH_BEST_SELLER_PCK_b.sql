create or replace PACKAGE BODY     MGDWH_BEST_SELLER AS 

  /* PACKAGE PER IL CALCOLO DEI BEST SELLER PER OGNI NEGOZIO*/ 
  -- COSTANTI A LIVELLO PACKAGE
  kNomePackage       CONSTANT VARCHAR2(30) := 'MGDW_BEST_SELLER';
  --
  PROCEDURE MAIN
  AS
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100); 
    PO_VAL              VARCHAR2(100);
  BEGIN
    --
    vProcName := 'MAIN';
    vPasso :='SERVICE_ON';
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'START', vPasso);
    --
     FOR REC
      IN (  SELECT DISTINCT HOST_ID
              FROM MG_SHOP@KPROMO A
             WHERE     FISC_CODE = '00103300448'
                   AND (DT_END IS NULL OR DT_END > SYSDATE)
          ORDER BY 1)
   LOOP
    --
    vPasso :='LOOP PDV';
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'LOOPING SID', vPasso);
     FOR rec_max_ven IN (WITH SUBQUERY_A
                            AS (SELECT /*PARALLEL_INDEX(TL_ITEMS_NOR,  I_TL_ITEMS_NOR_PK, 12, 2) */ SID,
                                      SUBSTR (DEPT,
                                              DECODE (LENGTH (DEPT), 8, 2, 3),
                                              DECODE (LENGTH (DEPT), 8, 4, 4))
                                         UG,
                                      INTCODE,
                                      ITEMDESCR,
                                      COUNT (0) CNT
                                 FROM TL_ITEMS_NOR@CRSMG
                                WHERE SID = rec.HOST_ID
                                  AND ITM_DISC=0
                                  AND TO_DATE (DT_DATE, 'YYYY-MM-DD') BETWEEN SYSDATE - 7
                                                                              AND SYSDATE
                             GROUP BY SID,
                                      SUBSTR (DEPT,
                                              DECODE (LENGTH (DEPT), 8, 2, 3),
                                              DECODE (LENGTH (DEPT), 8, 4, 4)),
                                      INTCODE,
                                      ITEMDESCR
                             ORDER BY SID,
                                      SUBSTR (DEPT,
                                              DECODE (LENGTH (DEPT), 8, 2, 3),
                                              DECODE (LENGTH (DEPT), 8, 4, 4)),
                                      COUNT (0) DESC),
                                SUBQUERY_B
                                 AS (SELECT SID,
                                            UG,
                                            INTCODE,
                                            CNT,
                                            ITEMDESCR,
                                            MAX (CNT) OVER (PARTITION BY SID, UG) MAX_UG
                                       FROM SUBQUERY_A)
                        SELECT *
                          FROM SUBQUERY_B
                         WHERE SUBQUERY_B.CNT = SUBQUERY_B.MAX_UG)
     LOOP
        --stringa da inserire nella tabella
        --DBMS_OUTPUT.PUT_LINE('Artcolo:'|| rec_max_ven.INTCODE||' descr: '||rec_max_ven.itemdescr|| ' UG:'|| rec_max_ven.UG||' qta:'||rec_max_ven.MAX_UG|| 'SID: '||rec_max_ven.sid);
        INSERT INTO MGDW_BEST_SELLER (BSL_PDV_COD                           
                                     ,BSL_UNITA_GESTIONE                            
                                     ,BSL_ARTICOLO_COD                        
                                     ,BSL_ARTICOLO_DESCR                     
                                     ,BSL_QUANTITA                      
                                     ,DATA_IMPORT
                                     )
        VALUES                       ( rec_max_ven.sid,
                                       rec_max_ven.UG,
                                       rec_max_ven.INTCODE,
                                       rec_max_ven.itemdescr,
                                       rec_max_ven.MAX_UG,
                                       SYSTIMESTAMP);
                                                                
     END LOOP;
   --
   END LOOP;
       COMMIT; 
    vPasso :='SERVICE OFF';
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'FINISHED', vPasso);
  END MAIN;
--PROCEDURA CHE SI OCCUPA DI ESPORTARE I DATI DAL DWH VERSO TABELLA DI KPROMO
PROCEDURE EXPORT_BEST_SELLER
(
dt_import timestamp
)
AS 
BEGIN 
--MERGE INTO MG.TOP_ARTICOLI_OUTPUT
    MERGE INTO MG.TOP_ARTICOLI_OUTPUT@KPROMO  USING(SELECT * FROM MGDW_BEST_SELLER where data_import >=To_char(dt_import,'DDMONYY')
) E/*+ PARALLEL(MG.TOP_ARTICOLI_OUTPUT@KPROMO 12) PARALLEL(E 12)*/
    --CHIAVE
    ON (    MG.TOP_ARTICOLI_OUTPUT.SID                     = E.BSL_PDV_COD
        AND MG.TOP_ARTICOLI_OUTPUT.DEPT                    = E.BSL_UNITA_GESTIONE
        AND MG.TOP_ARTICOLI_OUTPUT.INTCODE                 = E.BSL_ARTICOLO_COD
        AND MG.TOP_ARTICOLI_OUTPUT.DT_RUN                  = E.DATA_IMPORT
        )
    --SE IL RECORD ESISTE ALLORA AGGIORNO
    WHEN MATCHED THEN
    UPDATE SET MG.TOP_ARTICOLI_OUTPUT.QTY                  = E.BSL_QUANTITA
    WHERE  MG.TOP_ARTICOLI_OUTPUT.SID                      = E.BSL_PDV_COD
        AND MG.TOP_ARTICOLI_OUTPUT.DEPT                    = E.BSL_UNITA_GESTIONE
        AND MG.TOP_ARTICOLI_OUTPUT.INTCODE                 = E.BSL_ARTICOLO_COD
        AND MG.TOP_ARTICOLI_OUTPUT.DT_RUN                  = E.DATA_IMPORT 
    --SE NON ESISTE INSERISCO
    WHEN NOT MATCHED THEN
    INSERT ( SID,
             DEPT,
             INTCODE,
             ITEMDESCR,
             QTY,
             EXEC_TIMESTAMP,
             DT_RUN
             )
    VALUES (E.BSL_PDV_COD,
            E.BSL_UNITA_GESTIONE,
            E.BSL_ARTICOLO_COD,
            E.BSL_ARTICOLO_DESCR,
            E.BSL_QUANTITA,
            E.DATA_IMPORT,
            E.DATA_IMPORT
            );
--A QUESTO PUNTO ESEGUO UN COMMIT 
COMMIT;
END EXPORT_BEST_SELLER;
--
END  MGDWH_BEST_SELLER;