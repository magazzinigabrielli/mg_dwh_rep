CREATE OR REPLACE PACKAGE BODY MGDWH."MGDWH_ELABORAZIONI_PCK" AS
  --
  /******************************************************************************
   NAME:       MGDWH_ELABORAZIONI_PCK
   PURPOSE:    Package per la gestione delle elaborazioni delle entitï¿½ sul DWH

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        30/05/2019           r.doti   1. Created this package.
   ******************************************************************************/
  --
  kFlagAnn           CONSTANT VARCHAR2(1)  := 'A';
  kFormatMask        CONSTANT VARCHAR2(8)  := 'YYYYMMDD';
  kFormatMask2       CONSTANT VARCHAR2(10) := 'YYYY-MM-DD';
  kStatoElabNEW      CONSTANT VARCHAR2(5)  := 'NEW';
  kStatoElabELAB     CONSTANT VARCHAR2(5)  := 'ELAB';
  kStatoElabEND      CONSTANT VARCHAR2(5)  := 'END';
  kStatoElabERR      CONSTANT VARCHAR2(5)  := 'ERR';
  --
  kPolicyDAILY       CONSTANT VARCHAR2(10)  := 'DAILY';
  kPolicyHOURLY      CONSTANT VARCHAR2(10)  := 'HOURLY';
  kPolicyWEEKLY      CONSTANT VARCHAR2(10)  := 'WEEKLY';
  kPolicyMONTHLY     CONSTANT VARCHAR2(10)  := 'MONTHLY';
  kPolicyYEARLY      CONSTANT VARCHAR2(10)  := 'YEARLY';
  --
  kClienteFittizio   CONSTANT VARCHAR2(25)  := '-1';
  kStoreVirtual      CONSTANT NUMBER  := 99001;
  --
  kStampIni          CONSTANT TIMESTAMP  := TO_TIMESTAMP('01010001','DDMMYYYY');
  kStampIeri         CONSTANT TIMESTAMP  := TRUNC(SYSDATE) -1;
  kStampStartDWH     CONSTANT TIMESTAMP  := TO_TIMESTAMP('01092019','DDMMYYYY');
  kStampOggi         CONSTANT TIMESTAMP  := TRUNC(SYSDATE);
  --
  CURSOR cPdvVendHead(
    pJobTot         NUMBER,
    pJobNum         NUMBER,
    vFiltriPDVCNT   NUMBER,
    vFiltriPDVTAB   TAB_COD,
    vFiltroDataDa   TIMESTAMP
    )
  IS
    SELECT APD.PDV_PUNTO_VENDITA_COD,
           MOD (APD.PDV_PUNTO_VENDITA_COD, pJobTot),
           TRUNC (APD_DATA) DATA_DEC,
           APD_APERTO,
           APD_STAT,
           JOB_ID_VNH,
           DATA_IMPORT_VNH,
           APD_ERROR_MSG
      FROM MDWH_APERTURE_REL_PDV APD
     WHERE APD_APERTO != 'N'
       AND APD_STAT IS NULL
       AND DATA_IMPORT_VNH IS NULL
       AND TRUNC (APD_DATA) < TRUNC (SYSDATE)
       AND (vFiltroDataDa IS NULL
            OR
           TRUNC (APD_DATA) >= vFiltroDataDa
           )
       AND JOB_ID_VNH IS NULL
       AND EXISTS(SELECT 0
                    FROM ANA_PUNTI_VENDITA PDV
                   WHERE PDV_STAT IS NULL
                     AND PDV_DATA_APERTURA <= kStampIeri
                     AND NVL (PDV_DATA_CHIUSURA, kStampOggi) >= kStampIeri
                     AND PDV_PUNTO_VENDITA_COD <> kStoreVirtual --Store Virtuale
                     AND PDV.PDV_PUNTO_VENDITA_COD = APD.PDV_PUNTO_VENDITA_COD)
                     AND MOD (APD.PDV_PUNTO_VENDITA_COD, pJobTot) =
                               DECODE (
                                  MOD (APD.PDV_PUNTO_VENDITA_COD, pJobTot),
                                  APD.PDV_PUNTO_VENDITA_COD, APD.PDV_PUNTO_VENDITA_COD,
                                  pJobNum)
                     AND (NVL(vFiltriPDVCNT,0) = 0
                          OR EXISTS
                          (SELECT 1
                             FROM TABLE (vFiltriPDVTAB) FILTRI_PDV
                            WHERE FILTRI_PDV.COLUMN_VALUE = APD.PDV_PUNTO_VENDITA_COD))
               ORDER BY PDV_PUNTO_VENDITA_COD, APD_DATA;
  --
  CURSOR cPdvVendDett(
    pJobTot         NUMBER,
    pJobNum         NUMBER,
    vFiltriPDVCNT   NUMBER,
    vFiltriPDVTAB   TAB_COD,
    vFiltroDataDa   TIMESTAMP
    )
  IS
    SELECT APD.PDV_PUNTO_VENDITA_COD,
           MOD (APD.PDV_PUNTO_VENDITA_COD, pJobTot),
           TRUNC (APD_DATA) DATA_DEC,
           APD_APERTO,
           APD_STAT,
           JOB_ID_VND,
           DATA_IMPORT_VND,
           APD_ERROR_MSG
      FROM MDWH_APERTURE_REL_PDV APD
     WHERE APD_APERTO != 'N'
       AND APD_STAT IS NULL
       AND DATA_IMPORT_VND IS NULL
       AND TRUNC (APD_DATA) < TRUNC (SYSDATE)
       AND (vFiltroDataDa IS NULL
            OR
           TRUNC (APD_DATA) >= vFiltroDataDa
           )
       AND JOB_ID_VND IS NULL
       AND EXISTS(SELECT 0
                    FROM ANA_PUNTI_VENDITA PDV
                   WHERE PDV_STAT IS NULL
                     AND PDV_DATA_APERTURA <= kStampIeri
                     AND NVL (PDV_DATA_CHIUSURA, kStampOggi) >= kStampIeri
                     AND PDV_PUNTO_VENDITA_COD <> kStoreVirtual --Store Virtuale
                     AND PDV.PDV_PUNTO_VENDITA_COD = APD.PDV_PUNTO_VENDITA_COD)
                     AND MOD (APD.PDV_PUNTO_VENDITA_COD, pJobTot) =
                               DECODE (
                                  MOD (APD.PDV_PUNTO_VENDITA_COD, pJobTot),
                                  APD.PDV_PUNTO_VENDITA_COD, APD.PDV_PUNTO_VENDITA_COD,
                                  pJobNum)
                     AND (NVL(vFiltriPDVCNT,0) = 0
                          OR EXISTS
                          (SELECT 1
                             FROM TABLE (vFiltriPDVTAB) FILTRI_PDV
                            WHERE FILTRI_PDV.COLUMN_VALUE = APD.PDV_PUNTO_VENDITA_COD))
               ORDER BY PDV_PUNTO_VENDITA_COD, APD_DATA;
  --
  ErroreGestito EXCEPTION;
  --
  FUNCTION CHECK_JOB_END
    RETURN BOOLEAN
  IS
    vVerificaJOBS   BOOLEAN;
    vContaJOBS      NUMBER;
    kNomePackage    CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100) :='CHECK_JOB_RUNNING';
  BEGIN
   -- La vista di sistema user_scheduler_running_jobs
   -- mostra i job in esecuzione dello user oracle corrente
   SELECT COUNT(1)
     INTO vContaJOBS
     FROM USER_SCHEDULER_JOBS
    WHERE JOB_NAME LIKE 'MGDWH_LOAD%'
      AND STATE IN ('SCHEDULED','RUNNING');
      --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' vContaJOBS: '||vContaJOBS);
    --
    IF vContaJOBS > 0 THEN
       RETURN FALSE;
      ELSE
        RETURN TRUE;
    END IF;
  --
  END CHECK_JOB_END;
  --
  FUNCTION CHECK_POLICY_ENT(
    P_ENTITA_COD          IN     MDWH_ELABORAZIONI.ENT_ENTITA_COD%TYPE,
    P_PUNTO_VENDITA_COD   IN     MDWH_ELABORAZIONI.PDV_PUNTO_VENDITA_COD%TYPE DEFAULT NULL,
    P_DATA_DEC            IN     MDWH_ELABORAZIONI.ELB_DATA_DEC%TYPE DEFAULT NULL
  )
    RETURN BOOLEAN
  IS
    kNomePackage    CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100) :='CHECK_POLICY_ENT';
    vPasso          VARCHAR2(100);
    vVerifica       BOOLEAN := FALSE;
    vTipoFreq       MDWH_POLICY_ACTION.PLC_TIPO_FREQUENZA%TYPE;
    vFreq           MDWH_POLICY_ACTION.PLC_FREQUENZA%TYPE;
    vDayNum         NUMBER := NULL;
  BEGIN
    vPasso := 'GET_POLICY';
    --
    SELECT PLC_TIPO_FREQUENZA, PLC_FREQUENZA
      INTO vTipoFreq, vFreq
      FROM MDWH_ENTITA ENT,
           MDWH_POLICY_ACTION ACT
     WHERE ENT.ENT_ENTITA_COD = P_ENTITA_COD
       AND ENT.PLC_POLICY_COD_IMPORT = ACT.PLC_POLICY_ACTION_COD;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ENT_ENTITA_COD:'||P_ENTITA_COD , vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PLC_TIPO_FREQUENZA:'||vTipoFreq , vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PLC_FREQUENZA:'||vFreq, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_PUNTO_VENDITA_COD:'||P_PUNTO_VENDITA_COD, vPasso);
    --
    IF vTipoFreq = kPolicyDAILY THEN
    --
      IF P_PUNTO_VENDITA_COD IS NULL THEN
        vPasso := 'GET_ELAPSED_DAY';
        --SELECT EXTRACT(DAY FROM(SYSDATE)) - EXTRACT(DAY FROM(MAX(ENT_LAST_IMPORT)))
        SELECT TO_CHAR(SYSDATE, 'J') - TO_CHAR(MAX(ENT_LAST_IMPORT),'J')
          INTO vDayNum
          FROM MDWH_ENTITA ENT
         WHERE ENT.ENT_ENTITA_COD = P_ENTITA_COD
           AND ENT.ENT_STAT IS NULL;
       ELSE
       --
        NULL;
         --vPasso := 'GET_ELAPSED_DAY';
         --SELECT EXTRACT(DAY FROM(SYSDATE)) - EXTRACT(DAY FROM(MAX(ELB_DATA_DEC)))
         --  INTO vDayNum
         --  FROM MDWH_ELABORAZIONI ELB
         -- WHERE ELB.ENT_ENTITA_COD = P_ENTITA_COD
         --   AND PDV_PUNTO_VENDITA_COD = P_PUNTO_VENDITA_COD
         --   AND ELB.ELB_STATO = kStatoEND;
       END IF;
       --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vDayNum:'||vDayNum, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vFreq:'||vFreq, vPasso);
      --
      IF vDayNum >= vFreq OR vDayNum IS NULL THEN
        vVerifica := TRUE;
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vVerifica TRUE', vPasso);
      ELSE
        vVerifica := FALSE;
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vVerifica FALSE', vPasso);
      END IF;
    END IF;
  --

  RETURN vVerifica;
  --
  END CHECK_POLICY_ENT;
  --
  /**
  * CREA_ELAB(): procedura che creare una elaborazione
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE CREA_ELAB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE,
      P_ENTITA_COD          IN     MDWH_ELABORAZIONI.ENT_ENTITA_COD%TYPE,
      P_JOB_ID              IN     MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_ID_SESSIONE         IN     MDWH_ELABORAZIONI.LOG_ID_SESSIONE%TYPE,
      PO_CREATA                OUT BOOLEAN,
      P_PUNTO_VENDITA_COD   IN     MDWH_ELABORAZIONI.PDV_PUNTO_VENDITA_COD%TYPE DEFAULT NULL
  )
  IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vMaxDataElab        TIMESTAMP;
  BEGIN
    --
    vProcName := 'CREA_ELAB';
    --
    vPasso := 'CHECK_POLICY_ENT';
    PO_CREATA := CHECK_POLICY_ENT(P_ENTITA_COD, P_PUNTO_VENDITA_COD);
    --
    IF PO_CREATA THEN
    --
      vPasso := 'MAKE_ELAB';
      SELECT MGDWH_ELAB_COD_SEQ.NEXTVAL INTO PO_ELAB_ROW.ELB_ELABORAZIONE_COD FROM DUAL;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ELAB_COD:'||PO_ELAB_ROW.ELB_ELABORAZIONE_COD, vPasso);
      --
      PO_ELAB_ROW.ENT_ENTITA_COD  := P_ENTITA_COD;
      PO_ELAB_ROW.JOB_ID          := P_JOB_ID;
      PO_ELAB_ROW.LOG_ID_SESSIONE := P_ID_SESSIONE;
      PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD := P_PUNTO_VENDITA_COD;
      IF PO_ELAB_ROW.ELB_DATA_DEC IS NULL THEN
        PO_ELAB_ROW.ELB_DATA_DEC    := kStampIeri;
      END IF;
      --
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||' - '|| PO_ELAB_ROW.ELB_DATA_DEC,BASE_LOG_PCK.DEBUG);
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ELB_DATA_DEC:'||kStampIeri, vPasso);
      vPasso := 'INSERT_ELAB';
      INSERT INTO MDWH_ELABORAZIONI(
        ELB_ELABORAZIONE_COD,
        JOB_ID,
        ELB_STATO,
        ENT_ENTITA_COD,
        LOG_ID_SESSIONE,
        PDV_PUNTO_VENDITA_COD,
        ELB_DATA_DEC)
      VALUES(
        PO_ELAB_ROW.ELB_ELABORAZIONE_COD,
        PO_ELAB_ROW.JOB_ID,
        kStatoElabNEW,
        PO_ELAB_ROW.ENT_ENTITA_COD,
        PO_ELAB_ROW.LOG_ID_SESSIONE,
        PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD,
        PO_ELAB_ROW.ELB_DATA_DEC);
      --
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||' - '||PO_ELAB_ROW.ENT_ENTITA_COD,BASE_LOG_PCK.DEBUG);
      --
      COMMIT;
    ELSE
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, '-->ELSE', vPasso);
    END IF;

    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREA_ELAB;
  --
  /**
  * AGGIORNA_ELAB(): procedura che si occupa del'aggiornamento di una elaborazione
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE AGGIORNA_ELAB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
  )
  IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
  BEGIN
    --
    vProcName := 'AGGIORNA_ELAB';
    --
    vPasso := 'UPDT_ELAB';
    UPDATE MDWH_ELABORAZIONI
       SET ELB_STATO = PO_ELAB_ROW.ELB_STATO,
           ELB_LOG = PO_ELAB_ROW.ELB_LOG,
           ELB_ROWS = PO_ELAB_ROW.ELB_ROWS,
           ELB_DATA_INI = PO_ELAB_ROW.ELB_DATA_INI,
           ELB_DATA_FIN = PO_ELAB_ROW.ELB_DATA_FIN,
           ELB_DATA_DEC = PO_ELAB_ROW.ELB_DATA_DEC,
           PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
     WHERE ELB_ELABORAZIONE_COD = PO_ELAB_ROW.ELB_ELABORAZIONE_COD;
    --
    IF PO_ELAB_ROW.ELB_STATO = kStatoElabEND THEN
    --
      UPDATE MDWH_ENTITA
         SET ENT_JOB_ID = PO_ELAB_ROW.JOB_ID,
             ENT_LAST_IMPORT = PO_ELAB_ROW.ELB_DATA_FIN
       WHERE ENT_ENTITA_COD  = PO_ELAB_ROW.ENT_ENTITA_COD;
      --
      IF PO_ELAB_ROW.ENT_ENTITA_COD = 'VND_VNH' AND PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD IS NOT NULL AND PO_ELAB_ROW.ELB_ROWS>0 THEN
      --
        UPDATE MDWH_APERTURE_REL_PDV
           SET JOB_ID_VNH = PO_ELAB_ROW.JOB_ID,
               DATA_IMPORT_VNH = PO_ELAB_ROW.ELB_DATA_FIN
         WHERE PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
           AND TRUNC(APD_DATA)= TRUNC(PO_ELAB_ROW.ELB_DATA_DEC);
      END IF;
      --
      IF PO_ELAB_ROW.ENT_ENTITA_COD = 'VND_VND' AND PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD IS NOT NULL AND PO_ELAB_ROW.ELB_ROWS>0 THEN
      --
        UPDATE MDWH_APERTURE_REL_PDV
           SET JOB_ID_VND = PO_ELAB_ROW.JOB_ID,
               DATA_IMPORT_VND = PO_ELAB_ROW.ELB_DATA_FIN
         WHERE PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
           AND TRUNC(APD_DATA)= TRUNC(PO_ELAB_ROW.ELB_DATA_DEC);
      END IF;
    --
    END IF;
    --
    COMMIT;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END AGGIORNA_ELAB;
  --
  PROCEDURE CHECK_JOB_RUNNING
  IS
    vVerificaJOBS   BOOLEAN;
    vContaJOBS      NUMBER;
    kNomePackage    CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100) :='CHECK_JOB_RUNNING';
  BEGIN
    vVerificaJOBS := true;
    WHILE vVerificaJOBS
    LOOP
      -- La vista di sistema user_scheduler_running_jobs
      -- mostra i job in esecuzione dello user oracle corrente
      SELECT COUNT(1)
        INTO vContaJOBS
        FROM USER_SCHEDULER_JOBS
       WHERE JOB_NAME LIKE 'MGDWH_LOAD%'
         AND STATE IN ('SCHEDULED','RUNNING');
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' EsecuzioniAvviate: '||vContaJOBS);
      DBMS_APPLICATION_INFO.SET_CLIENT_INFO(
        kNomePackage||'.'||vProcName ||') =>  EsecuzioniAvviate: '||vContaJOBS);
      --CONF_SEGNALAZIONI_PCK.TRACE(kNomePackage, vProcName, 'Conta JOBS: '||vContaJOBS, vPasso);
      --Se esistno dei JOBS si attende 1 minuto
      --Altrimenti se non esiste alcun record l'elaborazione ï¿½ terminata
      IF vContaJOBS > 0 THEN
        DBMS_LOCK.SLEEP(60);
        vVerificaJOBS := TRUE;
      ELSE
        --la query ï¿½ andata in no data found. si esce dal ciclo
        vVerificaJOBS := FALSE;
      END IF;
    END LOOP;
  --
  END CHECK_JOB_RUNNING;
  --
  PROCEDURE RUN_JOB_ENTITA(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vSessioneId     NUMBER := 0;
    vElaboraJob     VARCHAR2(32000);
    vContaJOBS      NUMBER := 0;
    vVerificaJOBS   BOOLEAN := FALSE;
    vNomeJob        VARCHAR2(32000);
  BEGIN
    --
    vProcName  := 'RUN_JOB_ENTITA';
    --
    vElaboraJob :='DECLARE
                     PO_ERR_SEVERITA VARCHAR2(4000);
                     PO_ERR_CODICE VARCHAR2(4000);
                     PO_MESSAGGIO VARCHAR2(4000);
                     vElabROW   MDWH_ELABORAZIONI%ROWTYPE;
                   BEGIN
                       vElabROW.ENT_ENTITA_COD :='''||PO_ELAB_ROW.ENT_ENTITA_COD||''';
                       vElabROW.JOB_ID         :='''||PO_ELAB_ROW.JOB_ID||''';
                       vElabROW.LOG_ID_SESSIONE:='''||PO_ELAB_ROW.LOG_ID_SESSIONE||''';
                       vElabROW.ELB_ELABORAZIONE_COD:='''||PO_ELAB_ROW.ELB_ELABORAZIONE_COD||''';
                       vElabROW.PDV_PUNTO_VENDITA_COD:='''||PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD||''';
                       vElabROW.ELB_DATA_DEC := TO_TIMESTAMP('''||PO_ELAB_ROW.ELB_DATA_DEC||''',''DD-MON-RR HH24:MI:SS,FF'');
                   MGDWH_ELABORAZIONI_PCK.LOAD_'||PO_ELAB_ROW.ENT_ENTITA_COD||'
                            ( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO'
                         ||' ,PO_ELAB_ROW => vElabROW); COMMIT; END;';
    --
    IF PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD IS NOT NULL THEN
      vNomeJob := 'MGDWH_LOAD_' || PO_ELAB_ROW.ENT_ENTITA_COD||'_'||PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD||'_'||PO_ELAB_ROW.ELB_ELABORAZIONE_COD;
    ELSE
      vNomeJob := 'MGDWH_LOAD_' || PO_ELAB_ROW.ENT_ENTITA_COD;
    END IF;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' vElaboraJob: '||vElaboraJob);
    DBMS_SCHEDULER.CREATE_JOB (
       JOB_NAME             => vNomeJob,
       JOB_TYPE             => 'PLSQL_BLOCK',
       JOB_ACTION           =>  vElaboraJob,
       START_DATE           =>  SYSDATE,
       ENABLED              =>  TRUE,
       COMMENTS             => 'EntCod: ' || PO_ELAB_ROW.ENT_ENTITA_COD
       );
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END RUN_JOB_ENTITA;
  --
  /**
  * LOAD_ANA_PDV(): procedura che carica l'anagrafica dei Punti Vendita
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_PDV(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
      MERGE INTO ANA_PUNTI_VENDITA PDV
         USING (SELECT CAST (E2CPDV AS NUMBER (10)) E2CPDV,
                       E2DRID,
                       E2RAGS,
                       TRIM (E2STAT) E2STAT,
                       E2INDI,
                       E2CAVP,
                       E2LOCA,
                       E2PROV,
                       TO_TIMESTAMP (DECODE (E2DAPE, 0, NULL, E2DAPE), kFormatMask) E2DAPE,
                       TO_TIMESTAMP (DECODE (E2DCPV, 0, NULL, E2DCPV), kFormatMask) E2DCPV,
                       E2NTEL,
                       E2SUPE,
                       E2RAP,
                       E2RAPX,
                       E2TAF,
                       E2TAFX,
                       E2CAN,
                       E2CANX,
                       E2INS,
                       E2INSX,
                       E2FRM,
                       E2FRMX,
                       E2ARE,
                       E2AREX,
                       E2ISP,
                       E2ISPX,
                       E2CDA,
                       E2CDAX,
                       E2CLI,
                       E2PIV,
                       E2FIS,
                       E2RAG,
                       E2RA2,
                       E2IND,
                       E2CAP,
                       E2LOC,
                       E2PRO,
                       E2NAZ,
                       E2MAI,
                       E2FIST,
                       E2FIDE,
                       E2FLVS,
                       E2CLAT,
                       E2CLON,
                       E2IPPV,
                       E2SITO
                  FROM MG.GEAD020F@KPROMO) E2
            ON (PDV.PDV_PUNTO_VENDITA_COD = E2.E2CPDV)
          WHEN NOT MATCHED THEN
        INSERT (PDV_PUNTO_VENDITA_COD,
                PDV_SIGLA,
                PDV_RAGIONE_SOC,
                PDV_STAT,
                PDV_INDIRIZZO,
                PDV_CAP,
                PDV_LOCALITA,
                PDV_PROVINCIA,
                PDV_DATA_APERTURA,
                PDV_DATA_CHIUSURA,
                PDV_NUMERO_TELEFONICO,
                PDV_SUPERFICIE_MQ,
                PDV_RAPPORTO_COD,
                PDV_RAPPORTO_DESC,
                PDV_TIPO_AFFILIATO_COD,
                PDV_TIPO_AFFILIATO_DESC,
                PDV_CANALE_COD,
                PDV_CANALE_DESC,
                PDV_INSEGNA_COD,
                PDV_INSEGNA_DES,
                PDV_FORMAT_COD,
                PDV_FORMAT_DES,
                PDV_AREA_GEOGR_COD,
                PDV_AREA_GEOGR_DESC,
                PDV_ISPETTORE_COD,
                PDV_ISPETTORE_DESC,
                PDV_CDA_CONT_COD,
                PDV_CDA_CONT_DESC,
                PDV_CLIENTE_CONT_COD,
                PDV_CLIENTE_CONT_PIVA,
                PDV_CLIENTE_CONT_CF,
                PDV_CLIENTE_CONT_RAGGR_SOC,
                PDV_CLIENTE_CONT_RAGGR_SOC2,
                PDV_CLIENTE_CONT_IND,
                PDV_CLIENTE_CONT_CAP,
                PDV_CLIENTE_CONT_LOC,
                PDV_CLIENTE_CONT_PRO,
                PDV_CLIENTE_CONT_NAZ,
                PDV_CLIENTE_CONT_MAIL,
                PDV_STAT_PDV_COD,
                PDV_FIDELITY_ABLT,
                PDV_VISUAL_STORE,
                PDV_LATITUDINE,
                PDV_LONGITUDINE,
                PDV_INDIRIZZO_IP,
                PDV_SITO,
                JOB_ID,
                DATA_IMPORT)
        VALUES (E2.E2CPDV,
                E2.E2DRID,
                E2.E2RAGS,
                E2.E2STAT,
                E2.E2INDI,
                E2.E2CAVP,
                E2.E2LOCA,
                E2.E2PROV,
                E2.E2DAPE,
                E2.E2DCPV,
                E2.E2NTEL,
                E2.E2SUPE,
                E2.E2RAP,
                E2.E2RAPX,
                E2.E2TAF,
                E2.E2TAFX,
                E2.E2CAN,
                E2.E2CANX,
                E2.E2INS,
                E2.E2INSX,
                E2.E2FRM,
                E2.E2FRMX,
                E2.E2ARE,
                E2.E2AREX,
                E2.E2ISP,
                E2.E2ISPX,
                E2.E2CDA,
                E2.E2CDAX,
                E2.E2CLI,
                E2.E2PIV,
                E2.E2FIS,
                E2.E2RAG,
                E2.E2RA2,
                E2.E2IND,
                E2.E2CAP,
                E2.E2LOC,
                E2.E2PRO,
                E2.E2NAZ,
                E2.E2MAI,
                E2.E2FIST,
                E2.E2FIDE,
                E2.E2FLVS,
                E2.E2CLAT,
                E2.E2CLON,
                E2.E2IPPV,
                E2.E2SITO,
                PO_ELAB_ROW.JOB_ID,
                SYSTIMESTAMP
                )
          WHEN MATCHED THEN
        UPDATE SET PDV.PDV_SIGLA = E2.E2DRID,
                   PDV.PDV_RAGIONE_SOC = E2.E2RAGS,
                   PDV.PDV_STAT = E2.E2STAT,
                   PDV.PDV_INDIRIZZO = E2.E2INDI,
                   PDV.PDV_CAP = E2.E2CAVP,
                   PDV.PDV_LOCALITA = E2.E2LOCA,
                   PDV.PDV_PROVINCIA = E2.E2PROV,
                   PDV.PDV_DATA_APERTURA = E2.E2DAPE,
                   PDV.PDV_DATA_CHIUSURA = E2.E2DCPV,
                   PDV.PDV_NUMERO_TELEFONICO = E2.E2NTEL,
                   PDV.PDV_SUPERFICIE_MQ = E2.E2SUPE,
                   PDV.PDV_RAPPORTO_COD = E2.E2RAP,
                   PDV.PDV_RAPPORTO_DESC = E2.E2RAPX,
                   PDV.PDV_TIPO_AFFILIATO_COD = E2.E2TAF,
                   PDV.PDV_TIPO_AFFILIATO_DESC = E2.E2TAFX,
                   PDV.PDV_CANALE_COD = E2.E2CAN,
                   PDV.PDV_CANALE_DESC = E2.E2CANX,
                   PDV.PDV_INSEGNA_COD = E2.E2INS,
                   PDV.PDV_INSEGNA_DES = E2.E2INSX,
                   PDV.PDV_FORMAT_COD = E2.E2FRM,
                   PDV.PDV_FORMAT_DES = E2.E2FRMX,
                   PDV.PDV_AREA_GEOGR_COD = E2.E2ARE,
                   PDV.PDV_AREA_GEOGR_DESC = E2.E2AREX,
                   PDV.PDV_ISPETTORE_COD = E2.E2ISP,
                   PDV.PDV_ISPETTORE_DESC = E2.E2ISPX,
                   PDV.PDV_CDA_CONT_COD = E2.E2CDA,
                   PDV.PDV_CDA_CONT_DESC = E2.E2CDAX,
                   PDV.PDV_CLIENTE_CONT_COD = E2.E2CLI,
                   PDV.PDV_CLIENTE_CONT_PIVA = E2.E2PIV,
                   PDV.PDV_CLIENTE_CONT_CF = E2.E2FIS,
                   PDV.PDV_CLIENTE_CONT_RAGGR_SOC = E2.E2RAG,
                   PDV.PDV_CLIENTE_CONT_RAGGR_SOC2 = E2.E2RA2,
                   PDV.PDV_CLIENTE_CONT_IND = E2.E2IND,
                   PDV.PDV_CLIENTE_CONT_CAP = E2.E2CAP,
                   PDV.PDV_CLIENTE_CONT_LOC = E2.E2LOC,
                   PDV.PDV_CLIENTE_CONT_PRO = E2.E2PRO,
                   PDV.PDV_CLIENTE_CONT_NAZ = E2.E2NAZ,
                   PDV.PDV_CLIENTE_CONT_MAIL = E2.E2MAI,
                   PDV.PDV_STAT_PDV_COD = E2.E2FIST,
                   PDV.PDV_FIDELITY_ABLT = E2.E2FIDE,
                   PDV.PDV_VISUAL_STORE = E2.E2FLVS,
                   PDV.PDV_LATITUDINE = E2.E2CLAT,
                   PDV.PDV_LONGITUDINE = E2.E2CLON,
                   PDV.PDV_INDIRIZZO_IP = E2.E2IPPV,
                   PDV.PDV_SITO = E2.E2SITO,
                   PDV.JOB_ID = PO_ELAB_ROW.JOB_ID,
                   PDV.DATA_IMPORT =SYSTIMESTAMP;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END;
  BEGIN
    --
    vProcName := 'LOAD_ANA_PDV';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||' - '||PO_ELAB_ROW.ENT_ENTITA_COD,BASE_LOG_PCK.DEBUG);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_PDV;
  --
  /**
  * LOAD_ANA_ART(): procedura che carica l'anagrafica degli articoli
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_ART(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
      MERGE INTO ANA_ARTICOLI ART
         USING (SELECT CAST (E1CART AS NUMBER (10)) E1CART,
                       E1XARS,
                       E1XARC,
                       TRIM(E1STAT) E1STAT,
                       E1CFOR,
                       E1BRAND,
                       E1CUAQ,
                       E1CLPG,
                       E1CREP,
                       E1CUGE,
                       E1CRAG,
                       E1CFAM,
                       E1CIVI,
                       TO_TIMESTAMP (DECODE (E1DINS, 0, NULL, E1DINS), kFormatMask) E1DINS,
                       TO_TIMESTAMP (DECODE (E1DESP, 0, NULL, E1DESP), kFormatMask) E1DESP,
                       E1UMPR,
                       E1TCDT,
                       E1NPEZ,
                       E1TCNF,
                       E1UCNF,
                       E1VCNF,
                       E1QTBA,
                       E1UMBA,
                       E1MIVE,
                       E1MIAQ,
                       E1PIMB,
                       E1MARK,
                       E1MARX,
                       E1PLBL,
                       E1FESP,
                       E1FMPK,
                       E1FMPQ,
                       E1FCAT,
                       E1FVNO,
                       E1FPRE,
                       E1SPOT,
                       E1INFM,
                       E1FNVE,
                       E1RAEE,
                       E1CAOX,
                       E1PTIP,
                       E1SITO,
                       E1ECR1,
                       E1ECR2,
                       E1ECR3,
                       E1ECR4,
                       E1ECR5
                  FROM MG.GEAD010F@KPROMO) E1
            ON (ART.ART_ARTICOLO_COD = E1.E1CART)
          WHEN NOT MATCHED THEN
        INSERT (ART_ARTICOLO_COD,
                ART_DESCRIZIONE_STD,
                ART_DESCRIZIONE_SITO,
                ART_STAT,
                ART_FORNITORE_GEN_COD,
                ART_BRAND,
                ART_UFFICIO_ACQ_COD,
                ART_LINEA_PRODOTTO_COD,
                ART_REPARTO_COD,
                ART_UNITA_ORGANIZZATIVA_COD,
                ART_RAGGRUPPAMENTO_COD,
                ART_FAMIGLIA_COD,
                ART_CICLO_VITA,
                ART_DATA_INI,
                ART_DATA_ETO,
                ART_UNITA_MISURA,
                ART_PREZZO_KG_LT,
                ART_NUM_PEZZI,
                ART_TIPO_CONF,
                ART_UNIT_MIS_CONF,
                ART_QTA_PZ_CONF,
                ART_QTA_TOT_CONF,
                ART_UNITA_MISURA_PRZ,
                ART_IMBALLO_VEND,
                ART_IMBALLO_ACQ,
                ART_PESO_MEDIO,
                ART_MARCHIO_COD,
                ART_MARCHIO_DESC,
                ART_PRIVATE_LBL,
                ART_ESPOSITORE,
                ART_MULTIPACK,
                ART_QTA_MULTIPACK,
                ART_CATERING,
                ART_VINO_NOV,
                ART_PREMIO,
                ART_SPOT,
                ART_INFIAMM,
                ART_NON_VENDIBILE,
                ART_RAEE,
                ART_ARTO,
                ART_PROD_TIPICO,
                ART_SITO,
                JOB_ID,
                DATA_IMPORT,
                ART_ECR_LVL1_COD,
                ART_ECR_LVL2_COD,
                ART_ECR_LVL3_COD,
                ART_ECR_LVL4_COD,
                ART_ECR_LVL5_COD
                )
        VALUES (E1.E1CART,
                E1.E1XARS,
                E1.E1XARC,
                E1.E1STAT,
                E1.E1CFOR,
                E1.E1BRAND,
                E1.E1CUAQ,
                E1.E1CLPG,
                E1.E1CREP,
                E1.E1CUGE,
                E1.E1CRAG,
                E1.E1CFAM,
                E1.E1CIVI,
                E1.E1DINS,
                E1.E1DESP,
                E1.E1UMPR,
                E1.E1TCDT,
                E1.E1NPEZ,
                E1.E1TCNF,
                E1.E1UCNF,
                E1.E1VCNF,
                E1.E1QTBA,
                E1.E1UMBA,
                E1.E1MIVE,
                E1.E1MIAQ,
                E1.E1PIMB,
                E1.E1MARK,
                E1.E1MARX,
                E1.E1PLBL,
                E1.E1FESP,
                E1.E1FMPK,
                E1.E1FMPQ,
                E1.E1FCAT,
                E1.E1FVNO,
                E1.E1FPRE,
                E1.E1SPOT,
                E1.E1INFM,
                E1.E1FNVE,
                E1.E1RAEE,
                E1.E1CAOX,
                E1.E1PTIP,
                E1.E1SITO,
                PO_ELAB_ROW.JOB_ID,
                SYSTIMESTAMP,
                E1.E1ECR1,
                E1.E1ECR2,
                E1.E1ECR3,
                E1.E1ECR4,
                E1.E1ECR5
                )
          WHEN MATCHED THEN
        UPDATE SET ART.ART_DESCRIZIONE_STD          = E1.E1XARS,
                   ART.ART_DESCRIZIONE_SITO         = E1.E1XARC,
                   ART.ART_STAT                     = E1.E1STAT,
                   ART.ART_FORNITORE_GEN_COD        = E1.E1CFOR,
                   ART.ART_BRAND                    = E1.E1BRAND,
                   ART.ART_UFFICIO_ACQ_COD          = E1.E1CUAQ,
                   ART.ART_LINEA_PRODOTTO_COD       = E1.E1CLPG,
                   ART.ART_REPARTO_COD              = E1.E1CREP,
                   ART.ART_UNITA_ORGANIZZATIVA_COD  = E1.E1CUGE,
                   ART.ART_RAGGRUPPAMENTO_COD       = E1.E1CRAG,
                   ART.ART_FAMIGLIA_COD             = E1.E1CFAM,
                   ART.ART_CICLO_VITA               = E1.E1CIVI,
                   ART.ART_DATA_INI                 = E1.E1DINS,
                   ART.ART_DATA_ETO                 = E1.E1DESP,
                   ART.ART_UNITA_MISURA             = E1.E1UMPR,
                   ART.ART_PREZZO_KG_LT             = E1.E1TCDT,
                   ART.ART_NUM_PEZZI                = E1.E1NPEZ,
                   ART.ART_TIPO_CONF                = E1.E1TCNF,
                   ART.ART_UNIT_MIS_CONF            = E1.E1UCNF,
                   ART.ART_QTA_PZ_CONF              = E1.E1VCNF,
                   ART.ART_QTA_TOT_CONF             = E1.E1QTBA,
                   ART.ART_UNITA_MISURA_PRZ         = E1.E1UMBA,
                   ART.ART_IMBALLO_VEND             = E1.E1MIVE,
                   ART.ART_IMBALLO_ACQ              = E1.E1MIAQ,
                   ART.ART_PESO_MEDIO               = E1.E1PIMB,
                   ART.ART_MARCHIO_COD              = E1.E1MARK,
                   ART.ART_MARCHIO_DESC             = E1.E1MARX,
                   ART.ART_PRIVATE_LBL              = E1.E1PLBL,
                   ART.ART_ESPOSITORE               = E1.E1FESP,
                   ART.ART_MULTIPACK                = E1.E1FMPK,
                   ART.ART_QTA_MULTIPACK            = E1.E1FMPQ,
                   ART.ART_CATERING                 = E1.E1FCAT,
                   ART.ART_VINO_NOV                 = E1.E1FVNO,
                   ART.ART_PREMIO                   = E1.E1FPRE,
                   ART.ART_SPOT                     = E1.E1SPOT,
                   ART.ART_INFIAMM                  = E1.E1INFM,
                   ART.ART_NON_VENDIBILE            = E1.E1FNVE,
                   ART.ART_RAEE                     = E1.E1RAEE,
                   ART.ART_ARTO                     = E1.E1CAOX,
                   ART.ART_PROD_TIPICO              = E1.E1PTIP,
                   ART.ART_SITO                     = E1.E1SITO,
                   ART.JOB_ID                       = PO_ELAB_ROW.JOB_ID,
                   ART.DATA_IMPORT                  = SYSTIMESTAMP,
                   ART.ART_ECR_LVL1_COD             = E1.E1ECR1,
                   ART.ART_ECR_LVL2_COD             = E1.E1ECR2,
                   ART.ART_ECR_LVL3_COD             = E1.E1ECR3,
                   ART.ART_ECR_LVL4_COD             = E1.E1ECR4,
                   ART.ART_ECR_LVL5_COD             = E1.E1ECR5
                   ;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END RAWINSERT;
    --
    PROCEDURE RAWUPDT
    IS
    BEGIN
      UPDATE ANA_ARTICOLI ART
         SET ECR_CLASSIFICAZIONE_COD = (SELECT ECR_CLASSIFICAZIONE_COD
                                          FROM ANA_CLASSIFICAZIONI_ECR ECR
                                         WHERE ECR_CLASSIFICAZIONE_COD = ART.ART_ECR_LVL5_COD);
    END RAWUPDT;
    --
  BEGIN
    --
    vProcName := 'LOAD_ANA_ART';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    vPasso := 'RAWUPDT';
    RAWUPDT;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_ART;
  --
  /**
  * LOAD_ANA_ECR(): procedura che carica l'anagrafica ECR da MG.GEAD090F
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_ECR(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
      MERGE INTO ANA_CLASSIFICAZIONI_ECR ECR
         USING (SELECT  E9ECRL,
                        E9ECRD
                   FROM MG.GEAD090F@KPROMO) E9
            ON (ECR.ECR_CLASSIFICAZIONE_COD = E9.E9ECRL)
          WHEN NOT MATCHED THEN
        INSERT (ECR_CLASSIFICAZIONE_COD,
                ECR_DESCRIZIONE,
                JOB_ID,
                DATA_IMPORT
                )
        VALUES (E9.E9ECRL,
                E9.E9ECRD,
                PO_ELAB_ROW.JOB_ID,
                SYSTIMESTAMP
                )
          WHEN MATCHED THEN
        UPDATE SET ECR.ECR_DESCRIZIONE                  = E9.E9ECRD,
                   ECR.JOB_ID                           = PO_ELAB_ROW.JOB_ID,
                   ECR.DATA_IMPORT                      = SYSTIMESTAMP
                   ;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
      --
      UPDATE ANA_CLASSIFICAZIONI_ECR
         SET ECR_CLASSIFICAZIONE_SUP_COD = CASE
                                             WHEN LENGTH(ECR_CLASSIFICAZIONE_COD) >2 THEN
                                                  SUBSTR(ECR_CLASSIFICAZIONE_COD, 0,LENGTH(ECR_CLASSIFICAZIONE_COD)-2)
                                             ELSE NULL
                                           END;
    END;
  BEGIN
    --
    vProcName := 'LOAD_ANA_ECR';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_ECR;
  --
  /**
  * LOAD_ANA_CLI(): procedura che carica l'anagrafica dei clienti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_CLI(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE PREINSERT
    IS
      vDmlCLI        ANA_CLIENTI%ROWTYPE;
    BEGIN
    --
      FOR rec IN ( SELECT ID_CUSTOMER,
                          NVL2 (REPLACE (ADDR_STREET_DESCR, '.', NULL),
                               ADDR_STREET_DESCR || '-' || ADDR_STR_NAME,
                               ADDR_STR_NAME)
                          ADDR_STR_NAME,
                          ADDR_NO,
                          ADDR_PRV_FK,
                          ADDR_CITY,
                          ADDR_ZIPCODE,
                          NVL2 (MOB_PREFIX, MOB_PREFIX || '-' || MOB, MOB) MOB,
                          EMAIL,
                          FLG_PRIVACY_AUTH,
                          FLG_PRIVACY_MKTG_ACTIVITY,
                          FLG_PRIVACY_MKTG_PROFILING,
                          GREATEST(
                          NVL (TS_INS, TO_DATE ('01010001', 'ddmmyyyy')),
                          NVL (TS_DEL, TO_DATE ('01010001', 'ddmmyyyy')),
                          NVL (TS_UPD, TO_DATE ('01010001', 'ddmmyyyy'))
                          ) DATA_DEC
                     FROM KCRM.OCUSTOMER@KPROMO
                    WHERE TRUNC(TS_INS) >= kStampIeri
                       OR TRUNC(TS_DEL) >= kStampIeri
                       OR TRUNC(TS_UPD) >= kStampIeri
                 )
      LOOP
      --
        BEGIN
          vPasso := 'vDmlCLI';
          vDmlCLI.CLI_CLIENTE_COD := rec.ID_CUSTOMER;
          vDmlCLI.CLI_INDIRIZZO   := rec.ADDR_STR_NAME;
          vDmlCLI.CLI_NUMERO      := rec.ADDR_NO;
          vDmlCLI.CLI_PROVINCIA   := rec.ADDR_PRV_FK;
          vDmlCLI.CLI_CITTA       := rec.ADDR_CITY;
          vDmlCLI.CLI_CAP         := rec.ADDR_ZIPCODE;
          vDmlCLI.CLI_CELLULARE   := rec.MOB;
          vDmlCLI.CLI_EMAIL       := rec.EMAIL;
          vDmlCLI.CLI_PRIVACY_AUTH:= rec.FLG_PRIVACY_AUTH;
          vDmlCLI.CLI_PRIVACY_MKTG_ACTIVITY:= rec.FLG_PRIVACY_MKTG_ACTIVITY;
          vDmlCLI.CLI_PRIVACY_MKTG_PROFILING:= rec.FLG_PRIVACY_MKTG_PROFILING;
          --
          vPasso := 'PRE_UPDT_ANA_CLI';
          MGDWH_UTILITY_PCK.PRE_UPDT_ANA_CLI(
                PO_ERR_SEVERITA       => PO_ERR_SEVERITA,
                PO_ERR_CODICE         => PO_ERR_CODICE  ,
                PO_MESSAGGIO          => PO_MESSAGGIO   ,
                P_DECORRENZA          => rec.DATA_DEC   ,
                PO_DML_NEW            => vDmlCLI
          );
          --
          IF PO_MESSAGGIO IS NOT NULL THEN RAISE ErroreGestito; END IF;
        EXCEPTION
          WHEN ErroreGestito THEN
          --
            BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
            PO_ERR_SEVERITA := NULL;
            PO_ERR_CODICE   := NULL;
            PO_MESSAGGIO    := NULL;
        END;
      --
      END LOOP;
    --
    EXCEPTION
      WHEN OTHERS THEN
        PO_ERR_SEVERITA := 'F';
        PO_ERR_CODICE   := SQLCODE;
        PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
        --
         PO_ERR_SEVERITA := NULL;
         PO_ERR_CODICE   := NULL;
         PO_MESSAGGIO    := NULL;
    END PREINSERT;
    --
    PROCEDURE RAWINSERT
    IS
    BEGIN
    --
      MERGE INTO ANA_CLIENTI CLI
           USING (SELECT CAST (ID_CUSTOMER AS VARCHAR2 (25)) ID_CUSTOMER,
                  FIRST_NAME,
                  LAST_NAME,
                  SEX,
                  DT_BIRTH,
                  NVL2 (REPLACE (ADDR_STREET_DESCR, '.', NULL),
                        ADDR_STREET_DESCR || '-' || ADDR_STR_NAME,
                        ADDR_STR_NAME)
                     ADDR_STR_NAME,
                  ADDR_NO,
                  ADDR_PRV_FK,
                  ADDR_CITY,
                  ADDR_ZIPCODE,
                  NVL2 (TEL_PREFIX, TEL_PREFIX || '-' || PHO, PHO) PHO,
                  --NVL2 (MOB_PREFIX, MOB_PREFIX || '-' || MOB, MOB) MOB,
                  MOB,
                  EMAIL,
                  FLG_PRIVACY_AUTH,
                  FLG_PRIVACY_MKTG_ACTIVITY,
                  FLG_PRIVACY_MKTG_PROFILING,
                  --NVL(PREFERRED_STORE_FK, MGDWH_UTILITY_PCK.GET_STORE_PREFERITO(ID_CUSTOMER)) PREFERRED_STORE_FK,
                  TO_NUMBER(PREFERRED_STORE_FK) PREFERRED_STORE_FK,
                  greatest(nvl(TS_INS,kStampIni), nvl(TS_DEL,kStampIni), nvl(TS_UPD,kStampIni)) DATA_MOD,
                  --DECODE(INSTR(NVL2 (MOB_PREFIX, MOB_PREFIX || '-' || MOB, MOB),'3'),1, 1,0) FLAG_SMS,
                  NVL2(MOB,1,0) FLAG_SMS,
                  DECODE(INSTR(NVL (EMAIL, 'NO'),'@'),0, 0, 1) FLAG_EML,
                  FISCAL_CODE
             FROM KCRM.OCUSTOMER@KPROMO
           -- WHERE ID_CUSTOMER = '3148519'
            --   OR TRUNC(TS_DEL) >= kStampIeri
            --   OR TRUNC(TS_UPD) >= kStampIeri
               ) CUST
               ON (CLI.CLI_CLIENTE_COD = CUST.ID_CUSTOMER)
             WHEN NOT MATCHED
             THEN
           INSERT (CLI_CLIENTE_COD,
                   CLI_COGNOME,
                   CLI_NOME,
                   CLI_SESSO,
                   CLI_DATA_NASCITA,
                   CLI_INDIRIZZO,
                   CLI_NUMERO,
                   CLI_PROVINCIA,
                   CLI_CITTA,
                   CLI_CAP,
                   CLI_TELEFONO,
                   CLI_CELLULARE,
                   CLI_EMAIL,
                   CLI_PRIVACY_AUTH,
                   CLI_PRIVACY_MKTG_ACTIVITY,
                   CLI_PRIVACY_MKTG_PROFILING,
                   PDV_PUNTO_VENDITA_COD_SIT,
                   JOB_ID,
                   DATA_IMPORT,
                   CLI_DATA_MODIFICA,
                   CLI_FLAG_SMS,
                   CLI_FLAG_EMAIL,
                   CLI_CF_PIVA)
           VALUES (CUST.ID_CUSTOMER,
                   CUST.LAST_NAME,
                   CUST.FIRST_NAME,
                   CUST.SEX,
                   CUST.DT_BIRTH,
                   CUST.ADDR_STR_NAME,
                   CUST.ADDR_NO,
                   CUST.ADDR_PRV_FK,
                   CUST.ADDR_CITY,
                   CUST.ADDR_ZIPCODE,
                   CUST.PHO,
                   CUST.MOB,
                   CUST.EMAIL,
                   CUST.FLG_PRIVACY_AUTH,
                   CUST.FLG_PRIVACY_MKTG_ACTIVITY,
                   CUST.FLG_PRIVACY_MKTG_PROFILING,
                   CUST.PREFERRED_STORE_FK,
                   PO_ELAB_ROW.JOB_ID,
                   SYSTIMESTAMP,
                   CUST.DATA_MOD,
                   CUST.FLAG_SMS,
                   CUST.FLAG_EML,
                   CUST.FISCAL_CODE
                   )
              WHEN MATCHED
              THEN
            UPDATE SET CLI.CLI_COGNOME = CUST.LAST_NAME,
                       CLI.CLI_NOME = CUST.FIRST_NAME,
                       CLI.CLI_SESSO = CUST.SEX,
                       CLI.CLI_DATA_NASCITA = CUST.DT_BIRTH,
                       CLI.CLI_INDIRIZZO = CUST.ADDR_STR_NAME,
                       CLI.CLI_NUMERO = CUST.ADDR_NO,
                       CLI.CLI_PROVINCIA = CUST.ADDR_PRV_FK,
                       CLI.CLI_CITTA = CUST.ADDR_CITY,
                       CLI.CLI_CAP = CUST.ADDR_ZIPCODE,
                       CLI.CLI_TELEFONO = CUST.PHO,
                       CLI.CLI_CELLULARE = CUST.MOB,
                       CLI.CLI_EMAIL = CUST.EMAIL,
                       CLI.CLI_PRIVACY_AUTH = CUST.FLG_PRIVACY_AUTH,
                       CLI.CLI_PRIVACY_MKTG_ACTIVITY = CUST.FLG_PRIVACY_MKTG_ACTIVITY,
                       CLI.CLI_PRIVACY_MKTG_PROFILING = CUST.FLG_PRIVACY_MKTG_PROFILING,
                       CLI.PDV_PUNTO_VENDITA_COD_SIT = CUST.PREFERRED_STORE_FK,
                       CLI.JOB_ID = PO_ELAB_ROW.JOB_ID,
                       CLI.DATA_IMPORT = SYSTIMESTAMP,
                       CLI.CLI_DATA_MODIFICA = CUST.DATA_MOD,
                       CLI.CLI_FLAG_SMS = CUST.FLAG_SMS,
                       CLI.CLI_FLAG_EMAIL = CUST.FLAG_EML,
                       CLI.CLI_CF_PIVA =  CUST.FISCAL_CODE;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END;
  BEGIN
    --
    vProcName := 'LOAD_ANA_CLI';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'PREINSERT';
    PREINSERT;
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    MGDWH_UTILITY_PCK.UPDT_ANA_CLI(
          PO_ERR_SEVERITA,
          PO_ERR_CODICE  ,
          PO_MESSAGGIO
      );
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_CLI;
  --
  /**
  * LOAD_ANA_CRT(): procedura che carica l'anagrafica delle carte - loyalty
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_CRT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
    --
      MERGE INTO ANA_CARTE CRT
           USING (SELECT ID_CARD,
                         ID_CUST_FK,
                         CAST(ENT_FK AS NUMBER) ENT_FK,
                         DT_START,
                         COALESCE(DT_BLKLST,DT_END) DT_BLKLST,
                         BASE_UTIL_PCK.GETCHKDIGITEAN13(ID_CARD) CRT_EAN_COD
                    FROM KCRM.OCARD@KPROMO OC
                    --WHERE ID_CARD = '040290013141'
                     --AND (TRUNC(TS_INS) >= kStampIeri
                     --     OR TRUNC(TS_DEL) >= kStampIeri
                     --     OR TRUNC(TS_UPD) >= kStampIeri)
                     --AND EXISTS(SELECT 0
                     --             FROM ANA_CLIENTI CLI
                     --            WHERE CLI.CLI_CLIENTE_COD = OC.ID_CUST_FK
                     --          )
                         ) CARD
               ON (CRT.CRT_CARTA_COD = CARD.ID_CARD)
             WHEN NOT MATCHED
             THEN
           INSERT (CRT_CARTA_COD,
                   CLI_CLIENTE_COD_XK,
                   PDV_PUNTO_VENDITA_COD,
                   CRT_DATA_INIZIO,
                   CRT_DATA_FINE,
                   CRT_EAN_COD,
                   JOB_ID,
                   DATA_IMPORT)
           VALUES (ID_CARD,
                   ID_CUST_FK,
                   ENT_FK,
                   DT_START,
                   DT_BLKLST,
                   CRT_EAN_COD,
                   PO_ELAB_ROW.JOB_ID,
                   SYSTIMESTAMP)
              WHEN MATCHED
              THEN
            UPDATE SET CRT.CLI_CLIENTE_COD_XK = CARD.ID_CUST_FK,
                       CRT.PDV_PUNTO_VENDITA_COD = CARD.ENT_FK,
                       CRT.CRT_DATA_INIZIO = CARD.DT_START,
                       CRT.CRT_DATA_FINE = CARD.DT_BLKLST,
                       CRT.CRT_EAN_COD = CARD.CRT_EAN_COD,
                       CRT.JOB_ID = PO_ELAB_ROW.JOB_ID,
                       CRT.DATA_IMPORT = SYSTIMESTAMP;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END RAWINSERT;
    --
    PROCEDURE RAWUPDATE
    IS
    BEGIN
    --
      UPDATE ANA_CARTE CRT
         SET CRT.CLI_CLIENTE_COD =
                (SELECT CLI.CLI_CLIENTE_COD
                   FROM ANA_CLIENTI CLI
                  WHERE CLI.CLI_CLIENTE_COD = CRT.CLI_CLIENTE_COD_XK)
       WHERE CRT.CLI_CLIENTE_COD IS NULL;
    END RAWUPDATE;
    --
    PROCEDURE RAW_COD_XK
    IS
    BEGIN
    --
      UPDATE ANA_CARTE CRT
         SET CRT.CLI_CLIENTE_COD =
                (SELECT CLI.CLI_CLIENTE_COD
                   FROM ANA_CLIENTI CLI
                  WHERE CLI.CLI_CLIENTE_COD = CRT.CLI_CLIENTE_COD_XK)
       WHERE CRT.CLI_CLIENTE_COD != CRT.CLI_CLIENTE_COD_XK ;
    END RAW_COD_XK;
    --
  BEGIN
    --
    vProcName := 'LOAD_ANA_CRT';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    vPasso := 'RAWUPDATE';
    RAWUPDATE;
    --
    vPasso := 'RAW_COD_XK';
    RAW_COD_XK;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW.ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_CRT;
  --
  --
  /**
  * LOAD_ANA_PRM(): procedura che carica l'anagrafica delle promozioni
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_PRM(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
    --
      MERGE INTO ANA_PROMOZIONI PRM
           USING (WITH PROMO_CENTRAL AS (SELECT ID_PROMO
                                           FROM FRAME.PROMO_CAL@KPROMO CAL
                                          GROUP BY ID_PROMO)
                  SELECT  PRM.ID_PROMO_OFF PRM_PROMOZIONE_COD,
                          PRM.DESCR PRM_DESCRIZIONE,
                          DIV.ID_DIV CNL_CANALE_COD,
                          PRM.DT_START PRM_DATA_INIZIO,
                          PRM.DT_END PRM_DATA_FINE,
                          NVL2(PROMO_CENTRAL.ID_PROMO, 'S','N') PRM_SCENTRAL,
                          NULL PRM_TIPO,
                          NULL PRM_TIPO_DESCR,
                          PRM.PROMO_HOST_ID PRM_HOST_ID
                     FROM KCRM.OPROMO_OFF@KPROMO PRM,
                          PROMO_CENTRAL,
                          KCRM.ODIVISION@KPROMO DIV
                    WHERE PRM.FLG_STATE = 0
                      AND PRM.CHAN_FK = DIV.ID_DIV
                      AND DIV.FLG_STATE = 0
                      AND PRM.PROMO_HOST_ID = PROMO_CENTRAL.ID_PROMO(+)
                      AND (TRUNC(PRM.TS_INS) >= kStampIeri
                          OR TRUNC(PRM.TS_DEL) >= kStampIeri
                          OR TRUNC(PRM.TS_UPD) >= kStampIeri)
                        ) SRC_PROMO
               ON (PRM.PRM_PROMOZIONE_COD = SRC_PROMO.PRM_PROMOZIONE_COD)
             WHEN NOT MATCHED
             THEN
           INSERT (PRM_PROMOZIONE_COD,
                   PRM_DESCRIZIONE,
                   CNL_CANALE_COD,
                   PRM_DATA_INIZIO,
                   PRM_DATA_FINE,
                   PRM_SCENTRAL,
                   PRM_TIPO,
                   JOB_ID,
                   DATA_IMPORT,
                   PRM_TIPO_DESCR,
                   PRM_HOST_ID)
           VALUES (SRC_PROMO.PRM_PROMOZIONE_COD,
                   SRC_PROMO.PRM_DESCRIZIONE,
                   SRC_PROMO.CNL_CANALE_COD,
                   SRC_PROMO.PRM_DATA_INIZIO,
                   SRC_PROMO.PRM_DATA_FINE,
                   SRC_PROMO.PRM_SCENTRAL,
                   SRC_PROMO.PRM_TIPO,
                   PO_ELAB_ROW.JOB_ID,
                   SYSTIMESTAMP,
                   SRC_PROMO.PRM_TIPO_DESCR,
                   SRC_PROMO.PRM_HOST_ID)
              WHEN MATCHED
              THEN
            UPDATE SET PRM.PRM_DESCRIZIONE = SRC_PROMO.PRM_DESCRIZIONE,
                       PRM.CNL_CANALE_COD = SRC_PROMO.CNL_CANALE_COD,
                       PRM.PRM_DATA_INIZIO = SRC_PROMO.PRM_DATA_INIZIO,
                       PRM.PRM_DATA_FINE = SRC_PROMO.PRM_DATA_FINE,
                       PRM.PRM_SCENTRAL = SRC_PROMO.PRM_SCENTRAL,
                       PRM.PRM_TIPO = SRC_PROMO.PRM_TIPO,
                       PRM.PRM_TIPO_DESCR = SRC_PROMO.PRM_TIPO,
                       PRM.JOB_ID = PO_ELAB_ROW.JOB_ID,
                       PRM.DATA_IMPORT = SYSTIMESTAMP,
                       PRM.PRM_HOST_ID = SRC_PROMO.PRM_HOST_ID;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END RAWINSERT;
    --
    PROCEDURE RAWUPDATE
    IS
    BEGIN
    --
      UPDATE ANA_PROMOZIONI PRM
         SET (PRM_TIPO, PRM_TIPO_DESCR) =
             (SELECT HOST_PROMO_TYPE, HOST_DESCRIPTION
                FROM KCRM.OPROMO_HEAD@KPROMO,
                     FRAME.MG_SPROMO_WIZARD@KPROMO WIZ
               WHERE SUBSTR(ID_PROMO_TYPE_FK,5,10) = WIZ.KCRM_ID_PROMO_WIZARD
                 AND ID_PROMO_OFF = PRM.PRM_PROMOZIONE_COD
            GROUP BY ID_PROMO_OFF,SUBSTR(ID_PROMO_TYPE_FK,5,10),HOST_PROMO_TYPE, HOST_DESCRIPTION
              )
       WHERE PRM_TIPO IS NULL;

    END;
  BEGIN
    --
    vProcName := 'LOAD_ANA_PRM';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    vPasso := 'RAWINSERT';
    --RAWUPDATE;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_PRM;
  --
  /**
  * LOAD_ANA_CLU(): procedura che carica l'anagrafica dei cluster
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_CLU(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
    --
      MERGE INTO ANA_CLUSTER CLU
           USING (SELECT TRM_CLU_CODE CLU_CLUSTER_COD,
                         DESCR CLU_DESCRIZIONE,
                         DT_START CLU_DATA_INIZIO,
                         DT_END CLU_DATA_FINE,
                         'O' CLU_TIPOLOGIA ,
                         DECODE(FLG_STATE,0,NULL, 'A') CLU_STAT
                    FROM KCRM.OCLU@KPROMO CLU
                   --WHERE (TRUNC(CLU.TS_INS) >= kStampIeri
                   --       OR TRUNC(CLU.TS_DEL) >= kStampIeri
                   --       OR TRUNC(CLU.TS_UPD) >= kStampIeri
                   --      )
                        ) SRC_CLU
               ON (CLU.CLU_CLUSTER_COD = SRC_CLU.CLU_CLUSTER_COD)
             WHEN NOT MATCHED
             THEN
           INSERT (CLU_CLUSTER_COD,
                   CLU_DESCRIZIONE,
                   CLU_DATA_INIZIO,
                   CLU_DATA_FINE,
                   CLU_TIPOLOGIA,
                   CLU_STAT,
                   JOB_ID,
                   DATA_IMPORT)
           VALUES (SRC_CLU.CLU_CLUSTER_COD,
                   SRC_CLU.CLU_DESCRIZIONE,
                   SRC_CLU.CLU_DATA_INIZIO,
                   SRC_CLU.CLU_DATA_FINE,
                   SRC_CLU.CLU_TIPOLOGIA,
                   SRC_CLU.CLU_STAT,
                   PO_ELAB_ROW.JOB_ID,
                   SYSTIMESTAMP)
              WHEN MATCHED
              THEN
            UPDATE SET CLU.CLU_DESCRIZIONE = SRC_CLU.CLU_DESCRIZIONE,
                       CLU.CLU_DATA_INIZIO = SRC_CLU.CLU_DATA_INIZIO,
                       CLU.CLU_DATA_FINE   = SRC_CLU.CLU_DATA_FINE  ,
                       CLU.CLU_TIPOLOGIA   = SRC_CLU.CLU_TIPOLOGIA  ,
                       CLU.CLU_STAT        = SRC_CLU.CLU_STAT       ,
                       CLU.JOB_ID = PO_ELAB_ROW.JOB_ID,
                       CLU.DATA_IMPORT = SYSTIMESTAMP;
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END;
  BEGIN
    --
    vProcName := 'LOAD_ANA_CLU';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_CLU;
  --
  /**
  * LOAD_ANA_CRC(): procedura che carica l'associazione cluster con i  clienti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ANA_CRC(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    PROCEDURE RAWINSERT
    IS
    BEGIN
    --
      INSERT INTO ANA_CARTE_REL_CLU
      (CRC_CARTA_REL_CLU_ID,
       CRT_CARTA_COD_XK,
       CLU_CLUSTER_COD_XK,
       CRC_DATA_INIZIO,
       CRC_DATA_FINE,
       CRC_STAT,
       JOB_ID,
       DATA_IMPORT
      )
      SELECT BASE_UTIL_PCK.MG_GUID,
             ID_CARD_FK,
             ID_CLU_FK,
             DT_START,
             DT_END  ,
             DECODE(FLG_STATE,0,NULL, 'A'),
             PO_ELAB_ROW.JOB_ID,
             SYSTIMESTAMP
        FROM KCRM.OCARD_CLU_XREF@KPROMO SRC_CRC
       WHERE NOT EXISTS(SELECT 0
                          FROM ANA_CARTE_REL_CLU CRC
                         WHERE CRC.CRT_CARTA_COD_XK   = SRC_CRC.ID_CARD_FK
                           AND CRC.CLU_CLUSTER_COD_XK = SRC_CRC.ID_CLU_FK
                         );
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
      --
      FOR rec in (SELECT CRC.CRC_CARTA_REL_CLU_ID,
                         ID_CARD_FK CRT_CARTA_COD,
                         ID_CLU_FK CLU_CLUSTER_COD,
                         DT_START CRC_DATA_INIZIO,
                         DT_END CRC_DATA_FINE ,
                         DECODE(FLG_STATE,0,NULL, 'A') CRC_STAT
                    FROM KCRM.OCARD_CLU_XREF@KPROMO SRC_CRC,
                         ANA_CARTE_REL_CLU CRC
                   WHERE CRC.CRT_CARTA_COD_XK   = SRC_CRC.ID_CARD_FK
                     AND CRC.CLU_CLUSTER_COD_XK = SRC_CRC.ID_CLU_FK
                     AND(     TO_CHAR(CRC.CRC_DATA_INIZIO,'YYYYMMDD') != TO_CHAR(SRC_CRC.DT_START,'YYYYMMDD')
                           OR TO_CHAR(CRC.CRC_DATA_FINE,'YYYYMMDD') != TO_CHAR(SRC_CRC.DT_END,'YYYYMMDD')
                           OR NVL(CRC.CRC_STAT,'0') != NVL(FLG_STATE,'0')
                     )
                  )
      LOOP
      --
        UPDATE ANA_CARTE_REL_CLU CRC
           SET CRC.CRC_DATA_INIZIO = rec.CRC_DATA_INIZIO,
               CRC.CRC_DATA_FINE   = rec.CRC_DATA_FINE,
               CRC.CRC_STAT        = rec.CRC_STAT,
               CRC.JOB_ID          = PO_ELAB_ROW.JOB_ID,
               CRC.DATA_IMPORT     = SYSTIMESTAMP
         WHERE CRC.CRC_CARTA_REL_CLU_ID = rec.CRC_CARTA_REL_CLU_ID;
      END LOOP;
      --
    END RAWINSERT;
    --
    PROCEDURE RAWUPDATE
    IS
    BEGIN
    --
      UPDATE ANA_CARTE_REL_CLU CRC
         SET CLU_CLUSTER_COD = (SELECT CLU_CLUSTER_COD
                                  FROM ANA_CLUSTER CLU
                                 WHERE CLU.CLU_CLUSTER_COD= CRC.CLU_CLUSTER_COD_XK)
       WHERE CLU_CLUSTER_COD IS NULL;
      --
      UPDATE ANA_CARTE_REL_CLU CRC
         SET CRT_CARTA_COD = (SELECT CRT_CARTA_COD
                                  FROM ANA_CARTE CRT
                                 WHERE CRT.CRT_CARTA_COD = CRC.CRT_CARTA_COD_XK)
       WHERE CRT_CARTA_COD IS NULL;
    END RAWUPDATE;
    --
  BEGIN
    --
    vProcName := 'LOAD_ANA_CRC';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    RAWUPDATE;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_CRC;
  --
  /**
  * LOAD_VND_VNH(): procedura che carica le testate degli scontrini
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_VND_VNH(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    --
    vAnno           NUMBER;
    vMese           NUMBER;
    vDay            NUMBER;
    vDataDec        VARCHAR2(10);
    --
    PROCEDURE RAWINSERT(P_ANNO NUMBER, P_MESE NUMBER, P_DATA_DEC VARCHAR2, P_DAY NUMBER)
    IS
    --
      PROCEDURE CHECK_VEND_HEAD
      IS
        vCnt NUMBER := 0;
      BEGIN
      --
        vPasso:='CHECK_VEND_HEAD';
        SELECT COUNT(0) INTO vCnt
          FROM VND_VENDITE_HEAD
         WHERE VNH_ANNO = P_ANNO
           AND VNH_MESE = P_MESE
           AND EXTRACT (DAY FROM VNH_DATA_VEND) = vDay
           AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vCnt:'||vCnt ,vPasso);
        IF vCnt > 0 THEN
        --
          DELETE VND_VENDITE_HEAD
           WHERE VNH_ANNO = P_ANNO
             AND VNH_MESE = P_MESE
             AND EXTRACT (DAY FROM VNH_DATA_VEND) = vDay
             AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD;
          --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Delete:'||SQL%ROWCOUNT ,vPasso);
          --

        END IF;
      END CHECK_VEND_HEAD;
      --
      PROCEDURE INSERT_VND_HEAD
      IS

      BEGIN
         --
         --
         vPasso:= 'INSERT_VND_HEAD';
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV_PUNTO_VENDITA_COD:'||PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD ,vPasso);
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||P_DATA_DEC ,vPasso);
         --Step 1. inserisco i venduti non associati a carte
         INSERT INTO VND_VENDITE_HEAD (PDV_PUNTO_VENDITA_COD,
                                       VNH_DATA_VEND,
                                       VNH_ANNO,
                                       VNH_MESE,
                                       VNH_NUMERO_CASSA,
                                       VNH_NUMERO_SCONTRINO,
                                       VNH_CASSIERE_COD,
                                       VNH_CASSIERE,
                                       VNH_SCONTRINO_TOT,
                                       VNH_SCAN_ITEMS_TOT,
                                       VNH_ITEMS_TOT,
                                       JOB_ID,
                                       DATA_IMPORT,
                                       CRT_CARTA_COD_XK,
                                       VNH_RESO,
                                       VNH_ANNULLAMENTO,
                                       VNH_TERM_TYPE,
                                       VNH_DELIVERY_TYPE)
            SELECT HEAD.SID PDV_PUNTO_VENDITA_COD,
                   TO_TIMESTAMP (HEAD.DT_DATE || ' ' || HEAD.DT_TIME,'YYYY-MM-DD hh24:mi:ss') VNH_DATA_VEND,
                   P_ANNO VNH_ANNO,
                   P_MESE VNH_MESE,
                   HEAD.TID VNH_NUMERO_CASSA,
                   HEAD.XACTNBR VNH_NUMERO_SCONTRINO,
                   HEAD.OPID VNH_CASSIERE_COD,
                   HEAD.OPIDNAME VNH_CASSIERE,
                   HEAD.XACTTOTAL VNH_SCONTRINO_TOT,
                   HEAD.SCANITEMS VNH_SCAN_ITEMS_TOT,
                   HEAD.TOTALITEMS VNH_ITEMS_TOT,
                   PO_ELAB_ROW.JOB_ID,
                   SYSDATE,
                   DECODE (LENGTH (LOY.CARD),
                           13, SUBSTR (LOY.CARD, 1, 12),
                           LOY.CARD),
                   CASE WHEN
                      HEAD.PRV_VOID = 0
                   AND
                      HEAD.PRV_VOID2 = 0
                   AND
                      HEAD.XACTTOTAL < 0
                   THEN 'S'
                   ELSE NULL
                   END VNH_RESO,
                   CASE WHEN
                      HEAD.PRV_VOID = 1
                   AND
                      HEAD.PRV_VOID2 = 1
                   THEN 'S'
                   ELSE NULL
                   END VNH_ANNULLAMENTO,
                   HEAD.TERM_TYPE VNH_TERM_TYPE,
                   HEAD.DELIVERY_TYPE VNH_DELIVERY_TYPE
              FROM TL_HEADER@CRSMG HEAD
              LEFT JOIN TL_LOYALTY@CRSMG LOY
                ON HEAD.SID = LOY.SID
               AND HEAD.DT_DATE = LOY.DT_DATE
               AND HEAD.DT_TIME = LOY.DT_TIME
               AND HEAD.TID = LOY.TID
               AND HEAD.XACTNBR = LOY.XACTNBR
             WHERE HEAD.SID = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
               AND HEAD.DT_DATE = P_DATA_DEC
               AND HEAD.XVOID = 0 --
               AND HEAD.XSUSP = 0 --Escludo le sospese
               AND HEAD.XACTTOTAL <> 0 --Escludo gli scontrini di servizio
               --
               --AND NOT EXISTS(SELECT /*+ PUSH_SUBQ USE_NL(VNH) INDEX(VNH) */ 0
               --                 FROM VND_VENDITE_HEAD VNH
               --                WHERE VNH.PDV_PUNTO_VENDITA_COD = HEAD.SID
               --                  AND VNH.VNH_DATA_VEND = TO_TIMESTAMP (HEAD.DT_DATE || ' ' || HEAD.DT_TIME,'YYYY-MM-DD hh24:mi:ss')
               --                  AND VNH_ANNO = P_ANNO
               --                  AND VNH_MESE = P_MESE
               --                  AND VNH.VNH_NUMERO_CASSA = HEAD.TID
               --                  AND VNH.VNH_NUMERO_SCONTRINO = HEAD.XACTNBR)
                              ;
              BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROW INSERT:'||SQL%ROWCOUNT ,vPasso);

      END INSERT_VND_HEAD;
    --
      PROCEDURE UPDT_CARD_OK
      IS
      BEGIN
         FOR rec IN (SELECT HEAD.VNH_VENDUTO_HEAD_ID,
                            PDV_PUNTO_VENDITA_COD,
                            CRT_CARTA_COD,
                            CLI_CLIENTE_COD,
                            CRT_CARTA_COD_XK
                       FROM VND_VENDITE_HEAD HEAD
                      WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                        AND HEAD.PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                        AND CRT_CARTA_COD_XK IS NOT NULL
                        AND VNH_ANNO = P_ANNO
                        AND VNH_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_HEAD
               SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                      (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                         FROM ANA_CARTE
                        WHERE CRT_CARTA_COD = rec.CRT_CARTA_COD_XK)
             WHERE VNH_VENDUTO_HEAD_ID = rec.VNH_VENDUTO_HEAD_ID
               AND VNH_ANNO = P_ANNO
               AND VNH_MESE = P_MESE;
            --
         END LOOP;
      END UPDT_CARD_OK;
    --
      PROCEDURE UPDT_CARD_RECUPERO
      IS
      BEGIN
         FOR rec IN (SELECT HEAD.VNH_VENDUTO_HEAD_ID,
                            PDV_PUNTO_VENDITA_COD,
                            CRT_CARTA_COD,
                            CLI_CLIENTE_COD,
                            CRT_CARTA_COD_XK
                       FROM VND_VENDITE_HEAD HEAD
                      WHERE CRT_CARTA_COD_XK IS NOT NULL
                        AND (CRT_CARTA_COD IS NULL
                             OR CLI_CLIENTE_COD = '-1'
                             OR CLI_CLIENTE_COD IS NULL)
                        AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                        AND VNH_ANNO = P_ANNO
                        AND VNH_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_HEAD
               SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                      (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                         FROM ANA_CARTE
                        WHERE CRT_CARTA_COD = rec.CRT_CARTA_COD_XK)
             WHERE VNH_VENDUTO_HEAD_ID = rec.VNH_VENDUTO_HEAD_ID
               AND VNH_ANNO = P_ANNO
               AND VNH_MESE = P_MESE;
         END LOOP;
      END UPDT_CARD_RECUPERO;
    BEGIN
    --
      CHECK_VEND_HEAD;
      INSERT_VND_HEAD;
      PO_ELAB_ROW.ELB_ROWS     := SQL%ROWCOUNT;
      --
      UPDT_CARD_OK;
      --
      UPDT_CARD_RECUPERO;
      --
    END RAWINSERT;
  --
  BEGIN
    --
    vProcName := 'LOAD_VND_VNH';
    --
    vPasso := 'MAKE_DATA';
    SELECT EXTRACT (YEAR FROM PO_ELAB_ROW.ELB_DATA_DEC)  INTO vAnno FROM DUAL;
    SELECT EXTRACT (MONTH FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vMese FROM DUAL;
    SELECT EXTRACT (DAY FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vDay FROM DUAL;
    vDataDec := vAnno ||'-' ||LPAD(vMese,2,'0') ||'-'||LPAD(vDay,2,'0');
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDec:'||vDataDec ,vPasso);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO ,vPasso);
    --
    vPasso := 'RAWINSERT';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_ANNO:'||vAnno ,vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_MESE:'||vMese ,vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||vDataDec ,vPasso);
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'PDV:'||PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD, BASE_LOG_PCK.Info);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'P_DATA_DEC:'||vDataDec, BASE_LOG_PCK.Info);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName ||'PDV=' || PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD, 'P_DATA_DEC:'||vDataDec, BASE_LOG_PCK.Info);
    --
    RAWINSERT(P_ANNO => vAnno, P_MESE => vMese, P_DATA_DEC=> vDataDec, P_DAY=> vDay);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO ,vPasso);
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VND_VNH;
  --
  /**
  * LOAD_VND_VND(): procedura che carica il dettgalio degli scontrini
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_VND_VND(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    --
    vAnno           NUMBER;
    vMese           NUMBER;
    vDay            NUMBER;
    vDataDec        VARCHAR2(10);
    --
    PROCEDURE RAWINSERT(P_ANNO NUMBER, P_MESE NUMBER, P_DATA_DEC VARCHAR2, P_DAY NUMBER)
    IS
    --
      PROCEDURE CHECK_VEND_DETT
      IS
        vCnt NUMBER := 0;
      BEGIN
      --
        SELECT /*+ PARALLEL(DET 4) */ COUNT(0) INTO vCnt
          FROM VND_VENDITE_DETAIL DET
         WHERE VND_ANNO = P_ANNO
           AND VND_MESE = P_MESE
           AND EXTRACT (DAY FROM VND_DATA_VEND) = vDay
           AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD;
        --
        IF vCnt > 0 THEN
        --
          DELETE VND_VENDITE_DETAIL
           WHERE VND_ANNO = P_ANNO
             AND VND_MESE = P_MESE
             AND EXTRACT (DAY FROM VND_DATA_VEND) = vDay
             AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD;
          --

        END IF;
      END CHECK_VEND_DETT;
      --
      PROCEDURE INSERT_VND_DETT
      IS
      --
      BEGIN
         --Step 1. inserisco i venduti non associati a carte
         INSERT INTO VND_VENDITE_DETAIL(VND_DATA_VEND,
                                        PDV_PUNTO_VENDITA_COD,
                                        VND_NUMERO_CASSA,
                                        VND_NUMERO_SCONTRINO,
                                        VND_CASSIERE_COD,
                                        VND_PROGRESSIVO_RIGA,
                                        ART_ARTICOLO_COD_XK,
                                        VND_EAN_SCAN,
                                        VND_TIPO_ART,
                                        VND_QUANTITA,
                                        VND_PREZZO_UNITARIO,
                                        VND_IMPORTO_NETTO,
                                        VND_PERCENTUALE_IVA,
                                        VND_ARTICOLO_STORNATO,
                                        VND_STORNO_ARTICOLO,
                                        VND_ARTICOLO_RESO,
                                        VND_FORNITORE_COD,
                                        VND_SOURCE,
                                        CRT_CARTA_COD_XK,
                                        VND_ANNO,
                                        VND_MESE,
                                        JOB_ID,
                                        DATA_IMPORT,
                                        PRM_PROMOZIONE_COD_XK,
                                        VND_SCONTO_PROMO,
                                        VND_EAN_ORDER)
         WITH PROMO AS (SELECT DSCNT.SID,
                               DSCNT.DT_DATE,
                               DSCNT.DT_TIME,
                               DSCNT.XACTNBR,
                               DSCNT.TID,
                               DSCNT.ITMIDX,
                               TPROMO.ID_PROMO_OFF,
                               DSCNT.DISCOUNT
                          FROM TL_ITEMS_DSC@CRSMG DSCNT,
                                KCRM.OPROMO_HEAD@KPROMO TPROMO,
                                MGDWH.ANA_PROMOZIONI_DETT DPROMO
                          WHERE DSCNT.COMBID = TPROMO.PROMO_LINK_ID(+)
                            AND TO_NUMBER (DSCNT.INTCODE) = DPROMO.ART_ARTICOLO_COD_XK(+)
                            AND TPROMO.PROMO_HOST_ID = DPROMO.PRD_HOST_ID(+)
                            AND DSCNT.DT_DATE = P_DATA_DEC
                            AND DPROMO.PRD_HOST_ID IS NOT NULL
                            AND DSCNT.DISCTYPE IN (1, 2, 8)
                          )
         SELECT TO_TIMESTAMP (DVEND.DT_DATE || ' ' || DVEND.DT_TIME, 'YYYY-MM-DD hh24:mi:ss') VND_DATA_VEND,
                DVEND.SID PDV_PUNTO_VENDITA_COD,
                DVEND.TID VND_NUMERO_CASSA,
                DVEND.XACTNBR VND_NUMERO_SCONTRINO,
                DVEND.OPID VNH_CASSIERE_COD,
                DVEND.ITMIDX VND_PROGRESSIVO_RIGA,
                TO_NUMBER (DVEND.INTCODE) ART_ARTICOLO_COD,
                DVEND.EAN_SCAN VND_EAN_SCAN,
                DECODE (DVEND.ITM_WGT, 0, 'U', 'P') VND_TIPO_ART,
                CASE
                   WHEN DVEND.ITM_WGT = 0 THEN DVEND.QTY
                   ELSE ROUND (DVEND.QTY / 1000, 3)
                END VND_QUANTITA,
                DVEND.UNITP AS VND_PREZZO_UNITARIO,
                DVEND.NET_AMT AS VND_IMPORTO_VENDUTO,
                DVEND.VATRATE / 100 AS VND_PERCENTUALE_IVA,
                DVEND.ITM_VDED AS VND_ARTICOLO_STORNATO,
                DVEND.ITM_VOID AS VND_STORNO_ARTICOLO,
                DVEND.ITM_REF AS VND_ARTICOLO_RESO,
                DVEND.SUPPCODE AS VND_FORNITORE_COD,
                'VS' AS VND_SOURCE,
                LOY.CARD CRT_CARTA_COD_XK,
                P_ANNO VND_ANNO,
                P_MESE VND_MESE,
                PO_ELAB_ROW.JOB_ID JOB_ID,
                SYSDATE DATA_IMPORT,
                PRM.ID_PROMO_OFF PRM_PROMOZIONE_COD,
                PRM.DISCOUNT VND_SCONTO_PROMO,
                DVEND.EAN_ORDER
           FROM TL_ITEMS_NOR@CRSMG DVEND
      LEFT JOIN TL_LOYALTY@CRSMG LOY
             ON DVEND.SID = LOY.SID
            AND DVEND.DT_DATE = LOY.DT_DATE
            AND DVEND.DT_TIME = LOY.DT_TIME
            AND DVEND.TID = LOY.TID
            AND DVEND.XACTNBR = LOY.XACTNBR
      LEFT JOIN PROMO PRM
             ON DVEND.SID = PRM.SID
            AND DVEND.DT_DATE = PRM.DT_DATE
            AND DVEND.DT_TIME = PRM.DT_TIME
            AND DVEND.TID = PRM.TID
            AND DVEND.XACTNBR = PRM.XACTNBR
            AND DVEND.ITMIDX = PRM.ITMIDX
          WHERE DVEND.SID = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
            AND DVEND.DT_DATE = P_DATA_DEC
            --AND NOT EXISTS
            --       (SELECT /*+ PUSH_SUBQ USE_NL(VND) INDEX(VND) */ 0
            --          FROM VND_VENDITE_DETAIL VND
            --         WHERE VND.PDV_PUNTO_VENDITA_COD = DVEND.SID
            --           AND VND.PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
            --           AND VND.VND_DATA_VEND = TO_TIMESTAMP (
            --                          DVEND.DT_DATE || ' ' || DVEND.DT_TIME,
            --                           'YYYY-MM-DD hh24:mi:ss')
            --           AND VND_ANNO = P_ANNO
            --           AND VND_MESE = P_MESE
            --           AND VND.VND_NUMERO_CASSA = DVEND.TID
            --           AND VND.VND_NUMERO_SCONTRINO = DVEND.XACTNBR)
          ;
            --
      END INSERT_VND_DETT;
    --
      PROCEDURE UPDT_CARD_XK
      IS
      BEGIN
         FOR rec IN (SELECT DET.VND_VENDUTO_DETAIL_ID,
                            PDV_PUNTO_VENDITA_COD,
                            CRT_CARTA_COD,
                            CLI_CLIENTE_COD,
                            DECODE (LENGTH (CRT_CARTA_COD_XK), 13, SUBSTR (CRT_CARTA_COD_XK, 1, 12), CRT_CARTA_COD_XK) CRT_CARTA_COD_XK
                       FROM VND_VENDITE_DETAIL DET
                      WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                        AND DET.PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                        AND CRT_CARTA_COD_XK IS NOT NULL
                        AND VND_ANNO = P_ANNO
                        AND VND_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_DETAIL
               SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                      (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                         FROM ANA_CARTE
                        WHERE CRT_CARTA_COD = rec.CRT_CARTA_COD_XK)
             WHERE VND_VENDUTO_DETAIL_ID = rec.VND_VENDUTO_DETAIL_ID
               AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
               AND VND_ANNO = P_ANNO
               AND VND_MESE = P_MESE;
            --
         END LOOP;
      END UPDT_CARD_XK;
      --
      PROCEDURE UPDT_ART_XK
      IS
        BEGIN
          FOR rec IN (SELECT DET.VND_VENDUTO_DETAIL_ID,
                             PDV_PUNTO_VENDITA_COD,
                             ART_ARTICOLO_COD,
                             ART_ARTICOLO_COD_XK
                        FROM VND_VENDITE_DETAIL DET
                       WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                         AND DET.PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                         AND ART_ARTICOLO_COD_XK IS NOT NULL
                         AND VND_ANNO = P_ANNO
                         AND VND_MESE = P_MESE
                         )
          LOOP
          --
            UPDATE VND_VENDITE_DETAIL
               SET ART_ARTICOLO_COD =
                      (SELECT ART_ARTICOLO_COD
                         FROM ANA_ARTICOLI
                        WHERE ART_ARTICOLO_COD = rec.ART_ARTICOLO_COD_XK)
              WHERE VND_VENDUTO_DETAIL_ID = rec.VND_VENDUTO_DETAIL_ID
                AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                AND VND_ANNO = P_ANNO
                AND VND_MESE = P_MESE;
             --
        END LOOP;
      END UPDT_ART_XK;
      --
      PROCEDURE UPDT_PRM_XK
      IS
        BEGIN
          FOR rec IN (SELECT DET.VND_VENDUTO_DETAIL_ID,
                             PDV_PUNTO_VENDITA_COD,
                             ART_ARTICOLO_COD,
                             PRM_PROMOZIONE_COD_XK
                        FROM VND_VENDITE_DETAIL DET
                       WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                         AND DET.PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                         AND PRM_PROMOZIONE_COD_XK IS NOT NULL
                         AND VND_ANNO = P_ANNO
                         AND VND_MESE = P_MESE
                         )
          LOOP
          --
          NULL;
            UPDATE VND_VENDITE_DETAIL
               SET PRM_PROMOZIONE_COD =
                      (SELECT PRM_PROMOZIONE_COD
                         FROM ANA_PROMOZIONI
                        WHERE PRM_PROMOZIONE_COD = rec.PRM_PROMOZIONE_COD_XK)
              WHERE VND_VENDUTO_DETAIL_ID = rec.VND_VENDUTO_DETAIL_ID
                AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                AND VND_ANNO = P_ANNO
                AND VND_MESE = P_MESE;
             --
        END LOOP;
      NULL;
      END UPDT_PRM_XK;
      --
      PROCEDURE UPDT_CARD_RECUPERO
      IS
      BEGIN
         FOR rec IN (SELECT DET.VND_VENDUTO_DETAIL_ID,
                            CRT_CARTA_COD,
                            CLI_CLIENTE_COD,
                            CRT_CARTA_COD_XK
                       FROM VND_VENDITE_DETAIL DET
                      WHERE CRT_CARTA_COD_XK IS NOT NULL
                        AND (CRT_CARTA_COD IS NULL
                             OR CLI_CLIENTE_COD = '-1'
                             OR CLI_CLIENTE_COD IS NULL)
                        AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD
                        AND VND_ANNO = P_ANNO
                        AND VND_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_DETAIL
               SET (CRT_CARTA_COD, CLI_CLIENTE_COD) =
                      (SELECT CRT_CARTA_COD, CLI_CLIENTE_COD
                         FROM ANA_CARTE
                        WHERE CRT_CARTA_COD = rec.CRT_CARTA_COD_XK)
             WHERE VND_VENDUTO_DETAIL_ID = rec.VND_VENDUTO_DETAIL_ID
               AND VND_ANNO = P_ANNO
               AND VND_MESE = P_MESE;
         END LOOP;
      END UPDT_CARD_RECUPERO;
      --
    BEGIN
    --
      vPasso := 'CHECK_VEND_DETT';
      CHECK_VEND_DETT;
      vPasso := 'INSERT_VND_DETT';
      INSERT_VND_DETT;
      PO_ELAB_ROW.ELB_ROWS     := SQL%ROWCOUNT;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'InsertRows:'||PO_ELAB_ROW.ELB_ROWS , vPasso);
      --
      vPasso := 'UPDT_CARD_XK';
      UPDT_CARD_XK;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'UpdtRows:'||SQL%ROWCOUNT , vPasso);
      --
      vPasso := 'UPDT_ART_XK';
      UPDT_ART_XK;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'UpdtRows:'||SQL%ROWCOUNT , vPasso);
      --
      vPasso := 'UPDT_PRM_XK';
      UPDT_PRM_XK;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'UpdtRows:'||SQL%ROWCOUNT , vPasso);
      --
      vPasso := 'UPDT_CARD_RECUPERO';
      UPDT_CARD_RECUPERO;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'UpdtRows:'||SQL%ROWCOUNT , vPasso);
      ----
    END RAWINSERT;
  --
  BEGIN
    --
    vProcName := 'LOAD_VND_VND';
    --
    vPasso := 'VAL_VAR';
    SELECT EXTRACT (YEAR FROM PO_ELAB_ROW.ELB_DATA_DEC)  INTO vAnno FROM DUAL;
    SELECT EXTRACT (MONTH FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vMese FROM DUAL;
    SELECT EXTRACT (DAY FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vDay FROM DUAL;
    vDataDec := vAnno ||'-' ||LPAD(vMese,2,'0') ||'-'||LPAD(vDay,2,'0');
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'vDataDec:'||vDataDec , vPasso);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT(P_ANNO => vAnno, P_MESE => vMese, P_DATA_DEC=> vDataDec, P_DAY => vDay);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VND_VND;
  --
  /**
  * LOAD_VND_VNS(): procedura che carica il dettaglio degli sconti
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_VND_VNS(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    vAnno           NUMBER;
    vMese           NUMBER;
    vDay            NUMBER;
    vDataDec        VARCHAR2(10);
    --
    PROCEDURE RAWINSERT(P_ANNO NUMBER, P_MESE NUMBER, P_DATA_DEC VARCHAR2, P_DAY NUMBER)
    IS
    --
      PROCEDURE CHECK_VND_VNS
      IS
        vCnt NUMBER := 0;
      BEGIN
      --
        SELECT COUNT(0) INTO vCnt
          FROM VND_VENDITE_SCONTI
         WHERE VNS_ANNO = P_ANNO
           AND VNS_MESE = P_MESE
           AND EXTRACT (DAY FROM VNS_DATA_VEND) = vDay;
        --
        IF vCnt > 0 THEN
        --
          DELETE VND_VENDITE_SCONTI
           WHERE VNS_ANNO = P_ANNO
             AND VNS_MESE = P_MESE
             AND EXTRACT (DAY FROM VNS_DATA_VEND) = vDay;
          --

        END IF;
      END CHECK_VND_VNS;
      --
      PROCEDURE INSERT_VND_VNS
      IS
      BEGIN
         --
         vPasso:= 'INSERT_VND_VNS';
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||P_DATA_DEC ,vPasso);
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC(kFormatMask2):'||TO_TIMESTAMP(P_DATA_DEC, kFormatMask2) ,vPasso);
         --
         INSERT INTO VND_VENDITE_SCONTI
                      (VNS_DATA_VEND,
                       VNS_ANNO,
                       VNS_MESE,
                       VNS_SETTIMANA,
                       PDV_PUNTO_VENDITA_COD,
                       VNS_NUMERO_CASSA,
                       VNS_NUMERO_SCONTRINO,
                       VNS_NUMERO_RIGA_SCONTRINO,
                       VNS_EAN_SCAN,
                       VNS_NUM_SCNT,
                       VNS_IMPORTO_SCNT,
                       VNS_SCONTO,
                       VNS_QUANTITA,
                       VNS_NETTO_RIGA,
                       VNS_PREZZO_LORDO,
                       VNS_PROMO_LINK,
                       VNS_MECC_DESCR,
                       CRT_CARTA_COD_XK,
                       PRM_PROMOZIONE_COD_XK,
                       ART_ARTICOLO_COD_XK,
                       JOB_ID,
                       DATA_IMPORT,
                       VNS_SCONTO_DESCR,
                       VNS_DISCTYPE)
               WITH PRO_H AS ( SELECT PH.ID_PROMO_OFF,
                                      PH.ID_PROMO_HEAD,
                                      TO_NUMBER(PH.PROMO_LINK_ID) LINK_ID
                                 FROM KCRM.OPROMO_HEAD@KPROMO PH
                                WHERE TO_TIMESTAMP(P_DATA_DEC, kFormatMask2) BETWEEN PH.DT_START AND PH.DT_END
                              )
               SELECT TO_TIMESTAMP(DSC.DT_DATE || ' ' || DSC.DT_TIME, 'yyyy-mm-dd hh24:mi:ss')                    AS VNS_DATA_VEND,
                      TO_NUMBER(SUBSTR(DSC.DT_DATE, 1, 4))                                                        AS VNS_ANNO,
                      TO_NUMBER(SUBSTR(DSC.DT_DATE, 6, 2))                                                        AS VNS_MESE,
                      TO_NUMBER(TO_CHAR(TO_DATE(DSC.DT_DATE, kFormatMask2),'IW'))                                 AS VNS_SETTIMANA,
                      DSC.SID                                                                                     AS PDV_PUNTO_VENDITA_COD,
                      DSC.TID                                                                                     AS VNS_NUMERO_CASSA,
                      DSC.XACTNBR                                                                                 AS VNS_NUMERO_SCONTRINO,
                      DSC.ITMIDX                                                                                  AS VNS_NUMERO_RIGA_SCONTRINO,
                      DSC.EAN_SCAN                                                                                AS VNS_EAN_SCAN,
                      DSC.QTY                                                                                     AS VNS_NUM_SCNT,
                      DSC.AMOUNT                                                                                  AS VNS_IMPORTO_SCNT,
                      DSC.DISCOUNT                                                                                AS VNS_SCONTO,
                      CASE WHEN TLI.ITM_WGT = 0 THEN TLI.QTY ELSE ROUND(TLI.QTY / 1000, 3) END                    AS VNS_QUANTITA,
                      TLI.NET_AMT                                                                                 AS VNS_NETTO_RIGA,
                      TLI.UNITP                                                                                   AS VNS_PREZZO_LORDO,
                      DSC.COMBID                                                                                  AS VNS_PROMO_LINK,
                      NVL(OGC.RCP_DESCR_1, '---')                                                                 AS VNS_MECC_DESCR,
                      NVL(TLY.CARD, '0000000000000')                                                              AS CRT_CARTA_COD_XK,
                      NVL(PRO_H.ID_PROMO_OFF, '---')                                                              AS PRM_PROMOZIONE_COD_XK,
                      DSC.INTCODE                                                                                 AS ART_ARTICOLO_COD_XK,
                      PO_ELAB_ROW.JOB_ID                                                                          AS JOB_ID,
                      SYSTIMESTAMP                                                                                AS DATA_IMPORT,
                      DSC.COMBIDESCR                                                                              AS VNS_SCONTO_DESCR,
                      DSC.DISCTYPE                                                                                AS VNS_DISCTYPE
                 FROM ORACLEADMIN.TL_ITEMS_DSC@CRSMG      DSC,
                      PRO_H,
                      ORACLEADMIN.TL_LOYALTY@CRSMG        TLY,
                      KCRM.OPROMO_GEN_COND@KPROMO         OGC,
                      ORACLEADMIN.TL_ITEMS_NOR@CRSMG      TLI
                WHERE DSC.COMBID = PRO_H.LINK_ID (+)
                   AND DSC.SID = TLY.SID (+)
                   AND DSC.DT_DATE = TLY.DT_DATE (+)
                   AND DSC.DT_TIME = TLY.DT_TIME (+)
                   AND DSC.TID = TLY.TID (+)
                   AND DSC.XACTNBR = TLY.XACTNBR (+)
                   AND DSC.SID = TLI.SID (+)
                   AND DSC.DT_DATE = TLI.DT_DATE (+)
                   AND DSC.DT_TIME = TLI.DT_TIME (+)
                   AND DSC.TID = TLI.TID (+)
                   AND DSC.XACTNBR = TLI.XACTNBR (+)
                   AND DSC.ITMIDX = TLI.ITMIDX (+)
                   AND PRO_H.ID_PROMO_OFF = OGC.ID_PROMO_OFF_FK (+)
                   AND PRO_H.ID_PROMO_HEAD = OGC.ID_PROMO_HEAD_FK (+)
                   --AND DSC.DISCTYPE IN (1,2,8)
                   AND DSC.DT_DATE = P_DATA_DEC --localtimestamp -1
                   AND DSC.ITM_VOID = 0
                   AND DSC.ITM_VDED = 0;
         --
      END INSERT_VND_VNS;
      --
      PROCEDURE UPDT_ART_XK
      IS
        BEGIN
          --
          UPDATE VND_VENDITE_SCONTI VNS
             SET ART_ARTICOLO_COD =
                    (SELECT ART_ARTICOLO_COD
                       FROM ANA_ARTICOLI
                      WHERE ART_ARTICOLO_COD = TO_NUMBER (VNS.ART_ARTICOLO_COD_XK))
           WHERE VNS_ANNO = P_ANNO
             AND ART_ARTICOLO_COD IS NULL;
             --
      END UPDT_ART_XK;
      --
      PROCEDURE UPDT_PRM_XK
      IS
        BEGIN
          UPDATE VND_VENDITE_SCONTI VNS
             SET PRM_PROMOZIONE_COD = (SELECT PRM_PROMOZIONE_COD
                                         FROM ANA_PROMOZIONI
                                        WHERE PRM_PROMOZIONE_COD = VNS.PRM_PROMOZIONE_COD_XK)
           WHERE VNS_ANNO = P_ANNO
             AND PRM_PROMOZIONE_COD IS NULL
             AND PRM_PROMOZIONE_COD_XK IS NOT NULL;
      END UPDT_PRM_XK;
      --
      PROCEDURE UPDT_CRT_XK
      IS
        BEGIN
          UPDATE VND_VENDITE_SCONTI VNS
             SET CRT_CARTA_COD = (SELECT CRT_CARTA_COD
                                    FROM ANA_CARTE
                                   WHERE CRT_EAN_COD = VNS.CRT_CARTA_COD_XK)
           WHERE VNS_ANNO = P_ANNO
             AND CRT_CARTA_COD IS NULL
             AND CRT_CARTA_COD_XK IS NOT NULL;
      END UPDT_CRT_XK;
      --
    BEGIN
    --
      CHECK_VND_VNS;
      --
      INSERT_VND_VNS;
      --
      PO_ELAB_ROW.ELB_ROWS     := SQL%ROWCOUNT;
      --
      COMMIT;
      --
      UPDT_ART_XK;
      COMMIT;
      --
      UPDT_PRM_XK;
      COMMIT;
      --
      UPDT_CRT_XK;
      COMMIT;
      --
    END RAWINSERT;

BEGIN
    --
    vProcName := 'LOAD_VND_VNS';
    --
    vPasso := 'VAL_VAR';
    SELECT EXTRACT (YEAR FROM PO_ELAB_ROW.ELB_DATA_DEC)  INTO vAnno FROM DUAL;
    SELECT EXTRACT (MONTH FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vMese FROM DUAL;
    SELECT EXTRACT (DAY FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vDay FROM DUAL;
    vDataDec := vAnno ||'-' ||LPAD(vMese,2,'0') ||'-'||LPAD(vDay,2,'0');
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'vDataDec:'||vDataDec , vPasso);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT(P_ANNO => vAnno, P_MESE => vMese, P_DATA_DEC=> vDataDec, P_DAY => vDay);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VND_VNS;
  --
  /**
  * LOAD_ORD_HEAD(): procedura che carica le testate degli ordini click...
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ORD_ORH(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    vDataDec        TIMESTAMP;
    --
    PROCEDURE RAWINSERT(P_DATA_DEC TIMESTAMP)
    IS
    --
      PROCEDURE CHECK_ORD_HEAD
      IS
        vCnt NUMBER := 0;
      BEGIN
      --
        SELECT COUNT(0) INTO vCnt
          FROM ORD_ORDINI_HEAD
         WHERE TRUNC(ORH_DATA_CONSEGNA) = P_DATA_DEC;
        --
        IF vCnt > 0 THEN
        --
          DELETE ORD_ORDINI_HEAD
           WHERE TRUNC(ORH_DATA_CONSEGNA) = P_DATA_DEC;
          --

        END IF;
      END CHECK_ORD_HEAD;
      --
      PROCEDURE INSERT_ORD_HEAD
      IS
      BEGIN
         --
         vPasso:= 'INSERT_ORD_HEAD';
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||P_DATA_DEC ,vPasso);
         --BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC(kFormatMask2):'||TO_TIMESTAMP(P_DATA_DEC, kFormatMask2) ,vPasso);
         --
         INSERT INTO ORD_ORDINI_HEAD
                        (ORH_ORDINE_HEAD_ID,
                         ORH_ORDINE_COD,
                         ORH_ORDINE_EAN,
                         ORH_TOTALE,
                         ORH_NOME,
                         ORH_COGNOME,
                         ORH_CITTA,
                         ORH_PROVINCIA,
                         ORH_CAP,
                         ORH_VIA,
                         ORH_CIVICO,
                         ORH_VIA_EXT,
                         ORH_MOBILE,
                         ORH_EMAIL,
                         ORH_DATA_RICHIESTA,
                         ORH_DATA_CONSEGNA,
                         ORH_FASCIA_CONSEGNA,
                         ORH_NOTE,
                         STR_STATO_NOTE,
                         ORH_NUMERO_COLLI,
                         CLI_CLIENTE_COD_XK,
                         CRT_CARTA_COD_XK,
                         STR_STATO_ORDINE_COD_XK,
                         SRV_SERVIZIO_COD_XK,
                         PDV_PUNTO_VENDITA_COD_XK,
                         OPE_OPERATORE_COD_XK,
                         MOD_PAGAMENTO_COD_XK,
                         JOB_ID,
                         DATA_IMPORT)
              SELECT HEAD.ID,
                     ORDER_CODE,
                     ORDER_BARCODE,
                     TOTAL,
                     SH_NAME,
                     SH_SURNAME,
                     SH_CITY,
                     SH_PROVINCE,
                     SH_ZIP_CODE,
                     SH_ADDRESS_1,
                     SH_ADDRESS_NUMBER,
                     SH_ADDRESS_2,
                     SH_MOBILE,
                     SH_EMAIL,
                     COALESCE(HEAD.TS_INS, HEAD.TS_UPD, TS_STATUS) ORH_DATA_RICHIESTA,
                     TRUNC(DELIVERY_DATE) ORH_DATA_CONSEGNA,
                     SUBSTR (TIME_START, 1, 2) || ':' || SUBSTR (TIME_START, 3, 4)
                             ||'-'||
                     SUBSTR (TIME_END, 1, 2) || ':' || SUBSTR (TIME_END, 3, 4),
                     SH_NOTES,
                     STATUS_NOTE,
                     SH_PACKAGE,
                     CUSTOMER_HOST_ID,
                     CARD_NO,
                     ORDER_STATUS,
                     HEAD.DELIVERY_PID,
                     HEAD.STORE_ID,
                     OPERATOR_ID,
                     TID_ID,
                     PO_ELAB_ROW.JOB_ID      AS JOB_ID,
                     SYSTIMESTAMP            AS DATA_IMPORT
                FROM CAC.ORDER_HEADER@KPROMO HEAD,
                     CAC.DELIVERY_STORE@KPROMO DELY
               WHERE HEAD.DELIVERY_STORE_ID = DELY.ID
                 AND HEAD.DELIVERY_PID = DELY.DELIVERY_PID
                 AND HEAD.STORE_ID = DELY.STORE_ID
                 AND TRUNC(DELIVERY_DATE) = TRUNC(P_DATA_DEC)
                 AND ORDER_STATUS > 0;
                  --
      END INSERT_ORD_HEAD;
      --
      PROCEDURE UPDT_SRV_XK
      IS
        BEGIN
          --
          UPDATE ORD_ORDINI_HEAD
             SET SRV_SERVIZIO_COD =(SELECT SRV_SERVIZIO_COD
                                      FROM ORD_SERVIZI
                                     WHERE SRV_SERVIZIO_COD = SRV_SERVIZIO_COD_XK)
           WHERE SRV_SERVIZIO_COD IS NULL;
             --
      END UPDT_SRV_XK;
      --
      PROCEDURE UPDT_STR_XK
      IS
        BEGIN
          UPDATE ORD_ORDINI_HEAD
             SET STR_STATO_ORDINE_COD =(SELECT STR_STATO_ORDINE_COD
                                          FROM ORD_STATI_ORDINE
                                         WHERE STR_STATO_ORDINE_COD = STR_STATO_ORDINE_COD_XK)
           WHERE STR_STATO_ORDINE_COD IS NULL;
      END UPDT_STR_XK;
      --
      PROCEDURE UPDT_CRT_XK
      IS
        BEGIN
          UPDATE ORD_ORDINI_HEAD ORH
             SET CRT_CARTA_COD = (SELECT CRT_CARTA_COD
                                    FROM ANA_CARTE
                                   WHERE CRT_EAN_COD = ORH.CRT_CARTA_COD_XK)
           WHERE CRT_CARTA_COD IS NULL;
      END UPDT_CRT_XK;
      --
      PROCEDURE UPDT_CLI_XK
      IS
        BEGIN
           UPDATE ORD_ORDINI_HEAD ORH
             SET CLI_CLIENTE_COD = (SELECT CLI_CLIENTE_COD
                                    FROM ANA_CARTE
                                   WHERE CRT_EAN_COD = ORH.CRT_CARTA_COD_XK)
           WHERE CLI_CLIENTE_COD IS NULL OR CLI_CLIENTE_COD = '-1';
           --
      END UPDT_CLI_XK;
      --
      PROCEDURE UPDT_PDV_XK
      IS
        BEGIN
          UPDATE ORD_ORDINI_HEAD
             SET PDV_PUNTO_VENDITA_COD =(SELECT PDV_PUNTO_VENDITA_COD
                                           FROM ANA_PUNTI_VENDITA
                                          WHERE PDV_PUNTO_VENDITA_COD = PDV_PUNTO_VENDITA_COD_XK)
           WHERE PDV_PUNTO_VENDITA_COD IS NULL;
           --
      END UPDT_PDV_XK;
      --
      PROCEDURE UPDT_MOD_XK
      IS
        BEGIN
          UPDATE ORD_ORDINI_HEAD
             SET MOD_PAGAMENTO_COD =(SELECT MOD_PAGAMENTO_COD
                                           FROM ORD_MODALITA_PAGAMENTI
                                          WHERE MOD_PAGAMENTO_COD = MOD_PAGAMENTO_COD_XK)
           WHERE MOD_PAGAMENTO_COD IS NULL;
           --
      END UPDT_MOD_XK;
      --
    BEGIN
    --
      CHECK_ORD_HEAD;
      --
      INSERT_ORD_HEAD;
      --
      PO_ELAB_ROW.ELB_ROWS     := SQL%ROWCOUNT;
      --
      COMMIT;
      --
      UPDT_SRV_XK;
      COMMIT;
      --
      UPDT_STR_XK;
      COMMIT;
      --
      UPDT_CRT_XK;
      COMMIT;
      --
      UPDT_CLI_XK;
      COMMIT;
      --
      UPDT_PDV_XK;
      COMMIT;
      --
      UPDT_MOD_XK;
      COMMIT;
      --
    END RAWINSERT;

BEGIN
    --
    vProcName := 'LOAD_ORD_ORH';
    --
    vDataDec := PO_ELAB_ROW.ELB_DATA_DEC;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'vDataDec:'||vDataDec , vPasso);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT(P_DATA_DEC=> PO_ELAB_ROW.ELB_DATA_DEC);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ORD_ORH;
  --
  /**
  * LOAD_ORD_ORD(): procedura che carica il dettaglio degli ordini click...
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ORD_ORD(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    vDataDec        TIMESTAMP;
    --
    PROCEDURE RAWINSERT(P_DATA_DEC TIMESTAMP)
    IS
    --
      PROCEDURE CHECK_ORD_DETT
      IS
        vCnt NUMBER := 0;
      BEGIN
      --
        SELECT COUNT (0) INTO vCnt
          FROM ORD_ORDINI_DETAIL DETAIL
         WHERE EXISTS(SELECT 0
                        FROM ORD_ORDINI_HEAD HEAD
                       WHERE NVL (DETAIL.ORH_ORDINE_HEAD_ID, DETAIL.ORH_ORDINE_HEAD_ID_XK) = HEAD.ORH_ORDINE_HEAD_ID
                        AND TRUNC (ORH_DATA_CONSEGNA) = TRUNC (P_DATA_DEC));
        --
        IF vCnt > 0 THEN
        --
          DELETE ORD_ORDINI_DETAIL DETAIL
           WHERE EXISTS(SELECT 0
                        FROM ORD_ORDINI_HEAD HEAD
                       WHERE NVL (DETAIL.ORH_ORDINE_HEAD_ID, DETAIL.ORH_ORDINE_HEAD_ID_XK) = HEAD.ORH_ORDINE_HEAD_ID
                        AND TRUNC (ORH_DATA_CONSEGNA) = TRUNC (P_DATA_DEC));
          --

        END IF;
      END CHECK_ORD_DETT;
      --
      PROCEDURE INSERT_ORD_DETT
      IS
      BEGIN
         --
         vPasso:= 'INSERT_ORD_DETT';
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||P_DATA_DEC ,vPasso);
         --
         INSERT INTO ORD_ORDINI_DETAIL (ORD_ORDINI_DETAIL_ID,
                                        ORD_QUANTITA_UM,
                                        ORD_QUANTITA,
                                        ORD_QUANTITA_CALC,
                                        ORD_PREZZO,
                                        ORD_PREZZO_QTY,
                                        ORD_TIPO_ATTR,
                                        ORD_ATTR_VAL,
                                        ORD_STAT,
                                        ORH_ORDINE_HEAD_ID_XK,
                                        ART_ARTICOLO_COD_XK,
                                        STA_STATO_ARTICOLO_COD_XK,
                                        ORD_EAN_COD_SOS_XK,
                                        JOB_ID,
                                        DATA_IMPORT)
               SELECT DET.ID,
                      UPPER (NVL (WEIGTH_UM, DET.QTY_UM)) ORD_QUANTITA_UM,
                      NVL (WEIGTH, QTY) ORD_QUANTITA,
                      CASE
                         WHEN WEIGTH_UM = 'GR_PZ'
                         THEN
                            MGDWH_ELABORAZIONI_PCK.GET_PESO_MEDIO (ITEM_ID) * WEIGTH
                         ELSE
                            NVL (WEIGTH, QTY)
                      END
                         ORD_QUANTITA_CALC,
                      PRICE_UNIT ORD_PREZZO,
                      CASE
                         WHEN UPPER (UPPER (NVL (WEIGTH_UM, DET.QTY_UM))) = 'PZ'
                         THEN
                            PRICE_UNIT * QTY
                         WHEN UPPER (UPPER (NVL (WEIGTH_UM, DET.QTY_UM))) = 'GR_PZ'
                         THEN
                            TRUNC (
                                 PRICE_UNIT
                               / 1000
                               * MGDWH_ELABORAZIONI_PCK.GET_PESO_MEDIO (ITEM_ID)
                               * WEIGTH,
                               2)
                         ELSE
                            TRUNC (PRICE_UNIT / 1000 * WEIGTH, 2)
                      END
                         ORD_PREZZO_QTY,
                      ITEM_TIPO_ATTR ORD_TIPO_ATTR,
                      NULL ORD_ATTR_VAL,
                      DET.FLG_STATE ORD_STAT,
                      ORDER_HEADER_ID ORH_ORDINE_HEAD_ID_XK,
                      ITEM_ID ART_ARTICOLO_COD_XK,
                      ITEM_STATUS STA_STATO_ARTICOLO_COD_XK,
                      ITEM_STATUS_REF ANA_ARTICOLO_COD_SOS_XK,
                      PO_ELAB_ROW.JOB_ID      AS JOB_ID,
                      SYSTIMESTAMP            AS DATA_IMPORT
                 FROM CAC.ORDER_DETAIL@KPROMO DET, CAC.ORDER_HEADER@KPROMO HEAD
                WHERE DET.ORDER_HEADER_ID = HEAD.ID
                  AND TRUNC (DELIVERY_DATE) = TRUNC (P_DATA_DEC)
                  AND HEAD.ORDER_STATUS > 0;
                  --
      END INSERT_ORD_DETT;
      --
      PROCEDURE UPDT_HEAD_ID_XK
      IS
        BEGIN
          --
          UPDATE ORD_ORDINI_DETAIL
             SET ORH_ORDINE_HEAD_ID	= (SELECT ORH_ORDINE_HEAD_ID
                                         FROM ORD_ORDINI_HEAD
                                        WHERE ORH_ORDINE_HEAD_ID = ORH_ORDINE_HEAD_ID_XK)
           WHERE ORH_ORDINE_HEAD_ID IS NULL;
             --
      END UPDT_HEAD_ID_XK;
      --
      PROCEDURE UPDT_ART_XK
      IS
        BEGIN
          UPDATE ORD_ORDINI_DETAIL DET
             SET ART_ARTICOLO_COD =(SELECT ART_ARTICOLO_COD
                                      FROM ANA_ARTICOLI
                                    WHERE ART_ARTICOLO_COD = DET.ART_ARTICOLO_COD_XK)
           WHERE ART_ARTICOLO_COD IS NULL;
      END UPDT_ART_XK;
      --
      PROCEDURE UPDT_STA_XK
      IS
        BEGIN
          UPDATE ORD_ORDINI_DETAIL
             SET STA_STATO_ARTICOLO_COD = MGDWH_UTILITY_PCK.GET_STATO_ORD(STA_STATO_ARTICOLO_COD_XK)
           WHERE STA_STATO_ARTICOLO_COD IS NULL;
      END UPDT_STA_XK;
      --
      PROCEDURE UPDT_ART_SOS_XK
      IS
        BEGIN
           UPDATE ORD_ORDINI_DETAIL
             SET ANA_ARTICOLO_COD_SOS = (SELECT E6CART
                                           FROM MG.GEAD060F@KPROMO
                                          WHERE E6BCOD = ORD_EAN_COD_SOS_XK)
           WHERE ANA_ARTICOLO_COD_SOS IS NULL
             AND ORD_EAN_COD_SOS_XK IS NOT NULL;
           --
      END UPDT_ART_SOS_XK;
      --
    BEGIN
    --
      CHECK_ORD_DETT;
      --
      INSERT_ORD_DETT;
      --
      PO_ELAB_ROW.ELB_ROWS     := SQL%ROWCOUNT;
      --
      COMMIT;
      --
      UPDT_HEAD_ID_XK;
      COMMIT;
      --
      UPDT_ART_XK;
      COMMIT;
      --
      UPDT_STA_XK;
      COMMIT;
      --
      UPDT_ART_SOS_XK;
      COMMIT;
      --
    END RAWINSERT;

BEGIN
    --
    vProcName := 'LOAD_ORD_ORD';
    --
    vDataDec := PO_ELAB_ROW.ELB_DATA_DEC;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'vDataDec:'||vDataDec , vPasso);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT(P_DATA_DEC=> PO_ELAB_ROW.ELB_DATA_DEC);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ORD_ORD;
  --
  FUNCTION GET_PESO_MEDIO(
        P_ARTICOLO_COD ANA_ARTICOLI.ART_ARTICOLO_COD%TYPE
  ) RETURN NUMBER PARALLEL_ENABLE
  IS
    vPesoMedio NUMBER;
  BEGIN
  --
    SELECT AGG.E7KYCI
      INTO vPesoMedio
      FROM MG.DATIAGGIUNTIVI@KPROMO AGG
     WHERE AGG.E7KYAM = 'ARTICOLI'
       AND AGG.e7ky01 = P_ARTICOLO_COD
       AND AGG.e7ky02 = 'CLICK_COLLECT'
       AND AGG.e7ky03 IN ('VAL_MEDIO','OFFSET_SCOST');
      --
     RETURN vPesoMedio;
     --
  EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
  END GET_PESO_MEDIO;
  --
  PROCEDURE GET_INFO_ENTITA(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_ENTITA_COD          IN            VARCHAR2,
      PO_GRADO_PARALLEL        OUT        NUMBER,
      PO_TAB_FILTRI            OUT        TAB_COD
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
  BEGIN
  --
    SELECT ENT_GRADO_PARALLEL, BASE_UTIL_PCK.GetTAB_COD(ENT_FILTRO)
      INTO PO_GRADO_PARALLEL, PO_TAB_FILTRI
      FROM MDWH_ENTITA
     WHERE ENT_ENTITA_COD = P_ENTITA_COD;
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END GET_INFO_ENTITA;
  --
  PROCEDURE LOAD_VENDITE_HEAD_JOB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_JOB_TOT                           NUMBER,
      P_JOB_NUM                           NUMBER,
      P_JOB_ID                            MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_SESSION_ID                        BASE_LOG.LOG_ID_SESSIONE%TYPE,
      P_FILTRI_PDV_CNT                    NUMBER DEFAULT 0,
      P_FILTRI_PDV_TAB                    TAB_COD DEFAULT NULL
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    tsStartTime     TIMESTAMP;
    tsEndTime       TIMESTAMP;
    --
    tsStartTimePDV  TIMESTAMP;
    tsEndTimePDV    TIMESTAMP;
    vVndVNH_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vElabCreata     BOOLEAN;
    ErroreGestito   EXCEPTION;
    vPDV            ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
  BEGIN
  --
    vProcName := 'LOAD_VENDITE_HEAD_JOB';
    -- Start the timer
    vPasso:='START';
    tsStartTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, '', vPasso);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_JOB_TOT:'||P_JOB_TOT, vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'P_JOB_TOT:'||P_JOB_TOT,BASE_LOG_PCK.DEBUG);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_JOB_NUM:'||P_JOB_NUM, vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'P_JOB_NUM:'||P_JOB_NUM,BASE_LOG_PCK.DEBUG);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_FILTRI_PDV_CNT:'||P_FILTRI_PDV_CNT, vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'P_FILTRI_PDV_CNT:'||P_FILTRI_PDV_CNT,BASE_LOG_PCK.DEBUG);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'kStampStartDWH:'||kStampStartDWH, vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'kStampStartDWH:'||kStampStartDWH,BASE_LOG_PCK.DEBUG);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'kStampStartDWH:'||kStampStartDWH,BASE_LOG_PCK.DEBUG);
    --
    FOR rPun IN cPdvVendHead(P_JOB_TOT, P_JOB_NUM, P_FILTRI_PDV_CNT,P_FILTRI_PDV_TAB, kStampStartDWH)
    LOOP
    --
      BEGIN
        vElabCreata := FALSE;
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
        --
        vPasso := 'Run_VND_VND'||rPun.PDV_PUNTO_VENDITA_COD;
        tsStartTimePDV :=CURRENT_TIMESTAMP;
        vPasso := 'CREA_ELAB(VND_VND) - '||rPun.PDV_PUNTO_VENDITA_COD;
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Data:<'|| rPun.DATA_DEC || '>', vPasso);
        vVndVNH_ROW.ELB_DATA_DEC := rPun.DATA_DEC;
        CREA_ELAB(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                  PO_ELAB_ROW           => vVndVNH_ROW    ,
                  P_ENTITA_COD          => 'VND_VNH'      ,
                  P_JOB_ID              => P_JOB_ID       ,
                  P_ID_SESSIONE         => P_SESSION_ID   ,
                  PO_CREATA             => vElabCreata    ,
                  P_PUNTO_VENDITA_COD   => rPun.PDV_PUNTO_VENDITA_COD
                  );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
        --
        IF vElabCreata THEN
          vPasso := 'LOAD_VND_VNH';
          LOAD_VND_VNH(
            PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
            PO_ERR_CODICE      => PO_ERR_CODICE  ,
            PO_MESSAGGIO       => PO_MESSAGGIO   ,
            PO_ELAB_ROW        => vVndVNH_ROW
            );
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
          IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
          --
        END IF;
        --
        tsEndTimePDV := CURRENT_TIMESTAMP;
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'PDV: '|| rPun.PDV_PUNTO_VENDITA_COD || ' - Time Elapsed PDV:' ||to_char(tsStartTimePDV - tsEndTimePDV), vPasso);
        --
        IF vPDV <> rPun.PDV_PUNTO_VENDITA_COD THEN
          BASE_UTIL_PCK.DO_COMMIT(
             P_PROC_NAME  => vProcName,
             P_PASSO      => vPasso
             );
        END IF;
        --
        vPDV := rPun.PDV_PUNTO_VENDITA_COD;
      --
      EXCEPTION
        WHEN ErroreGestito THEN
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      END;
    --
    END LOOP;
    --
    vPasso:='END';
    tsEndTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Time Elapsed:' ||to_char(tsEndTime - tsStartTime), vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'End',BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VENDITE_HEAD_JOB;
  --
  PROCEDURE LOAD_VENDITE_HEAD(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_JOB_ID              IN     MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_SESSION_ID          IN     BASE_LOG.LOG_ID_SESSIONE%TYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    tsStartTime     TIMESTAMP;
    tsEndTime       TIMESTAMP;
    vNumJob         NUMBER;
    vTAB_COD        TAB_COD;
    vTAB_COD_COUNT  NUMBER := 0;
    vElaboraJob     VARCHAR2(32000);
    vNomeJob        VARCHAR2(32000);
  BEGIN
  --
    vProcName := 'LOAD_VENDITE_HEAD';
    -- Start the timer
    tsStartTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Start', vPasso);
    vPasso:='GET_INFO_ENTITA';
    GET_INFO_ENTITA(
      PO_ERR_SEVERITA       =>PO_ERR_SEVERITA  ,
      PO_ERR_CODICE         =>PO_ERR_CODICE    ,
      PO_MESSAGGIO          =>PO_MESSAGGIO     ,
      P_ENTITA_COD          =>'VND_VNH'        ,
      PO_GRADO_PARALLEL     =>vNumJob          ,
      PO_TAB_FILTRI         =>vTAB_COD
    );
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    vTAB_COD_COUNT := vTAB_COD.COUNT;
    BASE_TRACE_PCK.TRACE (kNomePackage,vProcName,'Count:' || vTAB_COD_COUNT,vPasso);
    --
    IF vTAB_COD.COUNT > 0 THEN
      vNumJob := 0;
    ELSE
      vNumJob := vNumJob-1;
    END IF;
    --
    vPasso:='infoNumJob';
    BASE_TRACE_PCK.TRACE (kNomePackage,vProcName,'vNumJob:' || vNumJob,vPasso);
    --
    vPasso := 'RUN_JOB';
    FOR nJob IN 0 .. vNumJob
    LOOP
    --
      DBMS_APPLICATION_INFO.SET_CLIENT_INFO (kNomePackage|| '.'|| vProcName
         || '('
         || vPasso
         || ') =>  Lancio del Job: '
         || nJob);
      --
      BASE_TRACE_PCK.TRACE (kNomePackage, vProcName,'  P_JOB_TOT :' || vNumJob, vPasso);
      BASE_TRACE_PCK.TRACE (kNomePackage, vProcName,'  P_JOB_NUM :' || nJob, vPasso);
      IF vNumJob = 0 THEN
      --
        vPasso := 'LOAD_VENDITE_HEAD_JOB';
        LOAD_VENDITE_HEAD_JOB(
          PO_ERR_SEVERITA       => PO_ERR_SEVERITA  ,
          PO_ERR_CODICE         => PO_ERR_CODICE    ,
          PO_MESSAGGIO          => PO_MESSAGGIO     ,
          P_JOB_TOT             => vNumJob          ,
          P_JOB_NUM             => nJob             ,
          P_JOB_ID              => P_JOB_ID         ,
          P_SESSION_ID          => P_SESSION_ID     ,
          P_FILTRI_PDV_CNT      => vTAB_COD_COUNT   ,
          P_FILTRI_PDV_TAB      => vTAB_COD
          );
      ELSE
      --
        vElaboraJob :='DECLARE
                         PO_ERR_SEVERITA VARCHAR2(4000);
                         PO_ERR_CODICE VARCHAR2(4000);
                         PO_MESSAGGIO VARCHAR2(4000);
                         P_JOB_TOT NUMBER;
                         P_JOB_NUM NUMBER;
                         P_FILTRI_PDV_CNT NUMBER;
                         P_FILTRI_PDV_TAB TAB_COD;
                         P_JOB_ID NUMBER;
                         P_SESSION_ID VARCHAR2(20);
                     BEGIN
                         P_JOB_TOT :='''||vNumJob||''';
                         P_JOB_NUM :='''||nJob||''';
                         P_JOB_ID:='''||P_JOB_ID||''';
                         P_SESSION_ID := '''||P_SESSION_ID||''';
                     MGDWH_ELABORAZIONI_PCK.LOAD_VENDITE_HEAD_JOB
                              ( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO'
                            ||' ,P_JOB_TOT => P_JOB_TOT'
                            ||' ,P_JOB_NUM => P_JOB_NUM'
                            ||' ,P_FILTRI_PDV_CNT => P_FILTRI_PDV_CNT'
                            ||' ,P_FILTRI_PDV_TAB => P_FILTRI_PDV_TAB'
                            ||' ,P_JOB_ID => P_JOB_ID'
                            ||' ,P_SESSION_ID => P_SESSION_ID)
                           ; COMMIT; END;';
       --
       vNomeJob := 'MGDWH_LOAD_VND_VNH_' ||nJob ;
       --
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' vElaboraJob: '||vElaboraJob);
       DBMS_SCHEDULER.CREATE_JOB (
          JOB_NAME             => vNomeJob,
          JOB_TYPE             => 'PLSQL_BLOCK',
          JOB_ACTION           =>  vElaboraJob,
          START_DATE           =>  SYSDATE,
          ENABLED              =>  TRUE,
          COMMENTS             => 'Dettaglio Vendite Job Tot: ' || nJob||'/'||vNumJob
          );
      --
      END IF;
    --
    END LOOP;
    --
    tsEndTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'End:' ||to_char(tsEndTime - tsStartTime), vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'End',BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VENDITE_HEAD;
  --
  PROCEDURE LOAD_VENDITE_DETT_JOB(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_JOB_TOT                           NUMBER,
      P_JOB_NUM                           NUMBER,
      P_JOB_ID                            MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_SESSION_ID                        BASE_LOG.LOG_ID_SESSIONE%TYPE,
      P_FILTRI_PDV_CNT                    NUMBER DEFAULT 0,
      P_FILTRI_PDV_TAB                    TAB_COD DEFAULT NULL
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    tsStartTime     TIMESTAMP;
    tsEndTime       TIMESTAMP;
    --
    tsStartTimePDV  TIMESTAMP;
    tsEndTimePDV    TIMESTAMP;
    vVndVND_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vElabCreata     BOOLEAN;
    ErroreGestito   EXCEPTION;
    vPDV            ANA_PUNTI_VENDITA.PDV_PUNTO_VENDITA_COD%TYPE;
  BEGIN
  --
    vProcName := 'LOAD_VENDITE_DETT';
    -- Start the timer
    vPasso:='START';
    tsStartTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, '', vPasso);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_JOB_TOT:'||P_JOB_TOT, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_JOB_NUM:'||P_JOB_NUM, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_FILTRI_PDV_CNT:'||P_FILTRI_PDV_CNT, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'kStampStartDWH:'||kStampStartDWH, vPasso);
    FOR rPun IN cPdvVendDett(P_JOB_TOT, P_JOB_NUM, P_FILTRI_PDV_CNT,P_FILTRI_PDV_TAB, kStampStartDWH)
    LOOP
    --
      BEGIN
        vElabCreata := FALSE;
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
        --
        vPasso := 'Run_VND_VND'||rPun.PDV_PUNTO_VENDITA_COD;
        tsStartTimePDV :=CURRENT_TIMESTAMP;
        vPasso := 'CREA_ELAB(VND_VND) - '||rPun.PDV_PUNTO_VENDITA_COD;
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Data:<'|| rPun.DATA_DEC || '>', vPasso);
        vVndVND_ROW.ELB_DATA_DEC := rPun.DATA_DEC;
        CREA_ELAB(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                  PO_ELAB_ROW           => vVndVND_ROW    ,
                  P_ENTITA_COD          => 'VND_VND'      ,
                  P_JOB_ID              => P_JOB_ID       ,
                  P_ID_SESSIONE         => P_SESSION_ID   ,
                  PO_CREATA             => vElabCreata    ,
                  P_PUNTO_VENDITA_COD   => rPun.PDV_PUNTO_VENDITA_COD
                  );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
        --
        IF vElabCreata THEN
          vPasso := 'LOAD_VND_VND';
          LOAD_VND_VND(
            PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
            PO_ERR_CODICE      => PO_ERR_CODICE  ,
            PO_MESSAGGIO       => PO_MESSAGGIO   ,
            PO_ELAB_ROW        => vVndVND_ROW
            );
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
          IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
          --
        END IF;
        --
        tsEndTimePDV := CURRENT_TIMESTAMP;
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName,'PDV: '|| rPun.PDV_PUNTO_VENDITA_COD || ' - Time Elapsed PDV:' ||to_char(tsStartTimePDV - tsEndTimePDV), vPasso);
        --
        COMMIT;
        IF vPDV <> rPun.PDV_PUNTO_VENDITA_COD THEN
          BASE_UTIL_PCK.DO_COMMIT(
             P_PROC_NAME  => vProcName,
             P_PASSO      => vPasso
             );
        END IF;
        --
        vPDV := rPun.PDV_PUNTO_VENDITA_COD;
      --
      EXCEPTION
        WHEN ErroreGestito THEN
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      END;
    --
    END LOOP;
    --
    vPasso:='END';
    tsEndTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Time Elapsed:' ||to_char(tsEndTime - tsStartTime), vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'End',BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VENDITE_DETT_JOB;
  --
  PROCEDURE LOAD_VENDITE_DETT(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_JOB_ID              IN     MDWH_ELABORAZIONI.JOB_ID%TYPE,
      P_SESSION_ID          IN     BASE_LOG.LOG_ID_SESSIONE%TYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    tsStartTime     TIMESTAMP;
    tsEndTime       TIMESTAMP;
    vNumJob         NUMBER;
    vTAB_COD        TAB_COD;
    vTAB_COD_COUNT  NUMBER := 0;
    vElaboraJob     VARCHAR2(32000);
    vNomeJob        VARCHAR2(32000);
  BEGIN
  --
    vProcName := 'LOAD_VENDITE_DETT';
    -- Start the timer
    tsStartTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Start', vPasso);
    vPasso:='GET_INFO_ENTITA';
    GET_INFO_ENTITA(
      PO_ERR_SEVERITA       =>PO_ERR_SEVERITA  ,
      PO_ERR_CODICE         =>PO_ERR_CODICE    ,
      PO_MESSAGGIO          =>PO_MESSAGGIO     ,
      P_ENTITA_COD          =>'VND_VND'        ,
      PO_GRADO_PARALLEL     =>vNumJob          ,
      PO_TAB_FILTRI         =>vTAB_COD
    );
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    vTAB_COD_COUNT := vTAB_COD.COUNT;
    BASE_TRACE_PCK.TRACE (kNomePackage,vProcName,'Count:' || vTAB_COD_COUNT,vPasso);
    --
    IF vTAB_COD.COUNT > 0 THEN
      vNumJob := 0;
    ELSE
      vNumJob := vNumJob-1;
    END IF;
    --
    vPasso := 'RUN_JOB';
    FOR nJob IN 0 .. vNumJob
    LOOP
    --
      DBMS_APPLICATION_INFO.SET_CLIENT_INFO (kNomePackage|| '.'|| vProcName
         || '('
         || vPasso
         || ') =>  Lancio del Job: '
         || nJob);
      --
      BASE_TRACE_PCK.TRACE (kNomePackage, vProcName,'  P_JOB_TOT :' || vNumJob, vPasso);
      BASE_TRACE_PCK.TRACE (kNomePackage, vProcName,'  P_JOB_NUM :' || nJob, vPasso);
      IF vNumJob = 0 THEN
      --
        vPasso := 'LOAD_VENDITE_DETT_JOB';
        LOAD_VENDITE_DETT_JOB(
          PO_ERR_SEVERITA       => PO_ERR_SEVERITA  ,
          PO_ERR_CODICE         => PO_ERR_CODICE    ,
          PO_MESSAGGIO          => PO_MESSAGGIO     ,
          P_JOB_TOT             => vNumJob          ,
          P_JOB_NUM             => nJob             ,
          P_JOB_ID              => P_JOB_ID         ,
          P_SESSION_ID          => P_SESSION_ID     ,
          P_FILTRI_PDV_CNT      => vTAB_COD_COUNT   ,
          P_FILTRI_PDV_TAB      => vTAB_COD
          );
      ELSE
      --
        vElaboraJob :='DECLARE
                         PO_ERR_SEVERITA VARCHAR2(4000);
                         PO_ERR_CODICE VARCHAR2(4000);
                         PO_MESSAGGIO VARCHAR2(4000);
                         P_JOB_TOT NUMBER;
                         P_JOB_NUM NUMBER;
                         P_FILTRI_PDV_CNT NUMBER;
                         P_FILTRI_PDV_TAB TAB_COD;
                         P_JOB_ID NUMBER;
                         P_SESSION_ID VARCHAR2(20);
                     BEGIN
                         P_JOB_TOT :='''||vNumJob||''';
                         P_JOB_NUM :='''||nJob||''';
                         P_JOB_ID:='''||P_JOB_ID||''';
                         P_SESSION_ID := '''||P_SESSION_ID||''';
                     MGDWH_ELABORAZIONI_PCK.LOAD_VENDITE_DETT_JOB
                              ( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO'
                            ||' ,P_JOB_TOT => P_JOB_TOT'
                            ||' ,P_JOB_NUM => P_JOB_NUM'
                            ||' ,P_FILTRI_PDV_CNT => P_FILTRI_PDV_CNT'
                            ||' ,P_FILTRI_PDV_TAB => P_FILTRI_PDV_TAB'
                            ||' ,P_JOB_ID => P_JOB_ID'
                            ||' ,P_SESSION_ID => P_SESSION_ID)
                           ; COMMIT; END;';
       --
       vNomeJob := 'MGDWH_LOAD_VND_VND_' ||nJob ;
       --
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,' vElaboraJob: '||vElaboraJob);
       DBMS_SCHEDULER.CREATE_JOB (
          JOB_NAME             => vNomeJob,
          JOB_TYPE             => 'PLSQL_BLOCK',
          JOB_ACTION           =>  vElaboraJob,
          START_DATE           =>  SYSDATE,
          ENABLED              =>  TRUE,
          COMMENTS             => 'Dettaglio Vendite Job Tot: ' || nJob||'/'||vNumJob
          );
      --
      END IF;
    --
    END LOOP;
    --
    tsEndTime := CURRENT_TIMESTAMP;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'End:' ||to_char(tsEndTime - tsStartTime), vPasso);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'End',BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VENDITE_DETT;
  --
  PROCEDURE LOAD_VND_VHG(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
  BEGIN
  --
    vProcName := 'LOAD_VND_VHG';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VND_VHG;
  --
  PROCEDURE LOAD_ANA_MOD(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    --
    PROCEDURE RAWINSERT
    IS
    BEGIN
      MERGE INTO ANA_MODALITA_PAGAMENTI VNP
         USING (SELECT TRIM(REASON_ELEMENT) REASON_ELEMENT,
                       REASON_DESC
                  FROM FIN_REASON@CRSMG
                 WHERE GROUP_ID=1) E2
            ON (VNP.MOD_MODALITA_PAGAMENTO_COD = E2.REASON_ELEMENT)
          WHEN NOT MATCHED THEN
        INSERT (MOD_MODALITA_PAGAMENTO_COD,
                MOD_DESCRIZIONE,
                JOB_ID,
                DATA_IMPORT)
        VALUES (E2.REASON_ELEMENT,
                E2.REASON_DESC,
                PO_ELAB_ROW.JOB_ID,
                SYSTIMESTAMP
                )
          WHEN MATCHED THEN
        UPDATE SET VNP.MOD_DESCRIZIONE = E2.REASON_DESC,
                   JOB_ID = PO_ELAB_ROW.JOB_ID,
                   VNP.DATA_IMPORT =SYSTIMESTAMP;
      --
      PO_ELAB_ROW. ELB_ROWS     := SQL%ROWCOUNT;
    END;
  --
  BEGIN
  --
    vProcName := 'LOAD_ANA_MOD';
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    vPasso := 'RAWINSERT';
    RAWINSERT;
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||' - '||PO_ELAB_ROW.ENT_ENTITA_COD,BASE_LOG_PCK.DEBUG);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ANA_MOD;
  --
  /**
  * LOAD_VND_VNP(): procedura che carica il dettaglio dei pagamenti delle vendite
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_VND_VNP(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      PO_ELAB_ROW           IN OUT MDWH_ELABORAZIONI%ROWTYPE
      )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    --
    vAnno           NUMBER;
    vMese           NUMBER;
    vDay            NUMBER;
    vDataDec        VARCHAR2(10);
    --
    PROCEDURE RAWINSERT(P_ANNO NUMBER, P_MESE NUMBER, P_DATA_DEC VARCHAR2, P_DAY NUMBER)
    IS
    --
      PROCEDURE CHECK_VEND_VNP
      IS
        vCnt NUMBER := 0;
      BEGIN
      --
        vPasso:='CHECK_VEND_VNP';
        SELECT COUNT(0) INTO vCnt
          FROM VND_VENDITE_PAGAMENTI
         WHERE VNP_ANNO = P_ANNO
           AND VNP_MESE = P_MESE
           AND EXTRACT (DAY FROM VNP_DATA_PAGAMENTO) = vDay
           AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vCnt:'||vCnt ,vPasso);
        IF vCnt > 0 THEN
        --
          DELETE VND_VENDITE_PAGAMENTI
           WHERE VNP_ANNO = P_ANNO
             AND VNP_MESE = P_MESE
             AND EXTRACT (DAY FROM VNP_DATA_PAGAMENTO) = vDay
             AND PDV_PUNTO_VENDITA_COD = PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD;
          --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Delete:'||SQL%ROWCOUNT ,vPasso);
          --
        END IF;
      END CHECK_VEND_VNP;
      --
      PROCEDURE INSERT_VND_VNP
      IS
      BEGIN
        --
        vPasso:= 'INSERT_VND_VNP';
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV_PUNTO_VENDITA_COD:'||PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD ,vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||P_DATA_DEC ,vPasso);
        --Step 1. inserisco l'associazione incasso tipo di pagamento
        INSERT INTO VND_VENDITE_PAGAMENTI (VNP_DATA_PAGAMENTO,
                                           VNP_ANNO,
                                           VNP_MESE,
                                           VNP_NUMERO_CASSA,
                                           VNP_NUMERO_SCONTRINO,
                                           VNP_TOTALE_PAGAMENTO_NUM,
                                           JOB_ID,
                                           DATA_IMPORT,
                                           PDV_PUNTO_VENDITA_COD_XK,
                                           CRT_CARTA_COD_XK,
                                           MOD_MODALITA_PAGAMENTO_COD_XK)
             SELECT TO_TIMESTAMP (T.DT_DATE || ' ' || T.DT_TIME, 'YYYY-MM-DD hh24:mi:ss') VNP_DATA_PAGAMENTO,
                    EXTRACT (YEAR FROM TO_TIMESTAMP (T.DT_DATE, 'YYYY-MM-DD')) VNP_ANNO,
                    EXTRACT (MONTH FROM TO_TIMESTAMP (T.DT_DATE, 'YYYY-MM-DD')) VNP_MESE,
                    T.TID VNP_NUMERO_CASSA,
                    T.XACT VNP_NUMERO_SCONTRINO,
                    T.AMOUNT VNP_TOTALE_PAGAMENTO_NUM,
                    PO_ELAB_ROW.JOB_ID  JOB_ID,
                    SYSDATE DATA_IMPORT,
                    T.SID PDV_PUNTO_VENDITA_COD_XK,
                    DECODE (LENGTH (LOY.CARD), 13, SUBSTR (LOY.CARD, 1, 12), LOY.CARD) CRT_CARTA_COD_XK,
                    T.NTND  MOD_MODALITA_PAGAMENTO_COD_XK
               FROM TL_TENDER@CRSMG T
         INNER JOIN TL_HEADER@CRSMG H
                 ON T.SID = H.SID
                AND T.DT_DATE = H.DT_DATE
                AND T.DT_TIME = H.DT_TIME
                AND T.TID = H.TID
                AND T.XACT = H.XACTNBR
          LEFT JOIN TL_LOYALTY@CRSMG LOY
                 ON H.SID = LOY.SID
                AND H.DT_DATE = LOY.DT_DATE
                AND H.DT_TIME = LOY.DT_TIME
                AND H.TID = LOY.TID
                AND H.XACTNBR = LOY.XACTNBR
              WHERE H.XVOID = 0
                AND H.XSUSP = 0
                AND H.TID <> 0
                AND H.DT_DATE = P_DATA_DEC
                /**
                AND NOT EXISTS( SELECT 0 --01/12/2021
                                  FROM TL_XACT_LINK@CRSMG XL
                                 WHERE H.XACTNBR = XL.XACT_ID
                                   AND H.TID = XL.TERM_ID
                                   AND H.DT_DATE = XL.XACT_DATE
                                   AND H.DT_TIME = XL.XACT_TIME
                                   AND H.SID = XL.SID
                                   AND XL.XACT_TYPE = '1'
                                 )
                AND NOT EXISTS( SELECT 0
                                  FROM TL_XACT_LINK@CRSMG XL
                                 WHERE H.XACTNBR = XL.PREV_XACT_ID
                                   AND H.TID = XL.PREV_TERM_ID
                                   AND H.DT_DATE = XL.PREV_XACT_DATE
                                   AND H.DT_TIME = XL.PREV_XACT_TIME
                                   AND H.SID = XL.SID
                                   AND XL.XACT_TYPE = '1'
                                 )**/ 
                                 ;
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROW INSERT:'||SQL%ROWCOUNT ,vPasso);
      --
      END INSERT_VND_VNP;
    --
    PROCEDURE UPDT_PDV_XK
      IS
      BEGIN
         FOR rec IN (SELECT VNP.VNP_VENDITE_PAGAMENTO_ID,
                            PDV_PUNTO_VENDITA_COD_XK
                       FROM VND_VENDITE_PAGAMENTI VNP
                      WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                        AND PDV_PUNTO_VENDITA_COD_XK IS NOT NULL
                        AND PDV_PUNTO_VENDITA_COD IS NULL
                        AND VNP_ANNO = P_ANNO
                        AND VNP_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_PAGAMENTI
               SET (PDV_PUNTO_VENDITA_COD) =
                      (SELECT PDV_PUNTO_VENDITA_COD
                         FROM ANA_PUNTI_VENDITA
                        WHERE PDV_PUNTO_VENDITA_COD = rec.PDV_PUNTO_VENDITA_COD_XK)
             WHERE VNP_VENDITE_PAGAMENTO_ID = rec.VNP_VENDITE_PAGAMENTO_ID
               AND VNP_ANNO = P_ANNO
               AND VNP_MESE = P_MESE;
            --
         END LOOP;
      END UPDT_PDV_XK;
      --
      PROCEDURE UPDT_CRT_XK
      IS
      BEGIN
         FOR rec IN (SELECT VNP.VNP_VENDITE_PAGAMENTO_ID,
                            CRT_CARTA_COD_XK
                       FROM VND_VENDITE_PAGAMENTI VNP
                      WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                        AND CRT_CARTA_COD_XK IS NOT NULL
                        AND CRT_CARTA_COD IS NULL
                        AND VNP_ANNO = P_ANNO
                        AND VNP_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_PAGAMENTI
               SET (CRT_CARTA_COD) =
                      (SELECT CRT_CARTA_COD
                         FROM ANA_CARTE
                        WHERE CRT_CARTA_COD = rec.CRT_CARTA_COD_XK)
             WHERE VNP_VENDITE_PAGAMENTO_ID = rec.VNP_VENDITE_PAGAMENTO_ID
               AND VNP_ANNO = P_ANNO
               AND VNP_MESE = P_MESE;
            --
         END LOOP;
      END UPDT_CRT_XK;
      --
      PROCEDURE UPDT_MOD_XK
      IS
      BEGIN
         FOR rec IN (SELECT VNP.VNP_VENDITE_PAGAMENTO_ID,
                            MOD_MODALITA_PAGAMENTO_COD_XK
                       FROM VND_VENDITE_PAGAMENTI VNP
                      WHERE JOB_ID = PO_ELAB_ROW.JOB_ID
                        AND MOD_MODALITA_PAGAMENTO_COD_XK IS NOT NULL
                        AND MOD_MODALITA_PAGAMENTO_COD IS NULL
                        AND VNP_ANNO = P_ANNO
                        AND VNP_MESE = P_MESE
                        )
         LOOP
            --
            UPDATE VND_VENDITE_PAGAMENTI
               SET (MOD_MODALITA_PAGAMENTO_COD) =
                      (SELECT MOD_MODALITA_PAGAMENTO_COD
                         FROM ANA_MODALITA_PAGAMENTI
                        WHERE MOD_MODALITA_PAGAMENTO_COD = rec.MOD_MODALITA_PAGAMENTO_COD_XK)
             WHERE VNP_VENDITE_PAGAMENTO_ID = rec.VNP_VENDITE_PAGAMENTO_ID
               AND VNP_ANNO = P_ANNO
               AND VNP_MESE = P_MESE;
            --
         END LOOP;
      END UPDT_MOD_XK;
    --
    BEGIN
    --
      CHECK_VEND_VNP;
      INSERT_VND_VNP;
      PO_ELAB_ROW.ELB_ROWS     := SQL%ROWCOUNT;
      --
      UPDT_PDV_XK;
      COMMIT;
      UPDT_CRT_XK;
      COMMIT;
      UPDT_MOD_XK;
      COMMIT;
      --
    END RAWINSERT;
  --
  BEGIN
    --
    vProcName := 'LOAD_VND_VNP';
    --
    vPasso := 'MAKE_DATA';
    SELECT EXTRACT (YEAR FROM PO_ELAB_ROW.ELB_DATA_DEC)  INTO vAnno FROM DUAL;
    SELECT EXTRACT (MONTH FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vMese FROM DUAL;
    SELECT EXTRACT (DAY FROM PO_ELAB_ROW.ELB_DATA_DEC) INTO vDay FROM DUAL;
    vDataDec := vAnno ||'-' ||LPAD(vMese,2,'0') ||'-'||LPAD(vDay,2,'0');
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vDataDec:'||vDataDec ,vPasso);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabELAB;
    PO_ELAB_ROW. ELB_DATA_INI := SYSTIMESTAMP;
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Stato:'||PO_ELAB_ROW. ELB_STATO, BASE_LOG_PCK.Info);
    vPasso := 'UPDT_ELAB(ELAB)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO ,vPasso);
    --
    vPasso := 'RAWINSERT';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_ANNO:'||vAnno ,vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_MESE:'||vMese ,vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_DATA_DEC:'||vDataDec ,vPasso);
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'PDV:'||PO_ELAB_ROW.PDV_PUNTO_VENDITA_COD, BASE_LOG_PCK.Info);
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'P_DATA_DEC:'||vDataDec, BASE_LOG_PCK.Info);
    --
    RAWINSERT(P_ANNO => vAnno, P_MESE => vMese, P_DATA_DEC=> vDataDec, P_DAY=> vDay);
    --
    PO_ELAB_ROW. ELB_STATO    := kStatoElabEND;
    PO_ELAB_ROW. ELB_DATA_FIN := SYSTIMESTAMP;
    --
    vPasso := 'UPDT_ELAB(END)';
    AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
      PO_ELAB_ROW => PO_ELAB_ROW
      );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO ,vPasso);
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'NumROWS:'||PO_ELAB_ROW. ELB_ROWS, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := vPasso ||'=> '|| SQLERRM;
      PO_ELAB_ROW.ELB_STATO := kStatoElabERR;
      PO_ELAB_ROW.ELB_LOG   := PO_MESSAGGIO;
      AGGIORNA_ELAB( PO_ERR_SEVERITA,PO_ERR_CODICE , PO_MESSAGGIO,
        PO_ELAB_ROW => PO_ELAB_ROW
      );
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_ERR_CODICE ||' - '||PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_VND_VNP;
  --
  /**
  * LOAD_ALL(): procedura che carica tutte le entitï¿½ presenti sulla tabella MDWH_ENTITA
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  */
  PROCEDURE LOAD_ALL(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2)
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MGDWH_ELABORAZIONI_PCK';
    vProcName       VARCHAR2(100);
    vPasso          VARCHAR2(100);
    vIdSessione     BASE_LOG.LOG_ID_SESSIONE%TYPE;
    vIdJob          MDWH_ELABORAZIONI.JOB_ID%TYPE;
    vAnaPDV_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaECR_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaART_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaCLI_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaCRT_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaPRM_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaCLU_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaCRC_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vVndVNH_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vVndVNS_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vOrdORH_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vOrdORD_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vAnaMOD_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vVndVNP_ROW     MDWH_ELABORAZIONI%ROWTYPE;
    vElabCreata     BOOLEAN;
  BEGIN
    --
    vProcName := 'LOAD_ALL';
    --
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'Start', BASE_LOG_PCK.Info);
    IF CHECK_JOB_END THEN
    --
      vPasso := 'GET_SESSION_ID';
      vIdSessione := BASE_LOG_PCK.GetSessionId;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vIdSessione:'||vIdSessione, vPasso);
      SELECT MGDWH_ELAB_JOB_ID_SEQ.NEXTVAL INTO vIdJob FROM DUAL;
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'vIdJob:'||vIdJob, vPasso);
      --
      vPasso := 'CREA_ELAB(ANA_PDV)';
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaPDV_ROW    ,
        P_ENTITA_COD          => 'ANA_PDV'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_PDV)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaPDV_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_ECR)';
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaECR_ROW    ,
        P_ENTITA_COD          => 'ANA_ECR'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
        vPasso := 'RUN_JOB_ENTITA(ANA_ECR)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaECR_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_ART)';
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaART_ROW    ,
        P_ENTITA_COD          => 'ANA_ART'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
        vPasso := 'RUN_JOB_ENTITA(ANA_ART)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaART_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_CLI)';
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaCLI_ROW    ,
        P_ENTITA_COD          => 'ANA_CLI'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_CLI)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaCLI_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_CRT)';
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaCRT_ROW    ,
        P_ENTITA_COD          => 'ANA_CRT'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_CRT)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaCRT_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_PRM)';--Anagrafica Promozioni
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaPRM_ROW    ,
        P_ENTITA_COD          => 'ANA_PRM'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_PRM)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaPRM_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_CLU)';--Anagrafica Cluster
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaCLU_ROW    ,
        P_ENTITA_COD          => 'ANA_CLU'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_CLU)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaCLU_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_CRC)';--Anagrafica Cluster
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaCRC_ROW    ,
        P_ENTITA_COD          => 'ANA_CRC'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_CRC)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaCRC_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'LOAD_VENDITE_HEAD';
      LOAD_VENDITE_HEAD(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
            P_JOB_ID          => vIdJob,
            P_SESSION_ID      => vIdSessione
      );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      vPasso := 'LOAD_VENDITE_DETT';
      LOAD_VENDITE_DETT(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
            P_JOB_ID          => vIdJob,
            P_SESSION_ID      => vIdSessione
      );
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      vPasso := 'CREA_ELAB(VND_VNS)';--Tabella degli sconti
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vVndVNS_ROW    ,
        P_ENTITA_COD          => 'VND_VNS'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(VND_VNS)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vVndVNS_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ORD_ORH)';--Tabella testata ordini
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vOrdORH_ROW    ,
        P_ENTITA_COD          => 'ORD_ORH'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ORD_ORH)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vOrdORH_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ORD_ORD)';--Tabella dettaglio ordini
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vOrdORD_ROW    ,
        P_ENTITA_COD          => 'ORD_ORD'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ORD_ORH)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vOrdORD_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(ANA_MOD)';--Tabella anagrafica delle modalitÃ  di pagamento
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vAnaMOD_ROW    ,
        P_ENTITA_COD          => 'ANA_MOD'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(ANA_MOD)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vAnaMOD_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;
      --
      vPasso := 'CREA_ELAB(VND_VNP)';--Tabella anagrafica delle modalitÃ  di pagamento
      CREA_ELAB( PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        PO_ELAB_ROW           => vVndVNP_ROW    ,
        P_ENTITA_COD          => 'VND_VNP'      ,
        P_JOB_ID              => vIdJob         ,
        P_ID_SESSIONE         => vIdSessione    ,
        PO_CREATA             => vElabCreata
      );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      --
      IF vElabCreata THEN
      --
        vPasso := 'RUN_JOB_ENTITA(VND_VNP)';
        RUN_JOB_ENTITA(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
          PO_ELAB_ROW        => vVndVNP_ROW
          );
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL  THEN RAISE ErroreGestito; END IF;
      END IF;

     --
     CHECK_JOB_RUNNING;
      --
    ELSE
      --
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'ELABORAZIONE PRECEDENTE IN ESECUZIONE!!!', BASE_LOG_PCK.DEBUG);
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ELABORAZIONE PRECEDENTE IN ESECUZIONE!!!');
    END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'FINE');
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'End', BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN ErroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END LOAD_ALL;
  --
END MGDWH_ELABORAZIONI_PCK;
/
