CREATE OR REPLACE PACKAGE MGDWH."BASE_UTIL_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_UTIL_PCK
   PURPOSE:    Package di utilit�

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   1.1        12/02/2019           r.doti       1.Eliminate procedure price.
   ******************************************************************************/
  --
  SUBTYPE tGUID                IS VARCHAR2(10);
  --
  FUNCTION newGuidfromRowid(
    P_ROWID      IN ROWID,
    P_LUNGHEZZA  IN NUMBER DEFAULT 10
  ) RETURN VARCHAR2;
  --
  FUNCTION newGuid(
    P_KEY       IN VARCHAR2,
    P_LUNGHEZZA IN NUMBER DEFAULT 10
  ) RETURN VARCHAR2;
  --
  FUNCTION MG_GUID RETURN tGUID;
  --               
  PROCEDURE STAMPA_PL (
      p_str         IN   VARCHAR2,
      p_len         IN   INTEGER := 80,
      p_expand_in   IN   BOOLEAN := TRUE
    );
  --
  PROCEDURE DO_COMMIT(
    P_PROC_NAME     IN   VARCHAR2,
    P_PASSO         IN   VARCHAR2 := NULL
    );
  --
  PROCEDURE DO_ROLLBACK(
    P_PROC_NAME     IN   VARCHAR2,
    P_PASSO         IN   VARCHAR2 := NULL
    );
  --
  FUNCTION GetTAB_COD(
    vStrIN      IN VARCHAR2, 
    vSeparatore IN VARCHAR2 DEFAULT '|'
    )
  RETURN TAB_COD; 
  /**
   * Description: La funzione restituisce il check digit di controllo di un EAN 13
   *
   * Author: R.Doti
   * Created: 02/12/2019
   *
   * Param: P_CARTA_COD Codice cliente
   * Param: P_GET_DATA_MIN_TRANS true/false per indicare se calcolare la data minima
   * Param: P_GET_DATA_MAX_TRANS true/false per indicare se calcolare la data massima
   * Param: PO_DATA_MIN_TRANS data minima calcolata
   * Param: PO_DATA_MAX_TRANS data massima calcolata
   */
  FUNCTION GetChkDigitEAN13(
    P_COD_NUM  VARCHAR2
  )
  RETURN VARCHAR2
  PARALLEL_ENABLE; 
  --
END BASE_UTIL_PCK;
/
