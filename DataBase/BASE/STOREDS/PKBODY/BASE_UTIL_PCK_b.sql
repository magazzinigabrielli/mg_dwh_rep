CREATE OR REPLACE PACKAGE BODY MGDWH."BASE_UTIL_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_UTIL_PCK
   PURPOSE:    Package di utilit�

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  kNomePackage        CONSTANT VARCHAR2(30) := 'BASE_UTIL_PCK';
  --
  TYPE tVettoreNumeri IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  -- ****************************************************************************************
  -- Generatore di numeri casuali per i GUID
  -- utilizzo il Delayed Fibonacci (preso da Knuth The Art of Computer Programming, vol.2)
  -- ****************************************************************************************
  vRandomSequenza      tVettoreNumeri; -- SEQUENZA PSEUDO CASUALE
  vRandomCounter       BINARY_INTEGER := 55; -- Contatore di valori estratti dalla sequenza
  vRandomInizializzato BOOLEAN := FALSE;        -- controlla se il generatore sia stato inizializzato
  lastRowid            ROWID;
  lastKEY              VARCHAR2(4000); 
  lastLEN              INTEGER := 0;      
  lastGUID             VARCHAR2(4000);
  lastDate             DATE; 
  lengthGuid           INTEGER := 10;        
  --
  PROCEDURE STAMPA_PL (
      p_str         IN   VARCHAR2,
      p_len         IN   INTEGER := 80,
      p_expand_in   IN   BOOLEAN := TRUE
    )
  IS
    str           VARCHAR2(32000)  := p_str;
    len           INTEGER          := NVL(p_len, 250);
    expand_in     BOOLEAN          := NVL(p_expand_in, TRUE);
    v_lf          PLS_INTEGER      := 0;
    v_len         PLS_INTEGER      := LEAST (NVL(p_len,250), 250);
    v_str         VARCHAR2(32000);
    --
  BEGIN
    --
    LOOP
      EXIT WHEN str IS NULL OR LENGTH(STR) = 0;
      --
      v_lf := INSTR(str, CHR(10));
      IF v_lf > 0 THEN
        v_len := LEAST(len, v_lf);
      ELSE
        v_len := NVL(p_len, 250);
      END IF;
      --
      BEGIN
        IF LENGTH (str) > v_len THEN
          v_str := REPLACE( SUBSTR (str, 1, v_len), CHR(10), NULL);
        ELSE
          v_str := str;
        END IF;
        --
        DBMS_OUTPUT.PUT_LINE ( SUBSTR(v_str, 1, 255));
        --
      EXCEPTION
        WHEN OTHERS THEN
          IF expand_in THEN
            DBMS_OUTPUT.ENABLE (1000000);
            DBMS_OUTPUT.PUT_LINE (v_str);
          ELSE
            RAISE;
          END IF;
      END;
      --
      str := SUBSTR (str, v_len + 1);
      --
    END LOOP;
    --
  END STAMPA_PL;
  --
  PROCEDURE DO_COMMIT(
    P_PROC_NAME     IN   VARCHAR2,
    P_PASSO         IN   VARCHAR2 := NULL 
    ) 
  IS
    vProcName VARCHAR2(30) := 'DO_COMMIT';
  BEGIN
  --
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'COMMIT');
    COMMIT;
  END DO_COMMIT;
  --
  PROCEDURE DO_ROLLBACK(
    P_PROC_NAME     IN   VARCHAR2,
    P_PASSO         IN   VARCHAR2 := NULL
    ) 
  IS
    vProcName VARCHAR2(30) := 'DO_ROLLBACK';
  BEGIN
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ROLLBACK');
    ROLLBACK;
  END DO_ROLLBACK;
--
  PROCEDURE RandomSeme IS
    vSeme           VARCHAR2(2000);
    vPezzo          VARCHAR2(20);
    vPseudocasuale  NUMBER;
    vTemp           NUMBER;
    ieri            BINARY_INTEGER;
    oggi            BINARY_INTEGER;
  BEGIN
    vRandomInizializzato   := TRUE;
    vRandomCounter         := 0;
    vSeme                  := SUBSTR(TO_SINGLE_BYTE(TO_CHAR(SYSTIMESTAMP,'DD-MM-YYYYY HH24:MI:SS.FF9')||USERENV('SESSIONID')), 1, 2000);
    <<CicloIeri>>
    FOR ieri IN 0..54 LOOP
      vPezzo             := SUBSTR(vSeme,1,19);
      vPseudocasuale     := 0;
      --vOffset          := 1;
      --
      -- converto 19 caratteri in un numero a 38 cifre
      FOR vOffset IN 1..19 LOOP
        vPseudocasuale := 1e2*vPseudocasuale + NVL(ASCII(SUBSTR(vPezzo,vOffset,1)),0.0);
      END LOOP;
      --
      -- cerco di evitare gli zeri
      vPseudocasuale := vPseudocasuale*1e-38+ieri*.01020304050607080910111213141516171819;
      vRandomSequenza(ieri)  := vPseudocasuale - TRUNC(vPseudocasuale);
      --
      -- passo ai 20 caratteri successivi
      vSeme := SUBSTR(vSeme,20);
    END LOOP CicloIeri;
    --
    vPseudocasuale := vRandomSequenza(54);
    <<CicloOggi>>
    FOR oggi IN 0..10 LOOP
      <<CicloIeri2>>
      FOR ieri IN 0..54 LOOP
        --
        -- faccio scorrere vRandomSequenza(ieri-1) di 24 cifre
        vPseudocasuale := vPseudocasuale * 1e24;
        vTemp  := TRUNC(vPseudocasuale);
        vPseudocasuale := (vPseudocasuale - vTemp) + (vTemp * 1e-38);
        --
        -- il numero ricavato lo aggiungo a vRandomSequenza(ieri)
        vPseudocasuale := vRandomSequenza(ieri)+vPseudocasuale;
        IF (vPseudocasuale >= 1.0) THEN
            vPseudocasuale := vPseudocasuale - 1.0;
        END IF;
        -- registro il risultato
        vRandomSequenza(ieri) := vPseudocasuale;
      END LOOP CicloIeri2;
    END LOOP CicloOggi;
    --
  END RandomSeme;
  --
  FUNCTION RandomValore RETURN NUMBER IS
    vPseudocasuale  NUMBER;
  BEGIN
    vRandomCounter := vRandomCounter + 1;
    -- verifico se ho consumato i primi 55 elementi della sequenza
    IF vRandomCounter >= 55 THEN
      -- initializzo la sequena di numeri pseudo casuali, se serve
      IF vRandomInizializzato THEN
        -- genero altri 55 resultati pseudo casuali
        FOR ieri IN 0..30 LOOP
          vPseudocasuale := vRandomSequenza(ieri+24) + vRandomSequenza(ieri);
          IF (vPseudocasuale >= 1.0) THEN
            vPseudocasuale := vPseudocasuale - 1.0;
          END IF;
          vRandomSequenza(ieri) := vPseudocasuale;
        END LOOP;
        --
        FOR ieri IN 31..54 LOOP
          vPseudocasuale := vRandomSequenza(ieri-31) + vRandomSequenza(ieri);
          IF (vPseudocasuale >= 1.0) THEN
            vPseudocasuale := vPseudocasuale - 1.0;
          END IF;
          vRandomSequenza(ieri) := vPseudocasuale;
        END LOOP;
        --
      ELSE
        RandomSeme;
      END IF;
      --
      vRandomCounter := 0;
      --
    END IF;
    --
    -- ritorno il valore calcolato
    RETURN vRandomSequenza(vRandomCounter);
    --
  END RandomValore;
  --
  FUNCTION MG_GUID RETURN tGUID
  IS
     kCharacterSet CONSTANT VARCHAR2(100)  := '1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm';
     kRange        CONSTANT BINARY_INTEGER := LENGTH(kCharacterSet);
     vNumero  BINARY_INTEGER;
     vGuidOut tGUID;
     vOffeset BINARY_INTEGER;
  BEGIN
    --
    <<CicloScan>>
    FOR vNumero IN 1..lengthGuid LOOP
      --
      vOffeset := MOD(TRUNC(RandomValore*kRange), kRange)+1; -- il mod serve nello sfigatissimo caso venga generato esattamente il numero 1
      vGuidOut := vGuidOut || SUBSTR(kCharacterSet, vOffeset, 1);
      --
    END LOOP CicloScan;
    --
    RETURN vGuidOut;
    --
  END MG_GUID;  
  --                 
  FUNCTION lenGuid(P_LUNGHEZZA  IN NUMBER) RETURN INTEGER
  IS
    v_lunghezza   INTEGER;
  BEGIN
      IF P_LUNGHEZZA IS NULL OR 
         P_LUNGHEZZA < 1 THEN
          v_lunghezza := 16;
      ELSE        
          v_lunghezza := TRUNC(P_LUNGHEZZA);
      END IF;                               
      RETURN v_lunghezza;
  EXCEPTION
    WHEN VALUE_ERROR THEN
        RETURN 16;
  END lenGuid;
  --
  FUNCTION newGuidfromRowid(
    P_ROWID      IN ROWID,
    P_LUNGHEZZA  IN NUMBER DEFAULT 10
  ) RETURN VARCHAR2
  IS 
    v_lunghezza   INTEGER := lenGuid(P_LUNGHEZZA);                  
  BEGIN  
    IF P_ROWID  != lastRowid
    OR lastLEN  != v_lunghezza  
    OR lastGUID IS NULL 
    OR P_ROWID  IS NULL THEN
        lastGUID := MG_GUID; 
        lastGUID := SUBSTR(LPAD(lastGUID,v_lunghezza,'x'),1, v_lunghezza);
    END IF;                                                                       
    --
    lastRowid   := P_ROWID;
    lastLen     := v_lunghezza;
    --
    RETURN lastGUID;
    --
  END newGuidfromRowid;
  --
  FUNCTION newGuid(
    P_KEY       IN VARCHAR2,
    P_LUNGHEZZA IN NUMBER DEFAULT 10
  ) RETURN VARCHAR2
  IS          
    v_lunghezza   INTEGER := lenGuid(P_LUNGHEZZA);                  
  BEGIN
    IF P_KEY    != lastKEY  
    OR lastLEN  != v_lunghezza  
    OR lastGUID IS NULL
    OR lastDate IS NULL
    OR (SYSDATE - lastDate) > 1 
    OR P_KEY    IS NULL THEN
        lastGUID := MG_GUID;
        lastGUID := SUBSTR(LPAD(lastGUID,v_lunghezza, SUBSTR(P_KEY,1,1)),1, v_lunghezza);
    END IF;
    --
    lastKEY     := P_KEY; 
    lastDate    := SYSDATE;
    lastLen     := v_lunghezza;
    RETURN lastGUID;       
    --
  END NEWGUID;
  --
  FUNCTION GET_TOKEN(
    P_STR   IN OUT NOCOPY VARCHAR2,
    TOKEN      OUT NOCOPY VARCHAR2,
    P_SEP   IN            VARCHAR2
    ) 
    RETURN VARCHAR2 IS
    --
    vPos       pls_integer;
    vPosErrore pls_integer := 9999999;
    vAppo      pls_integer;
  BEGIN
  --
    IF P_STR IS NULL THEN
      vPosErrore := 0;
    ELSE
      --
      vPos := INSTR(P_STR, P_SEP);
      IF nvl(vPos, 0) = 0 THEN
        vPos := LENGTH(P_STR)-1;
        vPosErrore := LENGTH(P_STR)-1;
      END IF;
      --
      vAppo := REGEXP_INSTR(P_STR,TOKEN);
      IF vAppo > 0 AND vAppo < vPos AND vAppo < vPosErrore THEN
        vPosErrore := vAppo;
      END IF;
    END IF;
    --
    IF vPosErrore < 9999999 THEN
      vPos := NULL;
    END IF;
    --
    TOKEN := SUBSTR(P_STR, 1 , vPos-1);
    P_STR := SUBSTR(P_STR, vPos);
    --
    RETURN vPos-1;
  END GET_TOKEN;
--
  FUNCTION GET_SEPARATORE(
    P_STR   IN OUT NOCOPY VARCHAR2,
    P_SEP   IN            VARCHAR2
    ) RETURN PLS_INTEGER IS
  BEGIN
    IF P_STR IS NULL THEN
      RETURN 0;
    ELSE
      IF SUBSTR(P_STR, 1 , 1) != P_SEP THEN
        RETURN 0;
      END IF;
      P_STR := SUBSTR(P_STR, 2);
    END IF;
    --
    RETURN 1;
  END GET_SEPARATORE;
  --
  FUNCTION GetTAB_COD(
    vStrIN      IN VARCHAR2,
    vSeparatore IN VARCHAR2 DEFAULT '|'
    )
  RETURN TAB_COD
  IS
    vTab TAB_COD;
    posAttuale     NUMBER;
    pos            NUMBER;
    idSTRP         VARCHAR2(16);
    vStr           VARCHAR2(4000);
  BEGIN
    vTab := TAB_COD();
    posAttuale := 0;
    pos := 0;
    vStr := vStrIN;
    IF vStr IS NOT NULL THEN
      LOOP
        pos := pos + 1;
        posAttuale := posAttuale + GET_TOKEN(vStr, idSTRP, '|');
        posAttuale := posAttuale + GET_SEPARATORE(vStr, '|');
        vTab.EXTEND;
        vTab(pos) := idSTRP;
        EXIT WHEN vStr IS NULL;
      END LOOP;
    ELSE
   --Lista vuota
   vTab := TAB_COD();
   END IF ;
   RETURN vTab;
  END GetTAB_COD;
  --
  /**
   * Description: La funzione restituisce il check digit di controllo di un EAN 13
   *
   * Author: R.Doti
   * Created: 02/12/2019
   *
   * Param: P_CARTA_COD Codice cliente
   * Param: P_GET_DATA_MIN_TRANS true/false per indicare se calcolare la data minima
   * Param: P_GET_DATA_MAX_TRANS true/false per indicare se calcolare la data massima
   * Param: PO_DATA_MIN_TRANS data minima calcolata
   * Param: PO_DATA_MAX_TRANS data massima calcolata
   */
  FUNCTION GetChkDigitEAN13(
    P_COD_NUM  VARCHAR2
  )
  RETURN VARCHAR2
  PARALLEL_ENABLE
  IS
    chk_digit      INTEGER;
    vCodNUM        INTEGER := TO_NUMBER(P_COD_NUM);
  BEGIN
  --
    SELECT SUM (
                SUBSTR (vCodNUM, LENGTH (vCodNUM) - num + 1, 1)
                * 
                CASE WHEN MOD (num, 2) = 1 THEN 3 ELSE 1 END)
        INTO chk_digit
        FROM (SELECT LEVEL num
                FROM DUAL
             CONNECT BY LEVEL <= 12) p
       WHERE p.num <= LENGTH (vCodNUM);
    --
    SELECT MOD (10 - MOD (chk_digit, 10), 10) INTO chk_digit FROM DUAL;
    --
    RETURN P_COD_NUM||TO_CHAR(chk_digit);
    --
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END GetChkDigitEAN13;
--
END BASE_UTIL_PCK;
/
